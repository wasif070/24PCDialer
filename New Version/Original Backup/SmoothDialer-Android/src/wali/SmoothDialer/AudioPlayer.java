package wali.SmoothDialer;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.util.Log;
import android.widget.SlidingDrawer;

public class AudioPlayer implements Runnable
{
	public String iPlayTargetStr = "";
	
	private boolean iRunning = false;
	private boolean iStopped = false;
	
	private AudioManager iAudioManager = null;
	private Context iContext = null;
	private SmoothDialer iActivity = null;
	
	private AudioTrack iAudioPlayer = null;
	
	private byte iPlayBuf[] = new byte[160000];
	private int iPlayBufSize = 0;
	private int	iPlayBufSizeMax = 16000;
	
	private int iLocked = 2;
	
	public AudioPlayer(SmoothDialer main_activity,Context context)
	{
		iActivity = main_activity;
		iContext = context;
				
		iRunning = true;
		Thread t = new Thread(this);
		
		iLocked = 2;
		t.start();
		
		while(iLocked != 0 )
		{
			try
			{
				Thread.sleep(50);
			}
			catch(Exception e){}
		}
		
		iLocked = 0;
	}
	
	public void Stop()
	{
		iRunning = false;
		for ( int i = 0; i < 10; i++ )
		{
			if ( iStopped )break;
			
			try
			{
				Thread.sleep(50);
			}
			catch(Exception e){}
		}
	}
	
	public boolean IsLoudSpeaker()
	{
		if ( iAudioManager == null )return false;
		try
		{
			return iAudioManager.isSpeakerphoneOn();
		}
		catch(Exception e){}
		return false;
	}
	
	public void SetLoudSpeaker(boolean b_on)
	{
		if ( iAudioManager == null )return;
		try
		{			
			iAudioManager.setSpeakerphoneOn(b_on);
		}
		catch(Exception e)
		{
			Log.d("wali loud speaker error", e.getMessage());
		}
	}

	public boolean IsBlueToothSpeaker()
	{
		if ( iAudioManager == null )return false;
		try
		{
			return iAudioManager.isBluetoothA2dpOn();
		}
		catch(Exception e){}
		return false;
	}
	
	public void SetBlueToothSpeaker(boolean b_on)
	{
		if ( iAudioManager == null )return;
		try
		{			
			iAudioManager.setBluetoothA2dpOn(b_on);
		}
		catch(Exception e)
		{
			Log.d("wali loud speaker error", e.getMessage());
		}
	}

	synchronized public void ResizeAudioBuffer(int max_size)
	{
		iPlayBufSizeMax = max_size * 16;
		if ( iPlayBufSizeMax > iPlayBuf.length )iPlayBufSizeMax = iPlayBuf.length;
		
		iPlayBufSize = 0;
	}
	
	synchronized public int ReadOrWriteBuffer(boolean is_read,byte dt[],int len)  // for read, len is max_len
	{
		if ( is_read )
		{
			if ( iPlayBufSize <= 0 )return 0;
			if ( len > iPlayBufSize )len = iPlayBufSize;
			System.arraycopy(iPlayBuf, 0, dt, 0, len);
			if ( iPlayBufSize > len )System.arraycopy(iPlayBuf, len, iPlayBuf, 0, iPlayBufSize - len);
			iPlayBufSize -= len;
			return len;
		}
		
		if ( len > iPlayBufSizeMax )len = iPlayBufSizeMax;
		
		if ( ( iPlayBufSize + len ) > iPlayBufSizeMax )
		{
			int delete_len = iPlayBufSize + len - iPlayBufSizeMax;
			if ( delete_len < iPlayBufSize )System.arraycopy(iPlayBuf, delete_len, iPlayBuf, 0, iPlayBufSize - delete_len);
			iPlayBufSize -= delete_len;
		}
		System.arraycopy(dt, 0, iPlayBuf, iPlayBufSize, len);
		iPlayBufSize += len;
		return len;
	}
	
	public void run()
	{		
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
				
		iAudioManager = (AudioManager)iContext.getSystemService(Context.AUDIO_SERVICE);

		int native_sample_rate = 8000;
		
		int play_buf_unit = AudioTrack.getMinBufferSize(native_sample_rate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);		
		if ( play_buf_unit < 3200 )
		{
			if ( ( 3200 % play_buf_unit ) != 0 )play_buf_unit = 3200 + play_buf_unit - 3200 % play_buf_unit;
			else play_buf_unit = 3200;
		}

		final int supported_targets[] = {AudioManager.STREAM_VOICE_CALL,AudioManager.STREAM_SYSTEM, MediaRecorder.AudioSource.VOICE_DOWNLINK, AudioManager.STREAM_MUSIC };
		
		int play_target = -1;
		
		for ( int i = 0; i < supported_targets.length; i++ )
		{
			play_target = supported_targets[i];
			try
			{				
				iAudioPlayer = new AudioTrack(play_target,native_sample_rate,AudioFormat.CHANNEL_OUT_MONO,AudioFormat.ENCODING_PCM_16BIT,play_buf_unit * 2,AudioTrack.MODE_STREAM);
				if ( iAudioPlayer.getState() == AudioTrack.STATE_UNINITIALIZED )continue;		
				break;
			}
			catch(Exception e){}
		}
		
		iAudioManager.setMode(AudioManager.MODE_IN_CALL);
        iAudioManager.setSpeakerphoneOn(false);  
        
        try
        {
			iAudioManager.setStreamVolume(play_target, iAudioManager.getStreamMaxVolume(play_target), 0 );
			
			iAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, iAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
			iAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, iAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
			iAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, iAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM), 0);
        }
        catch(Exception e){}
        
        iAudioPlayer.setPlaybackRate(native_sample_rate);		              
        iLocked = 0;
        
        byte play_buf[] = new byte[play_buf_unit];
        while ( iRunning )
        {
       		try
       		{
       			Thread.sleep(50);
       		}
       		catch(Exception e){};

       		for ( int i = 0; i < 4; i++ )
       		{
       			int play_len = ReadOrWriteBuffer(true, play_buf, play_buf.length);
       			if ( play_len <= 0 )break;
       			
       			if ( i == 0 && iAudioPlayer.getState() != AudioTrack.PLAYSTATE_PLAYING )
       			{       				
       				try
       				{
       					iAudioPlayer.play();
       					Thread.sleep(50);
       				}
       				catch(Exception e){}
       			}
       			iAudioPlayer.write(play_buf, 0, play_len);
       		}
        }

        if ( iAudioPlayer.getPlayState() == AudioTrack.PLAYSTATE_PLAYING )iAudioPlayer.stop();
        iAudioPlayer.release();
        
        iAudioManager.setMode(AudioManager.MODE_NORMAL);
        iStopped = true;
	}
};
