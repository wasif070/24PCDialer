package wali.SmoothDialer;

import java.io.InputStream;
import java.util.Date;

//import android.media.audiofx.AcousticEchoCanceler;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

public class SmoothDialer extends Activity implements Runnable
{
	public final int MAX_CALL_LOG = 30;

	public final static int RUNNING_STATE_AUDIO = 1, RUNNING_STATE_INGING = 2, RUNNING_STATE_AUDIO_SETTINGS = 4, RUNNING_STATE_ACCEPT_INCOMING_CALL = 8, RUNNING_STATE_SHOULD_CONFIRM_EXIT = 16,	RUNNING_STATE_EXITED = 32;
	public final static int CALLBACK_EVENT_NONE = 0, CALLBACK_EVENT_AUTH_ERROR = 1, CALLBACK_EVENT_CALL_COMPLETE = 2, CALLBACK_EVENT_INCOMING_CALL_COMPLETE = 3, CALLBACK_EVENT_COMPLETE_SIP_SETTINGS = 4, CALLBACK_EVENT_RESIZE_AUDIO_BUF = 5;
	public final static int TUNNEL_COMMAND_NONE = 0, TUNNEL_COMMAND_RECONNECT = 1, TUNNEL_COMMAND_CALL = 2, TUNNEL_COMMAND_CALL_IVR = 3, TUNNEL_COMMAND_END_CALL = 4, TUNNEL_COMMAND_EXIT = 5;
	public final static int TUNNEL_TIMER_MS = 100;
	
	private MainView iMainView = null;  
	private AudioPlayer iAudioPlayer = null;
	private AudioRecorder iAudioRecorder = null;

	private final int PICK_CONTACT = 1;	
	public  String	 iContactNameCache = "";
	public  String	 iContactNoCache = "";
	
	private EditText iOperatorCodeEdit = null;
	
	public static	 String iCallLogCurrent = "";
   
	public String	 iCallLogStr[] = new String[MAX_CALL_LOG];
	public String	 iDialLogStr[] = new String[MAX_CALL_LOG];
	
	private int iRunningState = 0;
	private int iCallbackEvent = CALLBACK_EVENT_NONE;
	private int iCallbackEventParam = 0;
	private String iCallbackEventParam1 = "";
	private String iDisplayStrings[] = new String[6]; // brandname,state,status,balance,duration,footer
	
	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialer);
        
       	iMainView = (MainView) findViewById(R.id.DialerSurface);
        iMainView.Create(this,this.getWindowManager().getDefaultDisplay().getWidth(),this.getWindowManager().getDefaultDisplay().getHeight());

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE );
        
        try
        {
        	LoadSettings();
        	LoadCallAndDialLog();
        }
        catch(Exception e){}
        
        iAudioPlayer = new AudioPlayer(this,this.getApplicationContext());
        iAudioRecorder = new AudioRecorder(this, this.getApplicationContext());
        
        new  Thread(this).start();
        
        if ( SettingsActivity.iFixedOperatorCode == true || SettingsActivity.iOperatorCode > 0 )
        {
        	ReConnect();
//        	ShowSplashScreen();
        }
        else ShowSettingsForm();
    }
	
	private void LoadCallAndDialLog()
	{
		for ( int i = 0; i < MAX_CALL_LOG; i++ )iCallLogStr[i] = "";
		for ( int i = 0; i < MAX_CALL_LOG; i++ )iDialLogStr[i] = "";
		
		String str_call_log = iMainView.getContext().getSharedPreferences("SmoothDialer", 0).getString("call_log","");		
		if ( str_call_log != null && str_call_log.length() > 0 )
		{
			str_call_log += "\n";
			int call_log_count = 0;
			int prev_pos = 0;
			int new_line_count = 0;
			
			for ( int i = 0; i < str_call_log.length() && call_log_count < MAX_CALL_LOG; i++ )
			{
				if ( str_call_log.charAt(i) != '\n' )continue;
				new_line_count++;
				if ( ( new_line_count % 2 ) != 0 )continue;
				String str_temp = str_call_log.substring(prev_pos,i).trim();
				if ( str_temp.length() > 0 )iCallLogStr[call_log_count++] = str_temp;
				prev_pos = i + 1;
			}
		}
		
		String str_dial_log = iMainView.getContext().getSharedPreferences("SmoothDialer", 0).getString("dial_log","");	
		if ( str_dial_log != null && str_dial_log.length() > 0 )
		{
			str_dial_log += "\n";
			int dial_log_count = 0;
			int prev_pos = 0;
			int new_line_count = 0;
			
			for ( int i = 0; i < str_dial_log.length() && dial_log_count < MAX_CALL_LOG; i++ )
			{
				if ( str_dial_log.charAt(i) != '\n' )continue;
				new_line_count++;
				if ( ( new_line_count % 2 ) != 0 )continue;
				String str_temp = str_dial_log.substring(prev_pos,i).trim();
				if ( str_temp.length() > 0 )iDialLogStr[dial_log_count++] = str_temp; 
				prev_pos = i + 1;
			}
		}		
	}
	
	public void SaveSettings()
	{
		if ( iMainView == null )return;
		
		String str_settings = "";
		if ( SettingsActivity.iOperatorCode >= 0 )str_settings += String.valueOf(SettingsActivity.iOperatorCode);
		
		str_settings += "\n" + SettingsActivity.iSipUsername + "\n" + SettingsActivity.iSipPassword + "\n" + SettingsActivity.iSipPhoneNo + "\n";
		iMainView.getContext().getSharedPreferences("SmoothDialer", 0).edit().putString("dialer_settings", str_settings).commit();
	}
		
	private void LoadSettings()
	{
		if ( iMainView == null )return;
		
		SettingsActivity.iAuthUrl = SettingsActivity.iAuthUrlOptional = "";
		SettingsActivity.iBaseOperatorCode = 0;
		SettingsActivity.iFixedOperatorCode = false;

		SettingsActivity.iOperatorCode = -1;
		SettingsActivity.iSipUsername = SettingsActivity.iSipPassword = SettingsActivity.iSipPhoneNo = "";

		try
		{
			InputStream in = this.getBaseContext().getResources().openRawResource(R.drawable.gui);
			byte gui_data[] = new byte[in.available() + 100];
			int len = in.read(gui_data);
			in.close();
			
			int prev_pos = 0;
			for ( int i = 0; i <= len; i++ )
			{
				if ( i != len && gui_data[i] != '\n' )continue;
				
				int j = prev_pos;
				
				String s1 = "", s2 = "";
				
				prev_pos = i + 1;
				for ( ; j < i; j++ )
				{
					if ( gui_data[j] == ':' )break;
					s1 += (char)gui_data[j];
				}
				j++;
				for ( ; j < i; j++ )s2 += (char)gui_data[j];
				s1 = s1.trim();
				s2 = s2.trim();
				
				if ( s1.compareToIgnoreCase("auth") == 0 )SettingsActivity.iAuthUrl = s2;
				else if ( s1.compareToIgnoreCase("auth1") == 0 )SettingsActivity.iAuthUrlOptional = s2;
				else
				{
					int tmp = 0;
					for ( int k = 0; k < s2.length(); k++ )if ( s2.charAt(k) >= '0' && s2.charAt(k) <= '9' )tmp = tmp * 10 + s2.charAt(k) - '0';
					if ( s1.compareToIgnoreCase("fixedpin") == 0 )SettingsActivity.iFixedOperatorCode = ( tmp != 0 );
					else if ( s1.compareToIgnoreCase("basepin") == 0 )SettingsActivity.iBaseOperatorCode = tmp;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		String str_settings = iMainView.getContext().getSharedPreferences("SmoothDialer", 0).getString("dialer_settings","");
		if ( str_settings == null || str_settings.length() <= 0 )return;
		
		str_settings += "\n";		
		int cnt = 0;
		int prev_pos = 0;
		
		for ( int i = 0; i < str_settings.length(); i++ )
		{
			char c = str_settings.charAt(i);
			if ( c != '\n' )continue;
			
			String s = str_settings.substring(prev_pos,i);
			if ( s == null )s = "";
			s = s.trim();
			
			prev_pos = i + 1;		

			if ( cnt == 0 )
			{
				SettingsActivity.iOperatorCode = 0;
				for ( int j = 0; j < s.length(); j++ )if ( s.charAt(j) >= '0' && s.charAt(j) <= '9' )SettingsActivity.iOperatorCode = SettingsActivity.iOperatorCode * 10 + s.charAt(j) - '0';
			}
			else if ( cnt == 1 )SettingsActivity.iSipUsername = s;
			else if ( cnt == 2 )SettingsActivity.iSipPassword = s;
			else if ( cnt == 3 )SettingsActivity.iSipPhoneNo = s;
			
			cnt++;
		}
	}
	
	private int iExitCountDown = 2;
	public void ExitApp()
	{
		iAudioPlayer.Stop();
		iAudioRecorder.Stop();
		SendToTunnel(TUNNEL_COMMAND_EXIT, "", "", "", "", "", "", "");
		
		iExitCountDown--;
		if ( iExitCountDown <= 0 )System.exit(1);
	}
	
	public boolean IsLoudSpeaker()
	{
		if ( iAudioPlayer != null )return iAudioPlayer.IsLoudSpeaker();
		return false;
	}
	
	public void SetLoudSpeaker(boolean b_on)
	{
		if ( iAudioPlayer != null )iAudioPlayer.SetLoudSpeaker(b_on);
	}
	
	public boolean IsBlueToothSpeaker()
	{
		if ( iAudioPlayer != null )return iAudioPlayer.IsBlueToothSpeaker();
		return false;
	}
	
	public void SetBlueToothSpeaker(boolean b_on)
	{
		if ( iAudioPlayer != null )iAudioPlayer.SetBlueToothSpeaker(b_on);
	}
	
	public void SetRunningState(int running_state)
	{
		iRunningState = running_state;
		iAudioRecorder.iRecording = ( ( iRunningState & RUNNING_STATE_AUDIO ) != 0 );
		
		if ( ( iRunningState & RUNNING_STATE_EXITED ) != 0 )System.exit(1);
	}
	
	public int GetRunningState()
	{
		return iRunningState;
	}
	
	public void ReConnect()
	{
		String str_operator_code = "";
		if ( SettingsActivity.iBaseOperatorCode != 0 )str_operator_code += String.valueOf(SettingsActivity.iBaseOperatorCode);
		if ( SettingsActivity.iFixedOperatorCode == false && SettingsActivity.iOperatorCode > 0 )str_operator_code += String.valueOf(SettingsActivity.iOperatorCode);
		
		if ( str_operator_code.length() <= 0 )
		{
			ShowSettingsForm();
			return;
		}
		
		SendToTunnel(TUNNEL_COMMAND_RECONNECT,SettingsActivity.iAuthUrl,SettingsActivity.iAuthUrlOptional,str_operator_code,SettingsActivity.iSipUsername,SettingsActivity.iSipPassword,SettingsActivity.iSipPhoneNo,String.valueOf(TUNNEL_TIMER_MS));
	}
	
    private void ShowOperatorForm()
    {
   	 	LayoutInflater factory = LayoutInflater.from(this.getApplicationContext());
        final View iView = factory.inflate(R.layout.operator_form, null);                  
        
        Dialog dlg = new AlertDialog.Builder(iMainView.getContext())
            .setTitle("Enter operator code")
            .setView(iView)
            .setNegativeButton("Cancel", null)
            .setPositiveButton("Save", new OnClickListener() 
            {				
				@Override
				public void onClick(DialogInterface dialog, int which) 
				{
					if ( iOperatorCodeEdit == null )return;
					
					String str_temp = iOperatorCodeEdit.getText().toString();
					if ( str_temp == null || str_temp.length() <= 0 )return;
					
					SettingsActivity.iOperatorCode = 0;
					for ( int i = 0; i < str_temp.length(); i++ )if ( str_temp.charAt(i) >= '0' && str_temp.charAt(i) <= '9' )SettingsActivity.iOperatorCode = SettingsActivity.iOperatorCode * 10 + str_temp.charAt(i) - '0';
					ReConnect();
				}
			})			
            .create();       

        	iOperatorCodeEdit = (EditText)iView.findViewById(R.id.operator_code);
        	if ( SettingsActivity.iOperatorCode >= 0 )iOperatorCodeEdit.setText(String.valueOf(SettingsActivity.iOperatorCode));   	
            
        	dlg.show();           
    } 
	
    public void ShowCallLog(boolean show_call_log)
    {    	
		CallLogActivity.iActivity = this;
		CallLogActivity.iShowDialLog = !show_call_log;
		
		try
		{
			Intent intent = new Intent(this,CallLogActivity.class);   		
			startActivity(intent);
		}
		catch(Exception e){}     	
    }
    
    public void ShowSettingsForm()
    {
    	if ( SettingsActivity.iFixedOperatorCode == false && SettingsActivity.iOperatorCode <= 0 )
    	{
    		ShowOperatorForm();
    		return;
    	}

    	SettingsActivity.iActivity = this;
   		Intent intent = new Intent(this,SettingsActivity.class);   		
		startActivity(intent);
    }
	
    public void ResizeAudioBuf(int max_size)
    {
    	iAudioPlayer.ResizeAudioBuffer(max_size);
    	iAudioRecorder.ResizeAudioBuffer(max_size);
    }
    
    private void ShowSplashScreen()
    {
    	try
    	{
       		Intent intent = new Intent(this,SplashActivity.class);   		
    		startActivity(intent);    		
    	}
    	catch(Exception e){}
    }
    
    public void AddCallLog(boolean is_incoming,String remote_phone_no,int duration)
    {
    	if ( remote_phone_no.length() <= 0 )return;		// Called IVR
    	boolean dial_log = false;
    	if ( is_incoming == true && duration == 0 )dial_log = true;
    	
    	try
    	{
    		if ( dial_log )for ( int i = MAX_CALL_LOG-1; i > 0; i-- )iDialLogStr[i] = iDialLogStr[i-1];
    		else for ( int i = MAX_CALL_LOG-1; i > 0; i-- )iCallLogStr[i] = iCallLogStr[i-1];
			
			int h = duration/3600;
			int m = ( duration % 3600 )/60;
			int s = duration % 60;
			
			String str_time = "";
			if ( h > 0 )str_time += String.valueOf(h) + " H:";
			if ( m < 10 )str_time += "0";
			str_time += String.valueOf(m) + " M:";;
				
			if ( s < 10 )str_time += "0";
			str_time += String.valueOf(s) + " S";;

	    	iCallLogCurrent = "";
   			if ( remote_phone_no.endsWith(iContactNoCache) )iCallLogCurrent += ( iContactNameCache + " " );
   			iCallLogCurrent += ( remote_phone_no + "\n" );
   			iCallLogCurrent += DateFormat.format("dd MMM hh:mm A", new Date().getTime()).toString();
			
   			if ( dial_log )iDialLogStr[0] = iCallLogCurrent;
   			else iCallLogStr[0] = iCallLogCurrent + " " + str_time;
						
			if ( dial_log )
			{
				String str_dial_log = "";
				for ( int i = 0; i < MAX_CALL_LOG; i++ )if ( iDialLogStr[i] != null && iDialLogStr[i].length() > 0 )str_dial_log += iDialLogStr[i] + "\n";
				iMainView.getContext().getSharedPreferences("SmoothDialer", 0).edit().putString("dial_log", str_dial_log).commit();				
			}
			else
			{
				String str_call_log = "";
				for ( int i = 0; i < MAX_CALL_LOG; i++ )if ( iCallLogStr[i] != null && iCallLogStr[i].length() > 0 )str_call_log += iCallLogStr[i] + "\n";
				iMainView.getContext().getSharedPreferences("SmoothDialer", 0).edit().putString("call_log", str_call_log).commit();
			}
    	}
    	catch(Exception e){}
    }
    
    public void CallFromLog(String str_log)
    {
    	if ( str_log.length() <= 0 )return;
    	
		int p = str_log.indexOf('\n');
		if ( p < 0 )return;
		p--;
	
		String str_phone_no = "";
		String str_display = "";
	
		while(p >= 0)
		{
			char c = str_log.charAt(p--);
			if ( c == ' ' )break;
			
			str_phone_no = c + str_phone_no;
		}
	
		str_phone_no = str_phone_no.trim();
	
		while(p >= 0)str_display = str_log.charAt(p--) + str_display;
		str_display = str_display.trim();
	
		if ( str_phone_no.length() <= 0 )return;
    	
    	iContactNameCache = str_display;
    	iContactNoCache = str_phone_no;
    	
    	iMainView.iPhoneNoStr = str_phone_no;
    	iMainView.invalidate();
    }
    
    public void Call(String dest_no)
    {
    	if ( dest_no.length() <= 0 )
    	{
    		if ( ( iRunningState & RUNNING_STATE_ACCEPT_INCOMING_CALL ) != 0 )SendToTunnel(TUNNEL_COMMAND_CALL, "", "", "", "", "", "", "");
    		else ShowCallLog(false);
    		return;
    	}
    	
/*    	if ( dest_no.equals("0000"))
    	{	
    		iMainView.ShowMessage("Player status", "Play: " + iAudioPlayer.iPlayTargetStr + "\nRecord: " + iAudioRecorder.iRecordSourceStr, 0);
    		return;
    	} */

    	iCallLogCurrent = "";
    	try
    	{
   			if ( dest_no.endsWith(iContactNoCache) )iCallLogCurrent += ( iContactNameCache + " " );
   			iCallLogCurrent += ( dest_no + "\n" );
   			iCallLogCurrent += DateFormat.format("dd MMM hh:mm A", new Date().getTime()).toString();
    			
   			for ( int i = MAX_CALL_LOG-1; i > 0; i-- )iDialLogStr[i] = iDialLogStr[i-1];
   			iDialLogStr[0] = iCallLogCurrent;
    			
   			String str_dial_log = "";
    			
   			for ( int i = 0; i < MAX_CALL_LOG; i++ )if ( iDialLogStr[i] != null && iDialLogStr[i].length() > 0 )str_dial_log += iDialLogStr[i] + "\n";
   			iMainView.getContext().getSharedPreferences("SmoothDialer", 0).edit().putString("dial_log", str_dial_log).commit();
    	}
    	catch(Exception e){}
    	
    	iAudioPlayer.SetLoudSpeaker(false);
    	SendToTunnel(TUNNEL_COMMAND_CALL, dest_no, "", "", "", "", "", "");
    }
    
    public void EndCall()
    {
    	SendToTunnel(TUNNEL_COMMAND_END_CALL, "", "", "", "", "", "", "");
    }
    
    static 
    {
    	try
    	{
    		System.loadLibrary("g729");
    		Log.e("dialer", "g729 loaded successfully !");    		
  		
    		System.loadLibrary("DialerAgent");    		
    		Log.e("dialer","DialerAgent loaded successfully...");
    	}
    	catch(Exception e)
    	{
    		Log.e("dialer","g729 or DialerAgent load failure...");
    		Log.e("dialer", e.getMessage());
    	}
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        super.onCreateOptionsMenu(menu);
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) 
    {
        super.onPrepareOptionsMenu(menu);
        menu.clear();

        String str_menu = "Settings:Reconnect:IVR:Call Logs:Dial log:Exit:"; // Sound Test: Exit

        int prev_pos = 0;
        int menu_id = 1000;
        
        for ( int i = 0; i < str_menu.length(); i++ )
        {
        	  if ( str_menu.charAt(i) != ':' )continue;
        	  if ( i > prev_pos )menu.add(0,menu_id++,0,str_menu.substring(prev_pos, i));
        	  prev_pos = i + 1;
        }        
        return true;
    }    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
    	if ( item.getItemId() == 1000 )
    	{
    		ShowSettingsForm();
    		return true;
    	}
    	else if ( item.getItemId() == 1001 )ReConnect();
    	else if ( item.getItemId() == 1002 )SendToTunnel(TUNNEL_COMMAND_CALL_IVR,"","","","","","","");
    	else if ( item.getItemId() == 1003 )ShowCallLog(true);
    	else if ( item.getItemId() == 1004 )ShowCallLog(false);
    	else
    	{
    		if ( ( iRunningState & RUNNING_STATE_SHOULD_CONFIRM_EXIT ) != 0 )iMainView.ConfirmExit();
    		else SendToTunnel(TUNNEL_COMMAND_EXIT,"","","","","","","");
    	}
    	
        return super.onOptionsItemSelected(item);
    }
    
    public void ShowContacts()
    {
   		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
   	    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
    }
    
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) 
    {
        super.onActivityResult(reqCode, resultCode, data);
        if ( reqCode == PICK_CONTACT && resultCode == Activity.RESULT_OK )
        {
        	Uri contactData = data.getData();  
            String id = contactData.getLastPathSegment();
                 
            Cursor c = getContentResolver().query(Phone.CONTENT_URI, null, Phone._ID + "=?", new String[]{id}, null);

            if(!c.moveToFirst())return;                
            do
            {
                int phone_no_index = c.getColumnIndex(Phone.NUMBER);
                int name_index = c.getColumnIndex(Phone.DISPLAY_NAME);
            	
              	String phone_no = ( ( phone_no_index < 0 ) ? "" : c.getString(phone_no_index));
               	String display_name = ( ( name_index < 0 ) ? "" : c.getString(name_index));

               	String phone_no1 = "";
               	for ( int i = 0; i < phone_no.length(); i++ )if ( phone_no.charAt(i) >= '0' && phone_no.charAt(i) <= '9' )phone_no1 += phone_no.charAt(i);
                   
               	if ( phone_no1.length() > 0 )
               	{
               		iMainView.iPhoneNoStr = phone_no1;

               		iContactNameCache = display_name;
               		iContactNoCache = phone_no1;
               		
               		iMainView.invalidate();
               		break;
               	}
            }
            while(c.moveToNext());            
        }
    }

    public static String view_str1 = "";
    private static byte view_str[] = new byte[512];
    private static byte play_buf[] = new byte[3200];
    private static byte record_buf[] = new byte[3200];
    
    private static String tunnel_command = "";
    private static byte tunnel_command_byte[] = new byte[512];
    
    synchronized private void SendToTunnel(int cmd,String param1,String param2,String param3,String param4,String param5,String param6,String param7 )
    {
    	if ( cmd == -1 )
    	{
    		int i = 0;
    		for ( ; i < tunnel_command.length() && i < ( tunnel_command_byte.length - 1 ); i++ )tunnel_command_byte[i] = (byte)tunnel_command.charAt(i);
    		tunnel_command_byte[i] = 0;
    		tunnel_command = "";
    		return;
    	}
    	
    	tunnel_command = String.valueOf(cmd);    	tunnel_command += "#";
    	tunnel_command += param1;					tunnel_command += "#";
    	tunnel_command += param2;					tunnel_command += "#";
    	tunnel_command += param3;					tunnel_command += "#";
    	tunnel_command += param4;					tunnel_command += "#";
    	tunnel_command += param5;					tunnel_command += "#";
    	tunnel_command += param6;					tunnel_command += "#";
    	tunnel_command += param7;					tunnel_command += "#";    	
    }
    
    public native int DialerTunnelTick( int current_time_ms, byte[] tunnel_command, byte[] record_buf, int record_len, byte[] play_buf, int play_buf_size, byte[] view_str,int view_str_size);

    private void update_view()
    {
    	for ( int i = 0; i < 6; i++ )iDisplayStrings[i] = "";
    	iCallbackEvent = iCallbackEventParam = 0;
    	iCallbackEventParam1 = "";
    	
    	int prev_pos = 0;
    	int cnt = 0;
    	
    	for ( int i = 0; i < view_str.length; i++ )
    	{    		
    		if ( view_str[i] != '\n' && view_str[i] != 0 )continue;

    		if ( cnt == 0 || cnt == 1 || cnt == 8 )
    		{
    			int tmp = 0;
    			for ( int j = prev_pos; j < i; j++ )if ( view_str[j] >= '0' && view_str[j] <= '9' )tmp = tmp * 10 + view_str[j] - '0';
    			
    			if ( cnt == 0 )iCallbackEvent = tmp;
    			else if ( cnt == 1 )SetRunningState(tmp);
    			else if ( cnt == 8 )iCallbackEventParam = tmp;
    		}
    		else
    		{
    			String s = "";
    			for ( int j = prev_pos; j < i; j++ )s += (char)view_str[j];
    			if ( cnt == 9 )iCallbackEventParam1 = s;
    			else if ( cnt >= 2 && cnt < 8 )iDisplayStrings[cnt-2] = s;
    		}
    		
    		cnt++;
    		prev_pos = i + 1;
    		if ( view_str[i] == 0 )break;
    	}
    	
    	if ( iDisplayStrings[1].length() > 0 && ( iDisplayStrings[1].startsWith("#") || iDisplayStrings[1].endsWith("#")))
    	{
    		int dot_count = (int)((System.currentTimeMillis()/500)%5);
    		String s = "";
    		if ( iDisplayStrings[1].startsWith("#"))
    		{
    			for ( int j = 0; j < dot_count; j++ )s += ". ";
    			s += iDisplayStrings[1].substring(1);
    			for ( int j = 0; j < dot_count; j++ )s += "  ";
    		}
    		
    		if ( iDisplayStrings[1].endsWith("#"))
    		{
    			for ( int j = 0; j < dot_count; j++ )s += "  ";
    			s += iDisplayStrings[1].substring(0, iDisplayStrings[1].length()-1);
    			for ( int j = 0; j < dot_count; j++ )s += " .";
    		}
    		iDisplayStrings[1] = s;
    	}
    	
    	iMainView.update_view(iDisplayStrings, iRunningState, iCallbackEvent, iCallbackEventParam,iCallbackEventParam1);    	
    }
    
    public void run()
    {
    	while(true)
    	{
    		try
    		{
    			// SendToTunnel() is a synchronized function.
    			// Calling SendToTunnel(-1...) converts tunnel_command_str to byte array.
    			
    			SendToTunnel(-1, "", "", "", "", "", "", ""); 
    	
    			long t1 = System.currentTimeMillis();
    			int record_len = iAudioRecorder.ReadOrWriteBuffer(true, record_buf, record_buf.length);    			
    			int play_len = DialerTunnelTick( (int)( System.currentTimeMillis() & 0x7FFFFFFF ), tunnel_command_byte, record_buf, record_len, play_buf, play_buf.length, view_str, 512);
    			long t2 = System.currentTimeMillis() - t1;
    			
    			if ( t2 > 50 )System.out.println("XXXXX DialerTunnelTick used " + String.valueOf(t2) + " ms. rec/play: " + String.valueOf(record_len) + " : " + String.valueOf(play_len));
    			
    			if ( play_len > 0 )
    			{
    				iAudioPlayer.ReadOrWriteBuffer(false,play_buf,play_len);
    			}
    		}
    		catch(Exception e)
    		{
    			e.printStackTrace();
    		}
    		
    		view_str1 = "";
    		for ( int i = 0; i < view_str.length; i++ )
    		{
    			if ( view_str[i] == 0 )break;
    			view_str1 += (char)view_str[i];
    		}    		
    		
    		try
    		{
    			Thread.sleep(TUNNEL_TIMER_MS,0);
    		}
    		catch(Exception e)
    		{
    			break;
    		}
    		
    		update_view();        	
    	}
    }	
}
