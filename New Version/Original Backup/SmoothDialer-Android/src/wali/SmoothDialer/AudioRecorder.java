package wali.SmoothDialer;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

public class AudioRecorder implements Runnable 
{
	private boolean iRunning = false;
	private boolean iStopped = false;
	public  boolean iRecording = false;
	
	private AudioManager iAudioManager = null;
	private Context iContext = null;
	private SmoothDialer iActivity = null;
	
	private AudioRecord iAudioRecorder = null;	
    int iBufSize = 0;

	private byte iRecordBuf[] = new byte[16000];
	private int  iRecordBufSize = 0;
	
	private int iLocked = 2;
	
	public AudioRecorder(SmoothDialer main_activity,Context context)
	{
		iActivity = main_activity;
		iContext = context;
				
		iRunning = true;
		Thread t = new Thread(this);
		
		iLocked = 2;
		t.start();
		
		while( iLocked != 0 )
		{
			try
			{
				Thread.sleep(100);
			}
			catch(Exception e){}
		}
	}
	
	public void Stop()
	{
		iRunning = false;

		for ( int i = 0; i < 10; i++ )
		{
			if ( iStopped )break;
			
			try
			{
				Thread.sleep(50);
			}
			catch(Exception e){}
		}		
	}
	
	synchronized public void ResizeAudioBuffer(int max_size)
	{
		iRecordBufSize = 0;
	}
	
	synchronized public int ReadOrWriteBuffer(boolean is_read,byte dt[],int len)  // for read, len is max_len
	{
		if ( is_read )
		{
			if ( iRecordBufSize <= 0 )return 0;
			if ( len > iRecordBufSize )len = iRecordBufSize;
			System.arraycopy(iRecordBuf, 0, dt, 0, len);
			if ( iRecordBufSize > len )System.arraycopy(iRecordBuf, len, iRecordBuf, 0, iRecordBufSize - len);
			iRecordBufSize -= len;
			return len;
		}
		
		if ( len > iRecordBuf.length )len = iRecordBuf.length;
		if ( ( iRecordBufSize + len ) > iRecordBuf.length )
		{
			int delete_len = iRecordBufSize + len - iRecordBuf.length;
			if ( delete_len < iRecordBufSize )System.arraycopy(iRecordBuf, delete_len, iRecordBuf, 0, iRecordBufSize - delete_len);
			iRecordBufSize -= delete_len;
		}
		System.arraycopy(dt, 0, iRecordBuf, iRecordBufSize, len);
		iRecordBufSize += len;
		return len;
	}
	
	public void run()
	{
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);				
		int native_sample_rate = 8000;
		
		int record_buf_unit = AudioRecord.getMinBufferSize(native_sample_rate, MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT);		
		if ( record_buf_unit < 3200 )
		{
			if ( ( 3200 % record_buf_unit ) != 0 )record_buf_unit = 3200 + record_buf_unit - 3200 % record_buf_unit;
			else record_buf_unit = 3200;
		}

		final int supported_sources[] = {MediaRecorder.AudioSource.DEFAULT, MediaRecorder.AudioSource.VOICE_UPLINK, MediaRecorder.AudioSource.MIC };
		
		int record_source = -1;		
		
		for ( int i = 0; i < supported_sources.length; i++ )
		{
			record_source = supported_sources[i];
			try
			{
				iAudioRecorder = new AudioRecord(record_source,native_sample_rate,AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT,record_buf_unit * 2);
				if ( iAudioRecorder.getState() == AudioRecord.STATE_UNINITIALIZED )continue;
		
	/*    		try
	    		{
	    			AcousticEchoCanceler aec = AcousticEchoCanceler.create(iAudioRecorder.get_session_id());
	    			if ( aec != null && aec.isAvailable() )aec.setEnabled(true);
	    		}
	    		catch(Exception e){}*/

				break;
			}
			catch(Exception e){}
		}		
		
        try
        {
			iAudioManager.setStreamVolume(record_source,iAudioManager.getStreamMaxVolume(record_source), 0);			
        }
        catch(Exception e){}
        
        
        iLocked = 0;
        iRecording = false;
        
        byte record_buf[] = new byte[record_buf_unit];
        
        while ( iRunning )
        {        	
       		try
       		{
       			Thread.sleep(50);
       		}
       		catch(Exception e){};        	
       		if ( !iRecording )continue;        	        	

       		if ( iAudioRecorder.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING )
       		{
       			try
       			{
       				iAudioRecorder.startRecording();
       			}
       			catch(Exception e){}
       		}
       		else
       		{
       			int record_len = iAudioRecorder.read(record_buf, 0, record_buf_unit );
       			if ( record_len > 0 )
       			{
       				ReadOrWriteBuffer(false, record_buf, record_len);
       			}
       		}
        }

        if ( iAudioRecorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING )iAudioRecorder.stop();        
        iAudioRecorder.release();
        iStopped = true;
	}
}
