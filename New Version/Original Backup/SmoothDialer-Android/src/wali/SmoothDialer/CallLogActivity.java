package wali.SmoothDialer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

public class CallLogActivity extends Activity 
{
	public static boolean iShowDialLog = false;
	public static SmoothDialer iActivity = null;
	
	private TableLayout iTable = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_log_view);
        
        this.setTitle(iShowDialLog ? "Dial Logs" : "Call Logs");
        iTable = (TableLayout)this.findViewById(R.id.call_log);        
        if ( iTable == null )return;
        
        try
        {
        	iTable.removeAllViews();
        	int cnt = 0;
       		for ( int i = 0; i < iActivity.MAX_CALL_LOG; i++ )
       		{
        		String str = ( iShowDialLog ? iActivity.iDialLogStr[i] : iActivity.iCallLogStr[i] );
        		if ( str == null || str.length() <= 0 )continue;
        		
        		AddLog(str);
        		cnt++;
       		}

       		if ( cnt <= 0 )AddLog(iShowDialLog ? "Dial log empty" : "Call log empty");
       		iTable.invalidate();
        }
        catch(Exception e){}
    }
    
    private View.OnClickListener iRowClickListener = new View.OnClickListener() 
    {		
		@Override
		public void onClick(View view) 
		{
			try
			{
				String str_log = ((TextView)view).getText().toString();
				iActivity.CallFromLog(str_log);				
			}
			catch(Exception e){}
			
			finish();
		}
	};
	
    private void AddLog(String str)
    {
    	if ( iTable == null )return;
    	
		TextView tv1 = new TextView(iTable.getContext());				
		
		tv1.setText(str);
		tv1.setPadding(2,2,2,5);
		tv1.setTextColor(Color.WHITE);
		iTable.addView(tv1);
		
		tv1.setOnClickListener(iRowClickListener);		
    }
}
