package wali.SmoothDialer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Paint.Align;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.shapes.Shape;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;

public class MainView extends SurfaceView implements DialogInterface.OnClickListener
{
	public  static	SmoothDialer iActivity = null;
	public  final static int QUERY_DEFAULT = 0,QUERY_MAIN_EXIT = 1;

	private int   	iScreenWidth = 320;
    private int   	iScreenHeight = 480;

    private int 	prev_running_state = 0;
    private int		prev_callback_event = SmoothDialer.CALLBACK_EVENT_NONE;
    private int		prev_callback_param = 0;
    private String	prev_callback_param1 = "";
    
    private boolean	iInitDone = false;

    private Bitmap	iCallAcceptBitmap = null;
    private Bitmap	iDialBitmap = null;
    private Bitmap  iDeleteBitmap = null;
    private Bitmap  iContactsBitmap = null;
    private Bitmap	iTickBitmap = null;
    private Bitmap	iMenuBitmaps[] = new Bitmap[3];

    private String	iBrandNameStr = "";
    private String	iStateStr = "";
    private String	iStatusStr = "";
    private String  iBalanceStr = "";
    private String  iCallDurationStr = "";
    private String  iFooterStr = "";
    public  String	iPhoneNoStr = "";

    private final int iItemY[] = {5,18,30,42,55,75,88,85};
    private final int iItemFontSize[] = {18,16,12,12,14,20,12};
    private int		iItemFontMultiply = 10;						// This is divided by 10 during operation
    
    private final int iItemColor[] = {Color.BLACK,Color.BLACK,Color.DKGRAY,Color.DKGRAY,Color.RED,Color.BLACK,Color.DKGRAY};
    private float iTextWidths[] = new float[128];

    private Paint	 iPaint = new Paint();
    private Bitmap 	 iBitmap = null;
    private Canvas 	 iCanvas = null;

    private int		 iKeyboardTop = 0;
	private int		 iKeyboardKeyCount = 15;
	private String	 iKeyboardText[] = {"1","2","3","4","5","6","7","8","9","*","0","#","","",""};
	private Rect	 iKeyboardKeyRects[] = null;
	private Rect	 iMenuRects[] = null;
    private int		 iKeyboardSelectedKey = -1;

	private int		iCursorPos = 0;
	private boolean iCursorState = false;

    private final Handler iRefreshFromThreadHandler = new Handler();

    private int	 iQueryID = QUERY_DEFAULT;

    public MainView(Context context){super(context);}
    public MainView(Context context, AttributeSet attrs){super(context, attrs);}
    public MainView(Context context, AttributeSet attrs, int defStyle){super(context, attrs, defStyle);}

    private Drawable	iKeyboardRow = null;
    private Drawable	iKeyboardRowFaded = null;
    private Drawable	iKeyboardRowSelected = null;
    
    final ToneGenerator iToneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

    public void onClick(DialogInterface d,int code)
    {
    	if ( iInitDone == false )return;

    	if ( code == -1 )code = 0;						//This is OK button of the ask message
    	else if ( code == -2 )code = 1;					//This is Cancel button of the ask message
    	else if ( code == -3 )code = 2;					//This is Middle button of the ask message

    	if ( iQueryID == QUERY_MAIN_EXIT )
    	{
    		if ( code == 0 )iActivity.ExitApp();
    	}
    }

    public void Create(SmoothDialer parent,int w,int h)
    {
    	iActivity = parent;

    	iScreenWidth = w;
    	iScreenHeight = h;

    	if ( iInitDone )return;

    	iBitmap = null;
    	iCanvas = null;

    	iActivity.getWindow().setFormat(PixelFormat.RGB_565);
   		iBitmap = Bitmap.createBitmap(iScreenWidth,iScreenHeight, Bitmap.Config.RGB_565);
    	iCanvas = new Canvas();
    	iCanvas.setBitmap(iBitmap);

        this.setFocusable(true);
        this.setFocusableInTouchMode(true);

        iInitDone = true;
        this.setWillNotDraw(false);

        this.setOnKeyListener(new OnKeyListener()
        {
			@Override
			public boolean onKey(View arg0, int key_code, KeyEvent event_action)
			{
		         if ( key_code == KeyEvent.KEYCODE_BACK )
		         {
		        	 if ( event_action.getAction() != KeyEvent.ACTION_DOWN )return true;

		        	 int running_state = iActivity.GetRunningState();
	                 if ( ( running_state & ( SmoothDialer.RUNNING_STATE_AUDIO | SmoothDialer.RUNNING_STATE_AUDIO_SETTINGS | SmoothDialer.RUNNING_STATE_INGING ) ) != 0 )
	                 {
	                	 iActivity.EndCall();
	                	 return true;
	                 }

	                 if ( ( running_state & SmoothDialer.RUNNING_STATE_SHOULD_CONFIRM_EXIT ) != 0 )ConfirmExit();
	                 else iActivity.ExitApp();
	                 
	                 return true;
	             }
	             return false;
			}
		 });


        iContactsBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.contacts);
        iCallAcceptBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.call_accept);
        iDialBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.call);
        iDeleteBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.delete);
        iTickBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.tick);

        iMenuBitmaps[0] = BitmapFactory.decodeResource(this.getResources(),R.drawable.speaker);
        iMenuBitmaps[1] = BitmapFactory.decodeResource(this.getResources(),R.drawable.call_end);
        iMenuBitmaps[2] = BitmapFactory.decodeResource(this.getResources(),R.drawable.bluetooth_in_call);

        iKeyboardRow = this.getContext().getResources().getDrawable(R.layout.keyboard_row);
        iKeyboardRowFaded = this.getContext().getResources().getDrawable(R.layout.keyboard_row_faded);
        iKeyboardRowSelected = this.getContext().getResources().getDrawable(R.layout.keyboard_row_selected);

        CalculateKeyRects();
        invalidate();
    }

    public void ConfirmExit()
    {
   	 	iQueryID = QUERY_MAIN_EXIT;

	    Dialog dlg = new AlertDialog.Builder(this.getContext())
	    .setIcon(R.drawable.icon_mini)
	    .setTitle("Confirmation")
	    .setPositiveButton("Yes",this)
	    .setNegativeButton("No",this)
	    .setMessage("Do you want to exit?")
	    .create();

	    dlg.show();
	}

    @Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
    	iScreenWidth = w;
    	iScreenHeight = h;

    	CalculateKeyRects();
    	invalidate();
    }

    private void CalculateKeyRects()
    {
    	if ( iKeyboardKeyRects == null )
    	{
    		iKeyboardKeyRects = new Rect[iKeyboardKeyCount];
    		for ( int i = 0; i < iKeyboardKeyCount; i++ )iKeyboardKeyRects[i] = new Rect(0,0,0,0);
    	}

    	if ( iMenuRects == null )
    	{
    		iMenuRects = new Rect[3];
    		for ( int i = 0; i < 3; i++ )iMenuRects[i] = new Rect(0,0,0,0);
    	}

    	if ( iScreenHeight > 640 )iItemFontMultiply = 20;
    	else if ( iScreenHeight > 480 )iItemFontMultiply = 15;
    	else iItemFontMultiply = 10;
    	
	 	int key_height = iScreenHeight/10;
	 	int key_width = iScreenWidth/3;

	    iKeyboardTop = iScreenHeight - key_height * 5;
	    for ( int i = 0; i < iKeyboardKeyCount; i++ )
	    {
	    	int key_x = iScreenWidth/2 - ( key_width * 3)/2 + key_width * ( i % 3 );
	    	int key_y = iKeyboardTop + key_height * ( i / 3 );

	    	iKeyboardKeyRects[i].left = key_x;
	    	iKeyboardKeyRects[i].top = key_y;
	    	iKeyboardKeyRects[i].right = key_x + key_width;
	    	iKeyboardKeyRects[i].bottom = key_y + key_height;
	    }

	    for ( int i = 0; i < 3; i++ )
	    {
	    	iMenuRects[i].left = iKeyboardKeyRects[i].left;
	    	iMenuRects[i].top = iKeyboardKeyRects[9+i].top;
	    	iMenuRects[i].right = iKeyboardKeyRects[i].right;
	    	iMenuRects[i].bottom = iKeyboardKeyRects[12+i].bottom;
	    }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	if ( iInitDone == false )return true;

    	int x = (int)event.getX();
    	int y = (int)event.getY();
   	 	int event_type = event.getAction();

   	 	int input_y = 15 + ( iItemY[5] * iScreenHeight )/200;
   	 	if ( y <= iKeyboardTop && y >= ( input_y - 25 ) && y < ( input_y + 25 ) && event_type == MotionEvent.ACTION_DOWN )
   	 	{
   	        iPaint.getTextWidths(iPhoneNoStr, iTextWidths);

   	        int phone_no_w = 0;
   	        for ( int i = 0; i < iPhoneNoStr.length(); i++ )phone_no_w += iTextWidths[i];

   	        int tmp_x = iScreenWidth/2 - phone_no_w/2;
   	        boolean b_found = false;

   	        for ( int i = 0; i < iPhoneNoStr.length(); i++ )
   	        {
   	        	tmp_x += iTextWidths[i];
   	        	if ( x < tmp_x )
   	        	{
   	        		iCursorPos = i;
   	        		b_found = true;
   	        		break;
   	        	}
   	        }

   	        if ( !b_found )iCursorPos = iPhoneNoStr.length();
   	        iCursorState = false;
   	        invalidate();
   	 		return true;
   	 	}

   	 	int running_state = iActivity.GetRunningState();
   	 	
   	 	if ( ( running_state & ( SmoothDialer.RUNNING_STATE_ACCEPT_INCOMING_CALL | SmoothDialer.RUNNING_STATE_AUDIO | SmoothDialer.RUNNING_STATE_AUDIO_SETTINGS | SmoothDialer.RUNNING_STATE_INGING ) ) != 0 )
   	 	{
   	 		if ( y >= iMenuRects[0].top && y <= iMenuRects[0].bottom )
   	 		{
   	 			if ( event_type != MotionEvent.ACTION_DOWN )return true;
   	 			if ( ( running_state & ( SmoothDialer.RUNNING_STATE_AUDIO_SETTINGS | SmoothDialer.RUNNING_STATE_AUDIO | SmoothDialer.RUNNING_STATE_ACCEPT_INCOMING_CALL ) ) == 0 )
   	 			{
   	 				iActivity.EndCall();
   	 				return true;
   	 			}

   	 			if ( x >= iMenuRects[1].left && x < iMenuRects[1].right )iActivity.EndCall();
   	 			else if ( x < iMenuRects[0].right )
   	 			{
   	 				if ( ( running_state & SmoothDialer.RUNNING_STATE_ACCEPT_INCOMING_CALL) != 0 )iActivity.Call("");
   	 				else iActivity.SetLoudSpeaker(!iActivity.IsLoudSpeaker());
   	 			}
   	 			else iActivity.SetBlueToothSpeaker(!iActivity.IsBlueToothSpeaker());

   	 			invalidate();
   	 			return true;
   	 		}
   	 	}

    	if ( y > iKeyboardTop && event_type == MotionEvent.ACTION_DOWN );
    	else if ( y > iKeyboardTop && event_type == MotionEvent.ACTION_UP )
    	{
    		iKeyboardSelectedKey = -1;
    		invalidate();
    		return true;
    	}
    	else return true;

		iKeyboardSelectedKey = -1;
		for ( int i = 0; i < iKeyboardKeyCount; i++ )
		{
			if ( iKeyboardKeyRects[i].contains(x,y) )
			{
				iKeyboardSelectedKey = i;
				break;
			}
		}

		if ( iKeyboardSelectedKey < 0 )return true;

		if ( iKeyboardSelectedKey == iKeyboardKeyCount - 1 )
		{
			if ( iCursorPos < 0 )iCursorPos = 0;
			if ( iCursorPos > iPhoneNoStr.length() )iCursorPos = iPhoneNoStr.length();

			if ( iPhoneNoStr.length() > 0 )
			{
				if ( iCursorPos <= 0 );
				else if ( iCursorPos >= iPhoneNoStr.length() )iPhoneNoStr = iPhoneNoStr.substring(0,iPhoneNoStr.length()-1);
				else iPhoneNoStr = iPhoneNoStr.substring(0,iCursorPos-1) + iPhoneNoStr.substring(iCursorPos);

				iCursorPos--;
				if ( iCursorPos < 0 )iCursorPos = 0;
			}
		}
		else if ( iKeyboardSelectedKey == iKeyboardKeyCount - 3 )
		{
			iActivity.ShowContacts();
			iKeyboardSelectedKey = -1;
		}
		else if ( iKeyboardSelectedKey == iKeyboardKeyCount - 2 )
		{
			iActivity.Call(iPhoneNoStr);		// Allow empty also. Because, call(empty_no) will show dial log
			iKeyboardSelectedKey = -1;
			return true;
		}
		else if ( iPhoneNoStr.length() < 20 )
		{
			if ( iCursorPos <= 0 )iPhoneNoStr = iKeyboardText[iKeyboardSelectedKey] + iPhoneNoStr;
			else if ( iCursorPos >= iPhoneNoStr.length() )iPhoneNoStr += iKeyboardText[iKeyboardSelectedKey];
			else iPhoneNoStr = iPhoneNoStr.substring(0,iCursorPos) + iKeyboardText[iKeyboardSelectedKey] + iPhoneNoStr.substring(iCursorPos);

			iCursorPos++;
		}

	    iToneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP);

    	invalidate();
    	return true;
    }


    private void DrawShortMenu()
    {       	
    	int running_state = iActivity.GetRunningState();
    	
    	if ( ( running_state & ( SmoothDialer.RUNNING_STATE_INGING | SmoothDialer.RUNNING_STATE_AUDIO_SETTINGS ) ) == 0 )return;

    	if ( ( running_state & SmoothDialer.RUNNING_STATE_AUDIO_SETTINGS ) == 0 && ( running_state & SmoothDialer.RUNNING_STATE_ACCEPT_INCOMING_CALL ) == 0 )
    	{
    		if ( iMenuBitmaps[1] != null )iCanvas.drawBitmap(iMenuBitmaps[1], iMenuRects[1].centerX() - iMenuBitmaps[1].getWidth()/2, iMenuRects[1].centerY() - iMenuBitmaps[1].getHeight()/2, iPaint);
    		return;
    	}

    	for ( int i = 0; i < 3; i++ )
    	{
    		if ( ( running_state & SmoothDialer.RUNNING_STATE_ACCEPT_INCOMING_CALL ) != 0 && i == 0 )
    		{
    			iCanvas.drawBitmap(iCallAcceptBitmap, iMenuRects[i].centerX() - iCallAcceptBitmap.getWidth()/2, iMenuRects[i].centerY() - iCallAcceptBitmap.getHeight()/2, iPaint);
    			continue;
    		}
    		
    		if ( iMenuBitmaps[i] != null )iCanvas.drawBitmap(iMenuBitmaps[i], iMenuRects[i].centerX() - iMenuBitmaps[i].getWidth()/2, iMenuRects[i].centerY() - iMenuBitmaps[i].getHeight()/2, iPaint);
    		if ( i == 0 && iActivity.IsLoudSpeaker() )
    		{
    			if ( iTickBitmap != null )iCanvas.drawBitmap(iTickBitmap, iMenuRects[i].right - iTickBitmap.getWidth(),iMenuRects[i].top,iPaint);
    		}
    	}
    }

    private void DrawKeyboard()
    {
    	int running_state = iActivity.GetRunningState();
   		if ( running_state != 0 )
   		{
   			for ( int i = 0; i < 3; i++ )
   			{
   				iKeyboardRowFaded.setBounds(0,iKeyboardKeyRects[i*3].top,iScreenHeight,iKeyboardKeyRects[i*3].bottom);
   				iKeyboardRowFaded.draw(iCanvas);
   			}   			
			
   			iKeyboardRowFaded.setBounds(0,iKeyboardKeyRects[3*3].top,iScreenHeight,iKeyboardKeyRects[4*3].bottom);
   			iKeyboardRowFaded.draw(iCanvas);
   		}
    	else
    	{
    		for ( int i = 0; i < 5; i++ )
    		{
    			iKeyboardRow.setBounds(0,iKeyboardKeyRects[i*3].top,iScreenHeight,iKeyboardKeyRects[i*3].bottom);
    			iKeyboardRow.draw(iCanvas);
    		}
    	}
   		
   		if ( iKeyboardSelectedKey >= 0 && iKeyboardSelectedKey < iKeyboardKeyCount )
   		{
   			iKeyboardRowSelected.setBounds(iKeyboardKeyRects[iKeyboardSelectedKey]);
   			iKeyboardRowSelected.draw(iCanvas);
   		} 	

		iPaint.setColor((running_state == 0) ? Color.GRAY : Color.LTGRAY);
		int divider_bottom = iScreenHeight;
		if ( ( running_state & SmoothDialer.RUNNING_STATE_AUDIO_SETTINGS ) == 0 && ( running_state & SmoothDialer.RUNNING_STATE_INGING ) != 0 )divider_bottom = iKeyboardKeyRects[6].bottom;
		
		iCanvas.drawLine(iScreenWidth/3, iKeyboardKeyRects[0].top, iScreenWidth/3, divider_bottom,iPaint);
		iCanvas.drawLine((iScreenWidth*2)/3, iKeyboardKeyRects[0].top, (iScreenWidth*2)/3, divider_bottom,iPaint);

   	 	iPaint.setTextAlign(Align.CENTER);
   	 	iPaint.setTextSize((iItemFontSize[1] * iItemFontMultiply)/10);
   	 	iPaint.setAntiAlias(true);

   	 	int key_count = ( ( running_state != 0 ) ? 9 : iKeyboardKeyCount );

   	 	for ( int i = 0; i < key_count; i++ )
   	 	{
   	 		if ( i == iKeyboardKeyCount - 3 )
   	 		{
   	 			if ( iContactsBitmap != null )iCanvas.drawBitmap(iContactsBitmap, iKeyboardKeyRects[i].centerX() - iContactsBitmap.getWidth()/2, iKeyboardKeyRects[i].centerY() - iContactsBitmap.getHeight()/2, iPaint );
   	 		}
   	 		else if ( i == iKeyboardKeyCount - 2 )
   	 		{
   	 			if ( iDialBitmap != null )iCanvas.drawBitmap(iDialBitmap, iKeyboardKeyRects[i].centerX() - iDialBitmap.getWidth()/2, iKeyboardKeyRects[i].centerY() - iDialBitmap.getHeight()/2, iPaint );
   	 		}
   	 		else if ( i == iKeyboardKeyCount - 1 )
   	 		{
   	 			if ( iDeleteBitmap != null )iCanvas.drawBitmap(iDeleteBitmap, iKeyboardKeyRects[i].centerX() - iDeleteBitmap.getWidth()/2, iKeyboardKeyRects[i].centerY() - iDeleteBitmap.getHeight()/2, iPaint );
   	 		}
   	 		else
   	 		{
   	 			iPaint.setColor( ( running_state != 0 ) ? Color.LTGRAY : Color.BLACK );
   	 			iCanvas.drawText(iKeyboardText[i], iKeyboardKeyRects[i].centerX(), iKeyboardKeyRects[i].centerY() + 8, iPaint);
   	 		}
   	 	}

   	 	iPaint.setAntiAlias(false);
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        iPaint.setColor(Color.WHITE);
        iCanvas.drawRect(0,0, iScreenWidth, iScreenHeight, iPaint);

        iPaint.setAntiAlias(true);
        iPaint.setTextAlign(Align.CENTER);

        for ( int i = 0; i < 7; i++ )
        {
        	String s = "";
        	if ( i == 0 )s = iBrandNameStr;
        	else if ( i == 1 )s = iStateStr;
        	else if ( i == 2 )s = iStatusStr;
        	else if ( i == 3 )s = iBalanceStr;
        	else if ( i == 4 )s = iCallDurationStr;
        	else if ( i == 5 )continue;
        	else s = iFooterStr;

        	if ( s.length() <= 0 )continue;

        	iPaint.setColor(iItemColor[i]);
        	iPaint.setTextSize((iItemFontSize[i] * iItemFontMultiply)/10);

        	iCanvas.drawText(s, iScreenWidth/2, 15 + ( iItemY[i] * iScreenHeight )/200, iPaint);
        }

        if ( iActivity.iContactNoCache.length() > 3 && iPhoneNoStr.endsWith(iActivity.iContactNoCache) )
        {
        	iPaint.setColor(Color.BLUE);
        	iPaint.setTextSize(12);

        	iCanvas.drawText(iActivity.iContactNameCache, iScreenWidth/2, 15 + ( iItemY[5] * iScreenHeight )/200 - 20, iPaint);
        }

        iPaint.setColor(Color.BLACK);
        iPaint.setTextSize((iItemFontSize[5] * iItemFontMultiply)/10);
        iPaint.getTextWidths(iPhoneNoStr, iTextWidths);
        iCanvas.drawText(iPhoneNoStr, iScreenWidth/2, 15 + ( iItemY[5] * iScreenHeight )/200, iPaint);

        if ( iCursorPos < 0 )iCursorPos = 0;
        if ( iCursorPos > iPhoneNoStr.length() )iCursorPos = iPhoneNoStr.length();

        int cursor_x = 0;
        int phone_no_w = 0;
        for ( int i = 0; i < iCursorPos; i++ )cursor_x += iTextWidths[i];
        for ( int i = 0; i < iPhoneNoStr.length(); i++ )phone_no_w += iTextWidths[i];

        cursor_x += ( iScreenWidth - phone_no_w)/2;
        iCursorState = !iCursorState;

        int cursor_y = 15 + ( iItemY[5] * iScreenHeight )/200 - 2;
        iPaint.setColor((iCursorState)?Color.BLACK : Color.WHITE);
        iCanvas.drawText("|", cursor_x, cursor_y, iPaint );

        iPaint.setAntiAlias(false);

        DrawKeyboard();
        DrawShortMenu();
        
        canvas.drawBitmap(iBitmap, 0, 0, iPaint);
     }

     private Runnable iRefreshRunnable = new Runnable()
     {
 		public void run()
		{
 			invalidate();
 			if ( prev_callback_event == iActivity.CALLBACK_EVENT_NONE )return;
 			
 			if ( prev_callback_event == iActivity.CALLBACK_EVENT_CALL_COMPLETE || prev_callback_event == iActivity.CALLBACK_EVENT_INCOMING_CALL_COMPLETE )iActivity.AddCallLog(prev_callback_event == iActivity.CALLBACK_EVENT_INCOMING_CALL_COMPLETE,prev_callback_param1,prev_callback_param);
 			if ( prev_callback_event == iActivity.CALLBACK_EVENT_AUTH_ERROR || prev_callback_event == iActivity.CALLBACK_EVENT_COMPLETE_SIP_SETTINGS )iActivity.ShowSettingsForm(); 			
 			if ( prev_callback_event == iActivity.CALLBACK_EVENT_RESIZE_AUDIO_BUF )iActivity.ResizeAudioBuf(prev_callback_param);

 			prev_callback_event = iActivity.CALLBACK_EVENT_NONE; 			
		}
     };

     public void update_view(String display_items[],int running_state,int callback_event,int callback_event_param,String callback_event_param1)
     {
    	 boolean has_changed = false;
    	 
    	 if ( running_state != prev_running_state )has_changed = true;
    	 else if ( callback_event != prev_callback_event )has_changed = true;
    	 else if ( callback_event_param != prev_callback_param )has_changed = true;
    	 else if ( callback_event_param1.compareTo(prev_callback_param1) != 0 )has_changed = true;
    	 else if ( display_items[0].compareTo(iBrandNameStr) != 0 )has_changed = true;
    	 else if ( display_items[1].compareTo(iStateStr) != 0 )has_changed = true;
    	 else if ( display_items[2].compareTo(iStatusStr) != 0 )has_changed = true;
    	 else if ( display_items[3].compareTo(iBalanceStr) != 0 )has_changed = true;
    	 else if ( display_items[4].compareTo(iCallDurationStr) != 0 )has_changed = true;
    	 else if ( display_items[5].compareTo(iFooterStr) != 0 )has_changed = true;
    	 
    	 if ( !has_changed )return;
    	 
    	 prev_running_state = running_state;
    	 
    	 // Sometimes, callback event may not be handled because of working thread & gui delay.
    	 //This check will ensure that, the callback_events are handled appropriately.
    	 
    	 if ( callback_event != iActivity.CALLBACK_EVENT_NONE )		
    	 {
    		 prev_callback_event = callback_event;
    		 prev_callback_param = callback_event_param;
    		 prev_callback_param1 = callback_event_param1;
    	 }
    	 
    	 iBrandNameStr = display_items[0];
    	 iStateStr = display_items[1];
    	 iStatusStr = display_items[2];
    	 iBalanceStr = display_items[3];
    	 iCallDurationStr = display_items[4];
    	 iFooterStr = display_items[5];
    	 
    	 iRefreshFromThreadHandler.post(iRefreshRunnable);
     }

     public void ShowMessage(String str_title,String str,int query_id)
     {
    	    iQueryID = query_id;

    	    Dialog dlg = new AlertDialog.Builder(this.getContext())
    	    .setIcon(R.drawable.icon_mini) 						
    	    .setTitle(str_title)
    	    .setMessage(str)
    	    .create();

    	    dlg.show();
     }
};
