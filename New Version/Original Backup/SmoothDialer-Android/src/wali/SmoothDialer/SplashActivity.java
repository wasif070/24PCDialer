package wali.SmoothDialer;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity implements Runnable
{
    	private final Handler iRefreshFromThreadHandler = new Handler();

	    @Override
	    protected void onCreate(Bundle savedInstanceState) 
	    {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.splash_view);
	        
	        new Thread(this).start();
	    }

	    private Runnable iRefreshRunnable = new Runnable()
	    {
	 		public void run()
			{
	 			finish();
			}    	 
	    };
	    
	    public void run()
	    {
	    	try
	    	{
	    		Thread.sleep(3000);
	    	}
	    	catch(Exception e){}
	    	iRefreshFromThreadHandler.post(iRefreshRunnable);
	    }
}
