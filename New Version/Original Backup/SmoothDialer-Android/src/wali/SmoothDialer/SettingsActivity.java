package wali.SmoothDialer;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends Activity 
{
	public static SmoothDialer iActivity = null;
	
	public static String  iAuthUrl = "";
	public static String  iAuthUrlOptional = "";
	public static boolean iFixedOperatorCode = false;
	public static int	  iBaseOperatorCode = 0;
	
	public static	 int	iOperatorCode = -1;
	public static	 String iSipUsername = "";
	public static	 String iSipPassword = "";
	public static 	 String iSipPhoneNo = "";

	private EditText iOperatorCodeEdit = null;
	private EditText iSipUsernameEdit = null;
	private EditText iSipPasswordEdit = null;
	private EditText iSipPhoneNoEdit = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        
        if ( iFixedOperatorCode )
        {
        	setContentView(R.layout.settings_view_platinum);
        	iOperatorCodeEdit = null;
        }
        else
        {
        	setContentView(R.layout.settings_view);        	
        	iOperatorCodeEdit = (EditText)findViewById(R.id.operator_code);
        }
        
    	iSipUsernameEdit = (EditText)findViewById(R.id.sip_user);
    	iSipPasswordEdit = (EditText)findViewById(R.id.sip_pwd);
    	iSipPhoneNoEdit = (EditText)findViewById(R.id.sip_phoneno);
    	
    	if ( iOperatorCodeEdit != null )
    	{
    		if ( iOperatorCode > 0 )iOperatorCodeEdit.setText(String.valueOf(iOperatorCode));
    		else iOperatorCodeEdit.setText("");
    	}
    	
    	if ( iSipUsernameEdit != null )iSipUsernameEdit.setText(iSipUsername);
    	if ( iSipPasswordEdit != null )iSipPasswordEdit.setText(iSipPassword);
    	if ( iSipPhoneNoEdit != null )iSipPhoneNoEdit.setText(iSipPhoneNo);
        	
    	Button button_save = (Button)findViewById(R.id.button_save);
    	Button button_cancel = (Button)findViewById(R.id.button_cancel);
    	
    	button_cancel.setOnClickListener(new OnClickListener() 
    	{			
			@Override
			public void onClick(View v) 
			{
				finish();			
			}
		});
    	
    	button_save.setOnClickListener(new OnClickListener() 
    	{			
			@Override
			public void onClick(View v) 
			{
				SaveSettings(false);
			}
		});
    }
	
	@Override
	public void onBackPressed()
	{
	    SaveSettings(true);
	}
	
	private void SaveSettings(boolean from_back)
	{	
		boolean has_changed = false;
		if ( iFixedOperatorCode == false )
		{
			String str_tmp = iOperatorCodeEdit.getText().toString();	
			if ( str_tmp == null )str_tmp = "";
			
			int operator_code = 0;
			for ( int i = 0; i < str_tmp.length(); i++ )if ( str_tmp.charAt(i) >= '0' && str_tmp.charAt(i) <= '9' )operator_code = operator_code * 10 + ( str_tmp.charAt(i) - '0' );
			if ( operator_code <= 0 )
			{
				if ( from_back )return;		
				Toast t = Toast.makeText(this.getApplicationContext(), "Enter valid operator code !!!", 1000);
				t.show();
				return;
			}
			
			if ( operator_code != iOperatorCode )has_changed = true;
			iOperatorCode = operator_code;
		}	
		
		if ( iSipUsername.compareTo(iSipUsernameEdit.getText().toString()) != 0 )has_changed = true;
		iSipUsername = iSipUsernameEdit.getText().toString();
		
		if ( iSipPassword.compareTo(iSipPasswordEdit.getText().toString()) != 0 )has_changed = true;
		iSipPassword = iSipPasswordEdit.getText().toString();
		
		if ( iSipPhoneNo.compareTo(iSipPhoneNoEdit.getText().toString()) != 0 )has_changed = true;
		iSipPhoneNo = iSipPhoneNoEdit.getText().toString();
	
		if ( iActivity != null && has_changed == true )
		{
			iActivity.SaveSettings();
			iActivity.ReConnect();
		}
		finish();
	}
}
