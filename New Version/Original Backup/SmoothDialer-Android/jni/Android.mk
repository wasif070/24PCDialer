LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

subdirs := $(addprefix $(LOCAL_PATH)/,$(addsuffix /Android.mk, \
		g729 \
		DialerAgent \
	))

include $(subdirs)