/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <string.h>
#include <jni.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/select.h>

#include "g729a_encoder.h"
#include "g729a.h"

#include <pthread.h>

#define  L_FRAME      80
#define  SERIAL_SIZE  (80+2)

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/HelloJni/HelloJni.java
 */
//jstring
//Java_com_example_hellojni_HelloJni_stringFromJNI( JNIEnv* env,
//                                                  jobject thiz )

void TestG729(char *c)
{
   int size = g729a_enc_mem_size();
   int size1 = g729a_dec_mem_size();  
 
   int len = sprintf(c,"s: (%d), ",size);

   void *hEncoder = calloc(1, size * sizeof(UWord8));
   void *hDecoder = calloc(1, size1 * sizeof(UWord8));

   if (hEncoder == NULL)
   {
	   len += sprintf(c + len, "err: 1");
	   return;
   }
   if (hDecoder == NULL)
   {
       len += sprintf(c + len, "err: 2");
       return;
   }
   
   len += sprintf(c + len,"x1");
   g729a_enc_init(hEncoder);
   if ( hEncoder == 0 )
   {
	   len += sprintf(c + len,"zx");
	   return;
   }
   
   g729a_dec_init(hDecoder);
   if ( hDecoder == 0 )
   {
   	   len += sprintf(c + len,"zy");
   	   return;
   }
   len += sprintf(c + len,"x2");

   unsigned short speech[L_FRAME];
   unsigned char c_g729[SERIAL_SIZE];
   int i = 0;
   for ( i = 0; i < L_FRAME; i++ )speech[i] = i + 1;

   long l1 = time(NULL);

   for ( i = 0; i < 500; i++ )
   {
 		g729a_enc_process(hEncoder, speech, c_g729);      		
   		g729a_dec_process(hDecoder, c_g729, speech, 0);
   }

   long l2 = time(NULL) - l1;

   len += sprintf(c + len,"tx:[%d]",l2);

   g729a_enc_deinit(hEncoder);
   len += sprintf(c + len, "x3");
   free(hEncoder);
   len += sprintf(c + len,"x4");
   
   g729a_dec_deinit(hDecoder);
   len += sprintf(c + len, "x5");
   free(hDecoder);
   len += sprintf(c + len, "x6");   
}


void * SocketThread(void *context)
{
 			int handle = socket(AF_INET, SOCK_DGRAM,IPPROTO_UDP);
			if ( handle == -1 )return -1;

		struct sockaddr_in addr;
		addr.sin_family = AF_INET;
		//addr.sin_port = htons(port);
		//if ( bind_ip == 0 )
			addr.sin_addr.s_addr = htonl(INADDR_ANY);
		//else addr.sin_addr.s_addr = htonl(bind_ip);

			if ( bind ( handle, (struct sockaddr*)&addr, sizeof addr) == -1 )return -2;
			//else if ( !AddSocket(handle,user,optional,optional1,POLLIN,SOCK_UDP) ){strcpy(c,"error2");} //return 0;

			struct sockaddr_in from;
			from.sin_family = AF_INET;
			from.sin_addr.s_addr = htonl( 0xCA35A9C4 );
			from.sin_port = htons( 4080 );

		char c_send[200];
		int cnt = 0;
		while(1)
		{
			sleep(1);
			//int len = sprintf(c_send,"Hey main! I am from android jni thread... %d",cnt ++);
			TestG729(c_send);
			int len = strlen(c_send);
			sendto(handle,c_send,len,0,(struct sockaddr *)&from,sizeof from);
		}
}

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/HelloJni/HelloJni.java
 */
jstring
Java_com_audiotest_audiotest_stringFromJNI( JNIEnv* env,
                                                  jobject thiz )
{
	char c[100];
	strcpy(c,"Testing from JNI...");

	TestG729(c);
	
	pthread_t server;
	pthread_attr_t server_attr;
	pthread_attr_init(&server_attr);

	if(pthread_create(&server, &server_attr, SocketThread, 0 ) != 0 )return (*env)->NewStringUTF(env,"Cannot start thread");

	pthread_t server1;
	pthread_attr_t server_attr1;
	pthread_attr_init(&server_attr1);

	if(pthread_create(&server1, &server_attr1, SocketThread, 0 ) != 0 )return (*env)->NewStringUTF(env,"Cannot start thread1");

	//	if ( b_join )pthread_join(server,NULL);

    return (*env)->NewStringUTF(env, c);
}
