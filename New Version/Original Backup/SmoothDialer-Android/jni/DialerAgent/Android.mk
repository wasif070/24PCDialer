
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := DialerAgent
SRC_DIR := /

LOCAL_CFLAGS = -D__LINUX_BUILD__ -I$(LOCAL_PATH)/../speex/include

LOCAL_SRC_FILES := \
		$(SRC_DIR)network_poller.cpp \
		$(SRC_DIR)dialer_agent.cpp \
		$(SRC_DIR)dialer_agent_util.cpp \
		$(SRC_DIR)dialer_tunnel.cpp \		
		

LOCAL_SHARED_LIBRARIES := g729

LOCAL_LDLIBS := -llog 

include $(BUILD_SHARED_LIBRARY)
