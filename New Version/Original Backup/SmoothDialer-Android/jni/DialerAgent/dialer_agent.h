#ifndef __DIALER_AGENT_30_MAY_2011_ADDED__
#define __DIALER_AGENT_30_MAY_2011_ADDED__

#include "platform_def.h"
#include "network_poller.h"

class dialer_agent : public network_poller_callback
{
protected:
	void network_poller_on_receive_data(int socket_id,const uchar* dt,int len);
	void network_poller_on_socket_closed(int socket_id);

private:

	enum
	{
		MAX_VPN_COUNT = 3,
		RTP_SEND_SIZE_MAX = 1000,

		VPN_RESEND_COUNT_MAX = 3,

		AUTH_RETRY_MAX = 6,

		SOCKET_AUTH = 0x80,
		SOCKET_BALANCE = 0x81,

		SOCKET_VPN0 = 0x00,		// socket_id = ( SOCKET_VPN0/1/2 << 4 ) + SOCKET_TCP/UDP1/UDP2
		SOCKET_VPN1 = 0x10,
		SOCKET_VPN2 = 0x20,
		SOCKET_TCP  = 0x03,
		SOCKET_UDP1 = 0x01,
		SOCKET_UDP2 = 0x02,

		ALT_CHANNEL_NOT_TESTED = 0,
		ALT_CHANNEL_BEING_TESTED = 1,
		ALT_CHANNEL_CONNECTED = 2,
		ALT_CHANNEL_NOT_CONNECTED = 3,

		RUNNING_STATE_AUDIO = 1,
		RUNNING_STATE_INGING = 2,
		RUNNING_STATE_AUDIO_SETTINGS = 4,
		RUNNING_STATE_ACCEPT_INCOMING_CALL = 8,
		RUNNING_STATE_SHOULD_CONFIRM_EXIT = 16,
		RUNNING_STATE_EXITED = 32,

		CALLBACK_EVENT_NONE = 0,
		CALLBACK_EVENT_AUTH_ERROR = 1,
		CALLBACK_EVENT_CALL_COMPLETE = 2,
		CALLBACK_EVENT_INCOMING_CALL_COMPLETE = 3,
		CALLBACK_EVENT_COMPLETE_SIP_SETTINGS = 4,
		CALLBACK_EVENT_RESIZE_AUDIO_BUF = 5,				// next 2 parameters are ringing_buf_size and connected_buf_size

		TUNNEL_COMMAND_NONE = 0,
		TUNNEL_COMMAND_RECONNECT = 1,	// cmd#auth_url#auth_url_optional#operator_code#username#pwd#caller-id#timer_ms
		TUNNEL_COMMAND_CALL = 2,
		TUNNEL_COMMAND_CALL_IVR = 3,
		TUNNEL_COMMAND_END_CALL = 4,
		TUNNEL_COMMAND_EXIT = 5,

		WAIT_BEFORE_RETRY_MS = 2000,
		TUNNEL_EXIT_TIMEOUT_MS = 1000,

		LOCAL_STATE_MINIMUM = 100,
		LOCAL_STATE_UNINITIALIZED = 100,		// These are not vpn state.
		LOCAL_STATE_AUTHENTICATING = 101,
		LOCAL_STATE_CONNECTING_VPN = 102,
		LOCAL_STATE_EXITED = 103,

		STATE_NONE = 0,
		STATE_REGISTERING = 1,
		STATE_REGISTERED = 2,
		STATE_UNREGISTERED = 3,
		STATE_CALLING = 4,
		STATE_RINGING = 5,
		STATE_CONNECTED = 6,
		STATE_CALL_PROGRESSING = 7,
		STATE_INCOMING_CALL = 8,
		STATE_INCOMING_RINGING = 9,
		STATE_INCOMING_CONNECTED = 10,

		STATUS_NONE = 0,
		STATUS_VPN_BUSY = 1,
		STATUS_CALL_CANCELLED = 2,
		STATUS_CALL_DISCONNECTED = 3,
		STATUS_INCOMING = 4,
		STATUS_CALL_ENDED = 5,
		STATUS_INVALID_USERNAME = 6,
		STATUS_INVALID_USERNAME_PWD = 7,
		STATUS_INVALID_BALANCE = 8,
		STATUS_UNKNOWN_ERROR = 9,
		STATUS_KEEP_ALIVE_ERROR = 10,
		STATUS_MEDIA_SERVER_ERROR = 11,
		STATUS_FORBIDDEN_403 = 12,
		STATUS_CALLEE_NOT_FOUND = 13,
		STATUS_SERVICE_UNAVAILABLE_503 = 14,
		STATUS_SIP_NO_REPLY = 15,
		STATUS_MISSED_CALL = 28,		// This is not vpn status. This is generated in dialer.

		STATUS_INTERNET_ERROR_LOCAL = 101,
		STATUS_AUTH_SERVER_NOT_FOUND = 102,
		STATUS_EMPTY_OPERATORCODE = 103,
		STATUS_INVALID_OPERATORCODE = 104,
		STATUS_USERNAME_NOT_SET = 105,
		STATUS_SELF_CANCELLED = 106,
		STATUS_VPN_NOT_FOUND = 107,
		STATUS_CUSTOM = 108,
		STATUS_INCOMING_CALL = 109   // In this status, remote phone no is displayed in status
	};


private:
	network_poller			*poller;

	uint					current_time_ms;
	uint					network_pulse_ms;
	uint					callback_event;
	uint					callback_param_int;
	uchar					callback_param_str[128];

	uchar					tunnel_command_str[512];
	uint					tunnel_exit_time_ms;

// --------------------------------------------------------------------
// ----------- VPN related variables ----------------------------------
// --------------------------------------------------------------------

private:
	uint	vpn_ping_time;
	uint	vpn_send_time;

	uint	vpn_send_seq;

	uint	vpn_connection_no;
	uint	vpn_session_id;
	uint	vpn_ticket_index;

	int		vpn_socket_id;
	int		vpn_socket_id_alt;

	bool	vpn_active_udp;
	int		vpn_alt_channel_state;		// ALT_CHANNEL_NOT_TESTED, ALT_CHANNEL_CONNECTED, ALT_CHANNEL_NOT_CONNECTED

	uint	vpn_ticket_id;
	char	vpn_ticket[32];

	uint	vpn_ip[MAX_VPN_COUNT];
	uint	vpn_port_tcp[MAX_VPN_COUNT];
	uint	vpn_port_udp1[MAX_VPN_COUNT];
	uint	vpn_port_udp2[MAX_VPN_COUNT];

	bool	vpn_tcp_priority;
	uint	vpn_connect_timeout_ms;
	uint	vpn_connect_timeout_ms_alt;

	uint	vpn_retry_count;
	uint	vpn_udp_timeout;
	uint	vpn_tcp_timeout;

	uchar	vpn_sip_transfer_info[128];	// This string is passed to sip server

	uchar	vpn_send_data[512];
	uchar	vpn_resend_data[256];
	uchar	vpn_ack_data[128];

	uchar	vpn_receive_data[256];

	uchar	vpn_ticket_request[128];
	uchar	vpn_channel_test_request[128];


// ------------------------------------------------------------------
// --------------  Audio related variables --------------------------
// ------------------------------------------------------------------

private:
	uint	rtp_send_seq;

	bool	rtp_waiting_after_call;

	uint	rtp_last_send_time_ms;

	uchar	rtp_record_buf[2000];
	uchar	rtp_play_buf[10000];		// in case of ringing, sometimes lots of data arrive in short time
	uchar	rtp_send_buf[RTP_SEND_SIZE_MAX * 2];

	uint	rtp_bundle_send_size;
	uint	rtp_ringing_cache_size;
	uint	rtp_connected_cache_size;

	uint	rtp_record_len;
	uint	rtp_play_len;


// ------------------------------------------------------------------
// --------------  Switch related variables -------------------------
// ------------------------------------------------------------------

	uchar	sip_username[32];
	uchar	sip_password[32];
	uchar	sip_caller_id[32];
	uchar	sip_remote_phone[32];

	int		sip_additional_call_time;
	bool	sip_caller_id_as_username;


// ------------------------------------------------------------------
// --------------  Auth related variables ---------------------------
// ------------------------------------------------------------------

private:
	int		auth_operator_code;
	bool	auth_operator_fixed;

	int		auth_random;
	int		auth_status;

	int		auth_retry_no;
	uint	auth_timeout;

	int		auth_platform_id;
	int		auth_dialer_version;
	uchar	auth_device_imei[64];

	uchar	auth_url[128];
	uchar	auth_url_optional[128];
	uchar	auth_reply[3000];
	uint	auth_reply_length;


// ------------------------------------------------------------------
// --------------  View related variables and functions -------------
// ------------------------------------------------------------------

private:
	int		dialer_state;
	int		dialer_status;

	uint	call_start_time_ms;
	uint	call_end_time_ms;

	uchar	balance_value[64];
	uchar	ivr_no[32];
	uchar	dialer_name[64];
	uchar	dialer_footer[64];

	uchar	auth_custom_status[256];
	uchar	call_status_temp[128];

	uchar	balance_reply[3000];
	int		balance_reply_length;

	uchar	balance_url[256];
	uchar	balance_parse_pre[32];
	uchar	balance_parse_post[32];

	bool	balance_url_tcp;

// --------------------------------------------------------------------------
// ---------------- Utility related functions -------------------------------
// --------------------------------------------------------------------------

private:
	int		check_range(int val,int min,int max);
	void	safe_copy(uchar* str_dest,int dest_offset,int dest_max_len,const uchar* str_src, int src_len );
	int		find_string(const uchar* str,int len,const char* str_search);
	int		find_token_pos(const uchar* str,int len,const char* token);
	int		find_token(const uchar* c,int len,const char* str_token,uchar* str_dest,int dest_size);
	uint	find_token_as_int(const uchar* c,int len,const char* str_token);

	uint	session_id_from_ticket(const uchar* c,int len);

	const uchar* next_string(uchar* c,int& st,int len,char separator1,char separator2 = 0);
	uint 	next_int(const uchar* c,int& st,int len,char separator1,char separator2 = 0);


private:
	void	reset_state();
	void	authenticate();
	int		parse_auth_reply(const uchar* dt,int length);
	void	start_alt_channel_request();

	void	send_dialer_image();
	void	send_balance_request();
	void	send_rtp_to_vpn();
	void	send_to_established_vpn(const uchar* dt,int len,bool b_retry_data,uint wait_ms);				//no delay

	int		on_receive_rtp(const uchar* dt,int len);
	void	on_receive_image(const uchar* dt,int len);
	void	handle_vpn_command(const uchar* dt,int len,int optional);	//optional is message type

	void	set_current_state(int new_state,int new_status = STATUS_NONE );

	void	set_callback_event(int event_id,int event_param_int,const uchar* event_param_str);
	void	reset(const char* _auth_url,const char* _auth_url_optional,int _operator_code,const char* _sip_username,const char* _sip_password,const char* _sip_caller_id,const char* _device_imei,uint _current_time_ms,uint _network_pulse_ms);
	void	call(const char* dialed_number);
	void	end_call();

	const	char* get_channel_type();
	const	char* get_dialer_state_str();
	const	char* get_dialer_status_str();
	int		calculate_call_duration(bool use_current_time);

public:
	dialer_agent();
	~dialer_agent();

	int		on_timer(uint _current_time_ms,uchar* play_buf,int play_buf_max_size,const uchar* record_buf,int record_len); // returns rtp play buf len
	bool	get_display_items(char* str_dest,int max_len);	// returns "true" if tunnel should be destroyed

	void	handle_tunnel_command(uint _current_time_ms,const char* _tunnel_command);
};

#endif

