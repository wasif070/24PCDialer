#ifndef __PLATFORM_DEF_ADDED_18_SEPTEMBER_2013__
#define __PLATFORM_DEF_ADDED_18_SEPTEMBER_2013__

#define __UNIX_BUILD__
//#undef __UNIX_BUILD__

#define __JNI_TUNNEL__
//#undef __JNI_TUNNEL__

//#define __PLATFORM_NAME__ "PC"
#define __PLATFORM_NAME__ "ANDROID"
//#define __PLATFORM_NAME__ "IPHONE"
//#define __PLATFORM_NAME__ "WP8"
//#define __PLATFORM_NAME__ "SYMBIAN"

#include <time.h>

typedef unsigned char uchar;
typedef unsigned int  uint;

#ifdef __UNIX_BUILD__
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <android/log.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/select.h>
#include <fcntl.h>

#include <netdb.h>
#include <sys/poll.h>

#include <errno.h>

#define closesocket close
#define strnicmp strncmp

#define LOGD(...) __android_log_print(ANDROID_LOG_ERROR , "dialer", __VA_ARGS__)

#else
#include <string>
#include <winsock.h>
#define snprintf _snprintf
#define strnicmp _strnicmp
#endif

#endif
