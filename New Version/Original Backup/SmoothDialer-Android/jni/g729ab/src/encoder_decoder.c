/* ITU-T G.729 Software Package Release 2 (November 2006) */
/*
   ITU-T G.729A Speech Coder with Annex B    ANSI-C Source Code
   Version 1.5    Last modified: October 2006

   Copyright (c) 1996,
   AT&T, France Telecom, NTT, Universite de Sherbrooke, Lucent Technologies,
   Rockwell International
   All rights reserved.
*/

/*-------------------------------------------------------------------*
 * Main program of the ITU-T G.729A  8 kbit/s encoder.               *
 *                                                                   *
 *    Usage : coder speech_file  bitstream_file                      *
 *-------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include "typedef.h"
#include "basic_op.h"
#include "ld8a.h"
#include "dtx.h"
#include "octet.h"


Word16 bad_lsf;        /* bad LSF indicator for decoder  */
/*
   This variable should be always set to zero unless transmission errors
   in LSP indices are detected.
   This variable is useful if the channel coding designer decides to
   perform error checking on these important parameters. If an error is
   detected on the  LSP indices, the corresponding flag is
   set to 1 signalling to the decoder to perform parameter substitution.
   (The flags should be set back to 0 for correct transmission).
*/

int decoder_main(int argc, char* argv[]);

int main(int argc, char *argv[] )
{
  FILE *f_speech;               /* File of speech data                   */
  FILE *f_serial;               /* File of serial bits for transmission  */

  extern Word16 *new_speech;     /* Pointer to new speech data            */

  Word16 prm[PRM_SIZE+1];        /* Analysis parameters + frame type      */
  Word16 serial[SERIAL_SIZE];    /* Output bitstream buffer               */

  Word16 frame;                  /* frame counter */
  Word32 count_frame;

  /* For G.729B */
  Word16 nb_words;
  Word16 vad_enable;

  printf("\n");
  printf("***********    ITU G.729A 8 KBIT/S SPEECH CODER    ***********\n");
  printf("                        (WITH ANNEX B)                        \n");
  printf("\n");
  printf("------------------- Fixed point C simulation -----------------\n");
  printf("\n");
  printf("------------ Version 1.5 (Release 2, November 2006) ----------\n");
  printf("\n");


/*--------------------------------------------------------------------------*
 * Open speech file and result file (output serial bit stream)              *
 *--------------------------------------------------------------------------*/

  if ( argc != 4 ){
    printf("Usage :%s speech_file  bitstream_file  VAD_flag\n", argv[0]);
    printf("\n");
    printf("Format for speech_file:\n");
    printf("  Speech is read from a binary file of 16 bits PCM data.\n");
    printf("\n");
    printf("Format for bitstream_file:\n");
    printf("  One (2-byte) synchronization word \n");
    printf("  One (2-byte) size word,\n");
    printf("  80 words (2-byte) containing 80 bits.\n");
    printf("\n");
    printf("VAD flag:\n");
    printf("  0 to disable the VAD\n");
    printf("  1 to enable the VAD\n");
    exit(1);
  }

  if ( (f_speech = fopen(argv[1], "rb")) == NULL) {
     printf("%s - Error opening file  %s !!\n", argv[0], argv[1]);
     exit(0);
   }
  printf(" Input speech file:  %s\n", argv[1]);

  if ( (f_serial = fopen(argv[2], "wb")) == NULL) {
     printf("%s - Error opening file  %s !!\n", argv[0], argv[2]);
     exit(0);
  }
  printf(" Output bitstream file:  %s\n", argv[2]);

  vad_enable = (Word16)atoi(argv[3]);
  if (vad_enable == 1)
    printf(" VAD enabled\n");
  else
    printf(" VAD disabled\n");

#ifndef OCTET_TX_MODE
  printf(" OCTET TRANSMISSION MODE is disabled\n");
#endif

/*--------------------------------------------------------------------------*
 * Initialization of the coder.                                             *
 *--------------------------------------------------------------------------*/

  Init_Pre_Process();
  Init_Coder_ld8a();
  Set_zero(prm, PRM_SIZE+1);

  /* for G.729B */
  Init_Cod_cng();


  /* Loop for each "L_FRAME" speech data. */

  frame = 0;
  count_frame = 0L;
  while( fread(new_speech, sizeof(Word16), L_FRAME, f_speech) == L_FRAME)
  {
    printf("Frame = %ld\r", count_frame++);

    if (frame == 32767) frame = 256;
    else frame++;

    Pre_Process(new_speech, L_FRAME);
    Coder_ld8a(prm, frame, vad_enable);
    prm2bits_ld8k( prm, serial);
    nb_words = serial[1] +  (Word16)2;
    fwrite(serial, sizeof(Word16), nb_words, f_serial);
  }

  printf("%ld frames processed\n", count_frame);

  decoder_main(argc,argv);
  return (0);
}

int decoder_main(int argc, char* argv[])
{
  Word16  synth_buf[L_FRAME+M], *synth; /* Synthesis                   */
  Word16  parm[PRM_SIZE+2];             /* Synthesis parameters        */
  Word16  Az_dec[MP1*2];                /* Decoded Az for post-filter  */
  Word16  T2[2];                        /* Pitch lag for 2 subframes   */


  Word16  i, Vad;
  Word32  count_frame;
  FILE   *f_syn, *f_serial;

  printf("\n");
  printf("************  ITU G.729A 8.0 KBIT/S SPEECH DECODER  ************\n");
  printf("                       (WITH ANNEX B)                           \n");
  printf("\n");
  printf("------------------ Fixed point C simulation --------------------\n");
  printf("\n");
  printf("------------- Version 1.5 (Release 2, November 2006) ------------\n");
  printf("\n");

   /* Passed arguments */

  if ( argc != 3){
    printf("Usage :%s bitstream_file  outputspeech_file\n",argv[0]);
    printf("\n");
    printf("Format for bitstream_file:\n");
    printf("  One (2-byte) synchronization word \n");
    printf("  One (2-byte) size word,\n");
    printf("  80 words (2-byte) containing 80 bits.\n");
    printf("\n");
    printf("Format for outputspeech_file:\n");
    printf("  Synthesis is written to a binary file of 16 bits data.\n");
    exit( 1 );
  }

  /* Open file for synthesis and packed serial stream */

  if( (f_serial = fopen(argv[1],"rb") ) == NULL )
    {
      printf("%s - Error opening file  %s !!\n", argv[0], argv[1]);
      exit(0);
    }

  if( (f_syn = fopen(argv[2], "wb") ) == NULL )
    {
      printf("%s - Error opening file  %s !!\n", argv[0], argv[2]);
      exit(0);
    }

  printf("Input bitstream file  :   %s\n",argv[1]);
  printf("Synthesis speech file :   %s\n",argv[2]);

#ifndef OCTET_TX_MODE
  printf("OCTET TRANSMISSION MODE is disabled\n");
#endif

/*-----------------------------------------------------------------*
 *           Initialization of decoder                             *
 *-----------------------------------------------------------------*/

  for (i=0; i<M; i++) synth_buf[i] = 0;
  synth = synth_buf + M;

  bad_lsf = 0;          /* Initialize bad LSF indicator */
  Init_Decod_ld8a();
  Init_Post_Filter();
  Init_Post_Process();

  /* for G.729b */
  Init_Dec_cng();

/*-----------------------------------------------------------------*
 *            Loop for each "L_FRAME" speech data                  *
 *-----------------------------------------------------------------*/

  count_frame = 0L;
  while(read_frame(f_serial, parm) != 0)
  {
    printf("Frame = %ld\r", count_frame++);

    Decod_ld8a(parm, synth, Az_dec, T2, &Vad);
    Post_Filter(synth, Az_dec, T2, Vad);        /* Post-filter */
    Post_Process(synth, L_FRAME);

    fwrite(synth, sizeof(short), L_FRAME, f_syn);

  }

  printf("%ld frames decoded\n", count_frame);
  return(0);
}
