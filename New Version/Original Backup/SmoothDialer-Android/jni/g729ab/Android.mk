# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := g729ab
SRC_DIR := /src

LOCAL_CFLAGS = -DFIXED_POINT -D__unix__ -I$(LOCAL_PATH)/include
#LOCAL_C_INCLUDES += /include

LOCAL_SRC_FILES :=  $(SRC_DIR)/acelp_ca.c $(SRC_DIR)/basic_op.c \
				$(SRC_DIR)/bits.c $(SRC_DIR)/cod_ld8a.c $(SRC_DIR)/encoder_decoder.c \
				$(SRC_DIR)/de_acelp.c $(SRC_DIR)/dec_gain.c \
				$(SRC_DIR)/dec_lag3.c $(SRC_DIR)/qsidlsf.c $(SRC_DIR)/dec_ld8a.c $(SRC_DIR)/dspfunc.c $(SRC_DIR)/filter.c \
				$(SRC_DIR)/gainpred.c $(SRC_DIR)/lpc.c $(SRC_DIR)/lpcfunc.c \
				$(SRC_DIR)/lspdec.c $(SRC_DIR)/lspgetq.c $(SRC_DIR)/oper_32b.c \
				$(SRC_DIR)/p_parity.c $(SRC_DIR)/pitch_a.c $(SRC_DIR)/pre_proc.c \
				$(SRC_DIR)/post_pro.c $(SRC_DIR)/pred_lt3.c $(SRC_DIR)/qua_gain.c \
				$(SRC_DIR)/qua_lsp.c $(SRC_DIR)/postfilt.c $(SRC_DIR)/tab_ld8a.c \
				$(SRC_DIR)/util.c $(SRC_DIR)/tab_dtx.c $(SRC_DIR)/dec_sid.c \
				$(SRC_DIR)/qsidgain.c $(SRC_DIR)/calcexc.c $(SRC_DIR)/taming.c \
				$(SRC_DIR)/cor_func.c $(SRC_DIR)/vad.c \
				$(SRC_DIR)/dtx.c

include $(BUILD_SHARED_LIBRARY)
