// AccountDialog.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "AccountDialog.h"


// AccountDialog dialog

IMPLEMENT_DYNAMIC(AccountDialog, CDialog)

AccountDialog::AccountDialog(CWnd* pParent /*=NULL*/)
	: CDialog(AccountDialog::IDD, pParent)
{
	iShowExitButton = false;
}

AccountDialog::~AccountDialog()
{
}

void AccountDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(AccountDialog, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &AccountDialog::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL_SETTINGS, &AccountDialog::OnBnClickedButtonCancelSettings)
END_MESSAGE_MAP()


// AccountDialog message handlers

BOOL AccountDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	if ( MyUtil::iFixedPin )
	{
		this->GetDlgItem(IDC_STATIC_OPERATORCODE)->ShowWindow(SW_HIDE);
		this->GetDlgItem(IDC_EDIT_OPERATORCODE)->ShowWindow(SW_HIDE);
	}

	this->SetDlgItemInt(IDC_EDIT_OPERATORCODE,MyUtil::iOperatorCode);
	this->SetDlgItemTextW(IDC_EDIT_SIP_USERNAME,MyUtil::iSipUsername);
	this->SetDlgItemTextW(IDC_EDIT_SIP_PASSWORD,MyUtil::iSipPassword);
	this->SetDlgItemTextW(IDC_EDIT_SIP_CALLERID,MyUtil::iSipCallerID);

	if ( iShowExitButton )this->SetDlgItemTextW(IDC_BUTTON_CANCEL_SETTINGS,L"Exit");
	return TRUE;
}
void AccountDialog::OnBnClickedButtonSave()
{
	MyUtil::iOperatorCode = this->GetDlgItemInt(IDC_EDIT_OPERATORCODE);
	this->GetDlgItemTextW(IDC_EDIT_SIP_USERNAME,MyUtil::iSipUsername);
	this->GetDlgItemTextW(IDC_EDIT_SIP_PASSWORD,MyUtil::iSipPassword);
	this->GetDlgItemTextW(IDC_EDIT_SIP_CALLERID,MyUtil::iSipCallerID);

	MyUtil::SaveSettings();
	EndDialog(IDOK);
}


void AccountDialog::OnBnClickedButtonCancelSettings()
{
	EndDialog(IDCANCEL);
}
