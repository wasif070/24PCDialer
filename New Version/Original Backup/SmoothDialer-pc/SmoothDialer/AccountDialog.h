#pragma once

#include "MyUtil.h"

// AccountDialog dialog

class AccountDialog : public CDialog
{
	DECLARE_DYNAMIC(AccountDialog)

public:
	AccountDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~AccountDialog();
	bool	iShowExitButton;

// Dialog Data
	enum { IDD = IDD_DIALOG_SETTINGS };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonCancelSettings();
};
