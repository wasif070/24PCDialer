// CallLog.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "CallLog.h"


// CCallLog dialog

IMPLEMENT_DYNAMIC(CCallLog, CDialog)

CCallLog::CCallLog(CWnd* pParent /*=NULL*/)
	: CDialog(CCallLog::IDD, pParent)
{
	iShowCallLog = true;
}

CCallLog::~CCallLog()
{
}

void CCallLog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_CALL_LOG, iCallTab);
	DDX_Control(pDX, IDC_LIST_CALL_LOG, iCallList);
}


BEGIN_MESSAGE_MAP(CCallLog, CDialog)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_CALL_LOG, &CCallLog::OnTcnSelchangeTabCallLog)
	ON_BN_CLICKED(IDC_BUTTON_ADD_TO_CONTACT, &CCallLog::OnBnClickedButtonAddToContact)
	ON_BN_CLICKED(IDC_BUTTON_CALL_SELECTED, &CCallLog::OnBnClickedButtonCallSelected)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CALL_LOG, &CCallLog::OnNMDblclkListCallLog)
END_MESSAGE_MAP()


// CCallLog message handlers

BOOL CCallLog::OnInitDialog()
{
	CDialog::OnInitDialog();

	iCallTab.DeleteAllItems();
	iCallTab.InsertItem(0,L"Dial Log");
	iCallTab.InsertItem(1,L"Call Log");

	iCallList.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	if ( iShowCallLog )iCallTab.SetCurSel(1);
	else iCallTab.SetCurSel(0);

	iSelectedNumber = L"";

	LoadCallLog(!iShowCallLog);	
	return TRUE;
}

void CCallLog::LoadCallLog(bool b_dial_log)
{
	iCallList.LockWindowUpdate();

	iCallList.DeleteAllItems();
	for ( int i = 0; i < 4; i++ )iCallList.DeleteColumn(0);

	iCallList.InsertColumn(0,L"Name",0,120);
	iCallList.InsertColumn(1,L"Phone no",0,150);
	iCallList.InsertColumn(2,L"Time",0,140);
	if ( !b_dial_log )iCallList.InsertColumn(3,L"Duration",0,100);

	CString str_name = L"", str_phone = L"", str_time = L"", str_duration = L"";
	for ( int i = 0; i < MyUtil::MAX_CALL_LOG; i++ )
	{
		if ( !MyUtil::GetCallLog(b_dial_log,i,str_name,str_phone,str_time,str_duration) )continue;

		int sel = iCallList.InsertItem(iCallList.GetItemCount(),str_name);
		iCallList.SetItemText(sel,1,str_phone);
		iCallList.SetItemText(sel,2,str_time);
		if ( !b_dial_log )iCallList.SetItemText(sel,3,str_duration);
	}
	iCallList.UnlockWindowUpdate();
}

void CCallLog::OnTcnSelchangeTabCallLog(NMHDR *pNMHDR, LRESULT *pResult)
{
	LoadCallLog(iCallTab.GetCurSel() == 0 );
	iShowCallLog = ( iCallTab.GetCurSel() != 0 );

	*pResult = 0;
}

void CCallLog::OnBnClickedButtonAddToContact()
{
	int sel = iCallList.GetSelectionMark();
	if ( sel < 0 )
	{
		AfxMessageBox(L"No item selected !!!",MB_ICONWARNING | MB_OK );
		return;
	}

	iAddContact.iEditMode = true;
	iAddContact.iContactName = iCallList.GetItemText(sel,0);
	iAddContact.iPhoneNo = iCallList.GetItemText(sel,1);

	if ( iAddContact.DoModal() != IDOK )return;

	if ( MyUtil::AddContact(iAddContact.iContactName,iAddContact.iPhoneNo) )AfxMessageBox(L"Contact added successfully ...",MB_OK | MB_ICONINFORMATION );
}

void CCallLog::OnBnClickedButtonCallSelected()
{
	int sel = iCallList.GetSelectionMark();
	if ( sel < 0 )
	{
		AfxMessageBox(L"No item selected !!!",MB_ICONWARNING);
		return;
	}

	iSelectedNumber = iCallList.GetItemText(sel,1);
	EndDialog(IDOK);
}

void CCallLog::OnNMDblclkListCallLog(NMHDR *pNMHDR, LRESULT *pResult)
{
//	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<NMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	int sel = iCallList.GetSelectionMark();
	if ( sel < 0 )return;

	iSelectedNumber = iCallList.GetItemText(sel,1);
	EndDialog(IDOK);
}
