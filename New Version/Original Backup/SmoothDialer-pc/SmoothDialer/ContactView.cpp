// ContactView.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "ContactView.h"


// CContactView dialog

IMPLEMENT_DYNAMIC(CContactView, CDialog)

CContactView::CContactView(CWnd* pParent /*=NULL*/)
	: CDialog(CContactView::IDD, pParent)
{

}

CContactView::~CContactView()
{
}

void CContactView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONTACTS, iContactList);
}


BEGIN_MESSAGE_MAP(CContactView, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_CONTACT, &CContactView::OnBnClickedButtonDeleteContact)
	ON_BN_CLICKED(IDC_BUTTON_NEW_CONTACT, &CContactView::OnBnClickedButtonNewContact)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_CONTACT, &CContactView::OnBnClickedButtonEditContact)
	ON_BN_CLICKED(IDC_BUTTON_CALL, &CContactView::OnBnClickedButtonCall)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTACTS, &CContactView::OnNMDblclkListContacts)
END_MESSAGE_MAP()


// CContactView message handlers

BOOL CContactView::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	iContactList.LockWindowUpdate();
	
	iContactList.DeleteAllItems();
	
	iContactList.InsertColumn(0,L"Name",LVCFMT_LEFT,200);
	iContactList.InsertColumn(1,L"Phone No",LVCFMT_LEFT,200);

	iContactList.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	for ( int i = 0; i < MyUtil::MAX_CONTACT_COUNT; i++ )
	{
		CString str_name = L"", str_phone = L"";
		if ( !MyUtil::GetContact(i,str_name,str_phone) )continue;

		int cnt = iContactList.GetItemCount();
		int index = iContactList.InsertItem(cnt,str_name);
		iContactList.SetItemText(index,1,str_phone);
		iContactList.SetItemData(index,i);
	}

	iContactList.UnlockWindowUpdate();

	iSelectedContact = L"";
	return TRUE;
}

void CContactView::OnBnClickedButtonDeleteContact()
{
	int sel = iContactList.GetSelectionMark();
	if ( sel < 0 )
	{
		AfxMessageBox(L"No contact selected !!!",MB_OK | MB_ICONWARNING );
		return;
	}

	int contact_index = iContactList.GetItemData(sel);
	MyUtil::DeleteContact(contact_index);
	iContactList.DeleteItem(sel);
}

void CContactView::OnBnClickedButtonNewContact()
{
	iContactEditor.iEditMode = false;
	if ( iContactEditor.DoModal() != IDOK )return;

	if ( iContactEditor.iContactName.GetLength() <= 0 || iContactEditor.iPhoneNo.GetLength() <= 0 )
	{
		AfxMessageBox(L"Invalid contact details !!!",MB_OK | MB_ICONWARNING);
		return;
	}

	int contact_index = MyUtil::AddContact(iContactEditor.iContactName,iContactEditor.iPhoneNo);
	if ( contact_index < 0 )
	{
		AfxMessageBox(L"So many contacts !!!",MB_OK | MB_ICONERROR);
		return;
	}

	int cnt = iContactList.GetItemCount();
	int tmp = iContactList.InsertItem(cnt,iContactEditor.iContactName);
	iContactList.SetItemText(tmp,1,iContactEditor.iPhoneNo);
	iContactList.SetItemData(tmp,contact_index);
}

void CContactView::OnBnClickedButtonEditContact()
{
	int sel = iContactList.GetSelectionMark();
	if ( sel < 0 )
	{
		AfxMessageBox(L"No contact selected !!!",MB_OK);
		return;
	}

	int contact_index = iContactList.GetItemData(sel);

	iContactEditor.iEditMode = true;
	iContactEditor.iContactName = iContactList.GetItemText(sel,0);
	iContactEditor.iPhoneNo = iContactList.GetItemText(sel,1);

	if ( iContactEditor.DoModal() != IDOK )return;

	if ( iContactEditor.iContactName.Compare(iContactList.GetItemText(sel,0)) == 0 &&
		iContactEditor.iPhoneNo.Compare(iContactList.GetItemText(sel,1)) == 0 )
	{
		AfxMessageBox(L"Nothing changed !!!");
		return;
	}
	
	iContactList.SetItemText(sel,0,iContactEditor.iContactName);
	iContactList.SetItemText(sel,1,iContactEditor.iPhoneNo);
	
	MyUtil::EditContact(contact_index,iContactEditor.iContactName,iContactEditor.iPhoneNo);
}

void CContactView::OnBnClickedButtonCall()
{
	int sel = iContactList.GetSelectionMark();
	if ( sel < 0 )
	{
		AfxMessageBox(L"No item selected !!!",MB_ICONWARNING);
		return;
	}

	iSelectedContact = iContactList.GetItemText(sel,1);
	EndDialog(IDOK);
}

void CContactView::OnNMDblclkListContacts(NMHDR *pNMHDR, LRESULT *pResult)
{
//	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<NMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	int sel = iContactList.GetSelectionMark();
	if ( sel < 0 )return;

	iSelectedContact = iContactList.GetItemText(sel,1);
	EndDialog(IDOK);
}
