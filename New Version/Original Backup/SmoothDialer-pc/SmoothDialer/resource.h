//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SmoothDialer.rc
//
#define IDD_SMOOTHDIALER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     133
#define IDB_HOMESCREEN                  133
#define IDD_DIALOG_SETTINGS             134
#define IDR_CONFIG                      136
#define IDD_DIALOG_CONTACTS             137
#define IDD_DIALOG_ADD_CONTACT          138
#define IDD_DIALOG_CALL_LOG             139
#define IDC_STATIC_BRANDNAME            1000
#define IDC_BRANDNAME                   1000
#define IDC_CALL                        1001
#define IDC_PHONE_NO                    1002
#define IDC_END                         1003
#define IDC_RECONNECT                   1004
#define IDC_CALL_LOG                    1005
#define IDC_NUM_1                       1006
#define IDC_NUM_2                       1007
#define IDC_NUM_3                       1008
#define IDC_NUM_4                       1009
#define IDC_NUM_5                       1010
#define IDC_NUM_6                       1011
#define IDC_NUM_7                       1012
#define IDC_NUM_8                       1013
#define IDC_NUM_9                       1014
#define IDC_NUM_0                       1015
#define IDC_NUM_STAR                    1016
#define IDC_NUM_HASH                    1017
#define IDC_SETTINGS                    1018
#define IDC_CONTACTS                    1019
#define IDC_STATE                       1020
#define IDC_STATUS                      1021
#define IDC_FOOTER                      1022
#define IDC_BALANCE                     1023
#define IDC_DURATION                    1024
#define IDC_NUM_C                       1025
#define IDC_BUTTON_IVR                  1026
#define IDC_EDIT_OPERATORCODE           1027
#define IDC_EDIT_SIP_USERNAME           1028
#define IDC_EDIT_SIP_PASSWORD           1029
#define IDC_EDIT4                       1030
#define IDC_EDIT_SIP_CALLERID           1030
#define IDC_BUTTON_SAVE                 1032
#define IDC_STATIC_OPERATORCODE         1034
#define IDC_LIST_CONTACTS               1036
#define IDC_BUTTON_DELETE_CONTACT       1037
#define IDC_BUTTON_NEW_CONTACT          1038
#define IDC_BUTTON_CALL                 1039
#define IDC_EDIT_NAME                   1040
#define IDC_EDIT_PHONE_NO               1041
#define IDC_BUTTON_ADD                  1042
#define IDC_BUTTON_EDIT_CONTACT         1043
#define IDC_TAB_CALL_LOG                1045
#define IDC_LIST_CALL_LOG               1046
#define IDC_BUTTON_CALL_SELECTED        1047
#define IDC_BUTTON2                     1048
#define IDC_BUTTON_ADD_TO_CONTACT       1048
#define IDC_PHONE_NAME                  1049
#define IDC_BUTTON_CANCEL_SETTINGS      1050

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
