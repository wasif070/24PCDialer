#include "dialer_agent.h"

void dialer_agent::safe_copy(uchar* str_dest,int dest_offset,int dest_max_len,const uchar* str_src, int src_len )
{
	str_dest[0] = 0;
	dest_max_len--;

	if ( src_len >= ( dest_max_len - dest_offset ) )src_len = dest_max_len - dest_offset;
	if ( src_len <= 0 )return;

	memcpy(str_dest + dest_offset,str_src,src_len);
	str_dest[dest_offset+src_len] = 0;
}

uint dialer_agent::session_id_from_ticket(const uchar* c,int len)
{
	uint session_id = 0;
	int prev_char = 1;

	for (int i = 0; i < len; i++ )
	{
		int tmp = c[i];

		int cur_char = tmp - 'A' + 1;
		session_id += ( prev_char * cur_char );
		session_id += ( ( i + 3 ) * cur_char );

		prev_char = ( tmp - '8' );
	}
	return (session_id % 100000);
}

int dialer_agent::find_string(const uchar* str,int len,const char* str_search)
{
	int l2 = strlen(str_search);
	for ( int i = 0; i <= ( len - l2 ); i++ )
	{
		int j;
		for ( j = 0; j < l2; j++ )if ( str[i+j] != str_search[j] )break;
		if ( j < l2 )continue;

		return i;
	}

	return -1;
}

int dialer_agent::find_token_pos(const uchar* str,int len,const char* token)
{
	int l2 = strlen(token);

	for ( int i = 0; i < ( len - l2 - 2); i++ )
	{
		if ( str[i] != '<' )continue;
		if ( str[i+1+l2] != '>' )continue;

		int j;
		for ( j = 0; j < l2; j++ )if ( str[i+j+1] != token[j] )break;
		if ( j < l2 )continue;

		return i;
	}

	return -1;
}

int dialer_agent::find_token(const uchar* c,int len,const char* str_token,uchar* str_dest,int dest_size)
{
	str_dest[0] = 0;

	int p = find_token_pos(c,len,str_token);
	if ( p < 0 )return -1;

	p += ( strlen(str_token) + 2 );

	dest_size--;
	int l = 0;

	for ( ; p < len - 1; p++ )
	{
		if ( c[p] == '<' )
		{
			if ( c[p+1] == '/' )break;
			if ( p < ( len - 4 ) && c[p+1] == 'b' && c[p+2] == 'r' && c[p+3] == '/' && c[p+4] == '>' )
			{
				str_dest[l++] = '\n';
				p += 4;
				continue;
			}
		}
		if ( l < dest_size )str_dest[l++] = c[p];
	}

	str_dest[l] = 0;
	return l;
}

uint dialer_agent::find_token_as_int(const uchar* c,int len,const char* str_token)
{
	int p = find_token_pos(c,len,str_token);
	if ( p < 0 )return 0;

	p += ( strlen(str_token) + 2 );

	unsigned int val = 0;

	for ( ; p < len; p++ )
	{
		if ( c[p] == '<' )break;
		if ( c[p] >= '0' && c[p] <= '9' )val = val * 10 + c[p] - '0';
	}
	return val;
}

//	NextStringX doesn't allocate new string. Rather, it places NULL at the end of the sub string. So, the source string is not usable next.
const uchar* dialer_agent::next_string(uchar* c,int& st,int len,char separator1,char separator2)
{
	if ( len <= 0 || st >= len )return (const uchar*)"";

	int base = st;
	for ( ; st < len; st++ )if ( c[st] == separator1 || c[st] == separator2 )break;
	c[st] = 0;
	st++;
	return c + base;
}

uint dialer_agent::next_int(const uchar* c,int& st,int len,char separator1,char separator2)
{
	if ( len <= 0 || st >= len )return 0;

	uint ret = 0;
	for ( ; st < len; st++ )
	{
		if ( c[st] == separator1 || c[st] == separator2 )break;
		if ( c[st] >= '0' && c[st] <= '9' )ret = ret * 10 + c[st] - '0';
	}
	st++;
	return ret;
}

const char* dialer_agent::get_channel_type()
{
	if ( vpn_active_udp )
	{
		if ( vpn_alt_channel_state == ALT_CHANNEL_CONNECTED )return "UDP 2 CHANNEL";
		if ( vpn_alt_channel_state == ALT_CHANNEL_NOT_TESTED || vpn_alt_channel_state == ALT_CHANNEL_BEING_TESTED )return "UDP TEST CHANNEL";
		return "UDP 1 CHANNEL";
	}

	if ( vpn_alt_channel_state == ALT_CHANNEL_CONNECTED )return "TCP UDP CHANNEL";
	if ( vpn_alt_channel_state == ALT_CHANNEL_NOT_TESTED || vpn_alt_channel_state == ALT_CHANNEL_BEING_TESTED )return "TCP TEST CHANNEL";
	return "TCP 1 CHANNEL";
}

int dialer_agent::check_range(int val,int min,int max)
{
	if ( val >= min && val <= max )return val;
	if ( val < min )return min;
	return max;
}

int dialer_agent::calculate_call_duration(bool use_current_time)
{
	int duration = 0;
	if ( use_current_time )duration = current_time_ms - call_start_time_ms;
	else duration = call_end_time_ms - call_start_time_ms;

	if ( sip_additional_call_time > 0 && sip_additional_call_time < 60 )duration = ( duration * 60 )/(60-sip_additional_call_time);
	duration /= 1000;

	return duration;
}

// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------

void dialer_agent::reset_state()
{
	dialer_state = LOCAL_STATE_UNINITIALIZED;
	dialer_status = STATUS_NONE;

	set_callback_event(0,0,(const uchar*)"");

	current_time_ms = 0;
	call_start_time_ms = call_end_time_ms = 0;
	tunnel_exit_time_ms = 0;

	balance_value[0] = 0;
	balance_reply_length = 0;

	ivr_no[0] = 0;
	dialer_name[0] = 0;
	dialer_footer[0] = 0;

	call_status_temp[0] = 0;
	auth_custom_status[0] = 0;

	vpn_ping_time = 0;
	vpn_send_time = current_time_ms/1000;
	vpn_send_seq = 1;

	vpn_connection_no = vpn_session_id = 0;
	vpn_alt_channel_state = ALT_CHANNEL_NOT_TESTED;		// ALT_CHANNEL_NOT_TESTED, ALT_CHANNEL_CONNECTED, ALT_CHANNEL_NOT_CONNECTED
	vpn_sip_transfer_info[0] = 0;
	vpn_socket_id = -1;
	vpn_socket_id_alt = -1;

	rtp_send_seq = 0;
	rtp_last_send_time_ms = 0;
	rtp_play_len = 0;
	rtp_record_len = 0;
	rtp_waiting_after_call = false;

	sip_username[0] = 0;
	sip_password[0] = 0;
	sip_caller_id[0] = 0;
	sip_remote_phone[0] = 0;

	sip_additional_call_time = 0;

	auth_operator_code = -1;
	auth_operator_fixed = false;

	auth_device_imei[0] = 0;

	auth_url[0] = 0;
	auth_url_optional[0] = 0;
	auth_reply_length = 0;
}

void dialer_agent::handle_tunnel_command(uint _current_time_ms,const char* _tunnel_command_str)
{
	current_time_ms = _current_time_ms;
	if ( _tunnel_command_str[0] == 0 )return;

	safe_copy(tunnel_command_str,0,sizeof(tunnel_command_str),(const uchar*)_tunnel_command_str,strlen(_tunnel_command_str));

	int st = 0;
	int len = strlen((const char*)tunnel_command_str);

	int tunnel_command = next_int(tunnel_command_str,st,len,'#');
	if ( tunnel_command == TUNNEL_COMMAND_RECONNECT )
	{
		const uchar* _auth_url = next_string(tunnel_command_str,st,len,'#');
		const uchar* _auth_url_optional = next_string(tunnel_command_str,st,len,'#');
		uint _operator_code = next_int(tunnel_command_str,st,len,'#');
		const uchar* _username = next_string(tunnel_command_str,st,len,'#');
		const uchar* _password = next_string(tunnel_command_str,st,len,'#');
		const uchar* _caller_id = next_string(tunnel_command_str,st,len,'#');
		uint _timer_ms = next_int(tunnel_command_str,st,len,'#');

		reset((const char*)_auth_url,(const char*)_auth_url_optional,_operator_code,(const char*)_username,(const char*)_password,(const char*)_caller_id,"",_current_time_ms,_timer_ms);
	}
	else if ( tunnel_command == TUNNEL_COMMAND_CALL )call((const char*)next_string(tunnel_command_str,st,len,'#'));
	else if ( tunnel_command == TUNNEL_COMMAND_CALL_IVR )call((const char*)ivr_no);
	else if ( tunnel_command == TUNNEL_COMMAND_END_CALL )end_call();
	else if ( tunnel_command == TUNNEL_COMMAND_EXIT )
	{
		if ( dialer_state >= LOCAL_STATE_MINIMUM || dialer_state == STATE_NONE || dialer_state == STATE_UNREGISTERED )dialer_state = LOCAL_STATE_EXITED;
		else
		{
			set_current_state(STATE_UNREGISTERED,STATUS_NONE);
			send_dialer_image();

			tunnel_exit_time_ms = current_time_ms + TUNNEL_EXIT_TIMEOUT_MS;
		}
	}
}

void dialer_agent::set_callback_event(int event_id,int event_param_int,const uchar* event_param_str)
{
	callback_event = event_id;
	callback_param_int = event_param_int;
	safe_copy(callback_param_str,0,sizeof(callback_param_str),event_param_str,strlen((const char*)event_param_str));
}

void dialer_agent::start_alt_channel_request()
{
	vpn_connect_timeout_ms_alt = 1000;
	uint main_vpn_no = ( ( vpn_socket_id & 0xF0 ) >> 4 );

	for ( int i = 0; i < MAX_VPN_COUNT; i++ )
	{
		int cur_vpn = ( main_vpn_no + i + 1 ) % MAX_VPN_COUNT;

		if ( vpn_ip[cur_vpn] == 0 )continue;
		if ( vpn_port_udp2[cur_vpn] == 0 )continue;

		uint socket_id = ( cur_vpn << 4 ) + SOCKET_UDP2;

		poller->add_udp_socket( socket_id, vpn_ip[cur_vpn],vpn_port_udp2[cur_vpn],vpn_connect_timeout_ms_alt);
		vpn_connect_timeout_ms_alt  += ( vpn_udp_timeout * 1000 ) / 2;

		vpn_channel_test_request[0] = 0;
		snprintf((char *)vpn_channel_test_request,sizeof(vpn_channel_test_request),"[CHANNELTEST:%u:%u:%u:%u]",current_time_ms/1000,vpn_connection_no,vpn_session_id,socket_id);

		for ( int j = 0; j < vpn_retry_count; j++ )
		{
			poller->send_data(socket_id,vpn_channel_test_request,strlen((char *)vpn_channel_test_request),vpn_connect_timeout_ms_alt);
			vpn_connect_timeout_ms_alt += ( vpn_udp_timeout * 1000 ) / 2;
		}
	}

	vpn_connect_timeout_ms_alt += ( vpn_udp_timeout * 1000 );
	vpn_connect_timeout_ms_alt += ( current_time_ms + 1000 );

	vpn_alt_channel_state = ALT_CHANNEL_BEING_TESTED;
}


bool dialer_agent::get_display_items(char* str_dest,int max_len) // returns "true" if "dialer tunnel" should destroyed.
{
	uint running_state = 0;
	switch(dialer_state)
	{
		case LOCAL_STATE_AUTHENTICATING:			running_state = RUNNING_STATE_INGING; break;
		case LOCAL_STATE_CONNECTING_VPN:			running_state = RUNNING_STATE_INGING; break;
		case LOCAL_STATE_EXITED:					running_state = RUNNING_STATE_EXITED; break;

		case STATE_REGISTERING:						running_state = RUNNING_STATE_INGING; break;
		case STATE_CALLING:							running_state = RUNNING_STATE_INGING | RUNNING_STATE_AUDIO_SETTINGS; break;
		case STATE_CALL_PROGRESSING:				running_state = RUNNING_STATE_INGING | RUNNING_STATE_AUDIO_SETTINGS; break;
		case STATE_RINGING:							running_state = RUNNING_STATE_INGING | RUNNING_STATE_AUDIO_SETTINGS | RUNNING_STATE_AUDIO; break;
		case STATE_CONNECTED:						running_state = RUNNING_STATE_AUDIO | RUNNING_STATE_AUDIO_SETTINGS; break;

		case STATE_INCOMING_CALL:					running_state = RUNNING_STATE_INGING | RUNNING_STATE_AUDIO_SETTINGS | RUNNING_STATE_ACCEPT_INCOMING_CALL; break;
		case STATE_INCOMING_RINGING:				running_state = RUNNING_STATE_INGING | RUNNING_STATE_AUDIO_SETTINGS | RUNNING_STATE_ACCEPT_INCOMING_CALL; break;
		case STATE_INCOMING_CONNECTED:				running_state = RUNNING_STATE_AUDIO | RUNNING_STATE_AUDIO_SETTINGS; break;
		default:									break;
	}

	if ( dialer_state < LOCAL_STATE_MINIMUM && dialer_state != STATE_UNREGISTERED && dialer_state != STATE_REGISTERED && dialer_state != STATE_NONE )running_state |= RUNNING_STATE_SHOULD_CONFIRM_EXIT;

	if ( dialer_state < LOCAL_STATE_MINIMUM )
	{
		int len = snprintf(str_dest,max_len,"%u\n%u\n%s\n%s\n%s\n%s\n",callback_event,running_state,dialer_name,get_dialer_state_str(),get_dialer_status_str(),balance_value);
		callback_event = 0;

		int duration = calculate_call_duration(dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CONNECTED );
		if ( duration > 0 )
		{
			if ( duration < 3600 )len += snprintf(str_dest + len,max_len - len,"Duration : %02d:%02d\n",duration/60,duration%60);
			else len += snprintf(str_dest + len,max_len - len,"Duration : %02d:%02d:%02d\n",duration/3600,(duration % 3600) / 60, duration % 60);
		}
		else len += snprintf(str_dest + len,max_len - len,"\n");
		len += snprintf(str_dest + len,max_len - len,"%s\n%u\n%s",dialer_footer,callback_param_int,callback_param_str);
		callback_param_int = callback_param_str[0] = 0;

		if ( running_state & RUNNING_STATE_EXITED )return true;
		return false;
	}

	snprintf(str_dest,max_len,"%u\n%u\n%s\n%s\n%s\n\n\n%s\n%u\n%s",callback_event,running_state,dialer_name,get_dialer_state_str(),get_dialer_status_str(),dialer_footer,callback_param_int,callback_param_str);
	callback_event = callback_param_int = callback_param_str[0] = 0;

	if ( running_state & RUNNING_STATE_EXITED )return true;
	return false;
}

const char* dialer_agent::get_dialer_state_str()
{
	if ( tunnel_exit_time_ms > 0 )return "Unregistering#";

	if ( dialer_state == STATE_RINGING && rtp_waiting_after_call != false )return "Call progressing#";
	if ( dialer_state == STATE_CALLING && rtp_waiting_after_call == false )return "Ringing#";

	if ( dialer_state == STATE_CALLING )
	{
		snprintf((char *)call_status_temp,sizeof(call_status_temp),"Calling... %s",sip_remote_phone);
		return (const char*)call_status_temp;
	}

	switch(dialer_state)
	{
		case LOCAL_STATE_UNINITIALIZED			:	return "UnInitialized";	break;
		case LOCAL_STATE_AUTHENTICATING			:	return "Authenticating#"; break;
		case LOCAL_STATE_CONNECTING_VPN			:	return "Connecting#"; break;
		case LOCAL_STATE_EXITED					:	return "Closed"; break;

		case STATE_NONE					:	return ""; break;
		case STATE_REGISTERING			:	return "Registering#"; break;
		case STATE_REGISTERED			:	return "Registered"; break;
		case STATE_UNREGISTERED			:	return "Unregistered"; break;
		case STATE_CALLING				:	return "Calling#"; break;
		case STATE_RINGING				:	return "Ringing#"; break;
		case STATE_CONNECTED			:	return "Connected"; break;
		case STATE_CALL_PROGRESSING		:	return "Call Progressing#";	break;
		case STATE_INCOMING_CALL		:	return "#Incoming Call"; break;
		case STATE_INCOMING_RINGING		:	return "#Incoming Call"; break;
		case STATE_INCOMING_CONNECTED	:	return "Connected"; break;

		default:	break;
	}

	return "UnInitialized";
}

const char* dialer_agent::get_dialer_status_str()
{
	if ( tunnel_exit_time_ms > 0 )return "";
	if ( dialer_state == STATE_INCOMING_CALL || dialer_state == STATE_INCOMING_RINGING || dialer_state == STATE_INCOMING_CONNECTED )return (const char*)call_status_temp;

	switch(dialer_status)
	{
		case STATUS_NONE					:	return ""; break;
		case STATUS_CUSTOM					:	return (const char*)auth_custom_status; break;
		case STATUS_INTERNET_ERROR_LOCAL	:	return "Internet Error !!!\nCheck internet connection."; break;
		case STATUS_AUTH_SERVER_NOT_FOUND	:	return "Auth server not found !!!\nContact your operator."; break;
		case STATUS_EMPTY_OPERATORCODE		:	return "Operator code not set !!!\nChange settings."; break;
		case STATUS_INVALID_OPERATORCODE	:	return "Operator code not found !!!\nContact your operator."; break;
		case STATUS_USERNAME_NOT_SET		:	return "Username not set !!!\nChange settings."; break;
		case STATUS_SELF_CANCELLED			:	return "You cancelled !!!"; break;
		case STATUS_VPN_NOT_FOUND			:	return "VPN not found !!!\nContact your operator."; break;

		case STATUS_VPN_BUSY				:	return "VPN busy !!!"; break;
		case STATUS_CALL_CANCELLED			:	return "Call cancelled"; break;
		case STATUS_CALL_DISCONNECTED		:	return "Call disconnected"; break;
		case STATUS_INCOMING				:	return "Incoming call"; break;
		case STATUS_CALL_ENDED				:	return "Call ended"; break;
		case STATUS_INVALID_USERNAME		:	return "Invalid username"; break;
		case STATUS_INVALID_USERNAME_PWD	:	return "Invalid username/pwd"; break;
		case STATUS_INVALID_BALANCE			:	return "Invalid balance"; break;
		case STATUS_UNKNOWN_ERROR			:	return "Unknown error"; break;
		case STATUS_KEEP_ALIVE_ERROR		:	return "Network Timeout. Reconnect"; break;
		case STATUS_FORBIDDEN_403			:	return "Forbidden 403."; break;
		case STATUS_CALLEE_NOT_FOUND		:	return "Callee not found"; break;
		case STATUS_SERVICE_UNAVAILABLE_503	:	return "Service unavailable"; break;
		case STATUS_SIP_NO_REPLY			:	return "No reply from switch"; break;
		case STATUS_MISSED_CALL				:	return (const char*)call_status_temp; break;
		default:	break;
	}
	return "";
}

int dialer_agent::parse_auth_reply(const uchar* c,int len)
{
	if ( len <= 0 )return -1;

	int i = 0;
	uchar str_temp[256], str_temp1[256];


	// ---------------- Check authentication output ----------------

	find_token(c,len,"STATUS",str_temp,sizeof(str_temp));

	auth_status = 0;
	if ( strlen((const char*)str_temp) < 1 )return -1;
	if ( str_temp[0] != '0' )auth_status = 1;

	if ( auth_status == 0 )
	{
		if ( strlen((const char *)str_temp) < 2 )return -1;

		safe_copy(auth_custom_status,0,sizeof(auth_custom_status),str_temp + 2,strlen((const char*)str_temp)-2);
		set_current_state(LOCAL_STATE_UNINITIALIZED,STATUS_CUSTOM);
		return 0;
	}

	// -------- Check vpn ip,port -----------------------------

	vpn_tcp_priority = false;
	if ( find_token_as_int(c,len,"TRYTCP") == 1 )vpn_tcp_priority = true;

	int tcp_port = find_token_as_int(c,len,"TCPPORT");
	find_token(c,len,"UDPPORT",str_temp,sizeof(str_temp));

	int st = 0;
	int udp_port1 = next_int(str_temp,st,strlen((const char*)str_temp),':');
	int udp_port2 = next_int(str_temp,st,strlen((const char*)str_temp),':');

	for ( i = 0; i < MAX_VPN_COUNT; i++ )
	{
		vpn_ip[i] = vpn_port_tcp[i] = vpn_port_udp1[i] = vpn_port_udp2[i] = 0;

		uint i_addr = 0;
		snprintf((char *)str_temp1,sizeof(str_temp1),"VPN%d",i+1);
		find_token(c,len,(const char*)str_temp1,str_temp,sizeof(str_temp));

		i_addr = poller->str_to_inet_addr((char *)str_temp);
		if ( i_addr == 0 )continue;

		vpn_ip[i] = i_addr;
		vpn_port_tcp[i] = tcp_port;
		vpn_port_udp1[i] = udp_port1;
		vpn_port_udp2[i] = udp_port2;
	}

// -------------- GENERATE TIMEOUT SCHEDULE FOR VPN CONNECT ---------------------------

	find_token(c,len,"VPNTIMEOUT",str_temp,sizeof(str_temp));
	st = 0;

	vpn_retry_count = check_range(next_int(str_temp,st,strlen((const char*)str_temp),':'),2,5);
	vpn_udp_timeout = check_range(next_int(str_temp,st,strlen((const char*)str_temp),':'),2,5);
	vpn_tcp_timeout = check_range(next_int(str_temp,st,strlen((const char*)str_temp),':'),3,8);

	snprintf((char *)vpn_ticket_request,sizeof(vpn_ticket_request),"[GETTICKET:%d:%d:10:1:%s]",current_time_ms,auth_operator_code,auth_device_imei);
	vpn_connect_timeout_ms = network_pulse_ms;

	for ( int k = 0; k < 2; k++ )
	{
		if ( ( k == 0 && vpn_tcp_priority == false ) || ( k == 1 && vpn_tcp_priority != false ) )
		{
			for ( int i = 0; i < MAX_VPN_COUNT; i++ )
			{
				if ( vpn_ip[i] == 0 || vpn_port_udp1[i] == 0 )continue;

				uint socket_id = ( i << 4 ) + SOCKET_UDP1;
				poller->add_udp_socket( socket_id, vpn_ip[i],vpn_port_udp1[i],vpn_connect_timeout_ms);
				for ( int j = 0; j < vpn_retry_count; j++ )poller->send_data(socket_id,vpn_ticket_request,strlen((const char*)vpn_ticket_request),vpn_connect_timeout_ms + ( vpn_udp_timeout * ( j + 1 ) * 1000 ) / 2 );
			}
			vpn_connect_timeout_ms += ( vpn_udp_timeout * 1000 + ( vpn_udp_timeout * 1000 * vpn_retry_count ) / 2 );
			continue;
		}

		for ( int i = 0; i < MAX_VPN_COUNT; i++ )
		{
			if ( vpn_ip[i] == 0 || vpn_port_tcp[i] == 0 )continue;

			uint socket_id = ( i << 4 ) + SOCKET_TCP;
			poller->add_tcp_socket(socket_id,vpn_ip[i],vpn_port_tcp[i],vpn_connect_timeout_ms);
			poller->send_data(socket_id,vpn_ticket_request,strlen((char *)vpn_ticket_request),vpn_connect_timeout_ms + network_pulse_ms * 5 );
		}
		vpn_connect_timeout_ms += ( vpn_tcp_timeout * 1000 + 1000 );
	}

	vpn_connect_timeout_ms += current_time_ms;
	vpn_connect_timeout_ms += 5000;

// ----------------------------------------------------------------------------------

	find_token(c,len,"SIP",vpn_sip_transfer_info,sizeof(vpn_sip_transfer_info));
	auth_timeout = check_range(find_token_as_int(c,len,"NETWORKTIMEOUT"),5,30);

	find_token(c,len,"SIPUSER",str_temp,sizeof(str_temp));
	if ( strlen((const char*)str_temp) > 0 )safe_copy((uchar *)sip_username,0,sizeof(sip_username),str_temp,strlen((const char*)str_temp));

	find_token(c,len,"SIPPWD",str_temp,sizeof(str_temp));
	if ( strlen((const char*)str_temp) > 0 )safe_copy((uchar *)sip_password,0,sizeof(sip_password),str_temp,strlen((const char*)str_temp));

	find_token(c,len,"IVR",(uchar *)ivr_no,sizeof(ivr_no));

	uchar symbian_tune[128];
	st = 0;

	find_token(c,len,"SYMBIANTUNE",symbian_tune,sizeof(symbian_tune));
	rtp_bundle_send_size = check_range(next_int(symbian_tune,st,strlen((const char*)symbian_tune),':'),100,sizeof(rtp_send_buf));
	next_int(symbian_tune,st,strlen((const char*)symbian_tune),':');	// skip rtp bundle receive size
	next_int(symbian_tune,st,strlen((const char*)symbian_tune),':');	// skip tick_ms
	next_int(symbian_tune,st,strlen((const char*)symbian_tune),':');	// skip vas_slot_count
	next_int(symbian_tune,st,strlen((const char*)symbian_tune),':');	// skip aps_slot_count
	rtp_ringing_cache_size = check_range(next_int(symbian_tune,st,strlen((const char*)symbian_tune),':'),1000,sizeof(rtp_play_buf));
	rtp_connected_cache_size = check_range(next_int(symbian_tune,st,strlen((const char*)symbian_tune),':'),500,sizeof(rtp_play_buf));

	set_callback_event(CALLBACK_EVENT_RESIZE_AUDIO_BUF, rtp_ringing_cache_size,(const uchar*)"" );

	vpn_ping_time = find_token_as_int(c,len,"PINGTIME");
	if ( vpn_ping_time < 15 )vpn_ping_time = 15;

	find_token(c,len,"BALANCEPRE",balance_parse_pre,sizeof(balance_parse_pre));
	find_token(c,len,"BALANCEPOST",balance_parse_post,sizeof(balance_parse_post));
	find_token(c,len,"BALANCEURL",balance_url,sizeof(balance_url));

	sip_additional_call_time = find_token_as_int(c,len,"ADDCALLTIME");
	sip_caller_id_as_username = ( find_token_as_int(c,len,"USERPHONE") != 0 );

	if ( find_token_as_int(c,len,"PINTYPE") == 1 )auth_operator_fixed = 1;
	else auth_operator_fixed = 0;

	find_token(c,len,"DIALERNAME",dialer_name,sizeof(dialer_name));
	find_token(c,len,"FOOTER",dialer_footer,sizeof(dialer_footer));

	if ( sip_caller_id_as_username )safe_copy(sip_caller_id,0,sizeof(sip_caller_id),sip_username,strlen((const char*)sip_username));
	else
	{
		if ( strlen((char *)sip_caller_id) == 0 )safe_copy(sip_caller_id,0,sizeof(sip_caller_id),sip_username,strlen((const char*)sip_username));

		for ( int i = strlen((const char*)sip_caller_id)-1; i >= 0; i-- )if ( sip_caller_id[i] < '0' || sip_caller_id[i] > '9' )sip_caller_id[i] = '0';
		if ( strlen((const char*)sip_caller_id) < 3 )snprintf((char *)sip_caller_id,sizeof(sip_caller_id),"%u",current_time_ms % 10000);
	}

	return 1;
}
