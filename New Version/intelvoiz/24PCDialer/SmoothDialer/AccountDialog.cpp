// AccountDialog.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "AccountDialog.h"


// AccountDialog dialog

IMPLEMENT_DYNAMIC(AccountDialog, CDialog)

AccountDialog::AccountDialog(CWnd* pParent /*=NULL*/)
	: CDialog(AccountDialog::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iShowExitButton = false;

	// CBrush::CBrush(CBitmap* pBitmap)
    CBitmap bmp;
    // Load a resource bitmap.
	bmp.LoadBitmap(IDB_BITMAP_LOGIN);
    m_pEditBkBrush = new CBrush(&bmp);	  
	//m_pEditBkBrush = new CBrush(RGB(255,255,100));
}

AccountDialog::~AccountDialog()
{
}

void AccountDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(AccountDialog, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &AccountDialog::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL_SETTINGS, &AccountDialog::OnBnClickedButtonCancelSettings)
	ON_COMMAND(ID_TOPUP_TOPUP, &AccountDialog::OnTopupSMS)
END_MESSAGE_MAP()

// AccountDialog message handlers

BOOL AccountDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if ( MyUtil::iFixedPin )
	{
		this->GetDlgItem(IDC_STATIC_OPERATORCODE)->ShowWindow(SW_HIDE);
		this->GetDlgItem(IDC_EDIT_OPERATORCODE)->ShowWindow(SW_HIDE);
	}

	this->SetDlgItemInt(IDC_EDIT_OPERATORCODE,MyUtil::iOperatorCode);
	this->SetDlgItemTextW(IDC_EDIT_SIP_USERNAME,MyUtil::iSipUsername);
	this->SetDlgItemTextW(IDC_EDIT_SIP_PASSWORD,MyUtil::iSipPassword);
	this->SetDlgItemTextW(IDC_EDIT_SIP_CALLERID,MyUtil::iSipCallerID);

	if ( iShowExitButton )this->SetDlgItemTextW(IDC_BUTTON_CANCEL_SETTINGS,L"Exit");
	return TRUE;
}


void AccountDialog::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
	//Sample 01: Required Declarations
     CDC MemDC ;
     CBitmap bmp ;
     CPaintDC dc(this);
                                  
     //Sample 02: Get the Client co-ordinates
     CRect rct;
     this->GetClientRect(&rct);

     //Sample 03: Create the Dialog Compatible DC in the memory,
     //Then load the bitmap to the memory.
     MemDC.CreateCompatibleDC ( &dc ) ;
     bmp.LoadBitmap ( IDB_BITMAP_LOGIN ) ;
     MemDC.SelectObject ( &bmp ) ;

     //Sample 04: Finally perform Bit Block Transfer (Blt) from memory dc to
     //dialog surface.
     dc.BitBlt ( 0, 0, rct.Width() , rct.Height() , &MemDC, 0, 0, SRCCOPY ) ;
		CDialog::OnPaint();
	}

}

HBRUSH AccountDialog::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(0, 0, 0));
		//pDC->SetBkColor(RGB(255,255,100));
		pDC->SetBkMode(TRANSPARENT);

       //return (HBRUSH)GetStockObject(NULL_BRUSH);
	   return (HBRUSH)(m_pEditBkBrush->GetSafeHandle());
    default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

void AccountDialog::OnDestroy()
{
          CDialog::OnDestroy();

          // Free the space allocated for the background brush
          delete m_pEditBkBrush;
}


// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR AccountDialog::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
void AccountDialog::OnBnClickedButtonSave()
{
	MyUtil::iOperatorCode = this->GetDlgItemInt(IDC_EDIT_OPERATORCODE);
	this->GetDlgItemTextW(IDC_EDIT_SIP_USERNAME,MyUtil::iSipUsername);
	this->GetDlgItemTextW(IDC_EDIT_SIP_PASSWORD,MyUtil::iSipPassword);
	this->GetDlgItemTextW(IDC_EDIT_SIP_CALLERID,MyUtil::iSipCallerID);

	MyUtil::SaveSettings();
	EndDialog(IDOK);
}


void AccountDialog::OnBnClickedButtonCancelSettings()
{
	EndDialog(IDCANCEL);
}


void AccountDialog::OnTopupSMS()
{
	// TODO: Add your command handler code here
}
