#pragma once
#include "MyUtil.h"
#include "Request.h"

#include <vector>
#include <stdio.h>
#include "json.h"
#include "afxcmn.h"

class SmsHistoryDlg : public CDialogEx
{
	DECLARE_DYNAMIC(SmsHistoryDlg)

public:
	SmsHistoryDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~SmsHistoryDlg();
	CString listType;
// Dialog Data
	enum { IDD = IDD_DIALOG_SMS_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	void print(json_value *value,  bool isList, int ident = 0);
	bool parse(char* source, bool isList);
	CStringA SMSHistory(CString userID, CString requestKey, CString requestCode, CString listType);
	BOOL json_list;
	CStringA list;
	CStringA listMsg[40];
	int counter;
	CListCtrl m_sms_list;
};
