// FlexiSMSSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "FlexiSMSSettingsDlg.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"


using namespace std;

// FlexiSMSSettingsDlg dialog

IMPLEMENT_DYNAMIC(FlexiSMSSettingsDlg, CDialogEx)

FlexiSMSSettingsDlg::FlexiSMSSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(FlexiSMSSettingsDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

FlexiSMSSettingsDlg::~FlexiSMSSettingsDlg()
{
}

void FlexiSMSSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(FlexiSMSSettingsDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &FlexiSMSSettingsDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &FlexiSMSSettingsDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


BOOL FlexiSMSSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	this->SetDlgItemTextW(IDC_FLEXI_SMS_NAME_EDIT,MyUtil::iFlexiSMSUsername);
	this->SetDlgItemTextW(IDC_FLEXI_SMS_PASSWORD_EDIT,MyUtil::iFlexiSMSPassword);

	return TRUE;
}

// FlexiSMSSettingsDlg message handlers


void FlexiSMSSettingsDlg::FlexiAuthenticate(CString userID, CString requestKey, CString requestCode){

	CStringA	str_url;
	str_url.Append((CStringA)MyUtil::iFlexiSMSurl);
	//str_url.Append("http://192.168.1.45:8080/m.inet/mobile/");
	str_url.Append("dialer_loginapi.jsp?RequestKey=");
	str_url.Append((CStringA)requestKey);
	str_url.Append("&RequestCode=");
	str_url.Append((CStringA)requestCode);
	str_url.Append("&UserId=");
	str_url.Append((CStringA)userID);
	str_url.Append("&PCDialer=1");

	Request		myRequest;
	CStringA	sHeaderSend, sHeaderReceive, sMessage;

	UpdateData(true);

	int			IsPost		= 1;

	myRequest.SendRequest(IsPost, (LPCSTR)str_url, sHeaderSend, sHeaderReceive, sMessage);


	INT st = -1;
	INT en = -1;

	if (sMessage.Find("{") >= 0){
	 st = sMessage.Find("{");
	 en = sMessage.GetLength();
	}
	sMessage.Format("%s", sMessage.Mid(st,en-st));
	char * flexiAuthReply = sMessage.GetBuffer();
	parse(flexiAuthReply);

	//if(json_parsed_msg.Find("Your Balance:") >=0){	
	// }else{
		AfxMessageBox((CString)json_parsed_msg, MB_ICONMASK);
	//}
	
	UpdateData(false);
	
}

void FlexiSMSSettingsDlg::OnBnClickedOk()
{
	this->GetDlgItemTextW(IDC_FLEXI_SMS_NAME_EDIT,MyUtil::iFlexiSMSUsername);
	this->GetDlgItemTextW(IDC_FLEXI_SMS_PASSWORD_EDIT,MyUtil::iFlexiSMSPassword);

	MyUtil::SaveSettings();

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);
	//WORD millis = (time.wSecond * 1000) + time.wMilliseconds;			
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
    reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + MyUtil::iFlexiSMSPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	FlexiAuthenticate(MyUtil::iFlexiSMSUsername , reqKey, (CString) hash.c_str());


	EndDialog(IDOK);

}


void FlexiSMSSettingsDlg::OnBnClickedCancel()
{
	UpdateData(true);

	EndDialog(IDCANCEL);
}

void FlexiSMSSettingsDlg::print(json_value *value, int ident)
{

	if (value->name)
		json_parsed_msg =  value->name;
	switch(value->type)
	{
	case JSON_NULL:
		break;
	case JSON_OBJECT:
	case JSON_ARRAY:
		for (json_value *it = value->first_child; it; it = it->next_sibling)
		{
			print(it, ident + 1);
		}
		break;
	case JSON_STRING:
		json_parsed_msg = value->string_value;
		break;
	case JSON_INT:
		json_parsed_msg.Format("%d", value->int_value);
		break;
	case JSON_FLOAT:
		json_parsed_msg.Format("%f", value->int_value);
		break;
	case JSON_BOOL:
		json_parsed_msg = value->int_value ? "true\n" : "false\n";
		break;
	}
	//return msg;
}

bool FlexiSMSSettingsDlg::parse(char* source)
{
	char *errorPos = 0;
	char *errorDesc = 0;
	int errorLine = 0;
	block_allocator allocator(1 << 10);


	json_value *root = json_parse(source, &errorPos, &errorDesc, &errorLine, &allocator);
	if(root)
	{
		print(root);
		return TRUE;
	}

	//printf("Error at line %d: %s\n%s\n\n", errorLine, errorDesc, errorPos);
	return FALSE;
}
