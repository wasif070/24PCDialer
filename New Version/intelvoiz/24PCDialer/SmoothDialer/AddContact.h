#pragma once

#include "MyUtil.h"

// AddContact dialog

class AddContact : public CDialog
{
	DECLARE_DYNAMIC(AddContact)

public:	
	bool iEditMode;
	CString iContactName;
	CString iPhoneNo;

	AddContact(CWnd* pParent = NULL);   // standard constructor
	virtual BOOL OnInitDialog();
	virtual ~AddContact();

// Dialog Data
	enum { IDD = IDD_DIALOG_ADD_CONTACT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonAdd();
};
