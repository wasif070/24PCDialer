#ifndef __AUDIO_RECORDER_1_OCTOBER_2013_INCLUDED__
#define __AUDIO_RECORDER_1_OCTOBER_2013_INCLUDED__

#include "WThread.h"
#include <mmsystem.h>

class audio_recorder : public CWinThread
{
private:
	enum
	{
		RECORD_HEADER_COUNT = 2,		// double buffer
		RECORD_BUF_SIZE = 16000,
		RECORD_BLOCK_SIZE = 1600	// 100 ms 16 bit pcm data
	};

	static		audio_recorder *this_pointer;

	HANDLE		mutex;

	bool		is_stop_request;
	bool		is_16_bit;
	bool		is_in_thread;	
	int			gain_percentage;

	unsigned char	record_buf[RECORD_BUF_SIZE];	// 2 second
	int			record_buf_length;

	HWND		h_window;
	HWAVEIN		h_wave_in;
	LPWAVEHDR	h_wave_header[RECORD_HEADER_COUNT];

private:
	virtual BOOL	InitInstance();			// CWinThread override
	virtual int		ExitInstance();			// "
	virtual int		Run();					// "

	bool		alloc_record_buffer();
	static void CALLBACK wave_in_proc(HWAVEIN _h_wave_in, UINT _msg, DWORD _instance, DWORD _param1, DWORD _param2);

	bool		init_audio_recorder();
	bool		start_audio_recorder();
	void		on_in_block(WPARAM wParam, LPARAM lParam);

public:
	audio_recorder(HWND m_hnd);
	~audio_recorder();

	bool		is_audio_running;
	int			read_or_write_record_buffer(bool b_write,unsigned char *c,int len); // for read, len = max length
	void		resize_audio_buffer(int max_len);
	void		stop();
};
#endif