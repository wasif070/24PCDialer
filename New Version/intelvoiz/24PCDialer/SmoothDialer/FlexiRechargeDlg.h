#pragma once
#include "MyUtil.h"
#include "Request.h"

#include <vector>
#include <stdio.h>
#include "json.h"

// FlexiRechargeDlg dialog

class FlexiRechargeDlg : public CDialogEx
{
	DECLARE_DYNAMIC(FlexiRechargeDlg)

public:
	FlexiRechargeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~FlexiRechargeDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_FLEXI_RECHARGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	CStringA json_parsed_msg;
	void print(json_value *value, int ident = 0);
	bool parse(char* source);
	void FlexiAuthenticate(CString userID, CString requestKey, CString requestCode);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	void FlexiRecharge(CString userID, CString requestKey, CString requestCode, CString mobileNum, CString moneyAmount, CString numberType);
};
