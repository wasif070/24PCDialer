#include "network_poller.h"

network_poller::network_poller()
{
	receive_buf[0] = 0;
	from_len = sizeof(from);

	poller_callback = NULL;
	current_time_ms = 0;

	stop_and_reset_all();
}

network_poller::~network_poller()
{
}

void network_poller::set_listener(network_poller_callback* _poller_callback)
{
	poller_callback = _poller_callback;
}

bool network_poller::make_socket_non_blocking(int sock_handle)
{
#ifdef __UNIX_BUILD__
	int i_mode = O_NONBLOCK;
	if ( ioctl(sock_handle,FIONBIO,&i_mode) < 0 )return false;
#else
	u_long iMode = 1;		// 1 for blocking
	if ( ioctlsocket(sock_handle, FIONBIO, &iMode) != NO_ERROR )return false;
#endif

	return true;
}

int network_poller::start()
{
#ifndef __UNIX_BUILD__
	WSADATA wsaData;
	if ( WSAStartup(MAKEWORD(2,2), &wsaData) != NO_ERROR )return 0;
	current_time_ms = 0;
#endif

	return 1;
}

int	network_poller::index_from_socket_handle(int sock_handle)
{
	for ( int i = 0; i < MAX_SOCKET_COUNT; i++ )if ( socket_empty[i] == false && socket_handle[i] == sock_handle )return i;
	return -1;
}

int	network_poller::index_from_socket_id(int sock_id)
{
	for ( int i = 0; i < MAX_SOCKET_COUNT; i++ )if ( socket_empty[i] == false && socket_id[i] == sock_id )return i;
	return -1;
}

int network_poller::get_empty_pending_request()
{
	for ( int i = 0; i < MAX_RESEND_COUNT; i++ )
	{
		if ( pending_requests[i].is_active )continue;
		return i;
	}

	return MAX_RESEND_COUNT - 1;
}

void network_poller::stop_and_reset_all()
{
	for ( int i = 0; i < MAX_SOCKET_COUNT; i++)
	{
		socket_empty[i] = true;
		socket_handle[i] = NULL;
		socket_type[i] = SOCK_UDP;
	}

#ifdef __UNIX_BUILD__
	for(int i = 0; i < MAX_SOCKET_COUNT; i++)
	{
		poll_fd[i].fd = -1;
		poll_fd[i].events = 0;
	}
#else
	FD_ZERO(&fd_set_active);
#endif

	for ( int i = 0; i < MAX_RESEND_COUNT; i++ )pending_requests[i].is_active = false;
}

void network_poller::remove_pending_data(int _socket_id )
{
	for ( int i = 0; i < MAX_RESEND_COUNT; i++ )if ( pending_requests[i].socket_id == _socket_id )pending_requests[i].is_active = false;
}

bool network_poller::send_data(int _socket_id,const uchar* dt,int len,uint send_time_ms,bool from_cache )
{
	int socket_index = index_from_socket_id(_socket_id);
	if ( socket_index < 0 )return false;

	if ( ( send_time_ms > 0 && send_time_ms > current_time_ms ) ||
		 ( ( socket_type[socket_index] & SOCK_TCP ) != 0 && ( socket_type[socket_index] & SOCK_ACTION_CONNECTED ) == 0 ) )
	{
		if ( len > MAX_RESEND_SIZE )len = MAX_RESEND_SIZE;
		if ( from_cache )return false;

		int empty_slot = get_empty_pending_request();

		pending_requests[empty_slot].is_active = true;
		pending_requests[empty_slot].request_type = PENDING_REQUEST_SEND_DATA;
		pending_requests[empty_slot].data_size = len;
		pending_requests[empty_slot].socket_id = _socket_id;
		pending_requests[empty_slot].send_time_ms = send_time_ms;

		memcpy(pending_requests[empty_slot].dt,dt,len);
		return true;
	}

	if ( socket_type[socket_index] & SOCK_UDP )
	{
		struct sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = htonl(socket_dest_ip[socket_index]);
		addr.sin_port = htons(socket_dest_port[socket_index]);

		sendto(socket_handle[socket_index],(const char*)dt,len,0,(struct sockaddr*)(&addr),sizeof(addr));
	}
	else if ( socket_type[socket_index] & SOCK_TCP )
	{
		if ( socket_type[socket_index] & SOCK_ACTION_CONNECTED )
		{
			int ret = send(socket_handle[socket_index],(const char*)dt,len,0);
#ifdef __UNIX_BUILD__
			if ( ret < 0 && errno == ENOTCONN )return false;
#else
			if ( ret < 0 && WSAGetLastError() == WSAENOTCONN )return false;
#endif
			return true;
		}
		else return false;
	}

	return true;
}

bool network_poller::fetch_url(int _socket_id,const char* str_url)
{
	int dest_port = 0;
	uint dest_ip = str_to_inet_addr(str_url,&dest_port);
	if ( dest_ip == 0 )return false;

	if ( dest_port == 0 )dest_port = 80;
	int socket_index = index_from_socket_id(_socket_id);

	if ( socket_index >= 0 )remove_socket(_socket_id);
	if ( !add_tcp_socket(_socket_id,dest_ip,dest_port,0) )return false;

	int send_len = snprintf(addr_get_request,sizeof(addr_get_request),"GET /%s HTTP/1.0\r\nHost:%s\r\n\r\n",addr_get_param,addr_host);
	addr_get_request[send_len] = 0;

	if ( !send_data(_socket_id,(const uchar*)addr_get_request,send_len,0) )return false;
	return true;
}


bool network_poller::remove_socket(int _socket_id)
{
	remove_pending_data(_socket_id);

	int socket_index = index_from_socket_id(_socket_id);
	if ( socket_index < 0 )return false;

	int remove_socket_handle = socket_handle[socket_index];

#ifdef __UNIX_BUILD__
	poll_fd[socket_index].fd = -1;
	poll_fd[socket_index].events = 0;
#else
	FD_CLR(remove_socket_handle, &fd_set_active);
	if(FD_ISSET(remove_socket_handle, &fd_set_active) != false )return false;
#endif

	socket_empty[socket_index] = true;
	return true;
}

bool network_poller::add_socket(int new_socket_type,int new_socket_id,int new_socket_handle,unsigned int dest_ip,int dest_port )
{
	if ( !make_socket_non_blocking(new_socket_handle) )return false;

	int index = -1;
	for ( int i = 0; i < MAX_SOCKET_COUNT; i++ )
	{
		if ( socket_empty[i] == true )
		{
			 index = i;
			 break;
		}
	}

	if ( index < 0 )return false;

#ifdef __UNIX_BUILD__
	poll_fd[index].fd = new_socket_handle;
	poll_fd[index].events = POLLIN | POLLOUT;
#else
	FD_SET(new_socket_handle, &fd_set_active);
	if(!FD_ISSET(new_socket_handle,&fd_set_active))return false;
#endif

	socket_empty[index] = false;

	socket_handle[index] = new_socket_handle;
	socket_id[index] = new_socket_id;
	socket_type[index] = new_socket_type;

	socket_dest_ip[index] = dest_ip;
	socket_dest_port[index] = dest_port;

	return true;
}

int network_poller::add_udp_socket(int _socket_id, unsigned int dest_ip, int dest_port, uint start_time )
{
	if ( start_time != 0 && start_time > current_time_ms )
	{
		int empty_slot = get_empty_pending_request();

		pending_requests[empty_slot].is_active = true;
		pending_requests[empty_slot].request_type = PENDING_REQUEST_ADD_UDP_SOCKET;
		pending_requests[empty_slot].dest_ip = dest_ip;
		pending_requests[empty_slot].dest_port = dest_port;
		pending_requests[empty_slot].send_time_ms = start_time;
		pending_requests[empty_slot].socket_id = _socket_id;
		return 0;
	}

#ifdef __UNIX_BUILD__
	int handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
#else
	int handle = socket(PF_INET, SOCK_DGRAM, 0); //getprotobyname("udp")->p_proto);
#endif

	if ( handle == -1 )return 0;

	if ( !add_socket(SOCK_UDP,_socket_id,handle,dest_ip,dest_port))return 0;
	return handle;
}

int network_poller::add_tcp_socket(int _socket_id, unsigned int dest_ip, int dest_port, uint start_time )
{
	if ( start_time != 0 && start_time > current_time_ms )
	{
		int empty_slot = get_empty_pending_request();

		pending_requests[empty_slot].is_active = true;
		pending_requests[empty_slot].request_type = PENDING_REQUEST_ADD_TCP_SOCKET;
		pending_requests[empty_slot].dest_ip = dest_ip;
		pending_requests[empty_slot].dest_port = dest_port;
		pending_requests[empty_slot].send_time_ms = start_time;
		pending_requests[empty_slot].socket_id = _socket_id;
		return 0;
	}

#ifdef __UNIX_BUILD__
	int handle = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
#else
	int handle = socket(PF_INET, SOCK_STREAM, 0); //getprotobyname("tcp")->p_proto);
#endif

	if ( handle == -1 )return 0;
	if ( !add_socket(SOCK_TCP,_socket_id,handle,dest_ip,dest_port) )return 0;

	int socket_index = index_from_socket_id(_socket_id);

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(socket_dest_ip[socket_index]);
	addr.sin_port = htons(socket_dest_port[socket_index]);

	socket_type[socket_index] |= SOCK_ACTION_CONNECTING;
	connect(socket_handle[socket_index],(struct sockaddr *)(&addr),sizeof(addr));
	return handle;
}

void network_poller::check_pending_requests()
{
	for ( int i = 0; i < MAX_RESEND_COUNT; i++ )
	{
		if ( !pending_requests[i].is_active )continue;
		if ( pending_requests[i].send_time_ms > current_time_ms )continue;

		if ( pending_requests[i].request_type == PENDING_REQUEST_ADD_UDP_SOCKET )
		{
			add_udp_socket(pending_requests[i].socket_id,pending_requests[i].dest_ip,pending_requests[i].dest_port,0);
			pending_requests[i].is_active = false;
		}
		else if ( pending_requests[i].request_type == PENDING_REQUEST_ADD_TCP_SOCKET )
		{
			add_tcp_socket(pending_requests[i].socket_id,pending_requests[i].dest_ip,pending_requests[i].dest_port,0);
		}
		else if ( pending_requests[i].request_type == PENDING_REQUEST_SEND_DATA )
		{
			if ( send_data(pending_requests[i].socket_id,pending_requests[i].dt,pending_requests[i].data_size,0,true) )pending_requests[i].is_active = false;
		}
	}
}


uint network_poller::str_to_inet_addr(const char* str_url,int* dest_port ) // returns IP
{
	int addr_len = strlen(str_url);
	if ( addr_len > sizeof(addr_str) )addr_len = sizeof(addr_str);

	memcpy(addr_str,str_url,addr_len);
	addr_str[addr_len] = 0;

	if ( dest_port != NULL )(*dest_port) = 0;

	addr_get_param[0] = 0;
	addr_host[0] = 0;

	if ( addr_len <= 1 )return 0;

	if ( addr_len >= 7 && strnicmp(addr_str,"http://",7) == 0 )
	{
		memcpy(addr_str,addr_str + 7,addr_len - 7);
		addr_len -= 7;
	}

	addr_str[addr_len] = 0;

	int j = 0;

	for ( j = 0; j < addr_len; j++ )
	{
		if ( addr_str[j] == ':' || addr_str[j] == '/' )break;
		addr_host[j] = addr_str[j];
	}
	addr_host[j] = 0;

	for ( j = 0; j < addr_len; j++ )if ( addr_str[j] == '/' )break;
	if ( j < addr_len )
	{
		memcpy(addr_get_param, addr_str + j + 1,addr_len - j - 1);
		addr_get_param[addr_len-j-1] = 0;
	}

	for ( j = 0; j < addr_len; j++ )
	{
		if ( addr_str[j] == '/' )break;
		if ( addr_str[j] != ':' )continue;

		if ( dest_port != NULL )(*dest_port) = 0;
		for ( int k = j + 1; k < addr_len; k++ )
		{
			if ( addr_str[k] >= '0' && addr_str[k] <= '9' )
			{
				if ( dest_port != NULL )(*dest_port) = (*dest_port) * 10 + addr_str[k] - '0';
			}
			else break;
		}

		break;
	}

	if ( dest_port != NULL && (*dest_port) == 0 )(*dest_port) = 80;

	for ( j = 0; j < addr_len; j++ )if ( addr_str[j] == ':' || addr_str[j] == '/' )break;
	addr_len = j;
	addr_str[j] = 0;

	bool b_ip = true;
	for ( j = 0; j < addr_len; j++ )
	{
		if ( addr_str[j] >= '0' && addr_str[j] <= '9' )continue;
		if ( addr_str[j] == '.' )continue;

		b_ip = false;
		break;
	}

	if ( !b_ip )
	{
		struct hostent *hstnm = gethostbyname (addr_str);
		if (!hstnm)return 0;

		unsigned int host_addr = 0;
		memcpy(&host_addr,hstnm->h_addr,sizeof(unsigned int));
		host_addr = htonl(host_addr);

		unsigned int addr = host_addr;
		return addr;
	}

	unsigned int addr = 0;
	int tmp = 0;
	int cnt = 0;

	for ( int i = 0; i <= addr_len; i++ )
	{
		if ( i == addr_len || addr_str[i] == '.' )
		{
			if ( i == addr_len )
			{
				addr = addr * 256 + tmp;

				cnt++;
				break;
			}
			addr = addr * 256 + tmp;

			tmp = 0;
			cnt++;
			continue;
		}
		if ( addr_str[i] >= '0' && addr_str[i] <= '9' )tmp = tmp * 10 + addr_str[i] - '0';
	}

	if ( cnt != 4 )addr = 0;
	return addr;
}




#ifdef __UNIX_BUILD__

int network_poller::on_timer(uint _current_time_ms)
{
	current_time_ms = _current_time_ms;
	check_pending_requests();

	int ret = poll(poll_fd,MAX_SOCKET_COUNT,1);	// 1 ms timeout
	if ( ret <= 0 )return 0;
	bool found_anything = false;

	for( int i = 0; i < MAX_SOCKET_COUNT; i++)
	{
		if(poll_fd[i].fd < 0 )continue;
		if(poll_fd[i].revents & (POLLERR|POLLNVAL))
		{
			int current_socket = poll_fd[i].fd;
			remove_socket(current_socket);
			close(current_socket);
			continue;
		}

		found_anything = true;

		int index = index_from_socket_handle(poll_fd[i].fd);
		if ( index < 0 )continue;

		int current_socket = socket_handle[index];
		int current_id = socket_id[index];
		int current_socket_type = socket_type[index];

		bool b_read = false;
		bool b_write = false;

		if ( poll_fd[i].revents & POLLIN )b_read = true;
		if ( poll_fd[i].revents & POLLOUT )b_write = true;


		int read_len = -1;

		if ( b_read )
		{
			if ( current_socket_type & SOCK_UDP )
			{
				read_len = recvfrom(current_socket, (char *)receive_buf, sizeof(receive_buf), 0, (struct sockaddr *)&from, &from_len);
				if ( read_len > 0 )
				{
					receive_buf[read_len] = 0;
					poller_callback->network_poller_on_receive_data(current_id,receive_buf,read_len);
				}
			}
			else if ( current_socket_type & SOCK_TCP )
			{
				read_len = recv(current_socket, (char *)receive_buf, sizeof(receive_buf), 0);
				if ( read_len <= 0 )				// socket closed by remote
				{
					remove_socket(current_id);
					closesocket(current_socket);
					poller_callback->network_poller_on_socket_closed(current_id);
					continue;
				}

				receive_buf[read_len] = 0;
				poller_callback->network_poller_on_receive_data(current_id,receive_buf,read_len);
			}
		}

		if ( b_write )
		{
			if ( ( current_socket_type & SOCK_TCP ) != 0 && ( current_socket_type & SOCK_ACTION_CONNECTING ) != 0 )
			{
				read_len = recv(current_socket,(char *)receive_buf,sizeof(receive_buf),0);
				if ( read_len == 0 )
				{
					remove_socket(current_id);
					closesocket(current_socket);

					poller_callback->network_poller_on_socket_closed(current_id);
					continue;
				}


				socket_type[index] &= ~(SOCK_ACTION_CONNECTING);
				socket_type[index] |= SOCK_ACTION_CONNECTED;

				continue;
			}
		}

	}

	if ( !found_anything )return 0;
	return 1;
}

#else

int network_poller::on_timer(uint _current_time_ms)
{
	current_time_ms = _current_time_ms;
	check_pending_requests();

	int i = 0;
	int selection_count = 0;
	int read_selection_count = 0;

	if ( fd_set_active.fd_count <= 0 )return 0;

	read_fd_set = fd_set_active;
	write_fd_set = fd_set_active;

	timeval t;
	t.tv_sec = 0;
	t.tv_usec = 1000;

	int ret_select = select(MAX_SOCKET_COUNT, &read_fd_set, &write_fd_set, NULL, &t); // don't set timeout t = null, because, that will cause blocking call
	if( ret_select == SOCKET_ERROR )return 0;

	i = 0;
	for ( i = 0; i < read_fd_set.fd_count; i++ )socket_event_index[selection_count++] = index_from_socket_handle(read_fd_set.fd_array[i]);
	read_selection_count = selection_count;
	for ( i = 0; i < write_fd_set.fd_count; i++ )socket_event_index[selection_count++] = index_from_socket_handle(write_fd_set.fd_array[i]);

	if ( selection_count <= 0 )return 0;

	for ( i = 0; i < selection_count; i++ )
	{
		int index = socket_event_index[i];
		if ( index < 0 )continue;

		int current_socket = socket_handle[index];
		int current_id = socket_id[index];
		int current_socket_type = socket_type[index];

		bool b_read = ( i < read_selection_count );
		int read_len = -1;

		if ( b_read )
		{
			if ( current_socket_type & SOCK_UDP )
			{
				read_len = recvfrom(current_socket, (char *)receive_buf, sizeof(receive_buf), 0, (struct sockaddr *)&from, &from_len);
				if ( read_len > 0 )
				{
					receive_buf[read_len] = 0;
					poller_callback->network_poller_on_receive_data(current_id,receive_buf,read_len);
				}
			}
			else if ( current_socket_type & SOCK_TCP )
			{
				read_len = recv(current_socket, (char *)receive_buf, sizeof(receive_buf), 0);
				if ( read_len <= 0 )				// socket closed by remote
				{
					remove_socket(current_id);
					closesocket(current_socket);
					poller_callback->network_poller_on_socket_closed(current_id);
					continue;
				}

				receive_buf[read_len] = 0;
				poller_callback->network_poller_on_receive_data(current_id,receive_buf,read_len);
			}
		}
		else
		{
			if ( ( current_socket_type & SOCK_TCP ) != 0 && ( current_socket_type & SOCK_ACTION_CONNECTING ) != 0 )
			{
				socket_type[index] &= ~(SOCK_ACTION_CONNECTING);
				socket_type[index] |= SOCK_ACTION_CONNECTED;
				continue;
			}
		}

	}
	return 1;
}
#endif