#include "stdafx.h"
#include "audio_recorder.h"

audio_recorder* audio_recorder::this_pointer;

audio_recorder::audio_recorder(HWND m_hnd)
{
	this_pointer = this;
	h_window = m_hnd;

	is_16_bit = true;
	is_in_thread = false;
	gain_percentage = 100;

	is_audio_running = false;
	is_in_thread = true;

	this_pointer = this;
	mutex = CreateMutex(NULL,FALSE,NULL);

	start_audio_recorder();
//	CreateThread(CREATE_SUSPENDED,100000);
//	ResumeThread();
}

audio_recorder::~audio_recorder()
{
	stop();
}

BOOL audio_recorder::InitInstance()
{
	return TRUE;
}

int audio_recorder::ExitInstance()
{
	return 1;
}

int audio_recorder::Run()
{
	start_audio_recorder();
	return 1;
}

void audio_recorder::resize_audio_buffer(int max_len)
{
	record_buf_length = 0;
}

int audio_recorder::read_or_write_record_buffer(bool b_write,unsigned char *c,int len) // for read, len = max length
{
	if ( !is_in_thread )return 0;

	if ( is_16_bit == true && ( len % 160 ) != 0 )len -= ( len % 160 );
	if ( is_16_bit == false && ( len % 80 ) != 0 )len -= ( len % 80 );

	if ( len <= 0 )return 0;

	if ( b_write == false && record_buf_length <= 0 )return 0;

	if ( b_write == false )
	{
//		if ( mutex != NULL )WaitForSingleObject(mutex,10);

		if ( len > record_buf_length )len = record_buf_length;
		memcpy(c,record_buf,len);

		if ( len < record_buf_length )memmove(record_buf,record_buf + len,record_buf_length - len);
		record_buf_length -= len;

//		if ( mutex != NULL )ReleaseMutex(mutex);
		return len;
	}

//	if ( mutex != NULL )WaitForSingleObject(mutex,10);

	if ( len > sizeof(record_buf) )len = sizeof(record_buf);
	if ( ( record_buf_length + len ) > sizeof(record_buf) )
	{
		int delete_len = record_buf_length + len - sizeof(record_buf);

		if ( is_16_bit == true && ( delete_len % 160 ) != 0 )delete_len += ( 160 - ( delete_len % 160 ) );
		if ( is_16_bit == false && ( delete_len % 80 ) != 0 )delete_len += ( 80 - ( delete_len % 80 ) );

		if ( delete_len > 0 && delete_len < sizeof(record_buf))memmove(record_buf,record_buf + delete_len,record_buf_length - delete_len);
		record_buf_length -= delete_len;
		if ( record_buf_length < 0 )record_buf_length = 0;
	}

	memcpy(record_buf + record_buf_length,c,len);
	record_buf_length += len;
	
//	if ( mutex != NULL )ReleaseMutex(mutex); 
	return len;
}

/*void audio_recorder::stop()
{
	if ( !is_in_thread )return;
	is_stop_request = true;

	int stop_countdown = 10;
	while( is_stop_request == true && ( stop_countdown-- ) > 0 )Sleep(50);	
}*/

void audio_recorder::stop()
{
	for (int i = 0; i < RECORD_HEADER_COUNT; i++)waveInUnprepareHeader(h_wave_in, h_wave_header[i], sizeof(WAVEHDR));	
	waveInStop(h_wave_in);
	waveInClose(h_wave_in);
}

bool audio_recorder::alloc_record_buffer()
{
	for ( int i = 0; i < RECORD_HEADER_COUNT; i++)
	{         
		h_wave_header[i] = new WAVEHDR;
		if ( h_wave_header[i] == NULL )return false;
		
		h_wave_header[i]->lpData = new char[RECORD_BLOCK_SIZE];
		h_wave_header[i]->dwBufferLength = RECORD_BLOCK_SIZE;
		h_wave_header[i]->dwFlags = 0;
	}
}

bool audio_recorder::start_audio_recorder()
{
	if ( !alloc_record_buffer() )return false;

	WAVEFORMATEX wfx;

	WAVEINCAPS   wic;                                               
	MMRESULT     rc;                                                
	UINT         max_device = waveInGetNumDevs();                  
	
	h_wave_in = NULL;
	               
	for( int i = 0; i < max_device; i++ )
	{
	   rc = waveInGetDevCaps(i, &wic, sizeof(wic));
	   if ( rc == MMSYSERR_NOERROR )
	   {
			wfx.nChannels = 1;
			wfx.nSamplesPerSec = 8000;
			wfx.wFormatTag      = WAVE_FORMAT_PCM;                          
			wfx.wBitsPerSample  = ( ( is_16_bit == true ) ? 16 : 8 );
			wfx.nBlockAlign     = wfx.nChannels * wfx.wBitsPerSample / 8;
			wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;     
			wfx.cbSize          = 0;

			rc = waveInOpen(&h_wave_in, i, &wfx, (DWORD)(VOID*)wave_in_proc, (DWORD)this->h_window,CALLBACK_FUNCTION);

			if (rc == MMSYSERR_NOERROR)break;
	        return false;
	   }
	}
		
	if ( h_wave_in == NULL )return false;

	for (int i = 0; i < RECORD_HEADER_COUNT; i++)
	{                                                               
		rc = waveInPrepareHeader(h_wave_in, h_wave_header[i], sizeof(WAVEHDR));
		if ( rc == MMSYSERR_NOERROR )rc = waveInAddBuffer(h_wave_in, h_wave_header[i], sizeof(WAVEHDR));

		if ( rc != MMSYSERR_NOERROR )return false;
	}
	
	MMRESULT ret = waveInStart(h_wave_in);
	if ( ret != MMSYSERR_NOERROR )return false;
	
	MMTIME mmtime;
	mmtime.wType = TIME_SAMPLES;
	ret = waveInGetPosition(h_wave_in, &mmtime, sizeof(MMTIME));

	if (ret != MMSYSERR_NOERROR)return false;	
	return true;
}


void audio_recorder::on_in_block(WPARAM wParam, LPARAM lParam)
{
	LPWAVEHDR lpwh = (LPWAVEHDR)lParam;	
	if ( lpwh == NULL )return;

	if ( is_audio_running && lpwh->dwBytesRecorded > 0 )read_or_write_record_buffer(true,(unsigned char*)lpwh->lpData,lpwh->dwBytesRecorded);
	
	MMRESULT rc = waveInPrepareHeader(h_wave_in, lpwh, sizeof(WAVEHDR));       
	if ( rc == MMSYSERR_NOERROR )rc = waveInAddBuffer(h_wave_in, lpwh, sizeof(WAVEHDR));
		
	if ( rc != MMSYSERR_NOERROR )return;
}

void audio_recorder::wave_in_proc(HWAVEIN _h_wave_in, UINT _msg, DWORD _instance, DWORD _param1, DWORD _param2)
{
	this_pointer->on_in_block(_msg,_param1);
}

