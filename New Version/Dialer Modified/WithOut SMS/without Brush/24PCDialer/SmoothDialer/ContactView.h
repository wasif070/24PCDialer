#pragma once

#include "MyUtil.h"
#include "AddContact.h"

#include "afxcmn.h"

// CContactView dialog

class CContactView : public CDialog
{
	DECLARE_DYNAMIC(CContactView)

public:
	CContactView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CContactView();

	CString iSelectedContact;

// Dialog Data
	enum { IDD = IDD_DIALOG_CONTACTS };

protected:
	AddContact iContactEditor;

	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl iContactList;
	afx_msg void OnBnClickedButtonDeleteContact();
	afx_msg void OnBnClickedButtonNewContact();
	afx_msg void OnBnClickedButtonEditContact();
	afx_msg void OnBnClickedButtonCall();
	afx_msg void OnNMDblclkListContacts(NMHDR *pNMHDR, LRESULT *pResult);
};
