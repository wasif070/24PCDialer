#include <stdafx.h>
#include "MyUtil.h"

CString	MyUtil::i_XmlItem[10];
char    MyUtil::i_temp[MAX_FILE_SIZE];
CString MyUtil::i_emptyString = L"";

CString	MyUtil::iContacts[MAX_CONTACT_COUNT];
CString	MyUtil::iCallLog[MAX_CALL_LOG];
CString	MyUtil::iDialLog[MAX_CALL_LOG];

CString	MyUtil::iAuthUrl = L"";
CString	MyUtil::iAuthUrlOptional = L"";
CString MyUtil::iAboutText = L"";

bool	MyUtil::iFixedPin = false;
int		MyUtil::iBasePin = 0;

int		MyUtil::iOperatorCode = 0;
CString MyUtil::iSipUsername = L"";
CString MyUtil::iSipPassword = L"";
CString MyUtil::iSipCallerID = L"";

bool MyUtil::LoadFile(const CString& file_name,CString& dt)
{
	dt = L"";

	TCHAR cur_dir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH,cur_dir);

	CString str_path = cur_dir;
	str_path += L"\\";
	str_path += file_name;

	CFile cfl;
	if ( !cfl.Open(str_path,CFile::modeRead) )return false;
	
	int len = cfl.GetLength();
	if ( len > MAX_FILE_SIZE )len = MAX_FILE_SIZE;

	len = cfl.Read(i_temp,len);
	i_temp[len] = 0;
	
	for ( int i = 0; i < len; i++ )dt += i_temp[i];
	cfl.Close();

	return true;
}

bool MyUtil::SaveFile(const CString& file_name,const CString& dt)
{
	TCHAR cur_dir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH,cur_dir);

	CString str_path = cur_dir;
	str_path += L"\\";
	str_path += file_name;

	CFile cfl;
	if ( !cfl.Open(str_path,CFile::modeReadWrite) )
	{
		if ( !cfl.Open(str_path,CFile::modeCreate) )
		{
			AfxMessageBox(L"Fatal error: Cannot save file :\r\n\r\n" + str_path);
			return false;
		}
		cfl.Close();
		if ( !cfl.Open(str_path,CFile::modeReadWrite))
		{
			AfxMessageBox(L"Error: Cannot save file :\r\n" + str_path);
			return false;
		}
	}

	const char* dt1 = StrToCharArray(dt);
	int len = strlen(dt1);
	if ( len > MAX_FILE_SIZE )len = MAX_FILE_SIZE;

	cfl.Write(StrToCharArray(dt),dt.GetLength());
	cfl.SetLength(dt.GetLength());

	cfl.Flush();
	cfl.Close();

	return true;
}

const CString MyUtil::ModifyItemForSeparator(const CString& str)
{
	CString str1 = L"";
	for ( int i = 0; i < str.GetLength(); i++ )
	{
		if ( str.GetAt(i) == ITEM_SEPARATOR )str1 += ' ';
		else str1 += str.GetAt(i);
	}

	return str1;
}

const CString MyUtil::IntToStr(int dt)
{
	if ( dt == 0 )return L"0";

	CString str1 = L"";
	for ( int i = 0; i < 10; i++ )
	{
		if ( dt <= 0 )break;

		str1 += (char)('0' + ( dt % 10 ));
		dt /= 10;
	}

	CString str2 = L"";
	for ( int i = str1.GetLength()-1; i >= 0; i-- )str2 += (char)str1.GetAt(i);
	return str2;
}

int MyUtil::StrToInt(const CString& str)
{
	int ret = 0;
	bool is_negative = false;
	for ( int i = 0; i < str.GetLength(); i++ )
	{
		char c = str.GetAt(i);
		if ( c == '-' )is_negative = false;
		if ( c >= '0' && c <= '9' )ret = ret * 10 + c - '0';
	}

	if ( is_negative )ret = 0 - ret;
	return ret;
}

const char* MyUtil::StrToCharArray(const CString& str)
{
	int len = str.GetLength();
	if ( len >= sizeof(i_temp)-1 )len = sizeof(i_temp)-1;
	int i = 0;
	for ( ; i < len; i++ )i_temp[i] = str.GetAt(i);
	i_temp[i] = 0;

	return i_temp;
}

int MyUtil::StrToCharArray(const CString& str,char* str_dest,int max_len)
{
	max_len--;

	int len = 0;
	while ( len < str.GetLength() && len < max_len )str_dest[len++] = str.GetAt(len);
	str_dest[len] = 0;

	return len;
}

const CString* MyUtil::BreakLine(const CString& dt,int& item_count)
{
	for ( int i = 0; i < 10; i++ )i_XmlItem[i] = i_emptyString;

	item_count = 0;
	int len = dt.GetLength();

	for ( int i = 0; i < len; i++ )
	{
		if ( dt.GetAt(i) == ITEM_SEPARATOR )
		{
			item_count++;
			if ( item_count >= 10 )break;
			continue;
		}

		i_XmlItem[item_count] += dt.GetAt(i);
	}
	item_count++;

	for ( int i = 0; i < item_count; i++ )i_XmlItem[i].Trim();
	return i_XmlItem;
}

const CString& MyUtil::ReadXmlItem(const CString& str_data,CString str_tag, int* _st )
{
	CString str_tag1 = L"<";
	str_tag1 += str_tag;
	str_tag1 += L">";

	int st = 0;
	if ( _st != NULL )
	{
		st = (*_st);
		if ( st >= str_data.GetLength() || st < 0 )
		{
			(*_st) = -1;
			return i_emptyString;
		}
	}

	int p = str_data.Find(str_tag1,st);	
	if ( p < st )
	{
		if ( _st != NULL )(*_st) = -1;
		return i_emptyString;
	}
	
	p += str_tag1.GetLength();

	i_XmlItem[0] = L"";
	int len = str_data.GetLength();

	for ( ; p < ( len - 2 ); p++ )
	{
		if ( str_data.GetAt(p) == '<' && str_data.GetAt(p+1) == '/' && str_data.GetAt(p+2) == '>' )break;
		i_XmlItem[0] += str_data.GetAt(p);	
	}
	
	if ( _st != NULL )(*_st) = p + 3;

	i_XmlItem[0].Trim();
	return i_XmlItem[0];
}










// ---------------------------------------------------------------
// ---------------------------------------------------------------
// ---------------------------------------------------------------

bool MyUtil::LoadBaseConfig()
{
	HRSRC hRes = FindResource(NULL, MAKEINTRESOURCE(IDR_CONFIG), L"Text"); 
	HGLOBAL hGlobal = NULL;
	DWORD dwTextSize = 0;

	if(hRes != NULL)
	{
		hGlobal = LoadResource(NULL, hRes);
		dwTextSize = SizeofResource(NULL, hRes);
	}

	if(hGlobal != NULL)
	{ 
		CString cs = L"";

		const char *lpszText = (const char *)LockResource(hGlobal);

		for ( int i = 0; i < strlen(lpszText); i++ )cs += lpszText[i];
		UnlockResource(hGlobal);

		iFixedPin = ( StrToInt(MyUtil::ReadXmlItem(cs,L"FIXEDPIN")) != 0 );
		iBasePin = StrToInt(MyUtil::ReadXmlItem(cs,L"BASEPIN"));
		
		iAuthUrl = MyUtil::ReadXmlItem(cs,L"AUTHSERVER");
		iAuthUrlOptional = MyUtil::ReadXmlItem(cs,L"AUTHSERVER_OPTIONAL");
		iAboutText = MyUtil::ReadXmlItem(cs,L"ABOUT");

		iAboutText.Replace(L"\\n",L"\r\n");
	}
	else return false;

/*	CString cs = L"";
	cs.Format(L"||%d||\r\n||%d||\r\n||%s||\r\n||%s||",iFixedPin,iBasePin,iAuthUrl,iAboutText);
	AfxMessageBox(cs);*/

	LoadSettings();
	return true;
}

void MyUtil::LoadSettings()
{
	for ( int i = 0; i < MAX_CONTACT_COUNT; i++ )iContacts[i] = L"";
	for ( int i = 0; i < MAX_CALL_LOG; i++ )iCallLog[i] = L"";
	for ( int i = 0; i < MAX_CALL_LOG; i++ )iDialLog[i] = L"";

	CString dt = L"";
	LoadFile(L"settings.conf",dt);
	if ( dt.GetLength() <= 10 )return;

	iOperatorCode = StrToInt(ReadXmlItem(dt,L"OPERATORCODE"));
	iSipUsername = ReadXmlItem(dt,L"SIPUSERNAME");
	iSipPassword = ReadXmlItem(dt,L"SIPPASSWORD");
	iSipCallerID = ReadXmlItem(dt,L"SIPCALLERID");

	int st = 0;
	int contact_count = 0;
	for ( int i = 0; i < MAX_CONTACT_COUNT; i++ )
	{
		iContacts[contact_count++] = ReadXmlItem(dt,L"CONTACT",&st);
		if ( st < 0 )break;
		if ( iContacts[contact_count-1].GetLength() <= 0 )contact_count--;		
	}

	int call_log_count = 0;
	st = 0;
	for ( int i = 0; i < MAX_CALL_LOG; i++ )
	{
		iCallLog[call_log_count++] = ReadXmlItem(dt,L"CALLLOG",&st);
		if ( st < 0 )break;
		if ( iCallLog[call_log_count-1].GetLength() <= 0 )call_log_count--;		
	}

	int dial_log_count = 0;
	st = 0;
	for ( int i = 0; i < MAX_CALL_LOG; i++ )
	{
		iDialLog[dial_log_count++] = ReadXmlItem(dt,L"DIALLOG",&st);
		if ( st < 0 )break;
		if ( iDialLog[dial_log_count-1].GetLength() <= 0 )dial_log_count--;		
	}
}

void MyUtil::SaveSettings()
{
	CString dt = L"";
	dt.Format(L"<OPERATORCODE>%d</>\r\n<SIPUSERNAME>%s</>\r\n<SIPPASSWORD>%s</>\r\n<SIPCALLERID>%s</>\r\n\r\n",iOperatorCode,iSipUsername,iSipPassword,iSipCallerID);

	for ( int i = 0; i < MAX_CONTACT_COUNT; i++ )
	{
		if ( iContacts[i].GetLength() <= 0 )continue;	
		dt += "<CONTACT>";
		dt += iContacts[i];
		dt += "</>\r\n";
	}
	dt += "\r\n";

	for ( int i = 0; i < MAX_CALL_LOG; i++ )
	{
		if ( iCallLog[i].GetLength() <= 0 )continue;	
		dt += "<CALLLOG>";
		dt += iCallLog[i];
		dt += "</>\r\n";
	}
	dt += "\r\n";

	for ( int i = 0; i < MAX_CALL_LOG; i++ )
	{
		if ( iDialLog[i].GetLength() <= 0 )continue;	
		dt += "<DIALLOG>";
		dt += iDialLog[i];
		dt += "</>\r\n";
	}
	dt += "\r\n";

	SaveFile(L"settings.conf",dt);
}

bool MyUtil::GetContact(int index,CString& str_name,CString& str_phone)
{
	str_name = L"";
	str_phone = L"";

	if ( index < 0 || index >= MAX_CONTACT_COUNT || iContacts[index].GetLength() <= 0 )return false;

	int item_count = 0;
	const CString* str_items = BreakLine(iContacts[index],item_count);
	str_name = str_items[0];
	str_phone = str_items[1];

	return true;
}

int MyUtil::AddContact(const CString& str_name,const CString& str_phone_no)
{
	int pos = 0;
	for ( ; pos < MAX_CONTACT_COUNT; pos++ )if ( iContacts[pos].GetLength() <= 0 )break;
	if ( pos >= MAX_CONTACT_COUNT )return -1;

	if ( str_name.GetLength() <= 0 )return -1;
	if ( str_phone_no.GetLength() <= 0 )return -1;

	iContacts[pos] = ModifyItemForSeparator(str_name);
	iContacts[pos] += (char)ITEM_SEPARATOR;
	iContacts[pos] += ModifyItemForSeparator(str_phone_no);

	SaveSettings();
	return pos;
}

void MyUtil::EditContact(int pos,const CString& str_name,const CString& str_phone_no)
{
	if ( pos < 0 || pos >= MAX_CONTACT_COUNT )return;
	if ( str_name.GetLength() <= 0 )return;
	if ( str_phone_no.GetLength() <= 0 )return;

	iContacts[pos] = ModifyItemForSeparator(str_name);
	iContacts[pos] += (char)ITEM_SEPARATOR;
	iContacts[pos] += ModifyItemForSeparator(str_phone_no);

	SaveSettings();
}

void MyUtil::DeleteContact(int index)
{
	if ( index < 0 || index >= MAX_CONTACT_COUNT )return;
	
	iContacts[index] = L"";
	SaveSettings();	
}

bool MyUtil::GetCallLog(bool b_dial_log,int index,CString& str_name,CString& phone_no,CString& str_time,CString& str_duration)
{
	str_name = L"";
	phone_no = L"";
	str_time = L"";
	str_duration = L"";

	if ( index < 0 || index >= MAX_CALL_LOG )return false;

	int cnt = 0;
	if ( b_dial_log )
	{
		if ( iDialLog[index].GetLength() <= 0 )return false;
		const CString* str = BreakLine(iDialLog[index],cnt);
				
		str_name = str[0];
		phone_no = str[1];
		str_time = str[2];

		return true;
	}

	if ( iCallLog[index].GetLength() <= 0 )return false;
	const CString* str = BreakLine(iCallLog[index],cnt);

	str_name = str[0];
	phone_no = str[1];	
	str_time = str[2];
	str_duration = str[3];
	
	return true;
}

void MyUtil::AddCallLog(bool b_dial_log,const CString& str_name,const CString& phone_no,int duration_sec)
{
	if ( phone_no.GetLength() <= 0 )return;

	time_t rawtime;
	struct tm * timeinfo;

	time (&rawtime);
	timeinfo = localtime (&rawtime);

	strftime(i_temp,sizeof(i_temp),"%I:%M %p %b %d",timeinfo);
	CString str_time = L"";
	for ( int i = 0; i < strlen(i_temp); i++ )str_time += (char)i_temp[i];

	if ( !b_dial_log )
	{
		for ( int i = MAX_CALL_LOG - 1; i > 0; i-- )iCallLog[i] = iCallLog[i-1];

		CString str_duration = L"";
		if ( duration_sec < 3600 )str_duration.Format(L"%02d:%02d",duration_sec/60,duration_sec%60);
		else str_duration.Format(L"%02d:%02d:%02d", (duration_sec/3600), (duration_sec%3600)/60,duration_sec%60);
		
		iCallLog[0] = ModifyItemForSeparator(str_name);
		iCallLog[0] += (char)ITEM_SEPARATOR;
		iCallLog[0] += ModifyItemForSeparator(phone_no);
		iCallLog[0] += (char)ITEM_SEPARATOR;
		iCallLog[0] += ModifyItemForSeparator(str_time);
		iCallLog[0] += (char)ITEM_SEPARATOR;
		iCallLog[0] += ModifyItemForSeparator(str_duration);
	}
	else
	{
		for ( int i = MAX_CALL_LOG - 1; i > 0; i-- )iDialLog[i] = iDialLog[i-1];

		iDialLog[0] = ModifyItemForSeparator(str_name);
		iDialLog[0] += (char)ITEM_SEPARATOR;
		iDialLog[0] += ModifyItemForSeparator(phone_no);
		iDialLog[0] += (char)ITEM_SEPARATOR;
		iDialLog[0] += ModifyItemForSeparator(str_time);
	}

	SaveSettings();
}

const CString MyUtil::PhoneNoToName(const CString& phone_no)
{
	if ( phone_no.GetLength() < 3 )return i_emptyString;

	CString str_best_case = L"";

	for ( int i = 0; i < MAX_CONTACT_COUNT; i++ )
	{
		if ( iContacts[i].GetLength() <= 0 )continue;
		int p = iContacts[i].Find(ITEM_SEPARATOR);
		if ( p < 0 )continue;

		CString phone_no1 = iContacts[i].Mid(p+1);
		if ( phone_no1.GetLength() <= 0 )continue;

		if ( phone_no.Compare(phone_no1) == 0 )return iContacts[i].Mid(0,p);
		if ( phone_no.GetLength() > phone_no1.GetLength() && phone_no.Find(phone_no1,phone_no.GetLength()-phone_no1.GetLength()) > 0 && (phone_no.GetLength() - phone_no1.GetLength()) <= 4 )str_best_case = iContacts[i].Mid(0,p);
		if ( phone_no1.GetLength() > phone_no.GetLength() && phone_no1.Find(phone_no,phone_no1.GetLength()-phone_no.GetLength()) > 0 && (phone_no1.GetLength() - phone_no.GetLength()) <= 4 )str_best_case = iContacts[i].Mid(0,p);
	}

	return str_best_case;
}