// SmoothDialerDlg.h : header file
//

#pragma once

#include <stdlib.h>
#define snprintf _snprintf
#define strnicmp _strnicmp

#ifndef uchar
typedef unsigned int uint;
typedef unsigned char uchar;
#endif

//#include "dialer_tunnel.h"
#include "MyUtil.h"
#include "AccountDialog.h"
#include "ContactView.h"
#include "CallLog.h"

#include "audio_player.h"
#include "audio_recorder.h"
#include "afxwin.h"
#include "afxcmn.h"

#define CurrentTimeMS GetTickCount
extern void dialer_tunnel_tick(uint _current_time_ms,const char* _tunnel_command,const uchar* _record_buf,int _record_len,uchar* _play_buf,int _play_buf_size,int& _play_len,char* _view_str,int _view_str_size);

// CSmoothDialerDlg dialog
class CSmoothDialerDlg : public CDialog
{
// Construction
public:
	CSmoothDialerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_SMOOTHDIALER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	enum
	{
		TUNNEL_TIMER_MS = 100,
		TUNNEL_COMMAND_NONE = 0,
		TUNNEL_COMMAND_RECONNECT = 1,	// cmd#auth_url#auth_url_optional#operator_code#username#pwd#caller-id#timer_ms
		TUNNEL_COMMAND_CALL = 2,
		TUNNEL_COMMAND_CALL_IVR = 3,
		TUNNEL_COMMAND_END_CALL = 4,
		TUNNEL_COMMAND_EXIT = 5,
		
		RUNNING_STATE_AUDIO = 1,				// This should be same as DialerAgent.h
		RUNNING_STATE_INGING = 2,
		RUNNING_STATE_AUDIO_SETTINGS = 4,
		RUNNING_STATE_ACCEPT_INCOMING_CALL = 8,
		RUNNING_STATE_SHOULD_CONFIRM_EXIT = 16,
		RUNNING_STATE_EXITED = 32,

		CALLBACK_EVENT_NONE = 0,
		CALLBACK_EVENT_AUTH_ERROR = 1,
		CALLBACK_EVENT_CALL_COMPLETE = 2,
		CALLBACK_EVENT_INCOMING_CALL_COMPLETE = 3,
		CALLBACK_EVENT_COMPLETE_SIP_SETTINGS = 4,
		CALLBACK_EVENT_RESIZE_AUDIO_BUF = 5,

		COLOR_BRAND_NAME = RGB(0,0,0),
		COLOR_STATE = RGB(0,0,0),
		COLOR_STATUS = RGB(128,64,64),
		COLOR_BALANCE = RGB(0,0,0),
		COLOR_DURATION = RGB(192,64,64),
		COLOR_FOOTER = RGB(0,0,0)
	};

	AccountDialog	iAccountView;
	CContactView	iContactView;
	CCallLog		iCallLog;

	audio_player*	iAudioPlayer;
	audio_recorder*	iAudioRecorder;
	
	uint			iRunningState;
	uint			iRunningStatePrev;
	uint			iCallbackEvent;
	uint			iCallbackEventParam;
	CString			iCallbackEventParam1;

	CFont			iSmallFont;
	CFont			iMediumFont;

	char			iViewStr[512];
	char			iTunnelCommand[256];
	uint			iShutDownTimeout;

	unsigned char	iPlayBuf[8000];		// 500 ms
	unsigned char	iRecordBuf[3200];	// 200 ms

	unsigned int	iPlayBufLength;
	unsigned int	iRecordBufLength;

	HICON m_hIcon;
	CDC m_HomeScreenDC;
	CBitmap m_HomeScreenBitmap;
	int iHomeScreenWidth;
	int iHomeScreenHeight;

	void UpdateLabel(int item_id,const CString& str);
	void SendToTunnel(int cmd,const char* param1 = "",const char* param2 = "",const char* param3 = "",const char* param4 = "",const char* param5 = "",const char* param6 = "",const char* param7 = "");
	
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedCall();
	afx_msg void OnBnClickedNumC();
	afx_msg void OnBnClickedEnd();
	afx_msg void OnBnClickedNum1();
	afx_msg void OnBnClickedNum2();
	afx_msg void OnBnClickedNum3();
	afx_msg void OnBnClickedNum4();
	afx_msg void OnBnClickedNum5();
	afx_msg void OnBnClickedNum6();
	afx_msg void OnBnClickedNum7();
	afx_msg void OnBnClickedNum8();
	afx_msg void OnBnClickedNum9();
	afx_msg void OnBnClickedNumStar();
	afx_msg void OnBnClickedNum0();
	afx_msg void OnBnClickedNumHash();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
	afx_msg BOOL OnCmdMsg(UINT nID,int nCode,void* pExtra,AFX_CMDHANDLERINFO* pHandlerInfo);
	afx_msg void OnEnChangePhoneNo();
	afx_msg void OnMenuIvr();
	afx_msg void OnMenuCalllog();
	afx_msg void OnMenuContacts();
	afx_msg void OnMenuReconnect();
	afx_msg void OnMenuSettings();
	afx_msg void OnMenuAboutus();
	CButton button_one;
	CButton button_two;
	CButton button_three;
	CButton button_four;
	CButton button_five;
	CButton button_six;
	CButton button_seven;
	CButton button_eight;
	CButton button_nine;
	CButton button_star;
	CButton button_zero;
	CButton button_hash;
	CButton button_call;
	CButton button_end;
	CButton button_c;
	CSliderCtrl slider_left;
	CSliderCtrl slider_right;
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};
