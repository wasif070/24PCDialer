#ifndef __WINDOWS_POLLER_8_AUGUST_2012_ADDED__
#define __WINDOWS_POLLER_8_AUGUST_2012_ADDED__

#include "platform_def.h"

class network_poller_callback
{
public:
	virtual void network_poller_on_socket_closed(int socket_id) = 0;
	virtual void network_poller_on_receive_data(int socket_id,const unsigned char* dt,int len) = 0;
};


class network_poller
{
private:
	enum
	{
		SOCK_UDP = 1,
		SOCK_TCP = 2,
		SOCK_ACTION_CONNECTING = 4,
		SOCK_ACTION_CONNECTED = 8,
		SOCK_ACTION_SENDING = 16,

		PENDING_REQUEST_SEND_DATA = 0,
		PENDING_REQUEST_ADD_UDP_SOCKET = 1,
		PENDING_REQUEST_ADD_TCP_SOCKET = 2,

		MAX_SOCKET_COUNT = 20,
		MAX_RECEIVE_BUF = 4096,
		MAX_RESEND_SIZE = 512,	// It should not be less. because, html request may be long.
		MAX_RESEND_COUNT = 20
	};

	typedef struct
	{
		bool is_active;
		uint request_type;	// 0: send data, 1: add udp socket, 2: add tcp socket

		uint socket_id;
		uint send_time_ms;	// for socket create request, this is the time when socket should be created
		uint dest_ip;		// this is valid, when is_socket_create_request = true
		uint dest_port;		// this is valid, when is_socket_create_request = true

		int  data_size;
		uchar dt[MAX_RESEND_SIZE];
	}
	pending_struct;

	network_poller_callback* poller_callback;
	uint	current_time_ms;

#ifdef __UNIX_BUILD__
	pollfd	poll_fd[MAX_SOCKET_COUNT];
#else
	fd_set	fd_set_active;
	fd_set	read_fd_set;
	fd_set	write_fd_set;
#endif

	struct	sockaddr_in from;
	int		from_len;

	int		socket_event_index[MAX_SOCKET_COUNT * 2]; // The sockets which has received/sent data
	bool	socket_empty[MAX_SOCKET_COUNT];
	int		socket_handle[MAX_SOCKET_COUNT];
	int		socket_id[MAX_SOCKET_COUNT];
	int		socket_type[MAX_SOCKET_COUNT];

	uint	socket_dest_ip[MAX_SOCKET_COUNT];
	int		socket_dest_port[MAX_SOCKET_COUNT];

	uchar	receive_buf[MAX_RECEIVE_BUF];
	pending_struct pending_requests[MAX_RESEND_COUNT];

	char	addr_str[256];
	char	addr_get_param[256];
	char	addr_get_request[512];
	char	addr_host[256];

	int	index_from_socket_handle(int sock_handle);
	int	index_from_socket_id(int sock_id);
	int get_empty_pending_request();

	bool make_socket_non_blocking(int sock_handle);
	bool add_socket(int new_socket_type,int new_socket_id,int new_socket_handle,unsigned int dest_ip,int dest_port );

public:
	network_poller();
	~network_poller();
	void set_listener(network_poller_callback* _poller_callback);
	int start();

	void stop_and_reset_all();
	void remove_pending_data(int _socket_id );
	bool send_data(int _socket_id,const uchar* dt,int len,uint send_time_ms,bool from_cache = false);
	bool fetch_url(int _socket_id,const char* str_url);
	bool remove_socket(int _socket_id);
	int add_udp_socket(int _socket_id, unsigned int dest_ip, int dest_port, uint start_time = 0 );
	int add_tcp_socket(int _socket_id, unsigned int dest_ip, int dest_port, uint start_time = 0 );
	void check_pending_requests();
	int on_timer(uint _current_time_ms);
	uint str_to_inet_addr(const char* str_url,int* dest_port = NULL); // returns IP
};

#endif