#pragma once

#include "MyUtil.h"

// AccountDialog dialog

class AccountDialog : public CDialog
{
	DECLARE_DYNAMIC(AccountDialog)

public:
	AccountDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~AccountDialog();
	bool	iShowExitButton;

// Dialog Data
	enum { IDD = IDD_DIALOG_SETTINGS };

protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	HICON m_hIcon;
    CBrush* m_pEditBkBrush;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonCancelSettings();


};
