#ifndef __AUDIO_THREAD_ADDED__
#define __AUDIO_THREAD_ADDED__

#include "WThread.h"
#include <mmsystem.h>

class audio_player : public CWinThread
{
private:
	enum
	{
		PLAY_HEAD_COUNT = 4,
		PLAY_BUF_SIZE = 160000, // 10 second data
		PLAY_BLOCK_SIZE	= 3200	// 200 ms 16 bit pcm data
	};

	HANDLE		mutex;

	bool		is_stop_request;
	bool		is_16_bit;
	bool		is_in_thread;
	int			volume_percentage;

	unsigned char	play_buf[PLAY_BUF_SIZE];
	int			play_buf_length;
	int			play_buf_max_length;

	HWAVEOUT	h_wave_out;
	WAVEHDR		h_wave_header[PLAY_HEAD_COUNT];
	unsigned char	h_play_buf[PLAY_HEAD_COUNT][PLAY_BLOCK_SIZE];

private:
	bool			init_audio_player();
	void			start_audio_player();

	virtual BOOL	InitInstance();			// CWinThread override
	virtual int		ExitInstance();			// "
	virtual int		Run();					// "

public:
	audio_player();
	~audio_player();

	void			stop();
	int				write_or_read_play_buffer(bool b_write,unsigned char *c,int len);
	void			resize_audio_buffer(int max_len);
};

#endif