// AddContact.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "AddContact.h"


// AddContact dialog

IMPLEMENT_DYNAMIC(AddContact, CDialog)

AddContact::AddContact(CWnd* pParent /*=NULL*/)
	: CDialog(AddContact::IDD, pParent)
{

}

AddContact::~AddContact()
{
}

void AddContact::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(AddContact, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &AddContact::OnBnClickedButtonAdd)
END_MESSAGE_MAP()


// AddContact message handlers

BOOL AddContact::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( iEditMode )
	{
		this->SetWindowTextW(L"Edit contact");

		this->SetDlgItemTextW(IDC_EDIT_NAME,iContactName);
		this->SetDlgItemTextW(IDC_EDIT_PHONE_NO,iPhoneNo);
		
		this->SetDlgItemTextW(IDC_BUTTON_ADD,L"Save contact");
	}
	else
	{
		this->SetWindowTextW(L"New contact");

		this->SetDlgItemTextW(IDC_EDIT_NAME,L"");
		this->SetDlgItemTextW(IDC_EDIT_PHONE_NO,L"");
		
		this->SetDlgItemTextW(IDC_BUTTON_ADD,L"Add Contact");
	}

	return TRUE;
}

void AddContact::OnBnClickedButtonAdd()
{
	this->GetDlgItemTextW(IDC_EDIT_NAME,iContactName);
	this->GetDlgItemTextW(IDC_EDIT_PHONE_NO,iPhoneNo);

	iContactName.Trim();
	iPhoneNo.Trim();

	EndDialog(IDOK);
}
