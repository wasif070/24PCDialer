// SmoothDialerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "SmoothDialerDlg.h"
#include <mmdeviceapi.h>
#include <endpointvolume.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSmoothDialerDlg dialog




CSmoothDialerDlg::CSmoothDialerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSmoothDialerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	//------------------
	 CBitmap bmp;
    // Load a resource bitmap.
    bmp.LoadBitmap(IDB_HOMESCREEN);
    m_pEditBkBrush = new CBrush(&bmp);	  
	//m_pEditBkBrush = new CBrush(RGB(255,255,100));
	//---------------
//	iDialerTunnel = NULL;
	iAudioPlayer = NULL;
	iAudioRecorder = NULL;
}

void CSmoothDialerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NUM_1, button_one);
	DDX_Control(pDX, IDC_NUM_2, button_two);
	DDX_Control(pDX, IDC_NUM_3, button_three);
	DDX_Control(pDX, IDC_NUM_4, button_four);
	DDX_Control(pDX, IDC_NUM_5, button_five);
	DDX_Control(pDX, IDC_NUM_6, button_six);
	DDX_Control(pDX, IDC_NUM_7, button_seven);
	DDX_Control(pDX, IDC_NUM_8, button_eight);
	DDX_Control(pDX, IDC_NUM_9, button_nine);
	DDX_Control(pDX, IDC_NUM_STAR, button_star);
	DDX_Control(pDX, IDC_NUM_0, button_zero);
	DDX_Control(pDX, IDC_NUM_HASH, button_hash);
	DDX_Control(pDX, IDC_CALL, button_call);
	DDX_Control(pDX, IDC_END, button_end);
	DDX_Control(pDX, IDC_NUM_C, button_c);
	DDX_Control(pDX, IDC_SLIDER_LEFT, slider_left);
	DDX_Control(pDX, IDC_SLIDER_RIGHT, slider_right);
}

BEGIN_MESSAGE_MAP(CSmoothDialerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CALL, &CSmoothDialerDlg::OnBnClickedCall)
	ON_BN_CLICKED(IDC_NUM_C, &CSmoothDialerDlg::OnBnClickedNumC)
	ON_BN_CLICKED(IDC_END, &CSmoothDialerDlg::OnBnClickedEnd)
	ON_BN_CLICKED(IDC_NUM_1, &CSmoothDialerDlg::OnBnClickedNum1)
	ON_BN_CLICKED(IDC_NUM_2, &CSmoothDialerDlg::OnBnClickedNum2)
	ON_BN_CLICKED(IDC_NUM_3, &CSmoothDialerDlg::OnBnClickedNum3)
	ON_BN_CLICKED(IDC_NUM_4, &CSmoothDialerDlg::OnBnClickedNum4)
	ON_BN_CLICKED(IDC_NUM_5, &CSmoothDialerDlg::OnBnClickedNum5)
	ON_BN_CLICKED(IDC_NUM_6, &CSmoothDialerDlg::OnBnClickedNum6)
	ON_BN_CLICKED(IDC_NUM_7, &CSmoothDialerDlg::OnBnClickedNum7)
	ON_BN_CLICKED(IDC_NUM_8, &CSmoothDialerDlg::OnBnClickedNum8)
	ON_BN_CLICKED(IDC_NUM_9, &CSmoothDialerDlg::OnBnClickedNum9)
	ON_BN_CLICKED(IDC_NUM_STAR, &CSmoothDialerDlg::OnBnClickedNumStar)
	ON_BN_CLICKED(IDC_NUM_0, &CSmoothDialerDlg::OnBnClickedNum0)
	ON_BN_CLICKED(IDC_NUM_HASH, &CSmoothDialerDlg::OnBnClickedNumHash)
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_WM_SYSCHAR()
	ON_EN_CHANGE(IDC_PHONE_NO, &CSmoothDialerDlg::OnEnChangePhoneNo)
	ON_COMMAND(ID_MENU_IVR, &CSmoothDialerDlg::OnMenuIvr)
	ON_COMMAND(ID_MENU_CALLLOG, &CSmoothDialerDlg::OnMenuCalllog)
	ON_COMMAND(ID_MENU_CONTACTS, &CSmoothDialerDlg::OnMenuContacts)
	ON_COMMAND(ID_MENU_RECONNECT, &CSmoothDialerDlg::OnMenuReconnect)
	ON_COMMAND(ID_MENU_SETTINGS, &CSmoothDialerDlg::OnMenuSettings)
	ON_COMMAND(ID_MENU_ABOUTUS, &CSmoothDialerDlg::OnMenuAboutus)
	ON_WM_VSCROLL()
END_MESSAGE_MAP()


// CSmoothDialerDlg message handlers

BOOL CSmoothDialerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    button_one.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_1)));
	button_two.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_2)));
	button_three.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_3)));
	button_four.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_4)));
	button_five.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_5)));
	button_six.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_6)));
	button_seven.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_7)));
	button_eight.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_8)));
	button_nine.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_9)));
	button_zero.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_0)));
	button_c.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_C)));
	button_call.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_CALL)));
	button_end.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_END)));
	button_star.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_STAR)));
	button_hash.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_HASH)));

	if ( !MyUtil::LoadBaseConfig() )
	{
		AfxMessageBox(L"Error reading resource !!!\r\n\r\nExiting....");
		EndDialog(IDOK);
		return TRUE;
	}

	/*VERIFY(m_HomeScreenBitmap.LoadBitmapW(IDB_HOMESCREEN));
	BITMAP bmp;
	m_HomeScreenBitmap.GetBitmap(&bmp);
	iHomeScreenWidth = bmp.bmWidth;
	iHomeScreenHeight = bmp.bmHeight;

	m_HomeScreenDC.CreateCompatibleDC(GetDC());
	m_HomeScreenDC.SelectObject(&m_HomeScreenBitmap);*/

	iSmallFont.CreatePointFont(110,L"Cambria Bold");	
	iMediumFont.CreatePointFont(120,L"Cambria Bold");

	this->GetDlgItem(IDC_BRANDNAME)->SetFont(&iMediumFont,false);
	this->GetDlgItem(IDC_STATE)->SetFont(&iMediumFont,false);
	this->GetDlgItem(IDC_STATUS)->SetFont(&iSmallFont,false);
	this->GetDlgItem(IDC_BALANCE)->SetFont(&iSmallFont,false);
	this->GetDlgItem(IDC_DURATION)->SetFont(&iSmallFont,false);
	this->GetDlgItem(IDC_PHONE_NAME)->SetFont(&iSmallFont,false);
	this->GetDlgItem(IDC_FOOTER)->SetFont(&iSmallFont,false);

	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();

	SetDlgItemText(IDC_BRANDNAME,L"");
	SetDlgItemText(IDC_STATE,L"Initializing . . .");
	SetDlgItemText(IDC_STATUS,L"");
	SetDlgItemText(IDC_BALANCE,L"");
	SetDlgItemText(IDC_DURATION,L"");
	SetDlgItemText(IDC_PHONE_NAME,L"");
	SetDlgItemText(IDC_FOOTER,L"");

	iTunnelCommand[0] = 0;
	
	iAccountView.iShowExitButton = true;
	while ( MyUtil::iSipUsername.GetLength() <= 0 || ( MyUtil::iFixedPin == false && MyUtil::iOperatorCode <= 0 ) )
	{
		if ( iAccountView.DoModal() != IDOK )
		{
			EndDialog(IDOK);
			return FALSE;
		}
	}
	
	iAudioPlayer = new audio_player();
	iAudioRecorder = new audio_recorder(this->GetSafeHwnd());

	iRunningState = 0;
	iRunningStatePrev = 0;
	OnMenuReconnect();
	iPlayBufLength = 0;
	iRecordBufLength = 0;
	SetTimer(1001,TUNNEL_TIMER_MS,NULL);

	// TODO: Add extra initialization here

	return FALSE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSmoothDialerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
	//Sample 01: Required Declarations
     CDC MemDC ;
     CBitmap bmp ;
     CPaintDC dc(this);
                                  
     //Sample 02: Get the Client co-ordinates
     CRect rct;
     this->GetClientRect(&rct);

     //Sample 03: Create the Dialog Compatible DC in the memory,
     //Then load the bitmap to the memory.
     MemDC.CreateCompatibleDC ( &dc ) ;
	 bmp.LoadBitmap ( IDB_HOMESCREEN ) ;
     MemDC.SelectObject ( &bmp ) ;

     //Sample 04: Finally perform Bit Block Transfer (Blt) from memory dc to
     //dialog surface.
     dc.BitBlt ( 0, 0, rct.Width() , rct.Height() , &MemDC, 0, 0, SRCCOPY ) ;

	CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSmoothDialerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSmoothDialerDlg::OnClose()
{
	if ( iRunningState & RUNNING_STATE_SHOULD_CONFIRM_EXIT )
	{
		if ( AfxMessageBox(L"Do you want to exit?",MB_YESNOCANCEL) != IDYES )return;		
	}
	
	SendToTunnel(TUNNEL_COMMAND_EXIT);
}

BOOL CSmoothDialerDlg::OnCmdMsg(UINT nID,int nCode,void* pExtra,AFX_CMDHANDLERINFO* pHandlerInfo)
{
	int a = GetKeyState(13);					// enter key
	if(a == -127 || a == -128)return false;
		

	a = GetKeyState(27);						// escape key
	if(a == -127 || a == -128)return false;
	
	return CDialog::OnCmdMsg(nID,nCode,pExtra,pHandlerInfo);
}

void CSmoothDialerDlg::UpdateLabel(int item_id,const CString& str)
{	
	CString str_prev = L"";
	GetDlgItemText(item_id,str_prev);
	if ( str_prev.Compare(str) == 0 )return;
	
	SetDlgItemText(item_id,str);

	CRect rect;
	this->GetDlgItem(item_id)->GetWindowRect(&rect);
	this->ScreenToClient(&rect);
	this->InvalidateRect(rect,1);
}

void CSmoothDialerDlg::SendToTunnel(int cmd,const char* param1,const char* param2,const char* param3,const char* param4,const char* param5,const char* param6,const char* param7)
{
	snprintf(iTunnelCommand,sizeof(iTunnelCommand),"%u#%s#%s#%s#%s#%s#%s#%s",cmd,param1,param2,param3,param4,param5,param6,param7);
}

void CSmoothDialerDlg::OnTimer(UINT_PTR nIDEvent)
{
	int play_len = 0;
	int record_len = iAudioRecorder->read_or_write_record_buffer(false,iRecordBuf,sizeof(iRecordBuf));
	
	dialer_tunnel_tick(CurrentTimeMS(),iTunnelCommand,iRecordBuf,record_len,iPlayBuf,sizeof(iPlayBuf),play_len,iViewStr,sizeof(iViewStr));
	iTunnelCommand[0] = 0;

	if ( play_len > 0 )iAudioPlayer->write_or_read_play_buffer(true,iPlayBuf,play_len);

	CString str_items[12];
	
	int i = 0;
	for ( i = 0; i < 12; i++ )str_items[i] = L"";
	int cnt = 0;

	for ( i = 0; i < strlen(iViewStr) && i < sizeof(iViewStr); i++ )
	{
		if ( iViewStr[i] == '\r' )continue;
		if ( iViewStr[i] == '\n' )
		{
			cnt++;
			if ( cnt >= 12 )break;
			continue;
		}

		str_items[cnt] += (char)iViewStr[i];
	}

	iCallbackEvent = MyUtil::StrToInt(str_items[0]);
	iRunningState = MyUtil::StrToInt(str_items[1]);
	iCallbackEventParam = MyUtil::StrToInt(str_items[8]);
	iCallbackEventParam1 = str_items[9];
	
	if ( ( iRunningState & RUNNING_STATE_ACCEPT_INCOMING_CALL ) != ( iRunningStatePrev & RUNNING_STATE_ACCEPT_INCOMING_CALL ) )
	{
		if ( iRunningState & RUNNING_STATE_ACCEPT_INCOMING_CALL )this->SetDlgItemTextW(IDC_CALL,L"Accept");
		else this->SetDlgItemTextW(IDC_CALL,L"Call");
	}
	iRunningStatePrev = iRunningState;

	iAudioRecorder->is_audio_running = ( ( iRunningState & RUNNING_STATE_AUDIO ) != 0 );
	if ( iRunningState & RUNNING_STATE_EXITED )
	{
		if ( iAudioPlayer != NULL )delete iAudioPlayer;
		iAudioPlayer = NULL;

		if ( iAudioRecorder != NULL )delete iAudioRecorder;
		iAudioRecorder = NULL;

		EndDialog(IDOK);
		return;
	}

	if ( iCallbackEvent == CALLBACK_EVENT_CALL_COMPLETE || iCallbackEvent == CALLBACK_EVENT_INCOMING_CALL_COMPLETE )
	{
		CString phone_no = L"", phone_name = L"";
		GetDlgItemText(IDC_PHONE_NO,phone_no);
		GetDlgItemText(IDC_PHONE_NAME,phone_name);

		MyUtil::AddCallLog(false,phone_name,iCallbackEventParam1,iCallbackEventParam);
	}
	
	if ( iCallbackEvent == CALLBACK_EVENT_RESIZE_AUDIO_BUF )
	{
		iAudioPlayer->resize_audio_buffer(iCallbackEventParam);
		iAudioRecorder->resize_audio_buffer(iCallbackEventParam);
	}

	CString str_state = str_items[3];
	if ( str_state.GetLength() > 0 && str_state.GetAt(str_state.GetLength()-1) == '#' )
	{
		int dot_count = ( CurrentTimeMS() / 300 ) % 5;
		str_state = L"";
		for ( int i = 0; i < dot_count; i++ )str_state += L"  ";
		str_state += str_items[3].Mid(0,str_items[3].GetLength()-1);
		for ( int i = 0; i < dot_count; i++ )str_state += L" .";
	}
	else if ( str_state.GetLength() > 0 && str_state.GetAt(0) == '#' )
	{
		int dot_count = ( CurrentTimeMS() / 300 ) % 5;
		str_state = L"";
		for ( int i = 0; i < dot_count; i++ )str_state += L"< ";
		str_state += str_items[3].Mid(1);
		for ( int i = 0; i < dot_count; i++ )str_state += "   ";
	}

	UpdateLabel(IDC_BRANDNAME,str_items[2]);
	UpdateLabel(IDC_STATE,str_state);
	UpdateLabel(IDC_STATUS,str_items[4]);
	UpdateLabel(IDC_BALANCE,str_items[5]);
	UpdateLabel(IDC_DURATION,str_items[6]);
	UpdateLabel(IDC_FOOTER,str_items[7]);

	CDialog::OnTimer(nIDEvent);
}

/*BOOL CSmoothDialerDlg::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	//delete m_pEditBkBrush;
	pDC->StretchBlt(0,0,rect.Width(),rect.Height(),&m_HomeScreenDC,0,0,iHomeScreenWidth,iHomeScreenHeight,SRCCOPY);
	return TRUE;
}*/
void CSmoothDialerDlg::OnDestroy()
{
	CRect rect;
	GetClientRect(&rect);
	CDialog::OnDestroy();
    delete m_pEditBkBrush;
}

HBRUSH CSmoothDialerDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{   	
	if ( pWnd->m_hWnd != this->GetDlgItem(IDC_PHONE_NO)->m_hWnd)
	{		
		if ( nCtlColor == CTLCOLOR_STATIC )
		{
			if ( pWnd->GetSafeHwnd() == this->GetDlgItem(IDC_BRANDNAME)->GetSafeHwnd() )pDC->SetTextColor(COLOR_BRAND_NAME);
			else if ( pWnd->GetSafeHwnd() == this->GetDlgItem(IDC_STATE)->GetSafeHwnd() )pDC->SetTextColor(COLOR_STATE);
			else if ( pWnd->GetSafeHwnd() == this->GetDlgItem(IDC_STATUS)->GetSafeHwnd() )pDC->SetTextColor(COLOR_STATUS);
			else if ( pWnd->GetSafeHwnd() == this->GetDlgItem(IDC_BALANCE)->GetSafeHwnd() )pDC->SetTextColor(COLOR_BALANCE);
			else if ( pWnd->GetSafeHwnd() == this->GetDlgItem(IDC_DURATION)->GetSafeHwnd() )pDC->SetTextColor(COLOR_DURATION);
			else if ( pWnd->GetSafeHwnd() == this->GetDlgItem(IDC_FOOTER)->GetSafeHwnd() )pDC->SetTextColor(COLOR_FOOTER);
		}
		pDC->SetBkMode(TRANSPARENT);
		//return (HBRUSH)GetStockObject(NULL_BRUSH);
		return (HBRUSH)(m_pEditBkBrush->GetSafeHandle());
	}
	
	pDC->SetBkMode(TRANSPARENT);
	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

    
}

void CSmoothDialerDlg::OnEnChangePhoneNo()
{
	CString str = L"";
	GetDlgItemText(IDC_PHONE_NO,str);
	
	UpdateLabel(IDC_PHONE_NAME,MyUtil::PhoneNoToName(str));
}

void CSmoothDialerDlg::OnBnClickedCall()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	
	if ( iRunningState & RUNNING_STATE_ACCEPT_INCOMING_CALL )
	{
		SendToTunnel(TUNNEL_COMMAND_CALL,"");
		return;
	}

	CString dest_no = L"", dest_name = L"";
	GetDlgItemText(IDC_PHONE_NO,dest_no);
	GetDlgItemText(IDC_PHONE_NAME,dest_name);

	dest_no.Trim();
	dest_name.Trim();
	
	if ( dest_no.GetLength() <= 0 )
	{
		iCallLog.iShowCallLog = false;
		if ( iCallLog.DoModal() != IDOK )return;
		SetDlgItemText(IDC_PHONE_NO,iCallLog.iSelectedNumber);
		return;
	}
	
	char dest_no_str[64];
	MyUtil::StrToCharArray(dest_no,dest_no_str,sizeof(dest_no_str));
	MyUtil::AddCallLog(true,dest_name,dest_no,0);
	
	SendToTunnel(TUNNEL_COMMAND_CALL,dest_no_str);
}

void CSmoothDialerDlg::OnBnClickedNumC()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,8,1);
}

void CSmoothDialerDlg::OnBnClickedEnd()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	SendToTunnel(TUNNEL_COMMAND_END_CALL);
}

void CSmoothDialerDlg::OnBnClickedNum1()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'1',1);	
}

void CSmoothDialerDlg::OnBnClickedNum2()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'2',1);
}

void CSmoothDialerDlg::OnBnClickedNum3()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'3',1);
}


void CSmoothDialerDlg::OnBnClickedNum4()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'4',1);
}

void CSmoothDialerDlg::OnBnClickedNum5()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'5',1);
}

void CSmoothDialerDlg::OnBnClickedNum6()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'6',1);
}


void CSmoothDialerDlg::OnBnClickedNum7()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'7',1);
}

void CSmoothDialerDlg::OnBnClickedNum8()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'8',1);
}

void CSmoothDialerDlg::OnBnClickedNum9()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'9',1);
}


void CSmoothDialerDlg::OnBnClickedNumStar()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'*',1);
}

void CSmoothDialerDlg::OnBnClickedNum0()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_KEYUP,'0',1);
}

void CSmoothDialerDlg::OnBnClickedNumHash()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	this->GetDlgItem(IDC_PHONE_NO)->PostMessageW(WM_CHAR,'#',1 );
}


void CSmoothDialerDlg::OnMenuIvr()
{
    this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	SetDlgItemText(IDC_PHONE_NAME,L"");
	SendToTunnel(TUNNEL_COMMAND_CALL_IVR);
}


void CSmoothDialerDlg::OnMenuCalllog()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	if ( iCallLog.DoModal() != IDOK )return;

	SetDlgItemText(IDC_PHONE_NO,iCallLog.iSelectedNumber);
}


void CSmoothDialerDlg::OnMenuContacts()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	if ( iContactView.DoModal() != IDOK )return;

	if ( iContactView.iSelectedContact.GetLength() > 0 )SetDlgItemText(IDC_PHONE_NO,iContactView.iSelectedContact);
}


void CSmoothDialerDlg::OnMenuReconnect()
{
	this->GetDlgItem(IDC_PHONE_NO)->SetFocus();

	iPlayBufLength = 0;
	iRecordBufLength = 0;

	char auth_url[128], auth_url_optional[128],usr[64], pwd[64], caller_id[64], tunnel_timer_ms[32],operator_code_str[32];
	int operator_code = MyUtil::iBasePin;

	int tmp = MyUtil::iOperatorCode;
	while(tmp > 0)
	{
		operator_code = operator_code * 10;
		tmp /= 10;
	}
	operator_code += MyUtil::iOperatorCode;

	MyUtil::StrToCharArray(MyUtil::iAuthUrl,auth_url,sizeof(auth_url));
	MyUtil::StrToCharArray(MyUtil::iAuthUrlOptional,auth_url_optional,sizeof(auth_url_optional));
	MyUtil::StrToCharArray(MyUtil::iSipUsername,usr,sizeof(usr));
	MyUtil::StrToCharArray(MyUtil::iSipPassword,pwd,sizeof(pwd));
	MyUtil::StrToCharArray(MyUtil::iSipCallerID,caller_id,sizeof(caller_id));
	
	snprintf(tunnel_timer_ms,sizeof(tunnel_timer_ms),"%u",TUNNEL_TIMER_MS);
	snprintf(operator_code_str,sizeof(operator_code_str),"%u",operator_code);

	SendToTunnel(TUNNEL_COMMAND_RECONNECT,auth_url,auth_url_optional,operator_code_str,usr,pwd,caller_id,tunnel_timer_ms);
}


void CSmoothDialerDlg::OnMenuSettings()
{
    this->GetDlgItem(IDC_PHONE_NO)->SetFocus();
	
	iAccountView.iShowExitButton = false;
	if ( iAccountView.DoModal() != IDOK )return;

	OnMenuReconnect();
}


void CSmoothDialerDlg::OnMenuAboutus()
{
	MessageBox(_T("24PCDialer Version 5.2.0.5 \n\u00A9 IPvision Canada Inc.\nAll rights reserved."), _T("About Us"), MB_ICONINFORMATION | MB_OK);
}


bool IsWindowsVistaOrHigher() {
   OSVERSIONINFO osvi;
   ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
   osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
   GetVersionEx(&osvi);
   return osvi.dwMajorVersion >= 6;
}


bool ChangeVolume(double nVolume,bool bScalar)
{
 
    HRESULT hr=NULL;
    bool decibels = false;
    bool scalar = false;
    double newVolume=nVolume;
 
    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER, 
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;
 
    hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);

	if(hr==S_OK){
		deviceEnumerator->Release();
		deviceEnumerator = NULL;
 
		IAudioEndpointVolume *endpointVolume = NULL;
		hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume), 
			CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
		defaultDevice->Release();
		defaultDevice = NULL;
		if (bScalar==false)
		{
			hr = endpointVolume->SetMasterVolumeLevel((float)newVolume, NULL);
		}
		else if (bScalar==true)
		{
			hr = endpointVolume->SetMasterVolumeLevelScalar((float)newVolume, NULL);
		}
		endpointVolume->Release();
 
		CoUninitialize();
	}
    return FALSE;
}

bool ChangeMicVolume(double nVolume,bool bScalar)
{
 
    HRESULT hr=NULL;
    bool decibels = false;
    bool scalar = false;
    double newVolume=nVolume;
 
    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;
    hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER, 
                          __uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
    IMMDevice *defaultDevice = NULL;
 
    hr = deviceEnumerator->GetDefaultAudioEndpoint(eCapture, eConsole, &defaultDevice);
	
	if(hr==S_OK){
		deviceEnumerator->Release();
		deviceEnumerator = NULL;
 
		IAudioEndpointVolume *endpointVolume = NULL;
		hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume), 
         CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
		defaultDevice->Release();
		defaultDevice = NULL;
		if (bScalar==false)
		{
			hr = endpointVolume->SetMasterVolumeLevel((float)newVolume, NULL);
		}
		else if (bScalar==true)
		{
			hr = endpointVolume->SetMasterVolumeLevelScalar((float)newVolume, NULL);
		}
		endpointVolume->Release();
 
		CoUninitialize();
	}
    return FALSE;
}





void CSmoothDialerDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	float mic =  slider_left.GetPos() - 100;
	float vol = slider_right.GetPos() - 100;
	if(mic < 0) mic = mic * -1;
	if(vol < 0) vol = vol * -1;
	if(IsWindowsVistaOrHigher()){
		ChangeVolume(vol/100, true);
		ChangeMicVolume(mic/100, true);
	}
	__super::OnVScroll(nSBCode, nPos, pScrollBar);
}
