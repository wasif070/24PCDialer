/*
* Copyright (C) 2009 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/
//#include "pch.h"
#include <stdlib.h>

#include "g729a_encoder.h"
#include "g729a.h"

#define  L_FRAME      80
#define  SERIAL_SIZE  (80+2)

/* This is a trivial JNI example where we use a native method
* to return a new VM String. See the corresponding Java source
* file located at:
*
*   apps/samples/hello-jni/project/src/com/example/HelloJni/HelloJni.java
*/
//jstring
//Java_com_example_hellojni_HelloJni_stringFromJNI( JNIEnv* env,
//                                                  jobject thiz )

void TestG729(char *c)
{
	int size = g729a_enc_mem_size();
	int size1 = g729a_dec_mem_size();  

	void *hEncoder = calloc(1, size * sizeof(UWord8));
	void *hDecoder = calloc(1, size1 * sizeof(UWord8));

	Word16 speech[L_FRAME];
	unsigned char c_g729[SERIAL_SIZE];
	int i = 0;

	if (hEncoder == NULL)
	{
		return;
	}

	if (hDecoder == NULL)
	{
		return;
	}


	g729a_enc_init(hEncoder);
	if ( hEncoder == 0 )
	{
		return;
	}

	g729a_dec_init(hDecoder);
	if ( hDecoder == 0 )
	{	   
		return;
	}

	for ( i = 0; i < L_FRAME; i++ )
		speech[i] = i + 1;

	g729a_enc_process(hEncoder, speech, c_g729);      		
    g729a_dec_process(hDecoder, c_g729, speech, 0);

	g729a_enc_deinit(hEncoder);
	free(hEncoder);

	g729a_dec_deinit(hDecoder);
	free(hDecoder);
}

void main()
{
	TestG729("dssafasfd");
}
