#include "stdafx.h"
#include "audio_player.h"

audio_player::audio_player()
{
	is_16_bit = true;
	is_in_thread = false;
	volume_percentage = 100;

	play_buf_max_length = sizeof(play_buf);

	mutex = CreateMutex(NULL,FALSE,NULL);

	CreateThread(CREATE_SUSPENDED,100000);
	ResumeThread();
}

audio_player::~audio_player()
{
	stop();
}

BOOL audio_player::InitInstance()
{
	return TRUE;
}

int audio_player::ExitInstance()
{
	return 1;
}

int audio_player::Run()
{
	start_audio_player();
	return 1;
}

void audio_player::resize_audio_buffer(int max_len)
{
	max_len = max_len * 16;

	play_buf_length = 0;
	if ( max_len > sizeof(play_buf))max_len = sizeof(play_buf);
	play_buf_max_length = max_len;
}

int audio_player::write_or_read_play_buffer(bool b_write,unsigned char *c,int len) // for read, len = max length
{
	if ( !is_in_thread )return 0;

	if ( is_16_bit == true && ( len % 160 ) != 0 )len -= ( len % 160 );
	if ( is_16_bit == false && ( len % 80 ) != 0 )len -= ( len % 80 );

	if ( len <= 0 )return 0;

	if ( b_write == false && play_buf_length <= 0 )return 0;

	if ( b_write == false )
	{
		if ( mutex != NULL )WaitForSingleObject(mutex,10);

		if ( len > play_buf_length )len = play_buf_length;
		memcpy(c,play_buf,len);

		if ( len < play_buf_length )memmove(play_buf,play_buf + len,play_buf_length - len);
		play_buf_length -= len;

		if ( mutex != NULL )ReleaseMutex(mutex);
		return len;
	}

	if ( mutex != NULL )WaitForSingleObject(mutex,10);

	if ( len > play_buf_max_length )len = play_buf_max_length;
	if ( ( play_buf_length + len ) > play_buf_max_length )
	{
		int delete_len = play_buf_length + len - play_buf_max_length;

		if ( is_16_bit == true && ( delete_len % 160 ) != 0 )delete_len += ( 160 - ( delete_len % 160 ) );
		if ( is_16_bit == false && ( delete_len % 80 ) != 0 )delete_len += ( 80 - ( delete_len % 80 ) );

		if ( delete_len > 0 && delete_len < sizeof(play_buf))memmove(play_buf,play_buf + delete_len,play_buf_length - delete_len);
		play_buf_length -= delete_len;
		if ( play_buf_length < 0 )play_buf_length = 0;
	}

	memcpy(play_buf + play_buf_length,c,len);
	play_buf_length += len;
	
	if ( mutex != NULL )ReleaseMutex(mutex);
	return len;
}

void audio_player::stop()
{
	if ( !is_in_thread )return;
	is_stop_request = true;

	int stop_countdown = 10;
	while( is_stop_request == true && ( stop_countdown-- ) > 0 )Sleep(50);	
}

void audio_player::start_audio_player()
{
	if ( !init_audio_player() )return;
	
	is_stop_request = false;
	is_in_thread = true;

	play_buf_length = 0;
	unsigned char temp_play_buf[PLAY_BLOCK_SIZE];

	int loop_count = 0;

	while(!is_stop_request)
	{
		if ( play_buf_length <= 0 )
		{
			Sleep(50);
			continue;
		}

		if ( ( ( loop_count++ ) % PLAY_HEAD_COUNT ) == 0 )Sleep(50);
		int sel = -1;

		for ( int i = 0; i < PLAY_HEAD_COUNT; i++ )
		{
			MMRESULT res = waveOutUnprepareHeader( h_wave_out, &h_wave_header[i], sizeof(WAVEHDR));
			if ( res == WAVERR_STILLPLAYING )continue;
			sel = i;
			break;
		}

		if ( sel < 0 )continue;

		int len = write_or_read_play_buffer(false,temp_play_buf,PLAY_BLOCK_SIZE);
		if ( len <= 0 )continue;

		memcpy(h_wave_header[sel].lpData,temp_play_buf,len);
		h_wave_header[sel].dwBufferLength = len;

		h_wave_header[sel].dwLoops = 1;
		h_wave_header[sel].lpNext = 0;
		h_wave_header[sel].dwFlags = 0;
		h_wave_header[sel].dwUser = 0;
		
		waveOutPrepareHeader(h_wave_out, &h_wave_header[sel], sizeof(WAVEHDR));
		waveOutWrite(h_wave_out, &h_wave_header[sel], sizeof(WAVEHDR));
	}

	waveOutReset(h_wave_out);
	waveOutClose(h_wave_out);

	is_stop_request = false;	
	AfxEndThread(1,FALSE);
	is_in_thread = false;
}

bool audio_player::init_audio_player()
{
	WAVEFORMATEX wfx;
	MMRESULT     rc;

	memset(&wfx, 0, sizeof(wfx));

	wfx.wFormatTag = WAVE_FORMAT_PCM;
	wfx.nChannels = 1;
	wfx.nSamplesPerSec = 8000;
	wfx.wBitsPerSample = 16;
	wfx.nBlockAlign = wfx.nChannels * wfx.wBitsPerSample / 8;
	wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;
	wfx.cbSize = 0;

	int res = waveOutOpen( &h_wave_out, WAVE_MAPPER, &wfx, 0, (DWORD)GetModuleHandle(NULL),CALLBACK_NULL);
	if( res != MMSYSERR_NOERROR )return false;

	ULONG vol = 0;
	
	rc = waveOutGetVolume(h_wave_out,&vol);
	rc = waveOutSetVolume(h_wave_out,0xFFFFFFFF); 

	for (int i = 0; i < PLAY_HEAD_COUNT; i++)
    {
		memset( &(h_wave_header[i]), 0, sizeof(h_wave_header[i]));
		
		h_wave_header[i].lpData = (char *)h_play_buf[i]; // + ( i * PLAY_BLOCK_SIZE ));
		h_wave_header[i].dwBufferLength = PLAY_BLOCK_SIZE;
		h_wave_header[i].dwFlags = 0;	
		h_wave_header[i].dwUser = i;

		rc = waveOutPrepareHeader( h_wave_out, &(h_wave_header[i]),sizeof(h_wave_header[i]));
		if (rc != MMSYSERR_NOERROR)return false;
    }

	return true;
}

