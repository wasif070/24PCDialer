//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SmoothDialer.rc
//
#define IDD_SMOOTHDIALER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     133
#define IDB_HOMESCREEN                  133
#define IDD_DIALOG_SETTINGS             134
#define IDR_CONFIG                      136
#define IDD_DIALOG_CONTACTS             137
#define IDD_DIALOG_ADD_CONTACT          138
#define IDD_DIALOG_CALL_LOG             139
#define IDR_MENU1                       140
#define IDB_BITMAP_VOL                  142
#define IDB_BITMAP_MIC                  143
#define IDB_BITMAP2                     144
#define IDB_BITMAP_1                    144
#define IDB_BITMAP_2                    145
#define IDB_BITMAP_3                    146
#define IDB_BITMAP_4                    147
#define IDB_BITMAP_5                    148
#define IDB_BITMAP_6                    149
#define IDB_BITMAP_7                    150
#define IDB_BITMAP_8                    151
#define IDB_BITMAP_9                    152
#define IDB_BITMAP_0                    153
#define IDB_BITMAP_CALL                 154
#define IDB_BITMAP_END                  155
#define IDB_BITMAP_STAR                 156
#define IDB_BITMAP_HASH                 157
#define IDB_BITMAP_C                    159
#define IDB_BITMAP3                     160
#define IDB_BITMAP_LOGIN                160
#define IDB_BITMAP_BRAND                161
#define IDC_STATIC_BRANDNAME            1000
#define IDC_BRANDNAME                   1000
#define IDC_CALL                        1001
#define IDC_PHONE_NO                    1002
#define IDC_END                         1003
#define IDC_RECONNECT                   1004
#define IDC_CALL_LOG                    1005
#define IDC_NUM_1                       1006
#define IDC_NUM_2                       1007
#define IDC_NUM_3                       1008
#define IDC_NUM_4                       1009
#define IDC_NUM_5                       1010
#define IDC_NUM_6                       1011
#define IDC_NUM_7                       1012
#define IDC_NUM_8                       1013
#define IDC_NUM_9                       1014
#define IDC_NUM_0                       1015
#define IDC_NUM_STAR                    1016
#define IDC_NUM_HASH                    1017
#define IDC_SETTINGS                    1018
#define IDC_CONTACTS                    1019
#define IDC_STATE                       1020
#define IDC_STATUS                      1021
#define IDC_FOOTER                      1022
#define IDC_BALANCE                     1023
#define IDC_DURATION                    1024
#define IDC_NUM_C                       1025
#define IDC_BUTTON_IVR                  1026
#define IDC_EDIT_OPERATORCODE           1027
#define IDC_EDIT_SIP_USERNAME           1028
#define IDC_EDIT_SIP_PASSWORD           1029
#define IDC_EDIT4                       1030
#define IDC_EDIT_SIP_CALLERID           1030
#define IDC_BUTTON_SAVE                 1032
#define IDC_STATIC_OPERATORCODE         1034
#define IDC_LIST_CONTACTS               1036
#define IDC_BUTTON_DELETE_CONTACT       1037
#define IDC_BUTTON_NEW_CONTACT          1038
#define IDC_BUTTON_CALL                 1039
#define IDC_EDIT_NAME                   1040
#define IDC_EDIT_PHONE_NO               1041
#define IDC_BUTTON_ADD                  1042
#define IDC_BUTTON_EDIT_CONTACT         1043
#define IDC_TAB_CALL_LOG                1045
#define IDC_LIST_CALL_LOG               1046
#define IDC_BUTTON_CALL_SELECTED        1047
#define IDC_BUTTON2                     1048
#define IDC_BUTTON_ADD_TO_CONTACT       1048
#define IDC_PHONE_NAME                  1049
#define IDC_BUTTON_CANCEL_SETTINGS      1050
#define IDC_SLIDER_RIGHT                1052
#define IDC_SLIDER_LEFT                 1053
#define IDC_PIC_VOL                     1054
#define IDC_BRAND_IMAGE                 1057
#define ID_MENU_IVR                     32771
#define ID_MENU_CALLLOG                 32772
#define ID_MENU_CONTACTS                32773
#define ID_MENU_ABOUTUS                 32774
#define ID_SMS_SENDSMS                  32775
#define ID_SMS_SUCCESSLIST              32776
#define ID_MENU_SETTINGS                32777
#define ID_MENU_ABOUTUS32778            32778
#define ID_MENU_RECONNECT               32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        162
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1058
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
