#pragma once

#include "MyUtil.h"
#include "AddContact.h"
#include "afxcmn.h"

// CCallLog dialog

class CCallLog : public CDialog
{
	DECLARE_DYNAMIC(CCallLog)

public:
	bool iShowCallLog;
	CString iSelectedNumber;

	AddContact	iAddContact;

	CCallLog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCallLog();

// Dialog Data
	enum { IDD = IDD_DIALOG_CALL_LOG };

protected:
	void LoadCallLog(bool b_dial_log);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CTabCtrl iCallTab;
	CListCtrl iCallList;
	afx_msg void OnTcnSelchangeTabCallLog(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonAddToContact();
	afx_msg void OnBnClickedButtonCallSelected();
	afx_msg void OnNMDblclkListCallLog(NMHDR *pNMHDR, LRESULT *pResult);
};
