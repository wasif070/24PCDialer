// This is the main DLL file.

#include "platform_def.h"

#ifdef __UNIX_BUILD__
#include "../g729/include/g729a_encoder.h"
#include "../g729/include/g729a.h"
#else
#include "g729a_encoder.h"
#include "g729a.h"
#endif

#include "dialer_agent.h"

#ifdef __JNI_TUNNEL__
#include <jni.h>
#endif

dialer_agent*	i_dialer_agent = NULL;

void*	i_g729_encoder = NULL;
void*	i_g729_decoder = NULL;

uchar	temp_record_buf[2000];
uchar	temp_play_buf[2000];

bool	is_configured = false;
bool	is_destroyed = false;

bool configure_tunnel()
{
	if ( is_configured )return true;

	is_configured = true;
	if ( i_dialer_agent == NULL )i_dialer_agent = new dialer_agent();

	if ( i_g729_encoder == NULL )
	{
		i_g729_encoder = calloc(1, g729a_enc_mem_size() * sizeof(UWord8));
		g729a_enc_init(i_g729_encoder);
	}

	if ( i_g729_decoder == NULL )
	{
		i_g729_decoder = calloc(1, g729a_dec_mem_size() * sizeof(UWord8));
		g729a_dec_init(i_g729_decoder);
	}

	return true;
}

void destroy_tunnel()
{
	is_destroyed = true;

	if ( i_dialer_agent != NULL )delete i_dialer_agent;
	i_dialer_agent = NULL;

	if ( i_g729_encoder != NULL )g729a_enc_deinit(i_g729_encoder);
	i_g729_encoder = NULL;

	if ( i_g729_decoder != NULL )g729a_dec_deinit(i_g729_decoder);
	i_g729_decoder = NULL;
}

void dialer_tunnel_tick(uint current_time_ms,const char* tunnel_command,const uchar* record_buf,int record_len,uchar* play_buf,int play_buf_size,int& play_len,char* view_str,int view_str_size)
{
	if ( is_destroyed )return;
	if ( is_configured ==  false && configure_tunnel() == false )return;
	i_dialer_agent->handle_tunnel_command(current_time_ms,tunnel_command);

	//----------------- Convert pcm record buf to g729 ----------------------

	int g729_record_len = 0;
	for ( int i = 0; i < record_len && g729_record_len < (sizeof(temp_record_buf) - 10); i += 160 )
	{
		for ( int j = 0; j < 10; j++ )temp_record_buf[g729_record_len + j] = 0;
		g729a_enc_process(i_g729_encoder,(short *)(record_buf + i),temp_record_buf + g729_record_len);
		g729_record_len += 10;
	}

	//-----------------------------------------------------------------------

	play_buf_size /= 16;
	if ( play_buf_size > sizeof(temp_play_buf) )play_buf_size = sizeof(temp_play_buf);

	play_len = i_dialer_agent->on_timer(current_time_ms,temp_play_buf,play_buf_size,temp_record_buf,g729_record_len);

	//----------------- Convert g729 play buf to PCM ------------------------

	for ( int i = 0; i < play_len; i += 10 )g729a_dec_process(i_g729_decoder,temp_play_buf + i,(short *)( play_buf + i * 16 ),0);
	play_len *= 16;

	if ( i_dialer_agent->get_display_items(view_str,view_str_size) )destroy_tunnel();
}


#ifdef __JNI_TUNNEL__
extern "C" jint Java_wali_SmoothDialer_SmoothDialer_DialerTunnelTick( JNIEnv* env, jobject thiz, jint current_time_ms,jbyteArray tunnel_command,jbyteArray record_buf,jint record_len,jbyteArray play_buf,jint play_buf_size,jbyteArray view_str,jint view_str_size)
{
    char* tunnel_command1 = (char*)env->GetByteArrayElements(tunnel_command, NULL);
	unsigned char* record_buf1 = (unsigned char*)env->GetByteArrayElements(record_buf, NULL);
	unsigned char* play_buf1 = (unsigned char*)env->GetByteArrayElements(play_buf, NULL);
	char* view_str1 = (char*)env->GetByteArrayElements(view_str, NULL);

    int play_len = 0;
    dialer_tunnel_tick(current_time_ms,tunnel_command1,record_buf1,record_len,play_buf1,play_buf_size,play_len,view_str1,view_str_size);

	env->ReleaseByteArrayElements(tunnel_command, (jbyte *)tunnel_command1, 0);
	env->ReleaseByteArrayElements(record_buf, (jbyte *)record_buf1, 0);
	env->ReleaseByteArrayElements(play_buf, (jbyte *)play_buf1, 0);
	env->ReleaseByteArrayElements(view_str, (jbyte *)view_str1, 0);

	return play_len;
}

#endif