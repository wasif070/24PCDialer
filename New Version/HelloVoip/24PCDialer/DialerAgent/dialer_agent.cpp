#include "dialer_agent.h"

dialer_agent::dialer_agent()
{
	poller = new network_poller();
	poller->set_listener(this);

	reset_state();
	poller->start();
}

dialer_agent::~dialer_agent()
{
	if ( poller != NULL )delete poller;
	poller = NULL;
}

void dialer_agent::reset(const char* _auth_url,const char* _auth_url_optional,int _operator_code,const char* _sip_username,const char* _sip_password,const char* _sip_caller_id,const char* _device_imei,uint _current_time_ms,uint _network_pulse_ms)
{
	current_time_ms = _current_time_ms;
	network_pulse_ms = _network_pulse_ms;

	if ( dialer_state < LOCAL_STATE_MINIMUM )
	{
		set_current_state(STATE_UNREGISTERED,STATUS_NONE);
		send_dialer_image();
	}

	reset_state();

	safe_copy((uchar *)auth_url,0,sizeof(auth_url),(uchar *)_auth_url,strlen(_auth_url));
	safe_copy((uchar *)auth_url_optional,0,sizeof(auth_url_optional),(uchar *)_auth_url_optional,strlen(_auth_url_optional));

	if ( strlen((const char*)auth_url) > 0 && ( auth_url[strlen((const char*)auth_url)-1] == '/' || auth_url[strlen((const char*)auth_url)-1] == '\\' ) )auth_url[strlen((const char*)auth_url)-1] = 0;
	if ( strlen((const char*)auth_url_optional) > 0 && ( auth_url_optional[strlen((const char*)auth_url_optional)-1] == '/' || auth_url_optional[strlen((const char*)auth_url_optional)-1] == '\\' ) )auth_url_optional[strlen((const char*)auth_url_optional)-1] = 0;

	auth_operator_code = _operator_code;

	safe_copy(sip_username,0,sizeof(sip_username),(const uchar *)_sip_username,strlen(_sip_username));
	safe_copy(sip_password,0,sizeof(sip_password),(const uchar *)_sip_password,strlen(_sip_password));
	safe_copy(sip_caller_id,0,sizeof(sip_remote_phone),(const uchar *)_sip_caller_id,strlen(_sip_caller_id));
	safe_copy(auth_device_imei,0,sizeof(auth_device_imei),(const uchar *)_device_imei,strlen(_device_imei));

	poller->stop_and_reset_all();

	if ( auth_operator_code <= 0 )
	{
		set_current_state(LOCAL_STATE_UNINITIALIZED,STATUS_EMPTY_OPERATORCODE);
		return;
	}

	set_current_state(LOCAL_STATE_AUTHENTICATING,STATUS_NONE);

	auth_retry_no = 0;
	authenticate();
}

void dialer_agent::authenticate()
{
	char str_url[256];
	str_url[0] = 0;

	if ( ( auth_retry_no % 2 ) == 0 && strlen((const char*)auth_url) > 0 )snprintf((char *)str_url,sizeof(str_url),"%s/dialersettings.php?username=",auth_url);
	else if ( ( auth_retry_no % 2 ) != 0 && strlen((const char*)auth_url_optional) > 0 )snprintf((char *)str_url,sizeof(str_url),"%s/dialersettings.php?username=",auth_url_optional);
	else if ( strlen((const char*)auth_url_optional) > 0 )snprintf((char *)str_url,sizeof(str_url),"%s/dialersettings.php?username=",auth_url_optional);
	else snprintf((char *)str_url,sizeof(str_url),"%s/dialersettings.php?username=",auth_url);

	int len = strlen(str_url);
	snprintf(str_url + len,sizeof(str_url) - len,"%d&usr=%s&platform=%s",auth_operator_code,sip_username,__PLATFORM_NAME__);

	auth_reply_length = 0;
	auth_reply[0] = 0;

	auth_retry_no++;
	if ( auth_retry_no >= AUTH_RETRY_MAX )
	{
		set_current_state(LOCAL_STATE_UNINITIALIZED,STATUS_INTERNET_ERROR_LOCAL);
		return;
	}

	if ( !poller->fetch_url(SOCKET_AUTH,str_url) )authenticate();
}

int	dialer_agent::on_timer(uint _current_time_ms,uchar* play_buf,int play_buf_max_size,const uchar* record_buf,int record_len) // returns rtp play buf len
{
	current_time_ms = _current_time_ms;
	if ( tunnel_exit_time_ms > 0 && current_time_ms > tunnel_exit_time_ms )
	{
		set_current_state(LOCAL_STATE_EXITED);
		return 0;
	}

	if ( dialer_state == STATE_NONE || dialer_state == STATE_UNREGISTERED || dialer_state == LOCAL_STATE_EXITED || dialer_state == LOCAL_STATE_UNINITIALIZED )return 0;

	for ( int i = 0; i < 5; i++ )if ( poller->on_timer(current_time_ms) == 0 )break;

	if ( dialer_state == LOCAL_STATE_CONNECTING_VPN )
	{
		if ( current_time_ms < vpn_connect_timeout_ms )return 0;
		set_current_state(LOCAL_STATE_UNINITIALIZED,STATUS_VPN_NOT_FOUND);
		return 0;
	}

	if ( dialer_state < LOCAL_STATE_MINIMUM )
	{
		if ( vpn_alt_channel_state == ALT_CHANNEL_BEING_TESTED && current_time_ms >= vpn_connect_timeout_ms_alt )vpn_alt_channel_state = ALT_CHANNEL_NOT_CONNECTED;
		if ( ( current_time_ms/1000 - vpn_send_time ) >= vpn_ping_time )send_dialer_image();
	}

	if ( dialer_state != STATE_RINGING && dialer_state != STATE_CONNECTED && dialer_state != STATE_INCOMING_CONNECTED )return 0;
	if ( record_len > 0 )
	{
		if ( record_len % 10 )record_len -= ( record_len % 10 );
		if ( record_len > sizeof(rtp_record_buf) )record_len = sizeof(rtp_record_buf);

		if ( ( rtp_record_len + record_len ) > sizeof(rtp_record_buf) )
		{
			int delete_len = rtp_record_len + record_len - sizeof(rtp_record_buf);
			memmove(rtp_record_buf,rtp_record_buf + delete_len,rtp_record_len - delete_len);
			rtp_record_len -= delete_len;
		}

		memcpy(rtp_record_buf + rtp_record_len,record_buf,record_len);
		rtp_record_len += record_len;

		if ( ( current_time_ms - rtp_last_send_time_ms ) >= rtp_bundle_send_size || rtp_record_len >= rtp_bundle_send_size )send_rtp_to_vpn();
	}

	if ( rtp_play_len > 0 && play_buf != NULL && play_buf_max_size > 0 )
	{
		int len = rtp_play_len;
		if ( len > play_buf_max_size )len = play_buf_max_size;

		memcpy(play_buf,rtp_play_buf,len);
		if ( rtp_play_len > len )memmove(rtp_play_buf,rtp_play_buf + len,rtp_play_len - len);
		rtp_play_len -= len;
		return len;
	}

	return 0;
}

void dialer_agent::send_dialer_image()
{
	vpn_send_time = current_time_ms/1000;
	vpn_send_seq += 20;

	vpn_resend_data[0] = 0;

	if ( dialer_state == STATE_REGISTERING )snprintf((char *)vpn_resend_data,sizeof(vpn_resend_data),"[REGIMAGE:%u:%u:%u:%u:%u:%u:%u:%u:%s:%u:%s:%s:%s:%s:%s]",current_time_ms,vpn_connection_no,vpn_session_id,vpn_send_seq,dialer_state,dialer_status,vpn_ticket_index,auth_operator_code,__PLATFORM_NAME__,auth_dialer_version,auth_device_imei,sip_username,sip_password,sip_caller_id,vpn_sip_transfer_info);
	else snprintf((char *)vpn_resend_data,sizeof(vpn_resend_data),"[IMAGE:%u:%u:%u:%u:%u:%u:%u:%s:]",current_time_ms,vpn_connection_no,vpn_session_id,vpn_send_seq, dialer_state, dialer_status, (call_end_time_ms - call_start_time_ms)/1000, sip_remote_phone);

	poller->remove_pending_data(vpn_socket_id);
	send_to_established_vpn(vpn_resend_data,strlen((const char *)vpn_resend_data),false,0);

	for ( int i = 1; i < VPN_RESEND_COUNT_MAX; i++ )send_to_established_vpn(vpn_resend_data,strlen((const char *)vpn_resend_data),true,WAIT_BEFORE_RETRY_MS * i );
}

void dialer_agent::on_receive_image(const uchar* c,int len)
{
	bool b_ack = false;
	int st = 0;

	safe_copy(vpn_receive_data,0,sizeof(vpn_receive_data),c,len);
	if ( len >= 4 && vpn_receive_data[0] == 'A' && vpn_receive_data[1] == 'C' && vpn_receive_data[2] == 'K' && vpn_receive_data[3] == ':' )
	{
		b_ack = true;
		st = 4;
	}
	else st = 6;					// IMAGE:

	uint _time_stamp = next_int(vpn_receive_data,st,len,':',']');
	uint _connection_no = next_int(vpn_receive_data,st,len,':',']');
	uint _session_id = next_int(vpn_receive_data,st,len,':',']');
	uint _dialer_seq = next_int(vpn_receive_data,st,len,':',']');
	uint _dialer_state = next_int(vpn_receive_data,st,len,':',']');
	uint _dialer_status = next_int(vpn_receive_data,st,len,':',']');
	uint _call_duration = next_int(vpn_receive_data,st,len,':',']');

	const uchar* _dest_phone = next_string(vpn_receive_data,st,len,':',']');
	const uchar* _str_balance = next_string(vpn_receive_data,st,len,':',']');

	if ( _session_id != vpn_session_id )return;
	vpn_connection_no = _connection_no;

	if ( b_ack )
	{
		if ( _dialer_seq == vpn_send_seq )
		{
			poller->remove_pending_data(vpn_socket_id);
			vpn_resend_data[0] = 0;
		}
		return;
	}

	if ( _dialer_seq >= vpn_send_seq )
	{
		poller->remove_pending_data(vpn_socket_id);
		vpn_ack_data[0] = 0;

		snprintf((char *)vpn_ack_data,sizeof(vpn_ack_data),"[ACK:%u:%u:%u:%u]",_time_stamp,_connection_no,_session_id,_dialer_seq);
		send_to_established_vpn(vpn_ack_data,strlen((const char*)vpn_ack_data),false,0);
	}

	if ( _dialer_seq <= vpn_send_seq )return;

	vpn_send_seq = _dialer_seq;

	if ( dialer_state != _dialer_state && ( _dialer_state == STATE_CONNECTED || _dialer_state == STATE_INCOMING_CONNECTED ))
	{
		call_start_time_ms = current_time_ms;
		rtp_play_len = 0;
		rtp_record_len = 0;

		set_callback_event(CALLBACK_EVENT_RESIZE_AUDIO_BUF,rtp_connected_cache_size,(const uchar*)"");
	}
	else if ( dialer_state != _dialer_state && ( dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CONNECTED ))
	{
		call_end_time_ms = current_time_ms;
		rtp_play_len = 0;
		rtp_record_len = 0;

		int duration = calculate_call_duration(false);

		if ( strcmp((const char*)ivr_no,(const char*)sip_remote_phone) == 0 );
		else if ( dialer_state == STATE_INCOMING_CONNECTED )set_callback_event(CALLBACK_EVENT_INCOMING_CALL_COMPLETE,duration,sip_remote_phone);
		else set_callback_event(CALLBACK_EVENT_CALL_COMPLETE,duration,sip_remote_phone);
	}
	else if ( _dialer_state == STATE_INCOMING_CALL && dialer_state == STATE_REGISTERED )
	{
		call_start_time_ms = call_end_time_ms = 0;
		safe_copy(sip_remote_phone,0,sizeof(sip_remote_phone),_dest_phone,strlen((const char*)_dest_phone));

		dialer_state = _dialer_state = STATE_INCOMING_RINGING;

		snprintf((char *)call_status_temp,sizeof(call_status_temp),"From : %s",sip_remote_phone);
		send_dialer_image();
	}
	else if ( _dialer_state == STATE_REGISTERED && ( dialer_state == STATE_INCOMING_CALL || dialer_state == STATE_INCOMING_RINGING ) )
	{
		_dialer_status = STATUS_MISSED_CALL;
		snprintf((char *)call_status_temp,sizeof(call_status_temp),"Missed call from %s",sip_remote_phone);

		rtp_play_len = 0;
		rtp_record_len = 0;

		set_callback_event(CALLBACK_EVENT_RESIZE_AUDIO_BUF,rtp_ringing_cache_size,(const uchar*)"");

		if ( dialer_state == STATE_INCOMING_CONNECTED )set_callback_event(CALLBACK_EVENT_INCOMING_CALL_COMPLETE,0,sip_remote_phone);
		else set_callback_event(CALLBACK_EVENT_CALL_COMPLETE,0,sip_remote_phone);
	}

	set_current_state(_dialer_state,_dialer_status);
}

int dialer_agent::on_receive_rtp(const unsigned char* dt,int len)
{
	if ( len <= 8 )return len;

	uint media_seq = dt[1];
	uint drop_percentage = dt[2];
	uint media_length = ( dt[3] << 8 )+ dt[4];

	uint padding_length = dt[5];
	uint packet_length = media_length + padding_length + 8;

	if ( packet_length > len || packet_length < 2 )return len;

	uint calculated_checksum = ( ( media_length * 2 + padding_length * 3 + 109 + media_seq ) & 0xFFFF );
	uint packet_checksum = dt[packet_length - 2];
	packet_checksum = ( ( packet_checksum << 8 ) + dt[packet_length - 1] );

	if ( packet_checksum != calculated_checksum )return packet_length;				// Invalid packet
	if ( dialer_state >= LOCAL_STATE_MINIMUM )return packet_length;
	if ( dialer_state == STATE_REGISTERED || dialer_state == STATE_UNREGISTERED )return packet_length;

	if ( rtp_waiting_after_call )
	{
		rtp_waiting_after_call = false;
		if ( dialer_state == STATE_CALLING || dialer_state == STATE_CALL_PROGRESSING )set_current_state(STATE_RINGING,dialer_status);
	}


	int media_st = 6, media_en = media_length + 6;

	for ( int i = media_st; i < media_en; i++ )
	{
		uint l = dt[i];
		if ( ( i + l + 1 ) > media_en )break;
		if ( l % 10 )
		{
			i += l;
			continue;
		}

		if ( ( rtp_play_len + l ) > sizeof(rtp_play_buf) )
		{
			int delete_len = rtp_play_len + l - sizeof(rtp_play_buf);

			memmove(rtp_play_buf, rtp_play_buf + delete_len, rtp_play_len - delete_len);
			rtp_play_len -= delete_len;
		}

		memcpy(rtp_play_buf + rtp_play_len, dt + i + 1, l);
		rtp_play_len += l;

		i += l;
	}


	return packet_length;
}

void dialer_agent::network_poller_on_receive_data(int socket_id,const uchar* dt,int len)
{
	if ( dialer_state == LOCAL_STATE_UNINITIALIZED )return;

	if ( socket_id == SOCKET_AUTH )
	{
		if ( ( auth_reply_length + len ) > sizeof(auth_reply) )len = sizeof(auth_reply) - auth_reply_length;
		if ( len > 0 )
		{
			memcpy(auth_reply + auth_reply_length,dt,len);
			auth_reply_length += len;
			auth_reply[auth_reply_length] = 0;
		}
		return;
	}

	if ( socket_id == SOCKET_BALANCE )
	{
		if ( ( balance_reply_length + len ) > sizeof(balance_reply) )len = sizeof(balance_reply) - balance_reply_length;
		if ( len > 0 )
		{
			memcpy(balance_reply + balance_reply_length,dt,len);
			balance_reply_length += len;
			balance_reply[balance_reply_length] = 0;
		}
	}

	int rtp_receive_len = 0;
	for ( int i = 0; i < len; i++ )
	{
		if ( dt[i] == 0 )							// Media data
		{
			int consumed_len = on_receive_rtp(dt + i,len-i);
			if ( consumed_len > 0 )i += ( consumed_len - 1 );
			continue;
		}

		if ( dt[i] != '[' )continue;


		int st = i + 1;
		for ( ; i < len; i++ )
		{
			if ( dt[i] != ']' )continue;
			handle_vpn_command(dt + st,i-st,socket_id);
			break;
		}
	}
}

void dialer_agent::network_poller_on_socket_closed(int socket_id)
{
	poller->remove_socket(socket_id);

	if ( socket_id == SOCKET_BALANCE )
	{
		if ( balance_reply_length <= 0 )return;

		int st = -1;
		int en = -1;

		if ( strlen((const char*)balance_parse_pre) > 0 )
		{
			st = find_string(balance_reply,balance_reply_length,(const char*)balance_parse_pre);
			if ( st >= 0 )st += strlen((const char*)balance_parse_pre);
		}
		if ( strlen((const char*)balance_parse_post) > 0 )en = find_string(balance_reply,balance_reply_length,(const char*)balance_parse_post);

		if ( st < 0 && find_string(balance_reply,balance_reply_length,"\r\n\r\n") >= 0 )st = find_string(balance_reply,balance_reply_length,"\r\n\r\n") + 4;

		if ( st < 0 )st = 0;
		if ( en < 0 )en = balance_reply_length;

		balance_value[0] = 0;

		bool balance_empty = true;
		for ( int i = st; i < en; i++ )
		{
			if ( balance_reply[i] == ' ' || balance_reply[i] == 13 || balance_reply[i] == 10 || balance_reply[i] == 9 )continue;
			balance_empty = false;
			break;
		}

		if ( !balance_empty )snprintf((char *)balance_value,sizeof(balance_value),"Balance : ");
		int l = strlen((const char*)balance_value);

		for ( int i = st; i < en; i++ )
		{
			if ( balance_reply[i] == 13 || balance_reply[i] == 10 )continue;
			if ( l < sizeof(balance_value) )balance_value[l++] = balance_reply[i];
		}
		balance_value[l] = 0;
		return;
	}

	if ( socket_id != SOCKET_AUTH )return;
	if ( dialer_state != LOCAL_STATE_AUTHENTICATING )return;

	int parse_reply = parse_auth_reply(auth_reply,auth_reply_length);
	if ( parse_reply < 0 )authenticate();

	if ( parse_reply <= 0 )return;
	bool proceed_connecting = false;

	if ( auth_operator_code <= 0 )
	{
		set_current_state(LOCAL_STATE_UNINITIALIZED,STATUS_INVALID_OPERATORCODE);
		set_callback_event(CALLBACK_EVENT_AUTH_ERROR,0,(const uchar*)"");
	}
	else if ( strlen((const char*)sip_username) <= 0 )
	{
		set_current_state(LOCAL_STATE_UNINITIALIZED,STATUS_USERNAME_NOT_SET);
		set_callback_event(CALLBACK_EVENT_COMPLETE_SIP_SETTINGS,0,(const uchar*)"");
	}
	else proceed_connecting = true;

	if ( proceed_connecting )set_current_state(LOCAL_STATE_CONNECTING_VPN);
}

void dialer_agent::handle_vpn_command(const uchar* c,int len,int socket_id)
{
	if ( find_string(c,len,"TICKET:") == 0 )
	{
		if ( dialer_state != LOCAL_STATE_CONNECTING_VPN )return;
		poller->remove_pending_data(socket_id);

		vpn_socket_id = socket_id;
		vpn_active_udp = ( ( vpn_socket_id & 0x0F ) == SOCKET_UDP1 );

		for ( int i = 0; i < MAX_VPN_COUNT; i++ )
		{
			if ( vpn_socket_id != ( i << 4 ) + SOCKET_UDP1 )poller->remove_socket( ( i << 4 ) + SOCKET_UDP1 );
			if ( vpn_socket_id != ( i << 4 ) + SOCKET_TCP )poller->remove_socket( ( i << 4 ) + SOCKET_TCP );
		}

		int st = 7;
		uint time_stamp = next_int(c,st,len,':',']');
		vpn_ticket_index = next_int(c,st,len,':',']');

		int tmp = 0;
		for ( ; st < len; st++ )
		{
			if ( c[st] == ':' || c[st] == ']' )break;
			if ( tmp < sizeof(vpn_ticket)-1 )vpn_ticket[tmp++] = c[st];
		}
		st++;
		vpn_ticket[tmp] = 0;

		vpn_connection_no = 0;
		vpn_session_id = session_id_from_ticket((const uchar*)vpn_ticket,strlen((const char *)vpn_ticket));

		vpn_alt_channel_state = ALT_CHANNEL_NOT_TESTED;
		set_current_state(STATE_REGISTERING,STATUS_NONE);

		send_dialer_image();
		return;
	}

	if ( find_string(c,len,"IMAGE:") == 0 || find_string(c,len,"ACK:") == 0 )
	{
		on_receive_image(c,len);
		return;
	}

	if ( find_string(c,len,"CHANNELOK:") == 0 )
	{
		if ( vpn_alt_channel_state != ALT_CHANNEL_BEING_TESTED )return;

		vpn_alt_channel_state = ALT_CHANNEL_CONNECTED;
		poller->remove_pending_data(socket_id);

		int st = 10;
		uint time_stamp = next_int(c,st,len,':',']');
		uint connection_no = next_int(c,st,len,':',']');
		uint session_id = next_int(c,st,len,':',']');
		vpn_socket_id_alt = next_int(c,st,len,':',']');

		for ( int i = 0; i < MAX_VPN_COUNT; i++ )
		{
			if ( vpn_socket_id_alt == ( i << 4 ) + SOCKET_UDP2 )continue;
			poller->remove_socket( ( i << 4 ) + SOCKET_UDP2 );
		}
	}
}

void dialer_agent::send_to_established_vpn(const uchar* dt,int len,bool b_retry_data,uint wait_ms)
{
	poller->send_data(vpn_socket_id,dt,len,wait_ms);
}

void dialer_agent::send_rtp_to_vpn()
{
	rtp_last_send_time_ms = current_time_ms;

	if ( dialer_state != STATE_RINGING && dialer_state != STATE_CONNECTED && dialer_state != STATE_INCOMING_CONNECTED )return;
	if ( rtp_record_len <= 0 )return;

	int len = rtp_record_len;
	if ( len > RTP_SEND_SIZE_MAX )len = RTP_SEND_SIZE_MAX;

	uint padding_len = 0;
	uint checksum_val = ( ( len * 2 + padding_len * 3 + 109 + ( rtp_send_seq & 0xFF ) ) & 0xFFFF );

	int l = 0;
	rtp_send_buf[l++] = 0;							// RTP IDENTIFIER
	rtp_send_buf[l++] = ( ( vpn_connection_no >> 8 ) & 0xFF );
	rtp_send_buf[l++] = ( vpn_connection_no & 0xFF );
	rtp_send_buf[l++] = ( vpn_session_id >> 8 );
	rtp_send_buf[l++] = ( vpn_session_id & 0xFF );
	rtp_send_buf[l++] = ( ( rtp_send_seq & 0xFF ) );
	rtp_send_buf[l++] = 0;							// Drop percentage
	rtp_send_buf[l++] = ( ( len >> 8 ) & 0xFF );
	rtp_send_buf[l++] = ( len & 0xFF );
	rtp_send_buf[l++] = padding_len;

	memcpy(rtp_send_buf + l,rtp_record_buf,len);
	l += len;

	if ( rtp_record_len > 0 )memmove(rtp_record_buf,rtp_record_buf + len,rtp_record_len - len);
	rtp_record_len -= len;

	for ( int i = 0; i < padding_len; i++ )rtp_send_buf[l++] = ( ( padding_len + i ) & 0xFF );
	rtp_send_buf[l++] = ( ( checksum_val >> 8 ) & 0xFF );
	rtp_send_buf[l++] = ( checksum_val & 0xFF );

	if ( vpn_alt_channel_state == ALT_CHANNEL_CONNECTED )poller->send_data(vpn_socket_id_alt,rtp_send_buf,l,0);
	else poller->send_data(vpn_socket_id,rtp_send_buf,l,0);

	rtp_send_seq++;
}

void dialer_agent::send_balance_request()
{
	int url_len = strlen((const char*)balance_url);
	if ( url_len < 7 )return;

	int p = find_string((const uchar *)balance_url,url_len,"REPLACE");
	if ( p > 0 )
	{
		int delete_len = strlen("REPLACE") - strlen((const char*)sip_username);
		if ( delete_len >= 0 )
		{
			for ( int i = p; i < ( url_len - delete_len ); i++ )balance_url[i] = balance_url[i+delete_len];
			balance_url[url_len-delete_len] = 0;
			url_len -= delete_len;
		}
		else
		{
			delete_len = 0 - delete_len;
			for ( int i = url_len + delete_len - 1; i >= ( p + delete_len ); i-- )balance_url[i] = balance_url[i-delete_len];
			url_len += delete_len;
		}

		memcpy(balance_url + p,sip_username,strlen((const char*)sip_username));
		balance_url[url_len] = 0;
	}

	balance_reply_length = 0;
	poller->fetch_url(SOCKET_BALANCE,(const char*)balance_url);
}

void dialer_agent::set_current_state(int new_state,int new_status)
{
	if ( ( dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CONNECTED || dialer_state == STATE_REGISTERING || dialer_state == STATE_UNREGISTERED || dialer_state == STATE_NONE ) && new_state == STATE_REGISTERED )send_balance_request();

	dialer_state = new_state;
	dialer_status = new_status;

	if ( dialer_state == STATE_REGISTERED && vpn_alt_channel_state == ALT_CHANNEL_NOT_TESTED )start_alt_channel_request();
}


void dialer_agent::call(const char* dest_no)
{
	if ( dialer_state == STATE_INCOMING_CALL || dialer_state == STATE_INCOMING_RINGING )
	{
		call_start_time_ms = call_end_time_ms = current_time_ms;

		rtp_record_len = rtp_play_len = 0;
		set_callback_event(CALLBACK_EVENT_RESIZE_AUDIO_BUF,rtp_connected_cache_size,(const uchar*)"");

		set_current_state(STATE_INCOMING_CONNECTED,STATUS_NONE);
		send_dialer_image();
		return;
	}

	if ( strlen(dest_no) == 0 )return;
	if ( dialer_state != STATE_REGISTERED )return;

	safe_copy(sip_remote_phone,0,sizeof(sip_remote_phone),(uchar *)dest_no,strlen(dest_no));

	rtp_waiting_after_call = true;
	rtp_last_send_time_ms = 0;

	rtp_send_seq = 0;
	rtp_play_len = 0;
	rtp_record_len = 0;

	call_start_time_ms = call_end_time_ms = 0;

	set_callback_event(CALLBACK_EVENT_RESIZE_AUDIO_BUF,rtp_ringing_cache_size,(const uchar*)"");

	set_current_state(STATE_CALLING,STATUS_NONE);
	send_dialer_image();
}

void dialer_agent::end_call()
{
	rtp_play_len = 0;
	rtp_record_len = 0;

	if ( dialer_state == LOCAL_STATE_UNINITIALIZED );
	else if ( dialer_state >= LOCAL_STATE_MINIMUM )
	{
		poller->stop_and_reset_all();
		set_current_state(LOCAL_STATE_UNINITIALIZED,STATUS_SELF_CANCELLED);
	}
	else if ( dialer_state == STATE_CALLING || dialer_state == STATE_RINGING || dialer_state == STATE_CONNECTED )
	{
		call_end_time_ms = current_time_ms;
		if ( dialer_state != STATE_CONNECTED )call_start_time_ms = current_time_ms;

		if ( dialer_state == STATE_CALLING || dialer_state == STATE_RINGING )set_current_state(STATE_REGISTERED,STATUS_CALL_CANCELLED);
		else
		{
			set_callback_event(CALLBACK_EVENT_CALL_COMPLETE,calculate_call_duration(false),sip_remote_phone);
			set_current_state(STATE_REGISTERED,STATUS_CALL_DISCONNECTED);
		}

		send_dialer_image();
	}
	else if ( dialer_state == STATE_INCOMING_CONNECTED || dialer_state == STATE_INCOMING_CALL || dialer_state == STATE_INCOMING_RINGING )
	{
		call_end_time_ms = current_time_ms;
		if ( dialer_state != STATE_INCOMING_CONNECTED )call_start_time_ms = current_time_ms;

		if ( dialer_state == STATE_INCOMING_CALL || dialer_state == STATE_INCOMING_RINGING )
		{
			set_current_state(STATE_REGISTERED,STATUS_CALL_CANCELLED);
			set_callback_event(CALLBACK_EVENT_INCOMING_CALL_COMPLETE,0,sip_remote_phone);
		}
		else
		{
			set_callback_event(CALLBACK_EVENT_INCOMING_CALL_COMPLETE,calculate_call_duration(false),sip_remote_phone);
			set_current_state(STATE_REGISTERED,STATUS_CALL_DISCONNECTED);
		}
		send_dialer_image();
	}
	else if ( dialer_state == STATE_REGISTERING )
	{
		set_current_state(STATE_UNREGISTERED,STATUS_NONE);
		send_dialer_image();
	}
}

