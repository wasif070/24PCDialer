========================================================================
    DYNAMIC LINK LIBRARY : G729 Project Overview
========================================================================

AppWizard has created this G729 DLL for you.  

This file contains a summary of what you will find in each of the files that
make up your G729 application.

G729.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

G729.cpp
    This is the main DLL source file.

G729.h
    This file contains a class declaration.

AssemblyInfo.cpp
	Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
