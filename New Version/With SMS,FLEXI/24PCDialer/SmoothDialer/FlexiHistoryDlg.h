#pragma once
#include "MyUtil.h"

#include "Request.h"

#include <vector>
#include <stdio.h>
#include "json.h"
#include "afxcmn.h"
// FlexiHistoryDlg dialog

class FlexiHistoryDlg : public CDialogEx
{
	DECLARE_DYNAMIC(FlexiHistoryDlg)

public:
	FlexiHistoryDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~FlexiHistoryDlg();
	CString listType;
// Dialog Data
	enum { IDD = IDD_DIALOG_FLEXI_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	void print(json_value *value,  bool isList, int ident = 0);
	bool parse(char* source, bool isList);
	CStringA FlexiRechargeHistory(CString userID, CString requestKey, CString requestCode, CString listType);
	CListCtrl m_flexi_list;
	BOOL json_list;
	CStringA list;
	CStringA listMsg[30];
	int counter;
};
