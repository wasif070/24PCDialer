#ifndef __MY_UTILS_ADDED__
#define __MY_UTILS_ADDED__

#include "resource.h"


class MyUtil
{
public:
	enum
	{
		MAX_FILE_SIZE = 20000,
		MAX_CALL_LOG = 20,
		MAX_CONTACT_COUNT = 100,

		ITEM_SEPARATOR = '='
	};

private:
	static CString	i_XmlItem[10];
	static char		i_temp[MAX_FILE_SIZE];
	static CString	i_emptyString;

	static CString	iContacts[MAX_CONTACT_COUNT];
	static CString	iCallLog[MAX_CALL_LOG];
	static CString	iDialLog[MAX_CALL_LOG];

	static const CString& ReadXmlItem(const CString& str_data,CString str_tag,int* _st = NULL);
	static const CString* BreakLine(const CString& dt,int& item_count);

	static bool LoadFile(const CString& file_name,CString& dt);
	static bool SaveFile(const CString& file_name,const CString& str_dt);

	static const CString ModifyItemForSeparator(const CString& str);

public:
	static CString iAuthUrl;
	static CString iAuthUrlOptional;
	static CString iAboutText;

	static bool iFixedPin;
	static int iBasePin;

	static int iOperatorCode;
	static CString iSipUsername;
	static CString iSipPassword;
	static CString iSipCallerID;

	static CString iFlexiSMSUsername;
	static CString iFlexiSMSPassword;
	static CString iFlexiSMSurl;

	
public:	
	static int  StrToInt(const CString& str);
	static const CString IntToStr(int dt);
	static const char* StrToCharArray(const CString& str);
	static int StrToCharArray(const CString& str,char* str_dest,int max_len);

	static bool LoadBaseConfig();
	static void LoadSettings();
	static void SaveSettings();

	static bool GetContact(int index,CString& str_name,CString& str_phone_no);
	static int AddContact(const CString& str_name,const CString& str_phone_no); // returns new contact index
	static void EditContact(int pos,const CString& str_name,const CString& str_phone_no);
	static void DeleteContact(int index);

	static void AddCallLog(bool b_dial_log,const CString& str_name,const CString& phone_no,int duration_second);
	static bool GetCallLog(bool b_dial_log,int index,CString& str_name,CString& phone_no,CString& str_time,CString& str_duration);

	static const CString PhoneNoToName(const CString& phone_no);

};

#endif