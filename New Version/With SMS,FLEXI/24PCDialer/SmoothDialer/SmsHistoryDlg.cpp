// SmsHistoryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmsHistoryDlg.h"
#include "SmoothDialer.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"

using namespace std;
// SmsHistoryDlg dialog

IMPLEMENT_DYNAMIC(SmsHistoryDlg, CDialogEx)

SmsHistoryDlg::SmsHistoryDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(SmsHistoryDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

SmsHistoryDlg::~SmsHistoryDlg()
{
}

static std::wstring s2ws(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_UTF8, 0, s.c_str(), slength, 0, 0); 
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_UTF8, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
}

static void AddData(CListCtrl &ctrl, int row, int col, const char *str)
{
    LVITEM lv;
    lv.iItem = row;
    lv.iSubItem = col;
    std::wstring stemp = s2ws(str);
	lv.pszText =  (LPWSTR) stemp.c_str();
    lv.mask = LVIF_TEXT;
    if(col == 0)
        ctrl.InsertItem(&lv);
    else
        ctrl.SetItem(&lv);   
}

BOOL SmsHistoryDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	m_sms_list.DeleteAllItems();
	m_sms_list.InsertColumn(0, _T("Phone Number"));
    m_sms_list.SetColumnWidth(0, 100);

    m_sms_list.InsertColumn(1, _T("Cost"));
    m_sms_list.SetColumnWidth(1, 40);
    
    m_sms_list.InsertColumn(2, _T("Message"));
    m_sms_list.SetColumnWidth(2, 100);

	m_sms_list.InsertColumn(3, _T("Time"));
    m_sms_list.SetColumnWidth(3, 120);

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + MyUtil::iFlexiSMSPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	CStringA list_json = SMSHistory(MyUtil::iFlexiSMSUsername, reqKey, (CString) hash.c_str(), listType);
	char * smslist = list_json.GetBuffer();
	counter = 0;
	parse(smslist, true);

	m_sms_list.DeleteAllItems();
	for(int i=0; i<counter; i+=4){
		for(int j = 0; j<4 ; j++){
			AddData(m_sms_list, i/4 , j, listMsg[i+j]);	
		}
	}

	return TRUE;
}

CStringA SmsHistoryDlg::SMSHistory(CString userID, CString requestKey, CString requestCode, CString listType){

	CStringA	str_url;
	str_url.Append((CStringA)MyUtil::iFlexiSMSurl);
	//str_url.Append("http://192.168.1.45:8080/m.inet/mobile/");
	str_url.Append("dialer_smslistapi.jsp?RequestKey=");
	str_url.Append((CStringA)requestKey);
	str_url.Append("&RequestCode=");
	str_url.Append((CStringA)requestCode);
	str_url.Append("&UserId=");
	str_url.Append((CStringA)userID);
	str_url.Append("&listType=");
	str_url.Append((CStringA)listType);


	Request		myRequest;
	CStringA	sHeaderSend,sHeaderReceive,sMessage;

	UpdateData(true);

	int			IsPost		= 1;

	myRequest.SendRequest(IsPost, (LPCSTR)str_url, sHeaderSend, sHeaderReceive, sMessage);


	INT st = -1;
	INT en = -1;
		
	if (sMessage.Find("{") >= 0){
	 st = sMessage.Find("{");
	 en = sMessage.GetLength();
	}
	sMessage.Format("%s", sMessage.Mid(st,en-st));

	char * full_msg = sMessage.GetBuffer();

	parse(full_msg, false);

	UpdateData(false);

	return list;
}

void SmsHistoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SMS_LIST, m_sms_list);
}


BEGIN_MESSAGE_MAP(SmsHistoryDlg, CDialogEx)
END_MESSAGE_MAP()


void SmsHistoryDlg::print(json_value *value, bool isList, int ident)
{
if (value->name)
		list =  value->name;
	switch(value->type)
	{
	case JSON_NULL:
		break;
	case JSON_OBJECT:
	case JSON_ARRAY:
		for (json_value *it = value->first_child; it; it = it->next_sibling)
		{
			print(it, isList, ident + 1);
		}
		break;
	case JSON_STRING:
		if(isList){
		listMsg[counter] = value->string_value;
		counter++;
		}else{
			list = value->string_value;
		}
		break;
	case JSON_INT:
		list.Format("%d", value->int_value);
		break;
	case JSON_FLOAT:
		list.Format("%f", value->int_value);
		break;
	case JSON_BOOL:
		list = value->int_value ? "true\n" : "false\n";
		break;
	}

}

bool SmsHistoryDlg::parse(char* source, bool isList)
{
	char *errorPos = 0;
	char *errorDesc = 0;
	int errorLine = 0;
	block_allocator allocator(1 << 10);

	json_value *root = json_parse(source, &errorPos, &errorDesc, &errorLine, &allocator);
	if(root)
	{
		print(root,isList);
		return TRUE;
	}
	return FALSE;
}
