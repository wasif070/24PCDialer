// SmsSendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmsSendDlg.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"

using namespace std;
// SmsSendDlg dialog

IMPLEMENT_DYNAMIC(SmsSendDlg, CDialogEx)

SmsSendDlg::SmsSendDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(SmsSendDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

SmsSendDlg::~SmsSendDlg()
{
}

BOOL SmsSendDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	CStatic * m_Label;
	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(100, _T("Cambria Bold"));
	m_Label = (CStatic *)GetDlgItem(IDC_STATIC_SMS_SEND);
	m_Label->SetFont(m_Font1);

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + MyUtil::iFlexiSMSPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	FlexiAuthenticate(MyUtil::iFlexiSMSUsername, reqKey, (CString) hash.c_str() );
	
	//iDialerAgent -> FlexiAuthenticate(flexiUser , reqKey, hash.c_str());
	
	/*if(phoneNum.GetLength()>0){
		SetDlgItemText(IDC_RECHARGE_NUMBER_EDIT, phoneNum);
	}*/
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void SmsSendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(SmsSendDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &SmsSendDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &SmsSendDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// SmsSendDlg message handlers
void SmsSendDlg::FlexiAuthenticate(CString userID, CString requestKey, CString requestCode){
	CStringA	str_url;
	str_url.Append((CStringA)MyUtil::iFlexiSMSurl);
	//str_url.Append("http://192.168.1.45:8080/m.inet/mobile/");
	str_url.Append("dialer_loginapi.jsp?RequestKey=");
	str_url.Append((CStringA)requestKey);
	str_url.Append("&RequestCode=");
	str_url.Append((CStringA)requestCode);
	str_url.Append("&UserId=");
	str_url.Append((CStringA)userID);
	str_url.Append("&PCDialer=1");

	Request		myRequest;
	CStringA	sHeaderSend, sHeaderReceive, sMessage;

	UpdateData(true);

	int			IsPost		= 1;

	myRequest.SendRequest(IsPost, (LPCSTR)str_url, sHeaderSend, sHeaderReceive, sMessage);

	INT st = -1;
	INT en = -1;
		
	if (sMessage.Find("{") >= 0){
	 st = sMessage.Find("{");
	 en = sMessage.GetLength();
	}
	sMessage.Format("%s", sMessage.Mid(st,en-st));
	char * flexiAuthReply = sMessage.GetBuffer();
	parse(flexiAuthReply);
	
	//if(json_parsed_msg.Find("Your Balance:") >=0){	
	// }else{
	SetDlgItemText(IDC_STATIC_SMS_SEND, (CString)json_parsed_msg);
		//AfxMessageBox((CString)json_parsed_msg, MB_ICONMASK);
	//}

	UpdateData(false);
	
}

void SmsSendDlg::print(json_value *value, int ident)
{

	if (value->name)
		json_parsed_msg =  value->name;
	switch(value->type)
	{
	case JSON_NULL:
		break;
	case JSON_OBJECT:
	case JSON_ARRAY:
		for (json_value *it = value->first_child; it; it = it->next_sibling)
		{
			print(it, ident + 1);
		}
		break;
	case JSON_STRING:
		json_parsed_msg = value->string_value;
		break;
	case JSON_INT:
		json_parsed_msg.Format("%d", value->int_value);
		break;
	case JSON_FLOAT:
		json_parsed_msg.Format("%f", value->int_value);
		break;
	case JSON_BOOL:
		json_parsed_msg = value->int_value ? "true\n" : "false\n";
		break;
	}
	//return msg;
}

bool SmsSendDlg::parse(char* source)
{
	char *errorPos = 0;
	char *errorDesc = 0;
	int errorLine = 0;
	block_allocator allocator(1 << 10);


	json_value *root = json_parse(source, &errorPos, &errorDesc, &errorLine, &allocator);
	if(root)
	{
		print(root);
		return TRUE;
	}

	//printf("Error at line %d: %s\n%s\n\n", errorLine, errorDesc, errorPos);
	return FALSE;
}


void SmsSendDlg::OnBnClickedOk()
{
	CString mobileNumber;
	GetDlgItemText(IDC_EDIT_SEND_SMS_NUMBER, mobileNumber);

	CString message;
	GetDlgItemText(IDC_EDIT_SEND_SMS_MESSAGE, message);


	if(mobileNumber.GetLength()>0 && message.GetLength()>0 ){

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + MyUtil::iFlexiSMSPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	sendSmsMessage(MyUtil::iFlexiSMSUsername, reqKey, (CString)hash.c_str(), mobileNumber.Trim(), message.Trim());

	}else{
		AfxMessageBox(_T("Phone number/ Recharge amount can not be empty."));
	}
}

CStringA wstring_to_utf8_hex(CString &input)
{
  CStringA output;
  int cbNeeded = WideCharToMultiByte(CP_UTF8, 0, input, -1, NULL, 0, NULL, NULL);
  if (cbNeeded > 0) {
    char *utf8 = new char[cbNeeded];
    if (WideCharToMultiByte(CP_UTF8, 0, input, -1, utf8, cbNeeded, NULL, NULL) != 0) {
      for (char *p = utf8; *p; *p++) {
        char onehex[5];
        _snprintf(onehex, sizeof(onehex), "%%%02.2X", (unsigned char)*p);
        output+=onehex;
      }
    }
    delete[] utf8;
  }
  return output;
}

void SmsSendDlg::sendSmsMessage(CString userID, CString requestKey, CString requestCode, CString mobileNum, CString message)
{

	CStringA	str_url;
	str_url.Append((CStringA)MyUtil::iFlexiSMSurl);
	//str_url.Append("http://192.168.1.45:8080/m.inet/mobile/");
	str_url.Append("dialer_smsapi.jsp?RequestKey=");
	str_url.Append((CStringA)requestKey);
	str_url.Append("&RequestCode=");
	str_url.Append((CStringA)requestCode);
	str_url.Append("&UserId=");
	str_url.Append((CStringA)userID);
	str_url.Append("&MobileNumber=");
	str_url.Append((CStringA)mobileNum);
	str_url.Append("&Message=");
	str_url.Append(wstring_to_utf8_hex(message));

	if(str_url.GetLength() > 1000){
		AfxMessageBox(_T("Too long message!"));
	}else{
	Request		myRequest;
	CStringA	sHeaderSend, sHeaderReceive, sMessage;

	UpdateData(true);

	int			IsPost		= 1;

	myRequest.SendRequest(IsPost, (LPCSTR)str_url, sHeaderSend, sHeaderReceive, sMessage);

	INT st = -1;
	INT en = -1;
		
	if (sMessage.Find("{") >= 0){
	 st = sMessage.Find("{");
	 en = sMessage.GetLength();
	}
	sMessage.Format("%s", sMessage.Mid(st,en-st));
	char * flexiAuthReply = sMessage.GetBuffer();
	parse(flexiAuthReply);
	

	SetDlgItemText(IDC_STATIC_SMS_SEND,(CString)json_parsed_msg);

	if(json_parsed_msg.Find("Successfully") >=0){	
		SetDlgItemText(IDC_EDIT_SEND_SMS_NUMBER, _T(""));
		SetDlgItemText(IDC_EDIT_SEND_SMS_MESSAGE, _T(""));
	 }else{
		 AfxMessageBox((CString)json_parsed_msg, MB_ICONMASK);
		}

	UpdateData(false);
	}
}


void SmsSendDlg::OnBnClickedCancel()
{
	UpdateData(true);

	EndDialog(IDCANCEL);
}
