#pragma once
#include "MyUtil.h"
#include "Request.h"

#include <vector>
#include <stdio.h>
#include "json.h"
// FlexiSMSSettingsDlg dialog

class FlexiSMSSettingsDlg : public CDialogEx
{
	DECLARE_DYNAMIC(FlexiSMSSettingsDlg)

public:
	FlexiSMSSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~FlexiSMSSettingsDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_FLEXI_SMS_SETTINGS };

protected:
    virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	void FlexiAuthenticate(CString userID, CString requestKey, CString requestCode);
	CStringA json_parsed_msg;
	void print(json_value *value, int ident = 0);
	bool parse(char* source);
};
