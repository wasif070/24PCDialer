#pragma once
#include "MyUtil.h"
#include "Request.h"

#include <vector>
#include <stdio.h>
#include "json.h"
// SmsSendDlg dialog

class SmsSendDlg : public CDialogEx
{
	DECLARE_DYNAMIC(SmsSendDlg)

public:
	SmsSendDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~SmsSendDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_SMS_SEND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CStringA json_parsed_msg;
	void print(json_value *value, int ident = 0);
	bool parse(char* source);
	void FlexiAuthenticate(CString userID, CString requestKey, CString requestCode);
	void sendSmsMessage(CString userID, CString requestKey, CString requestCode, CString phoneNum, CString message);
};
