// FlexiRechargeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "FlexiRechargeDlg.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"

using namespace std;



// FlexiRechargeDlg dialog

IMPLEMENT_DYNAMIC(FlexiRechargeDlg, CDialogEx)

FlexiRechargeDlg::FlexiRechargeDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(FlexiRechargeDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

FlexiRechargeDlg::~FlexiRechargeDlg()
{
}

BOOL FlexiRechargeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	CButton* pButton = (CButton*)GetDlgItem(IDC_FLEXI_PREPAID_RADIO);
	pButton->SetCheck(true);

	CStatic * m_Label;
	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(100, _T("Cambria Bold"));
	m_Label = (CStatic *)GetDlgItem(IDC_FLEXI_RECHARGE_STATIC);
	m_Label->SetFont(m_Font1);

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + MyUtil::iFlexiSMSPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	FlexiAuthenticate(MyUtil::iFlexiSMSUsername, reqKey, (CString) hash.c_str() );
	
	//iDialerAgent -> FlexiAuthenticate(flexiUser , reqKey, hash.c_str());
	
	/*if(phoneNum.GetLength()>0){
		SetDlgItemText(IDC_RECHARGE_NUMBER_EDIT, phoneNum);
	}*/
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void FlexiRechargeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(FlexiRechargeDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &FlexiRechargeDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &FlexiRechargeDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

// FlexiRechargeDlg message handlers
void FlexiRechargeDlg::FlexiAuthenticate(CString userID, CString requestKey, CString requestCode){
	CStringA	str_url;
	str_url.Append((CStringA)MyUtil::iFlexiSMSurl);
	//str_url.Append("http://192.168.1.45:8080/m.inet/mobile/");
	str_url.Append("dialer_loginapi.jsp?RequestKey=");
	str_url.Append((CStringA)requestKey);
	str_url.Append("&RequestCode=");
	str_url.Append((CStringA)requestCode);
	str_url.Append("&UserId=");
	str_url.Append((CStringA)userID);
	str_url.Append("&PCDialer=1");

	Request		myRequest;
	CStringA	sHeaderSend, sHeaderReceive, sMessage;

	UpdateData(true);

	int			IsPost		= 1;

	myRequest.SendRequest(IsPost, (LPCSTR)str_url, sHeaderSend, sHeaderReceive, sMessage);

	INT st = -1;
	INT en = -1;
		
	if (sMessage.Find("{") >= 0){
	 st = sMessage.Find("{");
	 en = sMessage.GetLength();
	}
	sMessage.Format("%s", sMessage.Mid(st,en-st));
	char * flexiAuthReply = sMessage.GetBuffer();
	parse(flexiAuthReply);
	
	//if(json_parsed_msg.Find("Your Balance:") >=0){	
	// }else{
	SetDlgItemText(IDC_FLEXI_RECHARGE_STATIC,(CString)json_parsed_msg);
		//AfxMessageBox((CString)json_parsed_msg, MB_ICONMASK);
	//}

	UpdateData(false);
	
}

void FlexiRechargeDlg::print(json_value *value, int ident)
{

	if (value->name)
		json_parsed_msg =  value->name;
	switch(value->type)
	{
	case JSON_NULL:
		break;
	case JSON_OBJECT:
	case JSON_ARRAY:
		for (json_value *it = value->first_child; it; it = it->next_sibling)
		{
			print(it, ident + 1);
		}
		break;
	case JSON_STRING:
		json_parsed_msg = value->string_value;
		break;
	case JSON_INT:
		json_parsed_msg.Format("%d", value->int_value);
		break;
	case JSON_FLOAT:
		json_parsed_msg.Format("%f", value->int_value);
		break;
	case JSON_BOOL:
		json_parsed_msg = value->int_value ? "true\n" : "false\n";
		break;
	}
	//return msg;
}

bool FlexiRechargeDlg::parse(char* source)
{
	char *errorPos = 0;
	char *errorDesc = 0;
	int errorLine = 0;
	block_allocator allocator(1 << 10);


	json_value *root = json_parse(source, &errorPos, &errorDesc, &errorLine, &allocator);
	if(root)
	{
		print(root);
		return TRUE;
	}

	//printf("Error at line %d: %s\n%s\n\n", errorLine, errorDesc, errorPos);
	return FALSE;
}

void FlexiRechargeDlg::OnBnClickedOk()
{
	CString mobileNumber;
	GetDlgItemText(IDC_FLEXI_RECHARGE_NUMBER_EDIT, mobileNumber);

	CString rechargeAmount;
	GetDlgItemText(IDC_FLEXI_RECHARGE_BALANCE_EDIT, rechargeAmount);

	CString numberType;
	int checkRadio = GetCheckedRadioButton(IDC_FLEXI_PREPAID_RADIO, IDC_FLEXI_POSTPAID_RADIO);
	switch(checkRadio){
		case IDC_FLEXI_PREPAID_RADIO:
			numberType = "1";
			break;
		case IDC_FLEXI_POSTPAID_RADIO:
			numberType = "2";
			break;
	}
	if(mobileNumber.GetLength()>0 && rechargeAmount.GetLength()>0 ){

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + MyUtil::iFlexiSMSPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	FlexiRecharge(MyUtil::iFlexiSMSUsername, reqKey, (CString)hash.c_str(), mobileNumber.Trim(), rechargeAmount, numberType);

	}else{
		AfxMessageBox(_T("Phone number/ Recharge amount can not be empty."));
	}
}

void FlexiRechargeDlg::FlexiRecharge(CString userID, CString requestKey, CString requestCode, CString mobileNum, CString moneyAmount, CString numberType)
{

	CStringA	str_url;
	str_url.Append((CStringA)MyUtil::iFlexiSMSurl);
	//str_url.Append("http://192.168.1.45:8080/m.inet/mobile/");
	str_url.Append("dialer_flexiapi.jsp?RequestKey=");
	str_url.Append((CStringA)requestKey);
	str_url.Append("&RequestCode=");
	str_url.Append((CStringA)requestCode);
	str_url.Append("&UserId=");
	str_url.Append((CStringA)userID);
	str_url.Append("&MobileNumber=");
	str_url.Append((CStringA)mobileNum);
	str_url.Append("&Amount=");
	str_url.Append((CStringA)moneyAmount);
	str_url.Append("&NumberType=");
	str_url.Append((CStringA)numberType);

	Request		myRequest;
	CStringA	sHeaderSend, sHeaderReceive, sMessage;

	UpdateData(true);

	int			IsPost		= 1;

	myRequest.SendRequest(IsPost, (LPCSTR)str_url, sHeaderSend, sHeaderReceive, sMessage);

	INT st = -1;
	INT en = -1;
		
	if (sMessage.Find("{") >= 0){
	 st = sMessage.Find("{");
	 en = sMessage.GetLength();
	}
	sMessage.Format("%s", sMessage.Mid(st,en-st));
	char * flexiAuthReply = sMessage.GetBuffer();
	parse(flexiAuthReply);
	

	SetDlgItemText(IDC_FLEXI_RECHARGE_STATIC,(CString)json_parsed_msg);

	if(json_parsed_msg.Find("Successfully") >=0){	
		SetDlgItemText(IDC_FLEXI_RECHARGE_NUMBER_EDIT, _T(""));
		SetDlgItemText(IDC_FLEXI_RECHARGE_BALANCE_EDIT, _T(""));
	 }else{
		 AfxMessageBox((CString)json_parsed_msg, MB_ICONMASK);
	}


	UpdateData(false);
}

void FlexiRechargeDlg::OnBnClickedCancel()
{
	UpdateData(true);

	EndDialog(IDCANCEL);
}
