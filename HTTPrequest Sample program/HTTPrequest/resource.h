//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by HTTPrequest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_HTTPREQUEST_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_URL                    1000
#define IDC_BUTTON_GO                   1001
#define IDC_EDIT_HTTP_SEND              1002
#define IDC_EDIT_HTTP_RECEIVE           1003
#define IDC_EDIT_POST_DATA              1004
#define IDC_RADIO_GET                   1005
#define IDC_RADIO_POST                  1006
#define IDC_BUTTON_VIEW_HTTP            1014
#define IDC_RADIO_VIEW_HTTP             1018
#define IDC_RADIO_VIEW_IE               1019
#define IDC_EXPLORER1                   1135

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
