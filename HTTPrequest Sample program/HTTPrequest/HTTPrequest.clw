; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CHTTPrequestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "HTTPrequest.h"

ClassCount=3
Class1=CHTTPrequestApp
Class2=CHTTPrequestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_HTTPREQUEST_DIALOG

[CLS:CHTTPrequestApp]
Type=0
HeaderFile=HTTPrequest.h
ImplementationFile=HTTPrequest.cpp
Filter=N

[CLS:CHTTPrequestDlg]
Type=0
HeaderFile=HTTPrequestDlg.h
ImplementationFile=HTTPrequestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_RADIO_GET

[CLS:CAboutDlg]
Type=0
HeaderFile=HTTPrequestDlg.h
ImplementationFile=HTTPrequestDlg.cpp
Filter=D
LastObject=IDOK

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_HTTPREQUEST_DIALOG]
Type=1
Class=CHTTPrequestDlg
ControlCount=13
Control1=IDC_STATIC,static,1342308352
Control2=IDC_EDIT_URL,edit,1350631552
Control3=IDC_BUTTON_GO,button,1342242817
Control4=IDC_EDIT_HTTP_SEND,edit,1353781444
Control5=IDC_EDIT_HTTP_RECEIVE,edit,1353781444
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT_POST_DATA,edit,1353781444
Control9=IDC_STATIC,static,1342308352
Control10=IDC_RADIO_GET,button,1342177289
Control11=IDC_RADIO_POST,button,1342177289
Control12=IDC_EXPLORER1,{8856F961-340A-11D0-A96B-00C04FD705A2},1342242816
Control13=IDC_BUTTON_VIEW_HTTP,button,1342242816

