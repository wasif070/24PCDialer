// SmoothDialerDlg.h : header file
//

#if !defined(AFX_SMOOTHDIALERDLG_H__0C0B9733_BEA1_4940_82F4_344B9280F402__INCLUDED_)
#define AFX_SMOOTHDIALERDLG_H__0C0B9733_BEA1_4940_82F4_344B9280F402__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WPoller.h"
#include "DialerAgent.h"

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerDlg dialog

class CSmoothDialerDlg : public CDialog, public DialerAgentUserInterface
{
// Construction
public:
	CSmoothDialerDlg(CWnd* pParent = NULL);	// standard constructor
	~CSmoothDialerDlg();

	void TestPollEngine();
	void StartDialerAgent();

	void UpdateExitMenuAndRefresh();
	void ReConnect(TBool show_accesspoint_menu);

	void OnDialerAgentError(int error_code,TPtrC8 str_error){AfxMessageBox(L"OnDialerAgentError");}
	void OnDialerAgentChangeState();
	void OnDialerAgentAuthComplete(TBool is_success,TBool show_settings_form,TBool user_cancelled,TBool internet_error){}
	void OnCallEnd(TInt duration){}
	void OnChangeAccessPoint(){}
	TBool IsAccountMenuViewAble(){return false;}
	TBool ShowMessage(TPtrC str_title,TPtrC str_message,TBool b_yes_no,TBool b_ok_cancel){AfxMessageBox((LPCTSTR)(char *)str_message.GetBuffer());return true;}

// Dialog Data
	//{{AFX_DATA(CSmoothDialerDlg)
	enum { IDD = IDD_SMOOTHDIALER_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSmoothDialerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	WPoller iPollEngine;
	
	AudioEngineSymbian*	iAudioEngine;
	DialerAgent*		iDialerAgent;

	// Generated message map functions
	//{{AFX_MSG(CSmoothDialerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonCall();
	afx_msg void OnButtonEndCall();
	afx_msg void OnButtonIvr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMOOTHDIALERDLG_H__0C0B9733_BEA1_4940_82F4_344B9280F402__INCLUDED_)
