// SmoothDialerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "SmoothDialerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerDlg dialog

CSmoothDialerDlg::CSmoothDialerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSmoothDialerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSmoothDialerDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CSmoothDialerDlg::~CSmoothDialerDlg()
{
	iPollEngine.ForceStop();
}

void CSmoothDialerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSmoothDialerDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSmoothDialerDlg, CDialog)
	//{{AFX_MSG_MAP(CSmoothDialerDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CALL, OnButtonCall)
	ON_BN_CLICKED(IDC_BUTTON_END_CALL, OnButtonEndCall)
	ON_BN_CLICKED(IDC_BUTTON_IVR, OnButtonIvr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerDlg message handlers

BOOL CSmoothDialerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	iAudioEngine = NULL;
	iDialerAgent = NULL;

	if ( iPollEngine.Start() )
	{
		//	TestPollEngine();
		StartDialerAgent();
	}
	else
	{
		AfxMessageBox("Ineternet error !!!\r\nExiting app.");
		EndDialog(IDOK);
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmoothDialerDlg::TestPollEngine()
{
	WString str_url;
	str_url.Zero();
	str_url.Copy("http://www.google.com");

	iPollEngine.FetchUrl(1,str_url,0,5000);

	str_url.Copy("http://www.yahoo.com");
	iPollEngine.FetchUrl(2,str_url,0,5000);

	iPollEngine.AddUdpSocket(3,0xD13BDAB8,13020,0);
	iPollEngine.SendData(3,(const unsigned char*)"[GETTICKET:100000:101:10:1:4343242424]",11,0,0);
}

void CSmoothDialerDlg::StartDialerAgent()
{
	const WString state_str = ":Registering#:Registered:Unregistered:Calling#:Ringing#:Connected:Call Progress#";
	const WString status_str = ":VPN Busy:Call cancelled:Call disconnected:Incoming call:Call ended:Invalid username:Invalid username/pwd\\nChange Settings:Insufficient balance:Unknown error:Keep alive Timeout:Media error:Forbidden 403:Callee not found:Service unavailable:No reply from SIP Switch";
	const WString lstate_str = "Uninitialized:Initializing#:Authenticating#:Authenticating#:Connecting#";
	const WString lstatus_str = ":Internet error:Auth server not found:Operator code not set !:Invalid Operator code:Account not complete\\nChange account:Operation cancelled:VPN not found:Custom message:Audio codec not supported:Audio server not found\\nPlease re-install dialer";

	iAudioEngine = new AudioEngineSymbian();
	iAudioEngine->iHwnd = this->m_hWnd;
	iAudioEngine->iStatViewer = (CStatic *)GetDlgItem(IDC_STATIC_STATUS);
	iAudioEngine->Configure();

	iDialerAgent = new DialerAgent(this,iAudioEngine,state_str,status_str,lstate_str,lstatus_str,"Balance","Duration");
	iDialerAgent->SetDefaultAccessPointID(1);
	iDialerAgent->SetNetworkManager(&iPollEngine);

	iPollEngine.SetListener(iDialerAgent);

/*	iMainView->ClearStatus();
	
	if ( iFixedPin == EFalse && iAccountSettingsView->GetOperatorType() == xAccountSettingsView::OPERATOR_CODE_TYPE_NOT_SET )
	{
		iDialerAgent->OnOperatorCodeNotSelected();
		ShowSettingsView();
		return;
	}
*/

	UpdateExitMenuAndRefresh();	
	ReConnect(EFalse);

	SetTimer(1001,50,NULL);
}

void CSmoothDialerDlg::UpdateExitMenuAndRefresh()
{
/*	TPtrC str_menu = iViewInfos[iViewCurrent]->GetCancelMenuStr();
	if ( str_menu.Length() == 0 )return;	

	CEikButtonGroupContainer::Current()->SetCommandL(2,SMOOTH_DIALER_MENU_EXIT,str_menu);
	CEikButtonGroupContainer::Current()->DrawNow();*/
}


void CSmoothDialerDlg::ReConnect(TBool show_accesspoint_menu)
{
/*	if ( iAudioEngineError )return;
	if ( iFixedPin == EFalse && iAccountSettingsView->GetOperatorType() == xAccountSettingsView::OPERATOR_CODE_TYPE_NOT_SET )
	{
		ShowSettingsView();
		return;
	}

	TInt operator_code = iBasePin;
	if ( iFixedPin == EFalse )
	{
		TInt x = iAccountSettingsView->GetOperatorCode();
		TInt y = x;

		if ( y <= 0 )operator_code *= 10;
		for ( ; y > 0; y /= 10 )operator_code *= 10;
		
		operator_code += x;
	}

	iDialerAgent->Reset(show_accesspoint_menu,operator_code,iAccountSettingsView->GetSipUserName(),iAccountSettingsView->GetSipPassword(),iAccountSettingsView->GetSipPhoneNo(),iAccountSettingsView->GetBalanceUrl(),iAccountSettingsView->GetSipIP(),iAccountSettingsView->GetSipPort(),strImei,strDeviceModel);*/
	
	iDialerAgent->Reset(show_accesspoint_menu,1011,"tup","","01818237189","balanceurl",0,5060,"123456789","ModelPCDialer");
}

void CSmoothDialerDlg::OnDialerAgentChangeState()
{
	if ( iDialerAgent == NULL )return;

	CString strDisplay = "";
	
	strDisplay = strDisplay + iDialerAgent->GetDialerNameStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerStateStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerStatusStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerBalanceStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerCallDurationStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerFooterStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	SetDlgItemText(IDC_STATIC_DISPLAY,strDisplay);
}

void CSmoothDialerDlg::OnTimer(UINT nIDEvent)
{
	if ( nIDEvent != 1001 )return;
	if ( iDialerAgent == NULL )return;
	
	iDialerAgent->OnNetworkTick();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSmoothDialerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CSmoothDialerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSmoothDialerDlg::OnButtonCall() 
{
	CString str_dest_no;
	GetDlgItemText(IDC_EDIT_CALL,str_dest_no);

	str_dest_no.TrimLeft();
	str_dest_no.TrimRight();

	if ( str_dest_no.IsEmpty() )return;

	iDialerAgent->Call(str_dest_no.GetBuffer(str_dest_no.GetLength()));
}

void CSmoothDialerDlg::OnButtonEndCall() 
{
	iDialerAgent->EndCall();	
}

void CSmoothDialerDlg::OnButtonIvr() 
{
	iDialerAgent->Call(iDialerAgent->GetIVRNo());	
}
