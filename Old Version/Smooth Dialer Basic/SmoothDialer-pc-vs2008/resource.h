//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SmoothDialer.rc
//
#define IDD_SMOOTHDIALER_DIALOG         102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_STATIC_DISPLAY              1000
#define IDC_EDIT_CALL                   1001
#define IDC_BUTTON_CALL                 1002
#define IDC_BUTTON_END_CALL             1003
#define IDC_BUTTON_IVR                  1004
#define IDC_STATIC_STATUS               1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
