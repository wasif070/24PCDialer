#ifndef __DENOISER_ADDED_27_MAY_2011
#define __DENOISER_ADDED_27_MAY_2011

#include <speex/speex_preprocess.h>
#include <speex/speex.h>

class CPcmDenoiser
{
private:

	SpeexPreprocessState *speex_preprocessor;
	
	void *enc_state;
	void *dec_state;

	SpeexBits bits_encode;
	SpeexBits bits_decode;

public:
	bool bAecEnabled;

	CPcmDenoiser()
	{
		__int16 input_frame[160];
		for ( int i = 0; i < 160; i++ )input_frame[i] = rand() % 65536;

		SpeexInit(10);
		bAecEnabled = true;

//		int len = 160;
//		char* ret = SpeexEncode(input_frame,len);
//		short* ret1 = (short *)SpeexDecode(ret,len);

	}

	int SpeexInit(int quality)
	{
		int denoise = 1,agc = 1, vad = 0;
		float agc_level = 16000;

		speex_preprocessor = speex_preprocess_state_init(160,8000);		

		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_SET_DENOISE,&denoise);
		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_SET_AGC,&agc);
		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_SET_AGC_LEVEL,&agc_level);
//		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_SET_VAD,&vad);

		denoise = agc = vad = agc_level = 0;

		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_GET_DENOISE,&denoise);
		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_GET_AGC,&agc);
		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_GET_AGC_LEVEL,&agc_level);
		speex_preprocess_ctl(speex_preprocessor,SPEEX_PREPROCESS_GET_VAD,&vad);

		int frame_size = 0;

		speex_bits_init(&bits_encode);
		enc_state = speex_encoder_init(&speex_nb_mode);

		speex_encoder_ctl(enc_state,SPEEX_GET_FRAME_SIZE,&frame_size);
		speex_encoder_ctl(enc_state,SPEEX_SET_QUALITY,&quality);

		speex_bits_init(&bits_decode);
		dec_state = speex_decoder_init(&speex_nb_mode);

		int enh = 1;
		speex_decoder_ctl(dec_state,SPEEX_GET_FRAME_SIZE,&frame_size);
		speex_decoder_ctl(dec_state,SPEEX_SET_ENH,&enh);

		return frame_size;
	}

	char strSpeexEncode[1024];
	char* SpeexEncode(__int16 *dt,int& len)
	{
		speex_bits_reset(&bits_encode);
		speex_encode_int(enc_state, dt, &bits_encode);
		len = speex_bits_write(&bits_encode, strSpeexEncode,1024);
		return strSpeexEncode;
	}

	__int16 strSpeexDecode[1024];
	__int16* SpeexDecode(char *dt,int& len)
	{
		speex_bits_read_from(&bits_decode,dt,len);
		len = speex_decode_int(dec_state, &bits_decode, strSpeexDecode);
		return strSpeexDecode;
	}

	void SpeexDestroy()
	{
		speex_preprocess_state_destroy(speex_preprocessor);

		speex_bits_destroy(&bits_encode);
		speex_bits_destroy(&bits_decode);

		speex_encoder_destroy(&enc_state);
		speex_decoder_destroy(&dec_state);		
	}

	// Calling Denoise() 100000 times (that means, 1 lakh * 20 ms = 100 * 20 sec = 2000 sec requires ) 12 seconds
	// that means, pcm data of 1 second (16000bytes) is denoised within 5 ms

	__int32 strPlayedBuf[320*10];
	short* Denoise(short *pcm_data,int len,short *last_played = NULL)
	{
		if ( bAecEnabled == false || last_played == NULL )
		{
			for ( int i = 0; i < len; i += 160 )speex_preprocess(speex_preprocessor,(__int16 *)( pcm_data + i ),NULL);
		}
		else
		{
			for ( int j = 0; j < len; j++ )strPlayedBuf[j] = last_played[j];
			for ( int i = 0; i < len; i += 160 )speex_preprocess(speex_preprocessor,(__int16 *)( pcm_data + i ),strPlayedBuf + i);
		}

		return pcm_data;
	}

	unsigned char* Denoise(unsigned char *pcm_data,int len)
	{
		if ( len <= 0 )return pcm_data;
		return pcm_data;
	}
};

#endif