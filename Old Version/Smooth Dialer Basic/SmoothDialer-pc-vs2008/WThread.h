#ifndef __WINTHREAD_17_JULY_2012_ADDED__
#define __WINTHREAD_17_JULY_2012_ADDED__

#include <string>
#include <afxwin.h>

class WThread : public CWinThread
{
private:
	bool bRunning;
	bool bStopRequest;

public:

	WThread()
	{
		bRunning = false;
		bStopRequest = false;
	};
	
	void StartWThread()
	{
		CreateThread(CREATE_SUSPENDED,10000);
		ResumeThread();
	};

	virtual void RunLoop(bool& b_stop_request) = 0;

	virtual BOOL InitInstance()
	{
		return TRUE;
	}

	virtual int ExitInstance()
	{
		return 1;
	}

	virtual int Run()
	{
		bRunning = true;
		bStopRequest = false;

		RunLoop(bStopRequest);

		return 1;
	}


	~WThread()
	{
		if ( !bRunning )return;

		bRunning = false;
		AfxEndThread(1,FALSE);
	}

	void ForceStop()
	{
		if ( !bRunning )return;

		bStopRequest = true;
		while(bRunning )Sleep(100);

				
//		AfxEndThread(1,FALSE);
	}
};

class WThread1 : public CWinThread
{
private:
	bool bRunning;
	bool bStopRequest;

public:

	WThread1()
	{
		bRunning = false;
		bStopRequest = false;
	};

	void StartWThread1()
	{
		CreateThread(CREATE_SUSPENDED,10000);
		ResumeThread();
	};

	virtual void RunLoop1(bool& b_stop_request) = 0;

	virtual BOOL InitInstance()
	{
		return TRUE;
	}

	virtual int ExitInstance()
	{
		return 1;
	}

	virtual int Run()
	{
		bRunning = true;
		bStopRequest = false;

		RunLoop1(bStopRequest);

		return 1;
	}


	~WThread1()
	{
		if ( !bRunning )return;

		bRunning = false;
		AfxEndThread(1,FALSE);
	}

	void ForceStop()
	{
		if ( !bRunning )return;

		bStopRequest = true;
		while(bRunning )Sleep(100);

				
//		AfxEndThread(1,FALSE);
	}
};
#endif