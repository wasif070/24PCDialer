#include "DialerAgent.h"

void DialerAgent::OnNetworkTick()
{	
	CheckDialerActivity();
	
	if ( IsAudioRunning() )
	{
		OnDialerStateChange();	// The call duration has changed
		
		iRecordBuf.Zero();
		iAudioEngine->OnNetworkTick(iRecordBuf);
		
		if ( iRecordBuf.Length() > 0 )
		{
			SendRtpFromAudioEngine(iRecordBuf,iRecordBuf.Length());
		}
	}
}

void DialerAgent::OnDialerStateChange()
{
	TInt current_state = GetDialerStateSuper();	
	
	if ( current_state == STATE_REGISTERED && iAlternateChannelState == ALT_CHANNEL_NOT_TESTED )StartAltChannelRequest();
	
	if ( current_state == STATE_REGISTERED && ( iPrevDialerStateForBalanceUpdate == STATE_CONNECTED || iPrevDialerStateForBalanceUpdate == STATE_INCOMING_CONNECTED ||  iPrevDialerStateForBalanceUpdate == STATE_RINGING || iPrevDialerStateForBalanceUpdate == STATE_CALLING ) )
	{		
		SendBalanceRequest();
	}	
	iPrevDialerStateForBalanceUpdate = current_state;		
	iDialerUser->OnDialerAgentChangeState();	
}

void DialerAgent::PauseRtpForGSM()
{
	iRtpPausedForGSM = ETrue;
	if ( iAudioEngine )iAudioEngine->ForceStop();
}

void DialerAgent::ResumeRtpForGSM()
{
	iRtpPausedForGSM = EFalse;
	if ( iAudioEngine )iAudioEngine->ResetAudioBuffer();
}

void DialerAgent::OnCallEnd(TInt duration)
{
	iDialerUser->OnCallEnd(duration);
	if ( iAudioEngine )iAudioEngine->ForceStop();
}

void DialerAgent::OnAccessPointDone(TBool b_success,TInt access_point_id)
{
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED )return;

	if ( b_success )
	{
		iSelectedAccessPointID = access_point_id;
		iDialerUser->OnChangeAccessPoint();
		Start();
	}
	else SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INTERNET_ERROR);
}

TBool DialerAgent::IsAudioRunning()
{
	if ( iAudioEngine == NULL )return EFalse;
	if ( iSoundTestRunning )return ETrue;
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )return EFalse;
	
	TInt dialer_state = GetDialerStateSuper();
	if ( dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CONNECTED || dialer_state == STATE_RINGING )return ETrue;
	if ( dialer_state == STATE_CALLING && iWaitingForRTPAfterCall == EFalse )return ETrue;
	
	return EFalse;
}

void DialerAgent::OnReceiveData(TInt socket_id,TPtrC8 dt,TInt len)
{
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED )return;
	
	if ( socket_id == SOCKET_FEEDBACK )
	{
#ifndef __C_PLUS_PLUS_BUILD__
		if ( dt.Find(_L8("\r\n\r\n")) > 0 )
		{
			if ( dt.Find(_L8("\r\n\r\n")) < dt.Length()-4 )iDialerUser->ShowMessage(_L("Server said"),dt.Mid(dt.Find(_L8("\r\n\r\n")) + 4),EFalse,EFalse);
		}
		else iDialerUser->ShowMessage(_L("Server said"),dt,EFalse,EFalse);
#endif
		return;
	}
	
	if ( socket_id == SOCKET_AUTH )
	{
		if ( ( iAuthReply.Length() + len ) > iAuthReply.MaxLength() )len = iAuthReply.MaxLength() - iAuthReply.Length();
		iAuthReply.Append(dt.Mid(0,len));
		return;
	}

	if ( socket_id == SOCKET_BALANCE )
	{
		TInt st = -1;
		TInt en = -1;
		
		if ( strBalancePre.Length() > 0 )
		{
			st = dt.Find(strBalancePre);
			if ( st >= 0 )st += strBalancePre.Length();
		}
		if ( strBalancePost.Length() > 0 )en = dt.Find(strBalancePost);
		
		if ( st < 0 && dt.Find(_L8("\r\n\r\n")) >= 0 )st = dt.Find(_L8("\r\n\r\n")) + 4;
		
		if ( st < 0 )st = 0;		
		if ( en < 0 )en = dt.Length();
		
		strBalance.Zero();
		if ( ( en - st ) > 0 )
		{
			strBalance.Append(strBalanceDisplayPrefix);
			strBalance.Append(_L8(" : "));
		}
		if ( ( en - st + strBalance.Length()) > strBalance.MaxLength() )strBalance.Append(dt.Mid(st,strBalance.MaxLength()-strBalance.Length()));
		else strBalance.Append(dt.Mid(st,en-st));
		
		OnDialerStateChange();
		return;
	}
	
	iRtpReceiveBuffer.Zero();
	for ( int i = 0; i < len; i++ )
	{
		if ( dt[i] == 0 )	// Media data
		{
			if ( i >= ( len - 8 ) )continue;
			TUint32 media_seq = dt[i+1];
			TUint32 drop_percentage = dt[i+2];
			TUint32 media_length = ( dt[i+3] << 8 ) + dt[i+4];
			TUint32 padding_length = dt[i+5];		
			TUint32 packet_length = media_length + padding_length + 8;
		
			if ( packet_length > len )continue;
			if ( i > ( len - packet_length ) )continue;
		
			TUint32 calculated_checksum = ( ( media_length * 2 + padding_length * 3 + 109 + media_seq ) & 0xFFFF );
			TUint32 packet_checksum = dt[i + packet_length - 2];
			packet_checksum = ( ( packet_checksum << 8 ) + dt[i + packet_length - 1] );
		
			if ( packet_checksum != calculated_checksum )continue;				// Invalid packet
			
			TInt media_start = i + 6;
			i += ( packet_length - 1 );
			if ( ( iRtpReceiveBuffer.Length() + media_length ) > iRtpReceiveBuffer.MaxLength() )continue;
			
			iRtpReceiveBuffer.Append(dt.Mid(media_start,media_length));
			continue;
		}
	
		if ( dt[i] != '[' )continue;
		int st = i + 1;
		for ( ; i < len; i++ )
		{			
			if ( dt[i] == ']' )
			{
				HandleVpnMessage(dt.Mid(st,i-st),i - st,socket_id);
				break;
			}
		}
	}
	
	if ( iRtpReceiveBuffer.Length() > 0 )
	{
		if ( iWaitingForRTPAfterCall )
		{
			iWaitingForRTPAfterCall = EFalse;
			OnDialerStateChange();
		}

		TInt x = iRtpReceiveBuffer.Length();
		if ( iRtpPausedForGSM == EFalse && IsAudioRunning() != EFalse )iAudioEngine->PlayRtp(iRtpReceiveBuffer);
	}	
}

void DialerAgent::OnSocketClosed(int socket_id)
{
	iNetworkManager->RemoveSocket(socket_id);
	
	if ( socket_id != SOCKET_AUTH )return;		
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED )return;	
	
	TInt parse_reply = ParseAuthenticationReply(iAuthReply,iAuthReply.Length());
	
	if ( parse_reply < 0 )
	{
		if ( iAuthRetryNo >= 6 )
		{
			SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INTERNET_ERROR);
			iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,EFalse,ETrue);
			return;
		}
		Authenticate(iAuthRetryNo + 1);
		return;
	}
	
	if ( parse_reply == 0 )
	{
		iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,EFalse,EFalse);
		return;
	}
	
	
	TBool proceed_connecting = EFalse;
	
	if ( iOperatorCode < 0 || iOperatorType < 0 )
	{
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INVALID_OPERATORCODE);
		iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,EFalse,EFalse);
	}
	else if ( strSipUsername.Length() <= 0 )
	{
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_ACCOUNT_NOT_COMPLETE);
		iDialerUser->OnDialerAgentAuthComplete(ETrue,ETrue,EFalse,EFalse);
	}
	else if ( iOperatorType == 2 )
	{
		if ( iSwitchIPLocal == 0 || iSwitchPortLocal == 0 )
		{
			SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_ACCOUNT_NOT_COMPLETE);
			iDialerUser->OnDialerAgentAuthComplete(ETrue,ETrue,EFalse,EFalse);
		}
		else
		{
			CharArray(24,str_tmp);
			str_tmp.Zero();
			str_tmp.AppendNum((iSwitchIPLocal >> 24) & 0xFF);	str_tmp.Append('.');
			str_tmp.AppendNum((iSwitchIPLocal >> 16) & 0xFF);	str_tmp.Append('.');
			str_tmp.AppendNum((iSwitchIPLocal >> 8 ) & 0xFF);	str_tmp.Append('.');
			str_tmp.AppendNum(iSwitchIPLocal & 0xFF);			str_tmp.Append('#');
			str_tmp.AppendNum(iSwitchPortLocal & 65535);		str_tmp.Append('#');
			
			iSipTransferInfo.Insert(0,str_tmp);
			
			strBalanceUrl.Zero();			
			strBalanceUrl.Copy(strBalanceUrlLocal);
			
			proceed_connecting = ETrue;
		}
	}
	else proceed_connecting = ETrue;
	
	if ( proceed_connecting )
	{
		iDialerUser->OnDialerAgentAuthComplete(ETrue,EFalse,EFalse,EFalse);
		SetCurrentState(LOCAL_STATE_CONNECTING);
	}	
	iDialerUser->OnDialerAgentChangeState();
}

void DialerAgent::HandleVpnMessage(TPtrC8 c,TInt len,TInt socket_id)
{
	if ( c.Find(_L8("TICKET:")) == 0 )
	{
		if ( iLocalState != LOCAL_STATE_CONNECTING )return;
		iNetworkManager->RemoveRequestFromQueue(-1,DATA_TYPE_TICKET,ETrue);
		
		iEstablishedVpnSocketID = socket_id;
		iVpnUdpActive = ( ( iEstablishedVpnSocketID & 0x0F ) == SOCKET_UDP1 );
		
		for ( TInt i = 0; i < MAX_VPN_COUNT; i++ )
		{
			if ( iEstablishedVpnSocketID != ( i << 4 ) + SOCKET_UDP1 )iNetworkManager->RemoveSocket( ( i << 4 ) + SOCKET_UDP1 );
			if ( iEstablishedVpnSocketID != ( i << 4 ) + SOCKET_TCP )iNetworkManager->RemoveSocket( ( i << 4 ) + SOCKET_TCP );
		}
		
		TInt st = 7;
		TUint32 time_stamp = Globals::GetNextInt(c,st,len,':',']');
		
		iVpnRoundTripMS = CurrentTimeMS() - time_stamp;

		TUint32 ticket_index = Globals::GetNextInt(c,st,len,':',']');
		
		TPtrC8 str_ticket = Globals::GetNextString(c,st,len,':',']');

		SetCurrentState(LOCAL_STATE_VPN_CONNECTED);
		SendBalanceRequest();

		iAlternateChannelState = ALT_CHANNEL_NOT_TESTED;
		
		ResetDialerStateSuper();
		RegisterSuper(ticket_index,str_ticket,iOperatorCode,10,1,strImei,strSipUsername,strSipPassword,strSrcPhoneNo);
				
		return;
	}

	if ( c.Find(_L8("IMAGE:")) == 0 || c.Find(_L8("ACK:")) == 0)
	{
		OnReceiveImageSuper(c,c.Length());
		return;
	}
	
	if ( c.Find(_L8("CHANNELOK:")) == 0 )
	{
		if ( iAlternateChannelState != ALT_CHANNEL_BEING_TESTED )return;
		
		iAlternateChannelState = ALT_CHANNEL_CONNECTED;
		iNetworkManager->RemoveRequestFromQueue(-1,DATA_TYPE_CHANNEL_TEST,ETrue);
		
		TInt st = 10;
		TUint32 time_stamp = Globals::GetNextInt(c,st,len,':',']');
		TUint32 connection_no = Globals::GetNextInt(c,st,len,':',']');
		TUint32 session_id = Globals::GetNextInt(c,st,len,':',']');
		iEstablishedVpnSocketIDAlt = Globals::GetNextInt(c,st,len,':',']');
				
		for ( TInt i = 0; i < MAX_VPN_COUNT; i++ )
		{
			if ( iEstablishedVpnSocketIDAlt == ( i << 4 ) + SOCKET_UDP2 )continue;
			iNetworkManager->RemoveSocket( ( i << 4 ) + SOCKET_UDP2 );
		}
	}
}

void DialerAgent::SendToEstablishedVPN(TPtrC8 dt,TInt len,TBool b_retry_data,TUint32 wait_ms)
{
	iNetworkManager->SendData(iEstablishedVpnSocketID,dt,( ( b_retry_data == EFalse ) ? DATA_TYPE_BASIC : DATA_TYPE_BASIC_RETRY ),wait_ms);
}

void DialerAgent::RemoveRetryData()
{
	iNetworkManager->RemoveRequestFromQueue(iEstablishedVpnSocketID,DATA_TYPE_BASIC_RETRY,EFalse);
}

void DialerAgent::SendRtpFromAudioEngine(TPtrC8 dt,TInt len)
{	
	if ( iSoundTestRunning )
	{		
		iRtpReceiveBuffer.Zero();
		for ( TInt i = 0; i < len; i += 40 )
		{
			TInt l = 40;
			if ( ( i + l ) > len )l = len - i;
			
			if ( l % 10 )l -= ( l % 10 );
			
			iRtpReceiveBuffer.Append(l);
			iRtpReceiveBuffer.Append(dt.Mid(i,l));
		}
		
		iAudioEngine->PlayRtp(iRtpReceiveBuffer);
		return;
	}
	
	if ( IsAudioRunning() == EFalse )return;
	if ( iRtpPausedForGSM )return;
	
	len = dt.Length();
	if ( len <= 0 )return;
	
	if ( len > 1000 )len = 1000;
	
	TUint padding_len = 0;
	if ( iRtpPaddingSize > 0 )padding_len = ( iAudioEngine->GetCurrentTimeMicroSecond() + 0xFF ) % iRtpPaddingSize;
	TUint32 checksum_val = ( ( len * 2 + padding_len * 3 + 109 + ( iRtpSendSeq & 0xFF ) ) & 0xFFFF );
	
	iRtpSendBuffer.Zero();
	iRtpSendBuffer.Append(0);							// RTP IDENTIFIER
	iRtpSendBuffer.Append( ( iVpnConnectionNo >> 8 ) & 0xFF );
	iRtpSendBuffer.Append( iVpnConnectionNo & 0xFF );
	iRtpSendBuffer.Append( iVpnSessionID >> 8 );
	iRtpSendBuffer.Append( iVpnSessionID & 0xFF );
	iRtpSendBuffer.Append( ( iRtpSendSeq & 0xFF ) );
	iRtpSendBuffer.Append(0);							// Drop percentage
	iRtpSendBuffer.Append( ( len >> 8 ) & 0xFF );
	iRtpSendBuffer.Append(len & 0xFF);
	iRtpSendBuffer.Append(padding_len);
	iRtpSendBuffer.Append(dt);
	for ( TInt i = 0; i < padding_len; i++ )iRtpSendBuffer.Append( ( padding_len + i ) & 0xFF );
	iRtpSendBuffer.Append( ( checksum_val >> 8 ) & 0xFF );
	iRtpSendBuffer.Append( checksum_val & 0xFF );
	
	if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )iNetworkManager->SendData(iEstablishedVpnSocketIDAlt,iRtpSendBuffer,DATA_TYPE_RTP,0);
	else iNetworkManager->SendData(iEstablishedVpnSocketID,iRtpSendBuffer,DATA_TYPE_RTP,0);
	
	iRtpSendSeq++;
}

void DialerAgent::SendBalanceRequest()
{
	if ( strBalanceUrl.Length() < 7 )return;
	if ( strBalanceUrl.Find(_L8("REPLACE")) > 0 )strBalanceUrl.Replace(strBalanceUrl.Find(_L8("REPLACE")),7,strSipUsername);
		
	iNetworkManager->FetchUrl(SOCKET_BALANCE,strBalanceUrl,DATA_TYPE_BALANCE_REQUEST,0);
}

void DialerAgent::SetCurrentState(int current_state,int current_state_message)
{
	if ( current_state >= 0 )
	{
		iLocalState = current_state;
		iLocalStateOptional = 0;

		iLocalStateStartTime = CurrentTime();
	}

	iLocalStateMessage = current_state_message;
	iLocalStateMessageStartTime = CurrentTime();
	
	if ( iDialerUser != NULL )iDialerUser->OnDialerAgentChangeState();
}


void DialerAgent::Call(TPtrC8 dest_no)
{
	 if ( dest_no.Length() == 0 )return;
	 
	 iAudioEngine->SetSpeaker(EFalse);
	 
	 tmCallStart = CurrentTime();
	 strDialedNo.Zero();
	 strDialedNo.Copy(dest_no);

	 iWaitingForRTPAfterCall = ETrue;
	 
	 iRtpSendSeq = 0;
	 InternalCallSuper(strDialedNo);
	 
	 iAudioEngine->ResetAudioBuffer();	 
}

void DialerAgent::EndCall()
{
	iSoundTestRunning = EFalse;
		
	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )CancelPressedSuper();
	else if ( iLocalState == LOCAL_STATE_UNINITIALIZED );	
	else
	{
		iNetworkManager->StopAndResetAll();
		
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_SELF_CANCELLED);
		if ( iLocalState == LOCAL_STATE_AUTHENTICATING || iLocalState == LOCAL_STATE_AUTHENTICATE_REQUEST )iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,ETrue,EFalse);			
	}
	
	if ( iDialerUser != NULL )iDialerUser->OnDialerAgentChangeState();
}

void DialerAgent::Reset(TBool show_accesspoint_menu,TInt operator_code,TPtrC str_user,TPtrC str_password,TPtrC str_src_phone,TPtrC str_balance_url,TUint32 switch_ip,TInt switch_port,TPtrC8 str_imei,TPtrC8 str_device_model)
{
	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )UnRegisterSuper();
	
	iOperatorCode = operator_code;
	
	strSipUsername.Zero();		strSipUsername.Copy(str_user);
	strSipPassword.Zero();		strSipPassword.Copy(str_password);
	strSrcPhoneNo.Zero();		strSrcPhoneNo.Copy(str_src_phone);
	strBalanceUrlLocal.Zero();	strBalanceUrlLocal.Copy(str_balance_url);
	strImei.Zero();				strImei.Copy(str_imei);
	strDeviceModel.Zero();		strDeviceModel.Copy(str_device_model);
	
	iSwitchIPLocal = switch_ip;
	iSwitchPortLocal = switch_port;
	
	ResetDialerStateSuper();
	SetCurrentState(LOCAL_STATE_INITIALIZING);
	
	iNetworkManager->ResetAndSelectAccessPoint((show_accesspoint_menu == EFalse) ? iSelectedAccessPointID : -1 );
	
	if ( iAudioEngine )iAudioEngine->ForceStop();
}

DialerAgent::DialerAgent(DialerAgentUserInterface* dialer_user,AudioEngineSymbian* audio_engine,TPtrC8 str_state,TPtrC8 str_status,TPtrC8 str_lstate,TPtrC8 str_lstatus,TPtrC8 balance_prefix,TPtrC8 duration_prefix)
{
	iDialerUser = dialer_user;
	iAudioEngine = audio_engine;
	iNetworkManager = NULL;

	iStateStr.Zero();	iStateStr.Copy(str_state);
	iStatusStr.Zero();	iStatusStr.Copy(str_status);
	
	iLStateStr.Zero();	iLStateStr.Copy(str_lstate);
	iLStatusStr.Zero();	iLStatusStr.Copy(str_lstatus);
	
	strBalanceDisplayPrefix.Zero();		strBalanceDisplayPrefix.Copy(balance_prefix);
	strDurationDisplayPrefix.Zero();	strDurationDisplayPrefix.Copy(duration_prefix);
	
	iSoundTestRunning = EFalse;
	iVpnRoundTripMS = 0;	

	iLocalState = LOCAL_STATE_UNINITIALIZED;
	iLocalStateMessage = LOCAL_STATE_MESSAGE_NONE;

	iOperatorCode = 2010;
	iSelectedAccessPointID = -1;
	
	strImei.Zero();
	strDeviceModel.Zero();
	
	InitStatusMessage();

	if ( iDialerUser != NULL )iDialerUser->OnDialerAgentChangeState();
}

TPtrC DialerAgent::GetChannelType()
{
#ifndef __C_PLUS_PLUS_BUILD__
	if ( iVpnUdpActive )
	{
		if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return _L("UDP 2 CHANNEL");
		if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return _L("UDP TEST CHANNEL");
		return _L("UDP 1 CHANNEL");
	}

	if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return _L("TCP UDP CHANNEL");
	if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return _L("TCP TEST CHANNEL");
	return _L("TCP 1 CHANNEL");
#else
	if ( iVpnUdpActive )
	{
		if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return Globals::TempWString("UDP 2 CHANNEL");
		if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return Globals::TempWString("UDP TEST CHANNEL");
		return Globals::TempWString("UDP 1 CHANNEL");
	}

	if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return Globals::TempWString("TCP UDP CHANNEL");
	if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return Globals::TempWString("TCP TEST CHANNEL");
	return Globals::TempWString("TCP 1 CHANNEL");
#endif
}

TUint32 DialerAgent::GetRoundTripMS()
{
	return iVpnRoundTripMS;
}

void DialerAgent::OnOperatorCodeNotSelected()
{
	strDialerName.Zero();
	strDialerFooter.Zero();
	
	SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_OPERATORCODE_NOT_SET);
}

void DialerAgent::OnInternetCancelled()
{
	strDialerName.Zero();
	strDialerFooter.Zero();
		
	SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INTERNET_ERROR);	
}

void DialerAgent::OnAudioServiceError(TBool service_not_found)
{
	strDialerName.Zero();
	strDialerFooter.Zero();
	
	if ( service_not_found == EFalse )SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_CODEC_NOT_SUPPORTED);
	else SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_AUDIO_SERVICE_NOT_FOUND);
}

bool DialerAgent::Start()
{
	iEstablishedVpnSocketID = -1;
	iEstablishedVpnSocketIDAlt = -1;
	
	iOperatorType = -1;	
	iAlternateChannelState = ALT_CHANNEL_NOT_TESTED;

	iSoundTestRunning = EFalse;
	
	strBalance.Zero();
	iPrevDialerStateForBalanceUpdate = 0;
	
	SetCurrentState(LOCAL_STATE_AUTHENTICATE_REQUEST);
	return true;
}

TBool DialerAgent::StartSoundTest()
{
	if ( iSoundTestRunning )return ETrue;
	
	CharArray(201,temp_buf);
	temp_buf.Zero();
	temp_buf.Append(200);
	for ( TInt i = 0; i < 200; i++ )temp_buf.Append(i+1);
	
	iAudioEngine->PlayRtp(temp_buf);
	
	iSoundTestRunning = ETrue;
	OnDialerStateChange();
	return iSoundTestRunning;
}

TBool DialerAgent::SendFeedbackToAuthServer(TPtrC8 dt)
{
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED || iLocalState == LOCAL_STATE_AUTHENTICATING || iLocalState == LOCAL_STATE_AUTHENTICATE_REQUEST )return EFalse;
	iNetworkManager->FetchUrl(SOCKET_FEEDBACK,dt,DATA_TYPE_BASIC,0);
}

void DialerAgent::Authenticate(TInt retry_no)
{
	SetCurrentState(LOCAL_STATE_AUTHENTICATING);
	iRtpPausedForGSM = EFalse;
	
	iOperatorType = -1;
	iSipTransferInfo.Zero();
	
	CharArray(256,str_url);
	str_url.Zero();
	
	iAuthRandom = 0;
	for ( TInt i = 0; i < strImei.Length(); i++ )iAuthRandom = iAuthRandom * 10 + strImei[i] - '0';
	
	iAuthRetryNo = retry_no;
	
	if ( iAuthRetryNo == 0 )str_url.Append(_L8("http://dialer.opentechbd.com/dialersettings.php?pin="));
	else if ( iAuthRetryNo == 1 )str_url.Append(_L8("http://209.59.218.184/dialersettings.php?pin="));				// vpn.opentechbd.com
	else if ( iAuthRetryNo == 2 )str_url.Append(_L8("http://smoothsolution.org/dialersettings.php?pin="));
	else if ( iAuthRetryNo == 3 )str_url.Append(_L8("http://smoothdialer.com/dialersettings.php?pin="));
	else if ( iAuthRetryNo == 4 )str_url.Append(_L8("http://204.15.78.227/dialersettings.php?pin="));			// vpn_227	
	else str_url.Append(_L8("http://dialer.opentechbd.com/dialersettings.php?pin="));
	
	str_url.AppendNum(iOperatorCode);
	str_url.Append(_L8("&auth="));
	str_url.AppendNum(iAuthRandom);
	str_url.Append(_L8("&v=1&type=dialer&dialerimei="));
	str_url.Append(strImei);
	str_url.Append(_L8("&dialeros=10&model="));
	str_url.Append(strDeviceModel);
	str_url.Append(_L8("&usr="));
	str_url.Append(strSipUsername);
	
	iAuthReply.Zero();
	iNetworkManager->FetchUrl(SOCKET_AUTH,str_url,DATA_TYPE_AUTHENTICATE,0);
}

void DialerAgent::CheckDialerActivity()			// This function should return immediately
{
	if ( iLocalState == LOCAL_STATE_AUTHENTICATE_REQUEST )
	{
		Authenticate();
		return;
	}

	if ( iLocalState == LOCAL_STATE_CONNECTING )
	{
		if ( CurrentTimeMS() < iVpnConnectTimeoutMS )return;		
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_VPN_NOT_FOUND);		
		return;
	}

	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )
	{
		if ( iAlternateChannelState == ALT_CHANNEL_BEING_TESTED && CurrentTimeMS() >= iVpnConnectTimeoutMSAlt )
		{
			iAlternateChannelState = ALT_CHANNEL_NOT_CONNECTED;		
		}
		OnDialerStateTickSuper();
	}
}
