// RechageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "RechageDlg.h"
#include "afxdialogex.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"
#include "resource.h"
#include "FlexiAccount.h"


// RechageDlg dialog

IMPLEMENT_DYNAMIC(RechageDlg, CDialogEx)

RechageDlg::RechageDlg(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(RechageDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
	iDialerAgent ->isFlexiRecharge = TRUE;
	iDialerAgent ->isSmsSend = FALSE;
	iDialerAgent -> setRechargeDialogue(this);
}

RechageDlg::~RechageDlg()
{

}

BOOL RechageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	CButton* pButton = (CButton*)GetDlgItem(IDC_RADIO1);
	pButton->SetCheck(true);

	CStatic * m_Label;
	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(100, _T("Cambria Bold"));
	m_Label = (CStatic *)GetDlgItem(IDC_FLEXI_BALANCE_STATIC);
	m_Label->SetFont(m_Font1);

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + flexiPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	iDialerAgent -> FlexiAuthenticate(flexiUser , reqKey, hash.c_str());
	
	if(phoneNum.GetLength()>0){
		SetDlgItemText(IDC_RECHARGE_NUMBER_EDIT, phoneNum);
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}
void RechageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(RechageDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &RechageDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// RechageDlg message handlers

void  RechageDlg::OnRechargeBalanceChangeState(){
	CString flexiReply = iDialerAgent ->GetFlexiBalanceStr().GetBuffer();
	SetDlgItemText(IDC_FLEXI_BALANCE_STATIC, flexiReply);

	iDialerAgent -> strFlexiBalance.Zero();
	iDialerAgent -> tempStrFlexiBalance.Zero();
	/*if(flexiReply.Find(_T("successfully"))>0){
		this->EndDialog(1);
	}*/

}

void RechageDlg::OnBnClickedOk()
{
	
	CString mobileNumber;
	GetDlgItemText(IDC_RECHARGE_NUMBER_EDIT, mobileNumber);

	CString rechargeAmount;
	GetDlgItemText(IDC_RECHARGE_AMOUNT_EDIT, rechargeAmount);

	CString numberType;
	int checkRadio = GetCheckedRadioButton(IDC_RADIO1, IDC_RADIO2);
	switch(checkRadio){
		case IDC_RADIO1:
			numberType = "1";
			break;
		case IDC_RADIO2:
			numberType = "2";
			break;
	}
	if(mobileNumber.GetLength()>0 && rechargeAmount.GetLength()>0 ){

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(_T("%f"), sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + flexiPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	iDialerAgent -> FlexiRecharge(flexiUser, reqKey, hash.c_str(), mobileNumber.Trim(), rechargeAmount, numberType);

	}else{
		AfxMessageBox(_T("Phone number/ Recharge amount can not be empty."));
	}

}
