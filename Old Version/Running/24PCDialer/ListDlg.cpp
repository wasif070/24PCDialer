// ListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ListDlg.h"
#include "afxdialogex.h"
#include <string>

#include <stdio.h>


using namespace std;


// ListDlg dialog

IMPLEMENT_DYNAMIC(ListDlg, CDialogEx)

ListDlg::ListDlg(CStringA str)
	: CDialogEx(ListDlg::IDD)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	strMsg = str;
	counter = 0;

}

ListDlg::~ListDlg()
{
}

void ListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_msg_list);
}


BEGIN_MESSAGE_MAP(ListDlg, CDialogEx)
END_MESSAGE_MAP()


static std::wstring s2ws(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_UTF8, 0, s.c_str(), slength, 0, 0); 
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_UTF8, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
}

static void AddData(CListCtrl &ctrl, int row, int col, const char *str)
{
    LVITEM lv;
    lv.iItem = row;
    lv.iSubItem = col;
    std::wstring stemp = s2ws(str);
	lv.pszText =  (LPWSTR) stemp.c_str();
    lv.mask = LVIF_TEXT;
    if(col == 0)
        ctrl.InsertItem(&lv);
    else
        ctrl.SetItem(&lv);   
}

void ListDlg::printer(json_value *value, int ident)
{
	
	if (value->name)
		list =  value->name;
	switch(value->type)
	{
	case JSON_NULL:
		break;
	case JSON_OBJECT:
	case JSON_ARRAY:
		for (json_value *it = value->first_child; it; it = it->next_sibling)
		{
			printer(it, ident + 1);
		}
		break;
	case JSON_STRING:
		listMsg[counter] = value->string_value;
		counter++;
		break;
	case JSON_INT:
		list.Format("%d", value->int_value);
		break;
	case JSON_FLOAT:
		list.Format("%f", value->int_value);
		break;
	case JSON_BOOL:
		list = value->int_value ? "true\n" : "false\n";
		break;
	}

	}


BOOL ListDlg::parser(char* source)
{
	char *errorPos = 0;
	char *errorDesc = 0;
	int errorLine = 0;
	block_allocator allocator(1 << 10);

	json_value *root = json_parse(source, &errorPos, &errorDesc, &errorLine, &allocator);
	if(root)
	{
		printer(root);
		return TRUE;
	}
	return FALSE;
}



BOOL ListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	m_msg_list.DeleteAllItems();
	m_msg_list.InsertColumn(0, _T("Phone Number"));
    m_msg_list.SetColumnWidth(0, 100);

    m_msg_list.InsertColumn(1, _T("Cost"));
    m_msg_list.SetColumnWidth(1, 40);
    
    m_msg_list.InsertColumn(2, _T("Message"));
    m_msg_list.SetColumnWidth(2, 100);

	m_msg_list.InsertColumn(3, _T("Time"));
    m_msg_list.SetColumnWidth(3, 120);

	char * smslist = strMsg.GetBuffer();
	parser(smslist);
	m_msg_list.DeleteAllItems();
	for(int i=0; i<counter; i+=4){
		for(int j = 0; j<4 ; j++){
			AddData(m_msg_list, i/4 , j, listMsg[i+j]);	
		}
	}





	/*int counter = 0;
	int lineNum = 0;
	CStringA line = strMsg.Tokenize("}", lineNum);
	BOOL sts;
	CString strFile;
	while(!line.IsEmpty()){	
		
			int nTokenPos = 0;
			//CString newLine;
			CStringA token = line.Tokenize(",", nTokenPos);
			while(!token.IsEmpty()){

				if(token.Mid(0,7)=="message"){
				     AddData(m_msg_list, counter , 1, token.Mid(8, token.GetLength()));		
				}else if(token.Mid(0,7)=="phoneNo"){ 
					AddData(m_msg_list, counter , 0,  token.Mid(8, token.GetLength()));
					//newLine += token.Mid(8, token.GetLength()) + ","; //phone
				}
				else if(token .Mid(0,6)=="amount"){
					AddData(m_msg_list, counter , 2, token.Mid(7, token.GetLength()));
					//newLine += token.Mid(7, token.GetLength()) + ","; //price
				}
				else if(token.Mid(0,4)=="time") {
					AddData(m_msg_list, counter , 3, token.Mid(5, token.GetLength()));
					//newLine += token.Mid(5, token.GetLength()) + "\n"; //time
				}
				token = line.Tokenize(",", nTokenPos);
			}
			line = strMsg.Tokenize("}", lineNum);
			counter++;
		}*/

	return TRUE; 
}
