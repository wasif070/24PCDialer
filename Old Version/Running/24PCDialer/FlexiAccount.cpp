// FlexiAccount.cpp : implementation file
//

#include "stdafx.h"
#include "FlexiAccount.h"
#include "afxdialogex.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"



// FlexiAccount dialog

IMPLEMENT_DYNAMIC(FlexiAccount, CDialogEx)

FlexiAccount::FlexiAccount(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(FlexiAccount::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
}

FlexiAccount::~FlexiAccount()
{
}

BOOL FlexiAccount::OnInitDialog()
{

	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon
	
	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_CHECK_SAVEINFO);
	m_ctlCheck->SetCheck(BST_CHECKED);

	//Read UserInfo
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("UserInfo.dat");
	int counter = 0;
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		counter ++;
		if(counter==4)
			SetDlgItemText(IDC_FLEXI_ACCOUNT_EDIT,strLine);
		if(counter==5)
			SetDlgItemText(IDC_FLEXI_PASS_EDIT,strLine);
		}
	readFile.Close();
	}
	//End UserInfo
	return TRUE;
	// return TRUE  unless you set the focus to a control
}

void FlexiAccount::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(FlexiAccount, CDialogEx)
	ON_BN_CLICKED(IDOK, &FlexiAccount::OnBnClickedOk)
END_MESSAGE_MAP()


// FlexiAccount message handlers

void FlexiAccount::OnBnClickedOk()
{
	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_CHECK_SAVEINFO);
	int ChkBox = m_ctlCheck->GetCheck();

	GetDlgItemText(IDC_FLEXI_ACCOUNT_EDIT,flexiUser);
	GetDlgItemText(IDC_FLEXI_PASS_EDIT,flexiPassword);

	if(flexiUser=="" || flexiPassword==""){
	    int Answer;
   		Answer = AfxMessageBox(_T("User/ Password can not be empty"),
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);

		if( Answer == IDOK ){
			EndDialog(1);
			FlexiAccount fdlg(this, iDialerAgent);
			fdlg.DoModal();
		}
		else // if( Answer == IDNO )
			EndDialog(1);

	}else{
		if(ChkBox == BST_CHECKED){

			//Write User Info
			CString strLine;
			CString strFile;
			CStdioFile writeToFile;
			CFileException fileException;
			CString strFilePath = _T("UserInfo.dat");
			CFileStatus status;
			int counter = 0;
			if( CFile::GetStatus( strFilePath, status ) ){
			// File exists
			writeToFile.Open( strFilePath, CFile::modeRead ), &fileException;
			while (writeToFile.ReadString(strLine) && counter <3){
				strFile += strLine + "\n";
				counter++;
				}
				strFile += flexiUser + "\n" + flexiPassword;
			}
			writeToFile.Close();
			writeToFile.Open( strFilePath, CFile::modeCreate | CFile::modeWrite ), &fileException;
			writeToFile.Seek (0, CFile :: begin);
			writeToFile.WriteString(strFile);
			writeToFile.Close();
			//End User Info
		}
			
			SYSTEMTIME timeInMillis;
			GetSystemTime(&timeInMillis);
			//WORD millis = (time.wSecond * 1000) + time.wMilliseconds;			
			double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
			CString reqKey;
			reqKey.Format(_T("%f"), sysTime);

			std::string hash;
			crypto::errorinfo_t lasterror;
			CT2A atext(reqKey + flexiPassword);

			crypto::md5_helper_t hhelper;
			hash = hhelper.hexdigesttext(atext.m_szBuffer);
			lasterror = hhelper.lasterror();

			iDialerAgent -> FlexiAuthenticate(flexiUser , reqKey, hash.c_str());


	}
			/*while(iDialerAgent ->GetFlexiBalanceStr().Length()==0){		
			}
			CString flexiReply = iDialerAgent ->GetFlexiBalanceStr().GetBuffer();
			AfxMessageBox(flexiReply, MB_ICONMASK);*/

	CDialogEx::OnOK();
}
