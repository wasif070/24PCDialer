#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "json.h"

// ListDlg dialog

class ListDlg : public CDialogEx
{
	DECLARE_DYNAMIC(ListDlg)

public:
	ListDlg(CStringA str);   // standard constructor
	virtual ~ListDlg();

// Dialog Data
	enum { IDD = IDD_LIST_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_msg_list;
	CStringA strMsg;
	CStringA list;
	CStringA listMsg[40];
	int counter;
	BOOL parser(char* source);
	void printer(json_value *value, int ident = 0);
};
