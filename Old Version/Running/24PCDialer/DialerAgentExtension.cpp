#include "DialerAgent.h"

void DialerAgent::InitStatusMessage()
{
		strDialerName.Zero();
		strDialerFooter.Zero();
		strSMSGatewayURL.Zero();
		strFlexiGatewayURL.Zero();

		strSipUsername.Zero();
		strSipPassword.Zero();
		strSrcPhoneNo.Zero();

		strBalanceUrlLocal.Zero();
		iSwitchIPLocal = 0;
		iSwitchPortLocal = 0;
		
		strStateOptionalCustomText.Zero();	strStateOptionalCustomText.Copy(_L8("Custom message"));
}

int DialerAgent::CheckRange(int val,int min,int max)
{
		if ( val >= min && val <= max )return val;
		if ( val < min )return min;
		return max;
}

void DialerAgent::StartAltChannelRequest()
{
		iVpnConnectTimeoutMSAlt = 500;
		TUint32 main_vpn_no = ( ( iEstablishedVpnSocketID & 0xF0 ) >> 4 );
	
		for ( TInt i = 0; i < MAX_VPN_COUNT; i++ )
		{
			TInt cur_vpn = ( main_vpn_no + i + 1 ) % MAX_VPN_COUNT;		
			if ( iVpnAddrUdp2[cur_vpn].iAddr == 0 || iVpnAddrUdp2[cur_vpn].iPort == 0 )continue;

			TUint32 socket_id = ( cur_vpn << 4 ) + SOCKET_UDP2;

			iNetworkManager->AddUdpSocket( socket_id, iVpnAddrUdp2[cur_vpn].iAddr,iVpnAddrUdp2[cur_vpn].iPort,iVpnConnectTimeoutMSAlt);
			iVpnConnectTimeoutMSAlt += ( iVpnUdpTimeout * 1000 ) / 2;
			
			strChannelTestRequest.Zero();
			strChannelTestRequest.AppendFormat(_L8("[CHANNELTEST:%u:%u:%u:%u]"),CurrentTime(),iVpnConnectionNo,iVpnSessionID,socket_id);

			for ( TInt j = 0; j < iVpnRetryCount; j++ )
			{
				iNetworkManager->SendData(socket_id,strChannelTestRequest,DATA_TYPE_CHANNEL_TEST,iVpnConnectTimeoutMSAlt);
				iVpnConnectTimeoutMSAlt += ( iVpnUdpTimeout * 1000 ) / 2;
			}
		}
		
		iVpnConnectTimeoutMSAlt += ( iVpnUdpTimeout * 1000 );
		iVpnConnectTimeoutMSAlt += ( CurrentTimeMS() + 1000 );
		
		iAlternateChannelState = ALT_CHANNEL_BEING_TESTED;
}

TInt DialerAgent::ParseAuthenticationReply(TPtrC8 c,TInt len)
{
		if ( c.Length() == 0 )return -1;
		
		TInt i = 0;
		TPtrC8 str_status = Globals::FindToken(c,len,_L8("STATUS"));

		iAuthStatus = 0;
		if ( str_status.Length() < 1 )return -1;		
		if ( str_status[0] != '0' )iAuthStatus = 1;
		
		if ( iAuthStatus == 0 )
		{
			 if ( str_status.Length() < 2 )return -1;
			 
			 strCustomMessageText.Zero();
			 strCustomMessageText.Copy(str_status.Mid(2));
			 
			 SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_CUSTOM);
			 return 0;
		}

		strAuthStatus.Zero();
		if ( str_status.Length() > 2 )strAuthStatus.Copy(str_status.Mid(2));		
		
		bVpnTcpPreferred = EFalse;
		if ( Globals::FindTokenAsInt(c,len,_L8("TRYTCP")) == 1 )bVpnTcpPreferred = ETrue;
		
		TInt tcp_port = Globals::FindTokenAsInt(c,len,_L8("TCPPORT"));
		TPtrC8 str_udp_port = Globals::FindToken(c,len,_L8("UDPPORT"));
		
		TInt st = 0;
		TInt udp_port1 = Globals::GetNextInt(str_udp_port,st,str_udp_port.Length(),':');
		TInt udp_port2 = Globals::GetNextInt(str_udp_port,st,str_udp_port.Length(),':');
		
		for ( i = 0; i < MAX_VPN_COUNT; i++ )
		{
			iVpnAddrTcp[i].iAddr = iVpnAddrUdp1[i].iAddr = iVpnAddrUdp2[i].iAddr = 0;
			iVpnAddrTcp[i].iPort = iVpnAddrUdp1[i].iPort = iVpnAddrUdp2[i].iPort = 0;
			
			TUint32 i_addr = 0;
			
			if ( i == 0 )i_addr = iNetworkManager->StrToInetAddr(Globals::FindToken(c,len,_L8("VPN1"))).Address(); // xSocket::ResolveUrl(Globals::FindToken(c,len,_L8("VPN1"))).Address();
			else if ( i == 1 )i_addr = iNetworkManager->StrToInetAddr(Globals::FindToken(c,len,_L8("VPN2"))).Address(); // xSocket::ResolveUrl(Globals::FindToken(c,len,_L8("VPN2"))).Address();
			else if ( i == 2 )i_addr = iNetworkManager->StrToInetAddr(Globals::FindToken(c,len,_L8("VPN3"))).Address(); // xSocket::ResolveUrl(Globals::FindToken(c,len,_L8("VPN3"))).Address();
			else if ( i == 3 )i_addr = iNetworkManager->StrToInetAddr(Globals::FindToken(c,len,_L8("VPN4"))).Address(); // xSocket::ResolveUrl(Globals::FindToken(c,len,_L8("VPN4"))).Address();
			
			if ( i_addr == 0 )continue;
			
			iVpnAddrTcp[i].iAddr = iVpnAddrUdp1[i].iAddr = iVpnAddrUdp2[i].iAddr = i_addr;
			iVpnAddrTcp[i].iPort = tcp_port;
			iVpnAddrUdp1[i].iPort = udp_port1;
			iVpnAddrUdp2[i].iPort = udp_port2;
		}
		
// -------------- GENERATE TIMEOUT SCHEDULE FOR VPN CONNECT ---------------------------

		TPtrC8 str_vpntimeout = Globals::FindToken(c,len,_L8("VPNTIMEOUT"));		
		st = 0;
		
		iVpnRetryCount = CheckRange(Globals::GetNextInt(str_vpntimeout,st,str_vpntimeout.Length(),':'),2,5);
		iVpnUdpTimeout = CheckRange(Globals::GetNextInt(str_vpntimeout,st,str_vpntimeout.Length(),':'),2,5);
		iVpnTcpTimeout = CheckRange(Globals::GetNextInt(str_vpntimeout,st,str_vpntimeout.Length(),':'),3,8);
				
		strTicketRequest.Zero();
		
#ifdef __C_PLUS_PLUS_BUILD__
		strTicketRequest.AppendFormat(_L8("[GETTICKET:%d:%d:10:1:%s]"),CurrentTimeMS(),iOperatorCode,strImei.GetBuffer());
#else
		strTicketRequest.AppendFormat(_L8("[GETTICKET:%d:%d:10:1:%S]"),CurrentTimeMS(),iOperatorCode,&strImei);
#endif

		iVpnConnectTimeoutMS = NetworkManager::NETWORK_SOCKET_PULSE_MS;
		
		for ( TInt k = 0; k < 2; k++ )
		{
			if ( ( k == 0 && bVpnTcpPreferred == EFalse ) || ( k == 1 && bVpnTcpPreferred != EFalse ) )
			{
				for ( TInt i = 0; i < MAX_VPN_COUNT; i++ )
				{
					if ( iVpnAddrUdp1[i].iAddr == 0 || iVpnAddrUdp1[i].iPort == 0 )continue;
					
					TUint32 socket_id = ( i << 4 ) + SOCKET_UDP1;
					iNetworkManager->AddUdpSocket( socket_id, iVpnAddrUdp1[i].iAddr,iVpnAddrUdp1[i].iPort,iVpnConnectTimeoutMS);
					for ( TInt j = 0; j < iVpnRetryCount; j++ )iNetworkManager->SendData(socket_id,strTicketRequest,DATA_TYPE_TICKET, iVpnConnectTimeoutMS + ( iVpnUdpTimeout * ( j + 1 ) * 1000 ) / 2 );
				}
				iVpnConnectTimeoutMS += ( iVpnUdpTimeout * 1000 + ( iVpnUdpTimeout * 1000 * iVpnRetryCount ) / 2 );
				continue;
			}
			
			for ( TInt i = 0; i < MAX_VPN_COUNT; i++ )
			{
				if ( iVpnAddrTcp[i].iAddr == 0 || iVpnAddrTcp[i].iPort == 0 )continue;
				
				TUint32 socket_id = ( i << 4 ) + SOCKET_TCP;
				iNetworkManager->AddTcpSocket(socket_id,iVpnAddrTcp[i].iAddr,iVpnAddrTcp[i].iPort,iVpnConnectTimeoutMS);
				iNetworkManager->SendData(socket_id,strTicketRequest,DATA_TYPE_TICKET,iVpnConnectTimeoutMS + NetworkManager::NETWORK_SOCKET_PULSE_MS * 5 );
			}
			iVpnConnectTimeoutMS += ( iVpnTcpTimeout * 1000 + 1000 );
		}
		
		iVpnConnectTimeoutMS += CurrentTimeMS();
		iVpnConnectTimeoutMS += 5000;

// ----------------------------------------------------------------------------------

		iSipTransferInfo.Zero();
		iSipTransferInfo.Copy(Globals::FindToken(c,len,_L8("SIP")));
		
		iNetworkTimeout = CheckRange(Globals::FindTokenAsInt(c,len,_L8("NETWORKTIMEOUT")),5,30);		

		TPtrC8 tmp_user = Globals::FindToken(c,len,_L8("SIPUSER"));
		TPtrC8 tmp_pwd = Globals::FindToken(c,len,_L8("SIPPWD"));
		
		if ( tmp_user.Length() > 0 )strSipUsername.Copy(tmp_user);
		if ( tmp_pwd.Length() > 0 )strSipPassword.Copy(tmp_pwd);
		
		iMaxCallLog = 20;
		iMaxDialedLog = 20;

		strIVRNo.Zero();
		strIVRNo.Copy(Globals::FindToken(c,len,_L8("IVR")));
		
		iRtpBundleSize = Globals::FindTokenAsInt(c,len,_L8("RTPBUNDLE"));
		iRtpPaddingSize = Globals::FindTokenAsInt(c,len,_L8("RTPPADDING"));
		
		iPingTime = Globals::FindTokenAsInt(c,len,_L8("PINGTIME"));
		if ( iPingTime < 15 )iPingTime = 15;
		
		if ( iAudioEngine )iAudioEngine->SetRtpBundleSize(iRtpBundleSize);

		strBalancePre.Zero();	strBalancePre.Copy(Globals::FindToken(c,len,_L8("BALANCEPRE")));
		strBalancePost.Zero();	strBalancePost.Copy(Globals::FindToken(c,len,_L8("BALANCEPOST")));		
		strBalanceUrl.Zero();	strBalanceUrl.Copy(Globals::FindToken(c,len,_L8("BALANCEURL")));

		iAdditionalCallTime = Globals::FindTokenAsInt(c,len,_L8("ADDCALLTIME"));
		iUsernameAsPhoneNo = ( Globals::FindTokenAsInt(c,len,_L8("USERPHONE")) != 0 );
		
		iOperatorType = Globals::FindTokenAsInt(c,len,_L8("PINTYPE"));
		if ( iOperatorType < 0 || iOperatorType > 2 )iOperatorType = 0;
		
		strDialerName.Zero();	strDialerName.Copy(Globals::FindToken(c,len,_L8("DIALERNAME")));
		strDialerFooter.Zero();	strDialerFooter.Copy(Globals::FindToken(c,len,_L8("FOOTER")));


		strSMSGatewayURL.Zero();	strSMSGatewayURL.Copy(Globals::FindToken(c,len,_L8("SMS_GATEWAY_URL")));
		strFlexiGatewayURL.Zero();	strFlexiGatewayURL.Copy(Globals::FindToken(c,len,_L8("FLEXI_GATEWAY_URL")));


		iDialerTempStatusTimeout = Globals::FindTokenAsInt(c,len,_L8("TEMPSTATUSTIMEOUT"));
		
		if ( iUsernameAsPhoneNo )strSrcPhoneNo.Copy(strSipUsername);
		else
		{
			if ( strSrcPhoneNo.Length() == 0 )strSrcPhoneNo.Copy(strSipUsername);
			for ( TInt i = strSrcPhoneNo.Length()-1; i >= 0; i-- )if ( strSrcPhoneNo[i] < '0' || strSrcPhoneNo[i] > '9' )strSrcPhoneNo.Delete(i,1);
			if ( strSrcPhoneNo.Length() < 3 )strSrcPhoneNo.AppendNum(CurrentTimeMS()%10000);
		}	
		
		return 1;
}

bool DialerAgent::CanEmptyPhoneNoOnRedKey()
{
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )return true;
		
	int dialer_state = GetDialerStateSuper();
	if ( dialer_state == STATE_REGISTERED || dialer_state == STATE_UNREGISTERED || dialer_state == STATE_NONE )return true;
	return false;
}

TPtrC8 DialerAgent::GetDialerNameStr()
{
	return strDialerName;
}

TPtrC8 DialerAgent::GetSMSGateway()
{
	return strSMSGatewayURL;
}

TPtrC8 DialerAgent::GetFlexiGateway()
{
	return strFlexiGatewayURL;
}

TPtrC8 DialerAgent::GetDialerStateStr()
{
#ifndef __C_PLUS_PLUS_BUILD__
	if ( iSoundTestRunning )return _L8("Sound Test");
#else
	if ( iSoundTestRunning )return Globals::TempWString("Sound Test");
#endif
	
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )return Globals::GetStateItem(iLStateStr,iLocalState);
	
	TInt dialer_state = GetDialerStateSuper();
	
	if ( dialer_state == STATE_RINGING && iWaitingForRTPAfterCall != EFalse )return Globals::GetStateItem(iStateStr,STATE_CALL_PROGRESSING); //9 Call Progress
	if ( dialer_state == STATE_CALLING && iWaitingForRTPAfterCall == EFalse )return Globals::GetStateItem(iStateStr,STATE_RINGING);
	
	return Globals::GetStateItem(iStateStr,dialer_state);
}

TPtrC8 DialerAgent::GetDialerStatusStr()
{
#ifndef __C_PLUS_PLUS_BUILD__
	if ( iSoundTestRunning )return _L8("Say something");
#else
	if ( iSoundTestRunning )return Globals::TempWString("Say something");
#endif
		
	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )return Globals::GetStateItem(iStatusStr,GetDialerStatusSuper());
	if ( iLocalStateMessage == LOCAL_STATE_MESSAGE_CUSTOM )return strCustomMessageText;
	
	return Globals::GetStateItem(iLStatusStr,iLocalStateMessage);
}

TInt DialerAgent::GetCancelMenuAction()
{
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )
	{
		if ( iLocalState == LOCAL_STATE_INITIALIZING || iLocalState == LOCAL_STATE_AUTHENTICATING || iLocalState == LOCAL_STATE_CONNECTING )return CANCEL_MENU_ACTION_RECONNECT;
		
		if ( iLocalStateMessage == LOCAL_STATE_MESSAGE_INVALID_OPERATORCODE || iLocalStateMessage == LOCAL_STATE_MESSAGE_OPERATORCODE_NOT_SET || iLocalStateMessage == LOCAL_STATE_MESSAGE_ACCOUNT_NOT_COMPLETE )return CANCEL_MENU_ACTION_ACCOUNT;
		if ( iLocalStateMessage == LOCAL_STATE_MESSAGE_AUDIO_SERVICE_NOT_FOUND || iLocalStateMessage == LOCAL_STATE_MESSAGE_CODEC_NOT_SUPPORTED )return CANCEL_MENU_ACTION_EXIT;
		if ( iLocalStateMessage == LOCAL_STATE_MESSAGE_INTERNET_ERROR )return CANCEL_MENU_ACTION_RECONNECT;
		
		if ( iDialerUser->IsAccountMenuViewAble() )return CANCEL_MENU_ACTION_ACCOUNT;
		return CANCEL_MENU_ACTION_RECONNECT;
	}
	
	TInt dialer_state = GetDialerStateSuper();
		
	if ( dialer_state == STATE_REGISTERED || dialer_state == STATE_REGISTERING )return CANCEL_MENU_ACTION_CONTACTS;
	if ( dialer_state == STATE_CALLING || dialer_state == STATE_RINGING || dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CALL || dialer_state == STATE_INCOMING_CONNECTED )return CANCEL_MENU_ACTION_TOGGLE_SPEAKER;
	
	TInt dialer_status = GetDialerStatusSuper();
	if ( dialer_status == STATUS_INVALID_USERNAME || dialer_status == STATUS_INVALID_USERNAME_PWD )return CANCEL_MENU_ACTION_ACCOUNT;
	return CANCEL_MENU_ACTION_RECONNECT;
}

TPtrC8 DialerAgent::GetDialerBalanceStr()
{
	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )return strBalance;
	
#ifndef __C_PLUS_PLUS_BUILD__	
	return _L8("");
#else
	return Globals::EmptyWString();
#endif
}

TPtrC8 DialerAgent::GetDialerFooterStr()
{
	return strDialerFooter;
}

TPtrC8 DialerAgent::GetFlexiBalanceStr()
{
	return strFlexiBalance;
}

TPtrC8 DialerAgent::GetFlexiStr()
{
	return tempStrFlexiBalance;
}

TPtrC8 DialerAgent::GetSMSStr()
{
	return strSMSReply;
}

TPtrC8 DialerAgent::GetDialerCallDurationStr()
{
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )
	{
#ifndef __C_PLUS_PLUS_BUILD__
		return _L8("");
#else
		return Globals::EmptyWString();
#endif
	}
	TInt duration = GetDialerCallDurationSuper();
	
	if ( duration < 0 )
	{
#ifndef __C_PLUS_PLUS_BUILD__
		return _L8("");
#else
		return Globals::EmptyWString();
#endif
	}
	
	strTemp.Zero();
	
#ifdef __C_PLUS_PLUS_BUILD__
	if ( duration < 3600 )strTemp.AppendFormat(_L8("%s %02d:%02d"),strDurationDisplayPrefix.GetBuffer(),duration/60,duration%60);
	else strTemp.AppendFormat(_L8("%s %02d:%02d:%02d"),strDurationDisplayPrefix.GetBuffer(),duration/3600,(duration % 3600) / 60, duration % 60);
#else
	if ( duration < 3600 )strTemp.AppendFormat(_L8("%S %02d:%02d"),&strDurationDisplayPrefix,duration/60,duration%60);
	else strTemp.AppendFormat(_L8("%S %02d:%02d:%02d"),&strDurationDisplayPrefix,duration/3600,(duration % 3600) / 60, duration % 60);
#endif
	
	return strTemp;
}

TBool DialerAgent::IsDialerStateStable()
{
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )return EFalse;
	
	if ( IsDialerWaitingForAckSuper() )return EFalse;
	return ETrue;
}

TBool DialerAgent::ShouldShowExitWarning()
{
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )return EFalse;
	
	TInt dialer_state = GetDialerStateSuper();
	if ( dialer_state == STATE_CALLING || dialer_state == STATE_RINGING || dialer_state == STATE_INCOMING_CALL || dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CONNECTED )return ETrue;
	return EFalse;
}

TBool DialerAgent::OnDialerExit()
{
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )return EFalse;
	
	if ( UnRegisterSuper() )
	{
		iLocalState = LOCAL_STATE_UNINITIALIZED;
		return ETrue;
	}
	return EFalse;	
}
