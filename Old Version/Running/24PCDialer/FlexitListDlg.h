#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "json.h"
// FlexitListDlg dialog

class FlexitListDlg : public CDialogEx
{
	DECLARE_DYNAMIC(FlexitListDlg)

public:
	FlexitListDlg(CStringA str);   // standard constructor
	virtual ~FlexitListDlg();

// Dialog Data
	enum { IDD = IDD_FLEXI_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_flexi_list;
	CStringA strMsg;
	CStringA list;
	CStringA listMsg[40];
	int counter;
	BOOL parser(char* source);
	void printer(json_value *value, int ident = 0);
};
