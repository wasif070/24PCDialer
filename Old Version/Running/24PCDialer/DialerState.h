#ifndef __X_STATE_DIALER_ADDED_11_MAY_2011
#define __X_STATE_DIALER_ADDED_11_MAY_2011

class DialerState
{
public:
	enum
	{
		/*WAIT_BEFORE_RETRY_MS = 2000,
		
		STATE_NONE = 0,
		STATE_REGISTERING = 1,
		STATE_REGISTERED = 2,
		STATE_UNREGISTERED = 3,
		STATE_CALLING = 4,
		STATE_RINGING = 5,
		STATE_CONNECTED = 6,
		STATE_INCOMING_CALL = 7,
		STATE_INCOMING_CONNECTED = 8,

		STATUS_NONE = 0,
		STATUS_VPN_BUSY = 1,
		STATUS_CALL_CANCELLED = 2,
		STATUS_CALL_DISCONNECTED = 3,
		STATUS_INCOMING = 4,
		STATUS_CALL_ENDED = 5,
		STATUS_INVALID_USERNAME = 6,
		STATUS_INVALID_USERNAME_PWD = 7,
		STATUS_INVALID_BALANCE = 8,
		STATUS_UNKNOWN_ERROR = 9,
		STATUS_KEEP_ALIVE_ERROR = 10,
		STATUS_MEDIA_SERVER_ERROR = 11,
		STATUS_FORBIDDEN_403 = 12,		
		STATUS_CALLEE_NOT_FOUND = 13,
		STATUS_SERVICE_UNAVAILABLE_503 = 14,
		STATUS_SIP_NO_REPLY = 15,

		LOCAL_STATE_UNINITIALIZED = 0,
		LOCAL_STATE_INITIALIZING,
		LOCAL_STATE_AUTHENTICATE_REQUEST,
		LOCAL_STATE_AUTHENTICATING,
		LOCAL_STATE_CONNECTING,
		LOCAL_STATE_VPN_CONNECTED,

		LOCAL_STATE_MESSAGE_NONE = 0,
		LOCAL_STATE_MESSAGE_INTERNET_ERROR,
		LOCAL_STATE_MESSAGE_AUTH_SERVER_NOT_FOUND,
		LOCAL_STATE_MESSAGE_OPERATORCODE_NOT_SET,
		LOCAL_STATE_MESSAGE_INVALID_OPERATORCODE,
		LOCAL_STATE_MESSAGE_ACCOUNT_NOT_COMPLETE,
		LOCAL_STATE_MESSAGE_SELF_CANCELLED,
		LOCAL_STATE_MESSAGE_VPN_NOT_FOUND,		
		LOCAL_STATE_MESSAGE_CUSTOM,
		LOCAL_STATE_MESSAGE_CODEC_NOT_SUPPORTED,
		LOCAL_STATE_MESSAGE_AUDIO_SERVICE_NOT_FOUND,
		LOCAL_STATE_MESSAGE_UNKNOWN*/
		
		WAIT_BEFORE_RETRY_MS = 2000,
  
		STATE_NONE = 0,
		STATE_REGISTERING = 1,
		STATE_REGISTERED = 2,
		STATE_UNREGISTERED = 3,
		STATE_CALLING = 4,
		STATE_RINGING = 5,
		STATE_CONNECTED = 6,
		STATE_CALL_PROGRESSING = 7,
		STATE_INCOMING_CALL = 8,
		STATE_INCOMING_RINGING = 9,
		STATE_INCOMING_CONNECTED = 10,

		STATUS_NONE = 0,
		STATUS_VPN_BUSY = 1,
		STATUS_CALL_CANCELLED = 2,
		STATUS_CALL_DISCONNECTED = 3,
		STATUS_INCOMING = 4,
		STATUS_CALL_ENDED = 5,
		STATUS_INVALID_USERNAME = 6,
		STATUS_INVALID_USERNAME_PWD = 7,
		STATUS_INVALID_BALANCE = 8,
		STATUS_UNKNOWN_ERROR = 9,
		STATUS_KEEP_ALIVE_ERROR = 10,
		STATUS_MEDIA_SERVER_ERROR = 11,
		STATUS_FORBIDDEN_403 = 12,  
		STATUS_CALLEE_NOT_FOUND = 13,
		STATUS_SERVICE_UNAVAILABLE_503 = 14,
		STATUS_SIP_NO_REPLY = 15,
		STATUS_INTERNET_ERROR = 26,

		LOCAL_STATE_UNINITIALIZED = 0,
		LOCAL_STATE_INITIALIZING,
		LOCAL_STATE_AUTHENTICATE_REQUEST,
		LOCAL_STATE_AUTHENTICATING,
		LOCAL_STATE_CONNECTING,
		LOCAL_STATE_VPN_CONNECTED,

		LOCAL_STATE_MESSAGE_NONE = 0,
		LOCAL_STATE_MESSAGE_INTERNET_ERROR,
		LOCAL_STATE_MESSAGE_AUTH_SERVER_NOT_FOUND,
		LOCAL_STATE_MESSAGE_OPERATORCODE_NOT_SET,
		LOCAL_STATE_MESSAGE_INVALID_OPERATORCODE,
		LOCAL_STATE_MESSAGE_ACCOUNT_NOT_COMPLETE,
		LOCAL_STATE_MESSAGE_SELF_CANCELLED,
		LOCAL_STATE_MESSAGE_VPN_NOT_FOUND,  
		LOCAL_STATE_MESSAGE_CUSTOM,
		LOCAL_STATE_MESSAGE_CODEC_NOT_SUPPORTED,
		LOCAL_STATE_MESSAGE_AUDIO_SERVICE_NOT_FOUND,
		LOCAL_STATE_MESSAGE_UNKNOWN
	};

protected:
		
	TTime iBaseTime;
	TTime iCurrentTime;
	
	TInt  iDialerState;
	TInt  iDialerStatus;

	TUint32 iPingTime;
	TUint32 iLastSendTime;

	TUint32 iCallStartTime;
	TUint32 iCallEndTime;

	CharArray(256,	cDialerResendData);
	CharArray(128,	iSipTransferInfo);	// This string is passed to sip server
	
	int  iDialerSendTime;
	int  iDialerMaxResendCount;
	int  iDialerSeq;

	int  iDialerSendTotal;
	int  iDialerAckTotal;
	int  iDialerOutOfOrderTotal;

	bool bDialerWaitingForAck;

	TUint32 iVpnConnectionNo;
	TUint32 iVpnSessionID;
	TUint32 iVpnTicketIndex;

	int	 iOperatorCode;
	int  iPlatformID;
	int	 iDialerVersion;
	
	CharArray(32,	strVpnTicket);
	CharArray(32,	strImei);
	CharArray(32,	strSipUsername);
	CharArray(32,	strSipHashedPassword);
	CharArray(32,	strSrcPhoneNo);
	CharArray(32,	strDestPhoneNo);	
	CharArray(128,	strAck);
	
	TBool bMediaG729;

	void ResetDialerStateSuper()
	{
		bDialerWaitingForAck = false;
		iDialerState = STATE_NONE;
		iDialerStatus = STATUS_NONE;

		cDialerResendData.Zero();
		
		iDialerMaxResendCount = 3;
		iDialerSeq = 0;

		iDialerSendTotal = 0;
		iDialerAckTotal = 0;
		iDialerOutOfOrderTotal = 0;

		iVpnConnectionNo = 0;
		iVpnSessionID = 0;
		iVpnTicketIndex = 0;

		strDestPhoneNo.Zero();
		
		iCallStartTime = 0;
		iCallEndTime = 0;

		iPingTime = 30;		
		iLastSendTime = CurrentTime();
	}

	void SafeCopy(TDes8& str_dest,TPtrC8 str_src,int not_used_var )
	{
		str_dest.Zero();
		
		if ( str_src.Length() > str_dest.MaxLength() )str_dest.Copy(str_src.Mid(0,str_dest.MaxLength()));
		else str_dest.Copy(str_src);
	}	

public:

	virtual void SendToEstablishedVPN(TPtrC8 c,TInt len,TBool b_retry_data,TUint32 wait_ms) = 0;
	virtual void RemoveRetryData() = 0;
	
	virtual void OnDialerStateChange() = 0;
	virtual void OnCallEnd(TInt duration_second) = 0;

	DialerState()
	{
		iBaseTime.HomeTime();
		ResetDialerStateSuper();
	}	  

	TInt CurrentTimeMS()
	{
		iCurrentTime.HomeTime();
		return iCurrentTime.MicroSecondsFrom(iBaseTime).Int64() / 1000 + 10000;
	}
	
	TInt CurrentTime()
	{
		return CurrentTimeMS() / 1000;
	}
	
	TInt GetDialerStateSuper()
	{
		return iDialerState;
	}
	
	TInt GetDialerStatusSuper()
	{
		return iDialerStatus;
	}
	
	TInt GetDialerCallDurationSuper()
	{
		if ( iDialerState == STATE_CONNECTED || iDialerState == STATE_INCOMING_CONNECTED )return CurrentTime() - iCallStartTime;
		if ( iCallEndTime > iCallStartTime )return iCallEndTime - iCallStartTime;
		return -1;
	}
	
	TBool IsDialerWaitingForAckSuper()
	{
		return ( ( bDialerWaitingForAck ) ? ETrue : EFalse );		
	}
	
	void SendDialerImageSuper()
	{
		iLastSendTime = CurrentTime();
		
		bDialerWaitingForAck = true;
		iDialerSendTime = CurrentTime();
		iDialerSeq += 20;
		iDialerSendTotal++;

		cDialerResendData.Zero();
		
#ifdef __C_PLUS_PLUS_BUILD__
		if ( iDialerState == STATE_REGISTERING )cDialerResendData.AppendFormat(_L8("[REGIMAGE:%u:%u:%u:%u:%u:%u:%u:%u:%u:%u:%s:%s:%s:%s:%s]"),CurrentTimeMS(),iVpnConnectionNo,iVpnSessionID,iDialerSeq,iDialerState,iDialerStatus,iVpnTicketIndex,iOperatorCode,iPlatformID,iDialerVersion,strImei.GetBuffer(),strSipUsername.GetBuffer(),strSipHashedPassword.GetBuffer(),strSrcPhoneNo.GetBuffer(),iSipTransferInfo.GetBuffer());
		else cDialerResendData.AppendFormat(_L8("[IMAGE:%u:%u:%u:%u:%u:%u:%u:%s:]"),CurrentTimeMS(),iVpnConnectionNo,iVpnSessionID,iDialerSeq,iDialerState,iDialerStatus,iCallEndTime-iCallStartTime,strDestPhoneNo.GetBuffer());		
#else
		if ( iDialerState == STATE_REGISTERING )cDialerResendData.AppendFormat(_L8("[REGIMAGE:%u:%u:%u:%u:%u:%u:%u:%u:%u:%u:%S:%S:%S:%S:%S]"),CurrentTimeMS(),iVpnConnectionNo,iVpnSessionID,iDialerSeq,iDialerState,iDialerStatus,iVpnTicketIndex,iOperatorCode,iPlatformID,iDialerVersion,&strImei,&strSipUsername,&strSipHashedPassword,&strSrcPhoneNo,&iSipTransferInfo);
		else cDialerResendData.AppendFormat(_L8("[IMAGE:%u:%u:%u:%u:%u:%u:%u:%S:]"),CurrentTimeMS(),iVpnConnectionNo,iVpnSessionID,iDialerSeq,iDialerState,iDialerStatus,iCallEndTime-iCallStartTime,&strDestPhoneNo);
#endif
		
		RemoveRetryData();
		
		SendToEstablishedVPN(cDialerResendData,cDialerResendData.Length(),EFalse,0);
		for ( TInt i = 1; i < iDialerMaxResendCount; i++ )
		{
			SendToEstablishedVPN(cDialerResendData,cDialerResendData.Length(),ETrue,WAIT_BEFORE_RETRY_MS * i );
		}
		OnDialerStateChange();
	}

	void OnDialerStateTickSuper()
	{
		if ( ( CurrentTime() - iLastSendTime ) >= iPingTime )SendDialerImageSuper();
	}


	TUint32 SessionIDFromTicket(TPtrC8 c,TInt len)
	{
		TUint32 session_id = 0;
		int prev_char = 1;

		for (int i = 0; i < len; i++ )
		{
			int tmp = c[i];

			int cur_char = tmp - 'A' + 1;
			session_id += ( prev_char * cur_char );
			session_id += ( ( i + 3 ) * cur_char );

			prev_char = ( tmp - '8' );
		}
		return (session_id % 100000);
	}


	TBool UnRegisterSuper()
	{
		if ( iDialerState == STATE_NONE || iDialerState == STATE_UNREGISTERED )return EFalse;
		
		iDialerState = STATE_UNREGISTERED;
		iDialerStatus = STATUS_NONE;
	
		SendDialerImageSuper();
		return ETrue;
	}
	
	void RegisterSuper(TInt ticket_index,TPtrC8 str_ticket,TInt operator_code,TInt platform_id,TInt dialer_version,TPtrC8 str_imei,TPtrC8 str_username,TPtrC8 str_hashed_pwd,TPtrC8 str_phone_no)
	{
		if ( iDialerState == STATE_REGISTERING )return;

		iVpnTicketIndex = ticket_index;
		SafeCopy(strVpnTicket,str_ticket,32);

		iVpnConnectionNo = 0;
		iVpnSessionID = SessionIDFromTicket(str_ticket,16);

		iOperatorCode = operator_code;
		iPlatformID = platform_id;
		iDialerVersion = dialer_version;

		SafeCopy(strImei,str_imei,32);
		SafeCopy(strSipUsername,str_username,32);
		SafeCopy(strSipHashedPassword,str_hashed_pwd,32);
		SafeCopy(strSrcPhoneNo,str_phone_no,32);

		iDialerState = STATE_REGISTERING;
		iDialerStatus = STATUS_NONE;
		
		SendDialerImageSuper();
	}

	void InternalCallSuper(TPtrC8 str_dest_no)
	{		
		iCallStartTime = iCallEndTime = 0;

		if ( iDialerState == STATE_REGISTERED );
		else if ( ( iDialerState == STATE_CALLING || iDialerState == STATE_RINGING || iDialerState == STATE_CONNECTED ) && strDestPhoneNo.Compare(str_dest_no) != 0 );
		else return;

		SafeCopy(strDestPhoneNo,str_dest_no,strDestPhoneNo.MaxLength());
		
		iDialerState = STATE_CALLING;
		iDialerStatus = STATUS_NONE;
		
		SendDialerImageSuper();
	}

	void OkPressedSuper()
	{
		if ( iDialerState != STATE_INCOMING_CALL )return;
			
		iDialerState = STATE_INCOMING_CONNECTED;
		iDialerStatus = STATUS_INCOMING;
		
		SendDialerImageSuper();
	}
	
	void CancelPressedSuper()
	{
		if ( iDialerState == STATE_CALLING || iDialerState == STATE_RINGING || iDialerState == STATE_CONNECTED )
		{			
			if ( iDialerState == STATE_CALLING || iDialerState == STATE_RINGING )iDialerStatus = STATUS_CALL_CANCELLED;
			else
			{
				iCallEndTime = CurrentTime();
				OnCallEnd(iCallEndTime - iCallStartTime);
				iDialerStatus = STATUS_CALL_DISCONNECTED;
			}

			iDialerState = STATE_REGISTERED;
		}
		else if ( iDialerState == STATE_INCOMING_CONNECTED || iDialerState == STATE_INCOMING_CALL )
		{
			iDialerState = STATE_REGISTERED;
			iDialerStatus = STATUS_CALL_DISCONNECTED;
		}
		else if ( iDialerState == STATE_REGISTERING )
		{
			iDialerState = STATE_UNREGISTERED;
			iDialerStatus = STATUS_NONE;
		}
		else return;

		SendDialerImageSuper();
	}

	TPtrC8 NextString(TPtrC8 c,TInt& st,TInt len,char separator1,char separator2)
	{
		if ( st >= c.Length() )
		{
#ifndef __C_PLUS_PLUS_BUILD__
			return _L8("");
#else
			return Globals::EmptyWString();
#endif
		}
		
		TInt base = st;
		for ( ; st < c.Length(); st++ )if ( c[st] == separator1 || c[st] == separator2 )break;
		st++;
		return c.Mid(base,st-base-1);
	}

	TUint32 NextInt(TPtrC8 c,TInt& st,TInt len,char separator1,char separator2)
	{
		if ( st >= c.Length() )return 0;
		
		TUint32 ret = 0;
		for ( ; st < c.Length(); st++ )
		{
			if ( c[st] == separator1 || c[st] == separator2 )break;
			if ( c[st] >= '0' && c[st] <= '9' )ret = ret * 10 + c[st] - '0';
		}
		st++;
		return ret;
	}	
	
	void OnReceiveImageSuper(TPtrC8 c,TInt len)
	{
		bool b_ack = false;
		int st = 0;

		if ( len >= 4 && c[0] == 'A' && c[1] == 'C' && c[2] == 'K' && c[3] == ':' )
		{
			b_ack = true;
			st = 4;
		}
		else st = 6;					// IMAGE:

		TUint32 time_stamp = NextInt(c,st,len,':',']');
		TUint32 connection_no = NextInt(c,st,len,':',']');
		TUint32 session_id = NextInt(c,st,len,':',']');
		TUint32 dialer_seq = NextInt(c,st,len,':',']');
		TUint32 dialer_state = NextInt(c,st,len,':',']');
		TUint32 dialer_status = NextInt(c,st,len,':',']');
		TUint32 call_duration = NextInt(c,st,len,':',']');
		
		TPtrC8 dest_phone = NextString(c,st,len,':',']');
		TPtrC8 str_balance = NextString(c,st,len,':',']');
		TPtrC8 str_media = NextString(c,st,len,':',']');		
		
		if ( session_id != iVpnSessionID )return;
		iVpnConnectionNo = connection_no;

		if ( b_ack )
		{
			iDialerAckTotal++;
			if ( dialer_seq == iDialerSeq )
			{
				bDialerWaitingForAck = false;
				RemoveRetryData();
				cDialerResendData.Zero();
			}
			return;
		}

		if ( dialer_seq >= iDialerSeq )
		{
			RemoveRetryData();
			strAck.Zero();
			
			strAck.AppendFormat(_L8("[ACK:%u:%u:%u:%u]"),time_stamp,connection_no,session_id,dialer_seq);
			SendToEstablishedVPN(strAck,strAck.Length(),EFalse,0);
		}
		
		if ( dialer_seq < iDialerSeq )iDialerOutOfOrderTotal++;
		if ( dialer_seq <= iDialerSeq )return;		
		
		iDialerSeq = dialer_seq;
		bDialerWaitingForAck = false;

		if ( iDialerState != dialer_state && ( dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CONNECTED ))
		{
			iCallStartTime = CurrentTime();
		}
		else if ( iDialerState != dialer_state && ( iDialerState == STATE_CONNECTED || iDialerState == STATE_INCOMING_CONNECTED ))
		{
			iCallEndTime = CurrentTime();
			OnCallEnd(iCallEndTime - iCallStartTime);
		}

		iDialerState = dialer_state;
		iDialerStatus = dialer_status;
				
		OnDialerStateChange();		
	}
};

#endif
