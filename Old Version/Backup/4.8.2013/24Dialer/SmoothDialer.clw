; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSmoothDialerDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SmoothDialer.h"

ClassCount=4
Class1=CSmoothDialerApp
Class2=CSmoothDialerDlg

ResourceCount=2
Resource1=IDR_MAINFRAME
Resource2=IDD_SMOOTHDIALER_DIALOG

[CLS:CSmoothDialerApp]
Type=0
HeaderFile=SmoothDialer.h
ImplementationFile=SmoothDialer.cpp
Filter=N

[CLS:CSmoothDialerDlg]
Type=0
HeaderFile=SmoothDialerDlg.h
ImplementationFile=SmoothDialerDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_SMOOTHDIALER_DIALOG]
Type=1
Class=CSmoothDialerDlg
ControlCount=6
Control1=IDC_STATIC_DISPLAY,static,1342308353
Control2=IDC_EDIT_CALL,edit,1350639744
Control3=IDC_BUTTON_CALL,button,1342242817
Control4=IDC_BUTTON_END_CALL,button,1342242816
Control5=IDC_BUTTON_IVR,button,1342242816
Control6=IDC_STATIC_STATUS,static,1342308353

