// Replacement for Symbian's TBuf<>

#ifndef __WSTRING_2_JULY_2012_ADDED__
#define __WSTRING_2_JULY_2012_ADDED__

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

class WString
{
private:
	enum
	{
			WSTRING_MAX_LENGTH = 10000
	};

	unsigned char*	iBuffer;
	int				iAllocation;
	int				iLength;

	void 	ReAllocate(int new_allocation)
	{
		if ( new_allocation <= iAllocation ) return;

		if ( iLength > 0 )
		{
			unsigned char* str_temp = new unsigned char[iLength];
			memcpy(str_temp,iBuffer,iLength);

			delete iBuffer;
			iBuffer = new unsigned char[new_allocation+1];
			memcpy(iBuffer,str_temp,iLength);
			iBuffer[iLength] = 0;

			delete str_temp;
			iAllocation = new_allocation;
			return;
		}

		delete iBuffer;
		iBuffer = new unsigned char[new_allocation];
		iBuffer[iLength] = 0;
		iAllocation = new_allocation;
	}

public:
	WString()
	{
		iAllocation = 10000;
		iBuffer = new unsigned char[iAllocation+1];
		
		iLength = 0;
		iBuffer[iLength] = 0;
	}

	WString(const unsigned char*c, int len)
	{
		iAllocation = len + 100;
		iLength = len;

		iBuffer = new unsigned char[iAllocation+1];
		memcpy(iBuffer,c,iLength);

		iBuffer[iLength] = 0;
	}

	WString(const char*c)		// This makes typecasting possible. for example, WString x = (const WString)"Who are you";
	{
		int len = strlen(c);
		iAllocation = len + 100;
		iLength = len;

		iBuffer = new unsigned char[iAllocation+1];
		memcpy(iBuffer,c,iLength);

		iBuffer[iLength] = 0;
	}

	WString(const WString& str)
	{
		int len = str.iLength;

		iAllocation = len + 100;
		iLength = len;

		iBuffer = new unsigned char[iAllocation+1];
		memcpy(iBuffer,str.iBuffer,iLength);

		iBuffer[iLength] = 0;
	}

	const unsigned char* GetBuffer()
	{
		return iBuffer;
	}

	int Length()
	{
		return iLength;
	}

	void SetLength(int new_length)
	{
		if ( new_length > iAllocation )ReAllocate(new_length);
		iLength = new_length;
		iBuffer[iLength] = 0;
	}
	
	void operator+=(char c)
	{
		if ( iLength >= iAllocation )ReAllocate(iLength * 2+1);
		iBuffer[iLength++] = c;
		iBuffer[iLength] = 0;
	}

	unsigned char operator[](int index)
	{
		return ( ( index < iLength ) ? iBuffer[index] : 0 );
	}

	WString& Mid(int pos,int len)
	{
		return *(new WString(iBuffer + pos,len));
	}

	WString& Mid(int pos)
	{
		return *(new WString(iBuffer + pos,iLength-pos));
	}

	void Zero()
	{
		iLength = 0;
		iBuffer[iLength] = 0;
	}

	void TerminateWithZero()
	{
		iBuffer[iLength] = 0;
	}

	int MaxLength()
	{
		return WSTRING_MAX_LENGTH;
	}

	void Copy(WString& x)
	{
		delete iBuffer;
		
		iLength = x.Length();
		iAllocation = x.Length() + 100;

		iBuffer = new unsigned char[iAllocation+1];
		memcpy(iBuffer,x.GetBuffer(),iLength);
		iBuffer[iLength] = 0;
	}

	void Copy(const char* str1)
	{
		Copy((const unsigned char*)str1,strlen(str1));
	}

	void Copy(const unsigned char* str1,int len)
	{
		iLength = len;

		if ( iLength >= iAllocation )
		{
			delete iBuffer;
			iAllocation = iLength + 100;
			iBuffer = new unsigned char[iAllocation+1];
		}

		memcpy(iBuffer,str1,iLength);
		iBuffer[iLength] = 0;
	}

	void AppendNum(unsigned int val)
	{
		ReAllocate(iLength+10);
		iLength += sprintf((char *)(iBuffer + iLength),"%u",val);
		iBuffer[iLength] = 0;
	}

	void Append(int c)
	{
		ReAllocate(iLength+10);
		iBuffer[iLength++] = c;
		iBuffer[iLength] = 0;
	}

	void Append(const unsigned char* str,int len)
	{
		ReAllocate(iLength + len + 10);
		memcpy(iBuffer + iLength,str,len);
		iLength += len;

		iBuffer[iLength] = 0;
	}

	void Replace(int pos,int len,WString& str)
	{
		Delete(pos,len);
		Insert(pos,str);
	}

	void Delete(int pos,int len)
	{
		if ( ( pos + len ) > iLength )len = iLength - pos;
		if ( len <= 0 )return;

		for ( int i = pos; i < ( iLength - len ); i++ )iBuffer[pos+i] = iBuffer[pos+i+len];
		iLength -= len;
		iBuffer[iLength] = 0;
	}

	void Insert(int pos,WString& str)
	{
		if ( pos > iLength )pos = iLength;

		int len = str.Length();
		ReAllocate(iLength + len + 1);

		int j;
		for ( j = iLength + len - 1; j >= (pos+len); j-- )iBuffer[j] = iBuffer[j-len];
		for ( j = 0; j < len; j++ )iBuffer[pos+j] = str[j];
		iLength += len;
		iBuffer[iLength] = 0;
	}

	void AppendFormat(const char *fmt,...)
	{
		va_list args;
		va_start(args,fmt);

		ReAllocate(iLength + 200);
		iLength += vsprintf((char *)(iBuffer + iLength),fmt,args);
		iBuffer[iLength] = 0;
	}

	void Append(WString& str1)
	{
		ReAllocate(iLength + str1.Length());
		for ( int j = 0; j < str1.Length(); j++ )iBuffer[iLength++] = str1[j];
		iBuffer[iLength] = 0;
	}

	void Append(const char* str)
	{
		int l = strlen(str);
		ReAllocate(iLength + l);
		for ( int j = 0; j < l; j++ )iBuffer[iLength++] = str[j];
		iBuffer[iLength] = 0;
	}

	int Find(WString& str1)
	{
		return Find((const char*)str1.GetBuffer());
	}

	int Find(const char* str1)
	{
		int l = strlen(str1);
		for ( int i = 0; i <= (iLength - l); i++ )
		{
			int j;
			for ( j = 0; j < l; j++ )if ( iBuffer[i+j] != str1[j] )break;
			if ( j < l )continue;

			return i;
		}
		return -1;
	}

	int FindTokenPos(const char* str1)
	{
		int l = strlen(str1);
		for ( int i = 0; i < ( iLength - l - 2); i++ )
		{
			if ( iBuffer[i] != '<' )continue;
			if ( iBuffer[i+1+l] != '>' )continue;

			int j;
			for ( j = 0; j < l; j++ )if ( iBuffer[i+j+1] != str1[j] )break;
			if ( j < l )continue;

			return i;
		}
		return -1;

	}

	int Compare(WString& str1)
	{
		int len = iLength;
		if ( str1.Length() < len )len = str1.Length();

		for ( int i = 0; i < len; i++ )
		{
			if ( iBuffer[i] < str1.iBuffer[i] )return -1;
			if ( iBuffer[i] > str1.iBuffer[i] )return 1;
		}

		if ( iLength == str1.Length() )return 0;
		if ( iLength > str1.Length() )return 1;
		return -1;
	}
};

#endif