// SmoothDialer.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "SmoothDialerDlg.h"
#include "LoginDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerApp

BEGIN_MESSAGE_MAP(CSmoothDialerApp, CWinApp)
	//{{AFX_MSG_MAP(CSmoothDialerApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerApp construction

CSmoothDialerApp::CSmoothDialerApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSmoothDialerApp object

CSmoothDialerApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerApp initialization

BOOL CSmoothDialerApp::InitInstance()
{
	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	LoginDlg ldlg;
	m_pMainWnd = &ldlg;
	int nResponse = ldlg.DoModal();
	/*CSmoothDialerDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();*/
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
