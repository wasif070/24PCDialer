#ifndef __DIALER_AGENT_30_MAY_2011_ADDED__
#define __DIALER_AGENT_30_MAY_2011_ADDED__

#include "Globals.h"
#include "DialerState.h"

#include "AudioEngineSymbian.h"
#include "NetworkSocketSymbian.h"

class DialerAgentUserInterface
{
public:
	virtual void OnDialerAgentError(int error_code,TPtrC8 str_error) = 0;
	virtual void OnDialerAgentChangeState() = 0;
	virtual void OnDialerAgentAuthComplete(TBool is_success,TBool show_settings_form,TBool user_cancelled,TBool internet_error) = 0;
	virtual void OnCallEnd(TInt duration) = 0;
	virtual void OnChangeAccessPoint() = 0;
	virtual TBool IsAccountMenuViewAble() = 0;
	virtual TBool ShowMessage(TPtrC str_title,TPtrC str_message,TBool b_yes_no,TBool b_ok_cancel) = 0;


#ifndef __C_PLUS_PLUS_BUILD__
	virtual TBool ShowMessage(TPtrC str_title,TPtrC8 str_message,TBool b_yes_no,TBool b_ok_cancel) = 0;
#endif
};







class DialerAgent : public DialerState, public NetworkManagerListener
{
protected:
	void OnAccessPointDone(TBool b_success,TInt access_point_id);
	void OnReceiveData(TInt socket_id,TPtrC8 dt,TInt len);	
	void OnSocketClosed(TInt socket_id);
	
public:
	void OnNetworkTick();
	void SetNetworkManager(NetworkManager* network_manager)
	{
		iNetworkManager = network_manager;
	}
private:
	DialerAgentUserInterface *iDialerUser;
	NetworkManager			*iNetworkManager;
	AudioEngineSymbian		*iAudioEngine;

	TInt					iSelectedAccessPointID;
		
public:
	
	enum
	{
		CANCEL_MENU_ACTION_EXIT = 0,			// In case of Audio compatibility error			
		CANCEL_MENU_ACTION_RECONNECT = 1,
		CANCEL_MENU_ACTION_CONTACTS = 2,
		CANCEL_MENU_ACTION_ACCOUNT = 3,
		CANCEL_MENU_ACTION_TOGGLE_SPEAKER = 4,
		
		PIN_NORMAL = 0,
		PIN_FIXED_OPERATOR = 1,
		PIN_FULL_TEST = 2
	};
private:

	enum
	{
		MAX_VPN_COUNT = 3,
		SOCKET_AUTH = 0x80,
		SOCKET_BALANCE = 0x81,
		SOCKET_FEEDBACK = 0x82,
		
		SOCKET_VPN0 = 0x00,		// socket_id = ( SOCKET_VPN0/1/2 << 4 ) + SOCKET_TCP/UDP1/UDP2 
		SOCKET_VPN1 = 0x10,
		SOCKET_VPN2 = 0x20,
		SOCKET_TCP  = 0x03,
		SOCKET_UDP1 = 0x01,
		SOCKET_UDP2 = 0x02,
		
		ALT_CHANNEL_NOT_TESTED = 0,
		ALT_CHANNEL_BEING_TESTED = 1,
		ALT_CHANNEL_CONNECTED = 2,
		ALT_CHANNEL_NOT_CONNECTED = 3,
		
		DATA_TYPE_BASIC = 0,			// State messages
		DATA_TYPE_BASIC_RETRY = 1,
		DATA_TYPE_AUTHENTICATE = 2,
		DATA_TYPE_TICKET = 3,
		DATA_TYPE_CHANNEL_TEST = 4,
		DATA_TYPE_BALANCE_REQUEST = 5,
		DATA_TYPE_RTP = 6
	};

	TInt	iEstablishedVpnSocketID;
	TInt	iEstablishedVpnSocketIDAlt;
	
	TBool	iVpnUdpActive;
	TInt	iAlternateChannelState;		// ALT_CHANNEL_NOT_TESTED, ALT_CHANNEL_CONNECTED, ALT_CHANNEL_NOT_CONNECTED 

	typedef struct
	{
		 TUint32			iAddr;
		 TUint32			iPort;
	}	
	InetAddr;
	
	TInt					iOperatorCode;
	
	TInt					iLocalState;
	TUint32					iLocalStateStartTime;		// Difference between app start time and current time
	TInt					iLocalStateMessage;
	TUint32					iLocalStateMessageStartTime;

	TInt					iLocalStateOptional;		// To handle sub-states
	
	TUint32					iVpnTicketID;

	CharArray(17,strVpnEncDecKey);
	CharArray(512,strTemp);

	TInt			  		iAuthRandom;
	TBool					iConnectionSuccessful;
	TBool					iRtpPausedForGSM;
	TBool					iWaitingForRTPAfterCall;
	
	TUint32					tmCallStart;
	TUint32					tmCallEnd;

	TInt					iAuthStatus;
	TInt					iPrevDialerStateForBalanceUpdate;
	
	CharArray(64,			strAuthStatus);
	CharArray(64,			strBalance);

	TInt					iMaxCallLog;
	TInt					iMaxDialedLog;

	TInt					iAuthenticationTimeout;

	InetAddr				iVpnAddrTcp[MAX_VPN_COUNT];
	InetAddr				iVpnAddrUdp1[MAX_VPN_COUNT];
	InetAddr				iVpnAddrUdp2[MAX_VPN_COUNT];

    TInt					iNetworkTimeout;
    
    TBool					bVpnTcpPreferred;
    TUint32					iVpnConnectTimeoutMS;
    TUint32					iVpnConnectTimeoutMSAlt;
	
	TUint32					iVpnRetryCount;
	TUint32					iVpnUdpTimeout;
	TUint32					iVpnTcpTimeout;

	CharArray(1000,			iRecordBuf );

	CharArray(32,			strBalanceDisplayPrefix);
	CharArray(32,			strDurationDisplayPrefix);
	
	CharArray(32,			strDialedNo);
	CharArray(32,			strSrcPhoneNo);
	CharArray(32,			strIVRNo);

	CharArray(64,			strImei);
	CharArray(32,			strDeviceModel);

	CharArray(512,			strVpnRequest);
	CharArray(128,			strTicketRequest);	
	CharArray(128,			strChannelTestRequest);
	
	TUint32					iRtpBundleSize;			// Goes to VPN. VPN replies with accepted bundle size.
	TUint32					iRtpPaddingSize;

	CharArray(32,			strSipUsername);
	CharArray(32,			strSipPassword);

	CharArray(256,			strBalanceUrl);
	CharArray(32,			strBalancePre);
	CharArray(32,			strBalancePost);
	
	CharArray(256,			strBalanceUrlLocal);			
	TUint32					iSwitchIPLocal;
	TInt					iSwitchPortLocal;

	TBool					iBalanceTCP;	
	TInt					iAdditionalCallTime;
	TBool					iUsernameAsPhoneNo;
	TInt					iOperatorType;

	CharArray(32,			strDialerName);
	CharArray(64,			strDialerFooter);	
	
	TUint32					iDialerTempStatusTimeout;	// In second

	CharArray(64,			strCustomMessageText);
	CharArray(64,			strStateOptionalCustomText);

protected:
	
	CharArray(3000,			iAuthReply);
	CharArray(1000,			iRtpSendBuffer);
	CharArray(1000,			iRtpReceiveBuffer);
	
	CharArray(150,			iStateStr);
	CharArray(500,			iStatusStr);
	CharArray(150,			iLStateStr);
	CharArray(500,			iLStatusStr);
	
	TUint32					iVpnRoundTripMS;
	TUint32					iRtpSendSeq;

	TBool					iSoundTestRunning;	
	
	int  CheckRange(int val,int min,int max);

	void SendToEstablishedVPN(TPtrC8 dt,TInt len,TBool b_retry_data,TUint32 wait_ms);				//no delay
	void RemoveRetryData();
	void SendBalanceRequest();
	
	void SendRtpFromAudioEngine(TPtrC8 dt,TInt len);	
	
public:
	void OnDialerStateChange();
	void OnCallEnd(TInt duration);
	void PauseRtpForGSM();
	void ResumeRtpForGSM();

	TBool IsAudioRunning();
	TBool IsCallLogMenuViewAble()
	{
		 if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )return ETrue;
		 return EFalse;
	}
		
protected:
	void InitStatusMessage();
	CharArray(256,			strVpnMessage);
	TInt  iAuthRetryNo;
	
	void HandleVpnMessage(TPtrC8 dt,TInt len,TInt optional);	//optional is message type

	void StartAltChannelRequest();

	TInt ParseAuthenticationReply(TPtrC8 dt,TInt length);

	void CheckDialerActivity();
	void SetCurrentState(TInt cur_state,TInt current_state_message = LOCAL_STATE_MESSAGE_NONE );

public:

	DialerAgent(DialerAgentUserInterface* dialer_user,AudioEngineSymbian *audio_engine,TPtrC8 str_state,TPtrC8 str_status,TPtrC8 str_lstate,TPtrC8 str_lstatus,TPtrC8 balance_prefix,TPtrC8 duration_prefix);

	bool InitConnections();
	bool Start();
	void OnInternetCancelled();
	void OnAudioServiceError(TBool service_not_found);	// or codec_not_supported
	void OnOperatorCodeNotSelected();
	
	void Call(TPtrC8 dialed_number);
	
	void EndCall();
	bool CanEmptyPhoneNoOnRedKey();
	
	TBool StartSoundTest();
	TUint32 GetRoundTripMS();
	
	TInt   GetSelectedAccessPointID(){return iSelectedAccessPointID;}
	void   SetDefaultAccessPointID(TInt access_point_id){iSelectedAccessPointID = access_point_id;}
	TInt   GetOperatorType(){return iOperatorType;}
	TBool  IsUsernameIsPhoneNo(){return iUsernameAsPhoneNo;}
	
	TPtrC  GetChannelType();
	
	TPtrC8 GetDialerNameStr();
	TPtrC8 GetDialerStateStr();
	TPtrC8 GetDialerStatusStr();
	TPtrC8 GetDialerBalanceStr();
	TPtrC8 GetDialerFooterStr();
	TPtrC8 GetDialerCallDurationStr();
	TBool  IsDialerStateStable();
	
	TInt   GetCancelMenuAction();
	TBool  ShouldShowExitWarning();
	TBool  OnDialerExit();	// Returns ETrue if the AppUi has to wait 1 second before exit (needed to UNREGISTER)
	
	void Reset(TBool show_accesspoint_menu,TInt operator_code,TPtrC str_user,TPtrC str_password,TPtrC str_src_phone,TPtrC str_balance_url,TUint32 switch_ip,TInt switch_port,TPtrC8 str_imei,TPtrC8 str_device_model);
	TPtrC8 GetIVRNo(){return strIVRNo;}
	
	int GetMaxDialLog(){return iMaxDialedLog;}
	int GetMaxCallLog(){return iMaxCallLog;}

	void Authenticate(TInt retry_no = 0);
	TBool  SendFeedbackToAuthServer(TPtrC8 dt);
};

#endif

