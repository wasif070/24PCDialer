// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "LoginDlg.h"
#include "afxdialogex.h"
#include "SmoothDialerDlg.h"
#include <afxwin.h>



// LoginDlg dialog

IMPLEMENT_DYNAMIC(LoginDlg, CDialogEx)

LoginDlg::LoginDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(LoginDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	// CBrush::CBrush(CBitmap* pBitmap)
    CBitmap bmp;
    // Load a resource bitmap.
    bmp.LoadBitmap(IDB_BITMAP_MAIN);
    m_pEditBkBrush = new CBrush(&bmp);	  
	//m_pEditBkBrush = new CBrush(RGB(255,255,100));
}

LoginDlg::~LoginDlg()
{
}


BOOL LoginDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	m_button_ok.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_OK)));
	m_button_cancel.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_CANCEL)));

	/////////Read UserInfo
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("UserInfo.dat");
	int counter = 0;
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		counter ++;
		if(counter==1)
			SetDlgItemText(IDC_OPCODE,strLine);
		if(counter==2)
			SetDlgItemText(IDC_USER,strLine);
		if(counter==3)
			SetDlgItemText(IDC_PASSWORD,strLine);
		}
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void LoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_button_ok);
	DDX_Control(pDX, IDC_BUTTON_CANCEL, m_button_cancel);
}


BEGIN_MESSAGE_MAP(LoginDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &LoginDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &LoginDlg::OnBnClickedButtonCancel)
END_MESSAGE_MAP()

void LoginDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
	//Sample 01: Required Declarations
     CDC MemDC ;
     CBitmap bmp ;
     CPaintDC dc(this);
                                  
     //Sample 02: Get the Client co-ordinates
     CRect rct;
     this->GetClientRect(&rct);

     //Sample 03: Create the Dialog Compatible DC in the memory,
     //Then load the bitmap to the memory.
     MemDC.CreateCompatibleDC ( &dc ) ;
     bmp.LoadBitmap ( IDB_BITMAP_MAIN ) ;
     MemDC.SelectObject ( &bmp ) ;

     //Sample 04: Finally perform Bit Block Transfer (Blt) from memory dc to
     //dialog surface.
     dc.BitBlt ( 0, 0, rct.Width() , rct.Height() , &MemDC, 0, 0, SRCCOPY ) ;

	CDialog::OnPaint();
	}
}

HBRUSH LoginDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(0, 0, 0));
		//pDC->SetBkColor(RGB(255,255,100));
		pDC->SetBkMode(TRANSPARENT);

       //return (HBRUSH)GetStockObject(NULL_BRUSH);
	   return (HBRUSH)(m_pEditBkBrush->GetSafeHandle());
    default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

void LoginDlg::OnDestroy()
{
          CDialog::OnDestroy();

          // Free the space allocated for the background brush
          delete m_pEditBkBrush;
}

// LoginDlg message handlers


void LoginDlg::OnBnClickedOk()
{

	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_CHECK_SAVEINFO);
	int ChkBox = m_ctlCheck->GetCheck();

	GetDlgItemText(IDC_OPCODE,opcode);
	GetDlgItemText(IDC_USER,user);
	GetDlgItemText(IDC_PASSWORD, password);

	 if(ChkBox == BST_CHECKED){
		//Write User Info
		CString strLine;
		CStdioFile writeToFile;
		CFileException fileException;
		CString strFilePath = _T("UserInfo.dat");
		CFileStatus status;
		//int counter = 0;
		if( CFile::GetStatus( strFilePath, status ) ){
		 // Open the file without the Create flag
		CFile::Remove(strFilePath);
		writeToFile.Open( strFilePath, CFile:: modeCreate | CFile::modeWrite ), &fileException;
		writeToFile.Seek (0, CFile :: begin);	
		writeToFile.WriteString(opcode + "\n" + user + "\n" + password);
		}else{
		// Open the file with the Create flag
		writeToFile.Open( strFilePath, CFile::modeCreate | CFile::modeWrite ), &fileException;
		writeToFile.Seek (0, CFile :: begin);
		writeToFile.WriteString(opcode + "\n" + user + "\n" + password);
		}
		writeToFile.Close();
		////////End User Info
	 }

	CSmoothDialerDlg dlg(this);
	dlg.opcode = atoi(opcode);
	dlg.user.Copy(user);
	dlg.password.Copy(password);
	if(opcode=="" || user=="" || password==""){
	    int Answer;
   		Answer = AfxMessageBox("Opcode/ User/ Password can not be empty",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);

		if( Answer == IDOK ){
		EndDialog(1);
		LoginDlg ldlg(this);
		ldlg.DoModal();
		}
		else // if( Answer == IDNO )
		EndDialog(1);
	}else{
		EndDialog(1);
		dlg.DoModal();
	}

	CDialogEx::OnOK();
}


void LoginDlg::OnBnClickedButtonCancel()
{
	exit(1);
}
