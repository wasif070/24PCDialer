// ContactsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ContactsDlg.h"
#include "afxdialogex.h"
#include "SmoothDialerDlg.h"
#include "SmsSendDlg.h"
#include "FlexiAccount.h"
#include "RechageDlg.h"

// ContactsDlg dialog

IMPLEMENT_DYNAMIC(ContactsDlg, CDialogEx)

	ContactsDlg::ContactsDlg(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(ContactsDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
}

ContactsDlg::~ContactsDlg()
{
}

void ContactsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CONTACTS_LIST, m_contacts_list);
}


BEGIN_MESSAGE_MAP(ContactsDlg, CDialogEx)
	ON_BN_CLICKED(ID_SAVE, &ContactsDlg::OnBnClickedSave)
	ON_NOTIFY(NM_DBLCLK, IDC_CONTACTS_LIST, &ContactsDlg::OnNMDblclkContactsList)
	ON_NOTIFY(NM_CLICK, IDC_CONTACTS_LIST, &ContactsDlg::OnNMClickContactsList)
	ON_BN_CLICKED(ID_REMOVE, &ContactsDlg::OnBnClickedRemove)
	ON_BN_CLICKED(IDC_NEW, &ContactsDlg::OnBnClickedNew)
	ON_BN_CLICKED(IDC_BUTTON3, &ContactsDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON1, &ContactsDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &ContactsDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON4, &ContactsDlg::OnBnClickedButton4)
END_MESSAGE_MAP()

static void AddData(CListCtrl &ctrl, int row, int col, const char *str)
{
    LVITEM lv;
    lv.iItem = row;
    lv.iSubItem = col;
	lv.pszText = (LPWSTR) str;
    lv.mask = LVIF_TEXT;
    if(col == 0)
        ctrl.InsertItem(&lv);
    else
        ctrl.SetItem(&lv);   
}

static void ReadFileContents(CListCtrl &listCtrl){
	listCtrl.DeleteAllItems();
	//Read ContactsInfo
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("ContactsInfo.dat");
	int counter = 0;

	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		int nTokenPos = 0;
		int pos = 0;
		CStringA strToken(strLine.Tokenize(_T("\t"), nTokenPos));
		AddData(listCtrl,counter,pos, strToken);
		while (!strToken.IsEmpty()){
			pos++;
			strToken = strLine.Tokenize(_T("\t"), nTokenPos);
			AddData(listCtrl,counter,pos, strToken);
			}
	counter++;
		}
	readFile.Close();
	listCtrl.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);
	}
	//End ContactsInfo
}

// ContactsDlg message handlers
BOOL ContactsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_contacts_list.InsertColumn(0, L"Name");
    m_contacts_list.SetColumnWidth(0, 80);

    m_contacts_list.InsertColumn(1, L"Phone Number");
    m_contacts_list.SetColumnWidth(1, 100);
    
    m_contacts_list.InsertColumn(2, L"Address");
    m_contacts_list.SetColumnWidth(2, 150);

	m_contacts_list.InsertColumn(3, L"E-mail");
    m_contacts_list.SetColumnWidth(3, 100);

	ReadFileContents(m_contacts_list);

	return TRUE;  // return TRUE  unless you set the focus to a control
}




void ContactsDlg::OnBnClickedSave()
{
	CString strLine;
	CStdioFile writeToFile;
	CFileException fileException;
	CString strFilePath = _T("ContactsInfo.dat");
	CFileStatus status;
	CString strName, strNum, strAddress, strMail;

	int row = m_contacts_list.GetSelectionMark();
	if(row < 0){
		GetDlgItemText(IDC_CONTACTS_NAME_EDIT, strName);
		GetDlgItemText(IDC_CONTACTS_NUMBER_EDIT, strNum);
		GetDlgItemText(IDC_CONTACTS_ADDRESS_EDIT, strAddress);
		GetDlgItemText(IDC_CONTACTS_EMAIL, strMail);

		if(strName.IsEmpty() || strNum.IsEmpty()){
			AfxMessageBox(L"Contact Name/ Number can not be empty.");
		}else{
			/*AddData(m_contacts_list,0,0, strName);
			AddData(m_contacts_list,0,1, strNum);
			AddData(m_contacts_list,0,2, strAddress);
			AddData(m_contacts_list,0,3, strMail);
			*/
			SetDlgItemText(IDC_CONTACTS_NAME_EDIT, L"");
			SetDlgItemText(IDC_CONTACTS_NUMBER_EDIT, L"");
			SetDlgItemText(IDC_CONTACTS_ADDRESS_EDIT, L"");
			SetDlgItemText(IDC_CONTACTS_EMAIL, L"");

			//Write Contacts Info
			if( CFile::GetStatus( strFilePath, status ) ){
				writeToFile.Open( strFilePath, CFile:: modeWrite ), &fileException;
			}else{
				writeToFile.Open( strFilePath, CFile::modeCreate | CFile::modeWrite ), &fileException;
			}
			writeToFile.Seek (0, CFile :: end);
			writeToFile.WriteString(strName + "\t" + strNum + "\t" + strAddress + "\t" + strMail + "\n");			
			writeToFile.Close();
			//End Contacts Info
			}
		}else{

		GetDlgItemText(IDC_CONTACTS_NAME_EDIT, strName);
		GetDlgItemText(IDC_CONTACTS_NUMBER_EDIT, strNum);
		GetDlgItemText(IDC_CONTACTS_ADDRESS_EDIT, strAddress);
		GetDlgItemText(IDC_CONTACTS_EMAIL, strMail);
		CString strFile;
		if (writeToFile.Open(strFilePath, CFile::modeReadWrite, &fileException)){
		int counter = 0;
		while (writeToFile.ReadString(strLine)){
			if(counter == row){
					strLine = strName + "\t" + strNum + "\t" + strAddress + "\t" + strMail + "\n";
					strFile += strLine;		
				}else{
					strFile += strLine + "\n";
				}
			counter++;
			}
		writeToFile.Close();
		writeToFile.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite, &fileException);
		writeToFile.Seek (0, CFile :: begin);
		writeToFile.WriteString(strFile);
		writeToFile.Close();

		SetDlgItemText(IDC_CONTACTS_NAME_EDIT, L"");
	    SetDlgItemText(IDC_CONTACTS_NUMBER_EDIT, L"");
		SetDlgItemText(IDC_CONTACTS_ADDRESS_EDIT, L"");
		SetDlgItemText(IDC_CONTACTS_EMAIL, L"");
		}		
	}
	ReadFileContents(m_contacts_list);
}




void ContactsDlg::OnNMDblclkContactsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	EndDialog(1);
	*pResult = 0;
	 int row = m_contacts_list.GetSelectionMark();
    if(row < 0)
        return;
    CString str_dest_no = m_contacts_list.GetItemText(row, 1); // Contact's phone number	
	m_pParentWnd -> SetDlgItemText(IDC_COMBO_CALL, str_dest_no);

	str_dest_no.TrimLeft();
	str_dest_no.TrimRight();
	if ( str_dest_no.IsEmpty() )return;
	CStringA str_dest_no_str(str_dest_no);
	iDialerAgent -> Call(str_dest_no_str.GetBuffer(str_dest_no.GetLength()));
}

std::wstring s2ws(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
}


void ContactsDlg::OnNMClickContactsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
	 int row = m_contacts_list.GetSelectionMark();
    if(row < 0)
        return;
    CString strName = m_contacts_list.GetItemText(row, 0); // Contact's Name	
	SetDlgItemText(IDC_CONTACTS_NAME_EDIT, strName);
	CString strNum = m_contacts_list.GetItemText(row, 1); // Contact's phone number
	SetDlgItemText(IDC_CONTACTS_NUMBER_EDIT, strNum);
	CString strAddress = m_contacts_list.GetItemText(row, 2); // Contact's Address
	SetDlgItemText(IDC_CONTACTS_ADDRESS_EDIT, strAddress);
	CString strMail = m_contacts_list.GetItemText(row, 3); // Contact's E-mail
	SetDlgItemText(IDC_CONTACTS_EMAIL, strMail);
}


void ContactsDlg::OnBnClickedRemove()
{
	int row = m_contacts_list.GetSelectionMark();
    if(row < 0)
        return;
	m_contacts_list.DeleteItem(row);

	CString strFile;
	CString strLine;
	CStdioFile writeToFile;
	CFileException fileException;
	CString strFilePath = _T("ContactsInfo.dat");
	CFileStatus status;

	if (writeToFile.Open(strFilePath, CFile::modeReadWrite, &fileException)){
	int counter = 0;
	while (writeToFile.ReadString(strLine)){
		if(counter == row){			
				strFile += "";		
			}else{
				strFile += strLine + "\n";
			}
			counter++;
		}
		writeToFile.Close();
		writeToFile.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite, &fileException);
		writeToFile.Seek (0, CFile :: begin);
		writeToFile.WriteString(strFile);
		writeToFile.Close();

		SetDlgItemText(IDC_CONTACTS_NAME_EDIT, L"");
	    SetDlgItemText(IDC_CONTACTS_NUMBER_EDIT, L"");
		SetDlgItemText(IDC_CONTACTS_ADDRESS_EDIT, L"");
		SetDlgItemText(IDC_CONTACTS_EMAIL, L"");

		ReadFileContents(m_contacts_list);
	}
}


void ContactsDlg::OnBnClickedNew()
{
	m_contacts_list.SetSelectionMark(-1);
	SetDlgItemText(IDC_CONTACTS_NAME_EDIT, L"");
	SetDlgItemText(IDC_CONTACTS_NUMBER_EDIT, L"");
	SetDlgItemText(IDC_CONTACTS_ADDRESS_EDIT, L"");
	SetDlgItemText(IDC_CONTACTS_EMAIL, L"");
}


void ContactsDlg::OnBnClickedButton3()
{
	EndDialog(1);
}


void ContactsDlg::OnBnClickedButton1()
{

	// TODO: Add your control notification handler code here

	int row = m_contacts_list.GetSelectionMark();
    if(row < 0)
        return;
    CString str_dest_no = m_contacts_list.GetItemText(row, 1); // Contact's phone number	
	m_pParentWnd -> SetDlgItemText(IDC_COMBO_CALL, str_dest_no);

	str_dest_no.TrimLeft();
	str_dest_no.TrimRight();
	if ( str_dest_no.IsEmpty() )return;
	CStringA str_dest_no_str(str_dest_no);
	iDialerAgent -> Call(str_dest_no_str.GetBuffer(str_dest_no.GetLength()));
	EndDialog(1);
}


void ContactsDlg::OnBnClickedButton2()
{
 if(iDialerAgent -> GetFlexiGateway().Length() > 0){
	int row = m_contacts_list.GetSelectionMark();
    if(row < 0)
        return;
    CString str_dest_no = m_contacts_list.GetItemText(row, 1); // Contact's phone number
	str_dest_no.TrimLeft();
	str_dest_no.TrimRight();
	if ( str_dest_no.IsEmpty() )return;
	CString flexiUser;
	CString flexiPassword;
	//Read UserInfo
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("UserInfo.dat");
	int counter = 0;
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		counter ++;
		if(counter==4)
			flexiUser = strLine;
		if(counter==5)
			flexiPassword = strLine;
		}
	readFile.Close();
	}
	//End UserInfo
	EndDialog(1);
	if(flexiUser != "" || flexiPassword!= ""){
		SmsSendDlg sdlg(this, iDialerAgent);
		sdlg.flexiUser = flexiUser;
		sdlg.flexiPassword = flexiPassword;
		sdlg.phoneNum = str_dest_no;
		sdlg.DoModal();
	}else{
		FlexiAccount fdlg(this, iDialerAgent);
		fdlg.DoModal();
	}
 }else{
	 MessageBox(L"Please contact with your service provider.", L"SMS/TopUp Features" , MB_ICONINFORMATION | MB_OK);
 }
	
}


void ContactsDlg::OnBnClickedButton4()
{
  if(iDialerAgent -> GetFlexiGateway().Length() > 0){
	int row = m_contacts_list.GetSelectionMark();
    if(row < 0)
        return;
    CString str_dest_no = m_contacts_list.GetItemText(row, 1); // Contact's phone number
	str_dest_no.TrimLeft();
	str_dest_no.TrimRight();
	if ( str_dest_no.IsEmpty() )return;

	CString flexiUser;
	CString flexiPassword;
	//Read UserInfo
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("UserInfo.dat");
	int counter = 0;
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		counter ++;
		if(counter==4)
			flexiUser = strLine;
		if(counter==5)
			flexiPassword = strLine;
		}
	readFile.Close();
	}
	//End UserInfo
	EndDialog(1);
	if(flexiUser != "" || flexiPassword!= ""){
		RechageDlg rdlg(this, iDialerAgent);
		rdlg.flexiUser = flexiUser;
		rdlg.flexiPassword = flexiPassword;
		rdlg.phoneNum = str_dest_no;
		rdlg.DoModal();
	}else{
		FlexiAccount fdlg(this, iDialerAgent);
		fdlg.DoModal();
	}
  }else{
	 MessageBox(L"Please contact with your service provider.", L"SMS/TopUp Features" , MB_ICONINFORMATION | MB_OK);
 }
}

