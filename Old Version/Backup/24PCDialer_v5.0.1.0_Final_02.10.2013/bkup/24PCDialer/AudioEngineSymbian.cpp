#include "stdafx.h"
#include "AudioEngineSymbian.h"

TTime AudioEngineSymbian::iBaseTime;
TTime AudioEngineSymbian::iCurrentTime;

AudioEngineSymbian::AudioEngineSymbian()
{
	iRtpBundleSize = 200;
	iPacketSizeDecoded = 162;
	
	iBaseTime.HomeTime();

	iConfigured = EFalse;			// this is not set false at Create().

	iVolumePercentage = 100;
	iLoudSpeaker = EFalse;

	/*iPlayQueue.SetLength(999);
	iPlayQueue.SetLength(0);

	iPlayQueueTemp.SetLength(999);
	iPlayQueueTemp.SetLength(0);

	iRecordQueue.SetLength(500);
	iRecordQueue.SetLength(0);*/
}

AudioEngineSymbian::~AudioEngineSymbian()
{
	if ( iConfigured == EFalse )return;
}

TUint32 AudioEngineSymbian::GetCurrentTimeMS()
{
	iCurrentTime.HomeTime();
	return iCurrentTime.MicroSecondsFrom(iBaseTime).Int64() / 1000 + 10000;
}

TUint32 AudioEngineSymbian::GetCurrentTimeMicroSecond()
{
	iCurrentTime.HomeTime();
	return (TUint32)( iCurrentTime.MicroSecondsFrom(iBaseTime).Int64() & 0xFFFFFFFF );
}

TInt AudioEngineSymbian::Configure()
{
	if ( iConfigured != EFalse )return ETrue;

	iAudioThread = new CAudioThread(this,iHwnd);
	if ( !iAudioThread->Init(true,iStatViewer) )
	{
		AfxMessageBox(L"Cannot start audio");
		return FALSE;
	}

	iSelectedCodec = 0;					// ECodecNone or ECodecNULL

	iConfigured = ETrue;

	ResetRtpAnalyzer();
	
	return KErrNone;
}

TInt AudioEngineSymbian::PlayRingTone(TPtrC8 str_path)
{
	return 0;
}

TInt AudioEngineSymbian::StopRingTone()
{
	return 0;
}

void AudioEngineSymbian::ForceStop()
{
	if ( iConfigured == EFalse )return;
}

void AudioEngineSymbian::SetRtpBundleSize(TInt bundle_size)
{
	iRtpBundleSize = bundle_size;
	ResetRtpAnalyzer();	
}

TBool AudioEngineSymbian::OnNetworkTick(TDes8& recorded_buf)
{
	OnPlayTick();
	
	recorded_buf.Zero();
	if ( iConfigured == EFalse )return EFalse;

	if ( iRecordQueue.Length() >= ( iRtpBundleSize - 80 ) && iRecordQueue.Length() >= 160 )
	{
		TInt rec_len = iRecordQueue.Length();
		if ( rec_len > iRtpBundleSize )rec_len = iRtpBundleSize;
		
		if ( rec_len % 20 )rec_len -= ( rec_len % 20 );
		
		if ( iSessionRecordSize == 0 )
		{
			recorded_buf.Copy(iRecordQueue.Mid(iRecordQueue.Length() - rec_len,rec_len));
			iRecordQueue.Zero();			
		}
		else 
		{
			if ( iRecordQueue.Length() > iRtpBundleSize )
			{
				TInt delete_percentage = ( ( iRecordQueue.Length() - iRtpBundleSize + iSessionRecordDropSize ) * 100 ) / ( iSessionRecordSize + rec_len );
				if ( delete_percentage < 5 )
				{
					iRecordQueue.Delete(0,iRecordQueue.Length() - iRtpBundleSize );
					iSessionRecordDropSize += ( iRecordQueue.Length() - iRtpBundleSize );
				}
			}
			
			recorded_buf.Copy(iRecordQueue.Mid(0,rec_len));
			
			if ( rec_len == iRecordQueue.Length())iRecordQueue.Zero();
			else iRecordQueue.Delete(0,rec_len);
		}
		
		iSessionRecordSize += rec_len;
	}
	
	if ( recorded_buf.Length() > 0 )return ETrue;
	return EFalse;
}

void AudioEngineSymbian::PlayRtp(TPtrC8 dt)
{
	if ( iConfigured == EFalse )return;

	iFillBufferLock = ETrue;

	iPlayQueueTemp.Zero();	
	for ( TInt i = 0; i < dt.Length(); i++ )
	{
		TInt l = dt[i];
		if ( ( i + l + 1 ) > dt.Length() )break;
		if ( l % 10 ){i += l; continue;}
		
		if ( ( iPlayQueueTemp.Length() + l ) < iPlayQueueTemp.MaxLength() )iPlayQueueTemp.Append(dt.Mid(i+1,l));
		i += l;
	}

	
	if ( iPlayQueueTemp.Length() == 0 || iPlayQueueTemp.Length() > iPlayQueue.MaxLength() )
	{
		iFillBufferLock = EFalse;
		return;
	}

	if ( ( iPlayQueue.Length() + iPlayQueueTemp.Length() ) > SESSION_PLAY_QUEUE_THRESHOLD_MS )
	{
		TInt delete_len = iPlayQueue.Length() + iPlayQueueTemp.Length() - SESSION_PLAY_QUEUE_THRESHOLD_MS;
		iSessionDropSize += delete_len;
		iPlayQueue.Delete(0,delete_len);
	}
	
	iPlayQueue.Append(iPlayQueueTemp);
	OnPlayTick();

	iFillBufferLock = EFalse;
	return;
}

void AudioEngineSymbian::OnPlayTick()
{
	if ( iPlayQueue.Length() <= 0 )return;

}

void AudioEngineSymbian::SetSpeaker(TBool b_speaker)
{
	iLoudSpeaker = b_speaker;
	if ( iConfigured == EFalse )return;
}

TBool AudioEngineSymbian::GetSpeaker()
{
	return iLoudSpeaker;
}

void AudioEngineSymbian::SetVolume(TInt vol_percentage)
{
	if ( vol_percentage >= 100 )vol_percentage = 100;
	iVolumePercentage = vol_percentage;
}

TInt AudioEngineSymbian::GetVolume()
{
	return iVolumePercentage;
}

TPtrC AudioEngineSymbian::GetCodecName(TBool b_aps,TInt codec_id)
{
	return "PCM16";
}

void AudioEngineSymbian::ResetRtpAnalyzer()
{
	iSessionPlayStarted = EFalse;
	
	iSessionRecordCountOriginal = 0;
	iSessionRecordDropSize = 0;
	iSessionRecordSize = 0;
	iSessionRecordSizeOriginal = 0;
	
	iSessionPlaySize = 0;
	iSessionDropSize = 0;
	
	iPlayQueue.Zero();
	iRecordQueue.Zero();

}

void AudioEngineSymbian::ResetAudioBuffer()
{
	ResetRtpAnalyzer();
}

void AudioEngineSymbian::GetLog(TDes& str_log)
{
}
