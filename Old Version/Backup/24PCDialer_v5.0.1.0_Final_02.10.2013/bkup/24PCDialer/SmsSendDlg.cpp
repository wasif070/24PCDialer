// SmsSendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmsSendDlg.h"
#include "afxdialogex.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"




// SmsSendDlg dialog

IMPLEMENT_DYNAMIC(SmsSendDlg, CDialogEx)

SmsSendDlg::SmsSendDlg(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(SmsSendDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
}

SmsSendDlg::~SmsSendDlg()
{
}

BOOL SmsSendDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	CStatic * m_Label;
	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(100, L"Cambria Bold");
	m_Label = (CStatic *)GetDlgItem(IDC_FLEXI_BALANCE_STATIC);
	m_Label->SetFont(m_Font1);

	//CreateWindowW( L"EDIT", L"", WS_CHILD|WS_VISIBLE, 10, 10, 50, 20, GetSafeHwnd(), 0, 0, 0 );
	//CreateWindow( "EDIT", "", WS_CHILD|WS_VISIBLE, 10, 10, 50, 20, GetSafeHwnd(), 0, 0, 0 );
	/*
	//Read UserInfo
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("UserInfo.dat");
	int counter = 0;
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		counter ++;
		if(counter==4)
			flexiUser = strLine;
		if(counter==5)
			flexiPassword = strLine;
		}
	readFile.Close();
	}
	//End UserInfo
	*/
	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format(L"%f", sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + flexiPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	iDialerAgent -> FlexiAuthenticate(flexiUser , reqKey, hash.c_str());

	while(iDialerAgent ->GetFlexiBalanceStr().Length()==0){		
	}
	CString flexiReply = iDialerAgent ->GetFlexiBalanceStr().GetBuffer();
	//if(flexiReply.Mid(0,7)=="Welcome" || flexiReply.Mid(0,6)=="Access"){
		SetDlgItemText(IDC_FLEXI_BALANCE_STATIC, flexiReply);
	//}
	if(phoneNum.GetLength()>0){
		SetDlgItemText(IDC_SMS_PHONE_NUM_EDIT, phoneNum);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void SmsSendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(SmsSendDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &SmsSendDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &SmsSendDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// SmsSendDlg message handlers

std::string expand(const char* p)
{
    std::string result;
    while (*p)
        if (p[1] == '-' && p[2])
        {
            for (int c = p[0]; c <= p[2]; ++c)
                result += (char)c;
            p += 3;
        }
        else
            result += *p++;
    return result;
}

// Test if the given string has anything not in A-Za-z0-9_
bool HasSpecialCharacters(char *str)
{
    return str[strspn(str, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_")] != 0;
}



void SmsSendDlg::OnBnClickedOk()
{
	CString phnNum;
	CString msg;
	GetDlgItemText(IDC_SMS_PHONE_NUM_EDIT,phnNum);
	GetDlgItemText(IDC_SMS_SEND_EDIT,msg);


	if(phnNum==""){
		int Answer;
   		Answer = AfxMessageBox(L"Phone Number can not be empty",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);
	}else{

		SYSTEMTIME timeInMillis;
		GetSystemTime(&timeInMillis);	
		double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
		CString reqKey;
		reqKey.Format(L"%f", sysTime);

		std::string hash;
		crypto::errorinfo_t lasterror;
		CT2A atext(reqKey + flexiPassword);

		crypto::md5_helper_t hhelper;
		hash = hhelper.hexdigesttext(atext.m_szBuffer);
		lasterror = hhelper.lasterror();
		int index;
		//expand("A-Za-z0-9_"); 
		CString temp;
		for(int i=0; i<msg.GetLength(); i++){
			//if(IsCharAlphaNumeric(msg[i])){
			//	temp += msg[i];
			//}else{
				char hexval[4];
				index = i;
				int intval = msg[i];
				sprintf(hexval,"%04X",intval);
				i = index;
				temp += "%";
				temp += hexval;
			//}			
		}
		//temp = "U+0995";
		AfxMessageBox(temp);
		iDialerAgent -> SendSMSMessage(flexiUser , phnNum.Trim(), temp, reqKey, hash.c_str());
	}

	CDialogEx::OnOK();
}


void SmsSendDlg::OnBnClickedCancel()
{
	EndDialog(1);
	CDialogEx::OnCancel();
}


/*void SmsSendDlg::OnCbnDropdownComboCountryCode()
{
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("CountryCode.dat");
	//str_dest_no.Mid(0, str_dest_no.GetLength()-1
	m_combo_country_code.ResetContent();
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		m_combo_country_code.AddString(strLine);
	}
		readFile.Close();
	}
}*/
