#include "stdafx.h"
#include "AudioThread.h"

CAudioThread *CAudioThread::this_pointer;

unsigned char* CAudioThread::G729ToPcmu(unsigned char *str)
{
	bool b2 = EasyG729A_decoder(hDecoder,str,synth);
	if ( !b2 )return NULL;

	for ( int j = 0; j < 80; j++ )synthx[j] = (unsigned char)(( synth[j] >> 8) + 128);
	return synthx;
}

unsigned short* CAudioThread::G729ToPcm(unsigned char *str)
{
	if ( !EasyG729A_decoder(hDecoder,str,synth) )return NULL;
	return (unsigned short*)synth;
}

void CAudioThread::PlayBuffer(bool b_g729,const unsigned char *c,int len)
{
	if ( !b_g729 ) //pcmu
	{
		if ( ( len % 80 ) != 0 )len -= ( len % 80 );
		if ( len < 80 )return;

		memcpy(strPlayBuffer + iPlayBufferLength,c,len);
		iPlayBufferLength += len;
		return;
	}

	if ( ( len % 10 ) != 0 )len -= ( len % 10 );
	if ( len < 10 )return;

	for ( int i = 0; i < len; i += 10 )
	{
		bool b2 = EasyG729A_decoder(hDecoder,(unsigned char *)(c + i), synth );
		if ( !b2 )
		{
//			if ( iStatusViewer != NULL )iStatusViewer->SetWindowText("Error in decoding received G729 data");
			continue;
		}
		for ( int j = 0; j < 80; j++ )synthx[j] = (unsigned char)(( synth[j] >> 8) + 128);

		memcpy(strPlayBuffer + iPlayBufferLength,synthx,80);
		iPlayBufferLength += 80;
	}
}

void CAudioThread::RunAudioRecorder()
{
	AllocRecordBuffer();
	if ( !InitAudioRecorder())
	{
		//AfxMessageBox("Error: Cannot init audio recorder !!!");
		AfxMessageBox(L"Error: No audio recorder found !!!");
		return;
	}

	hEncoder = EasyG729A_init_encoder();
	bRunning = true;
}

void CAudioThread::RunAudioPlayer()
{
	AllocPlayBuffer();

	if ( !InitAudioPlayer())
	{
		//AfxMessageBox("Error: Cannot init audio player !!!");
		AfxMessageBox(L"Error: No audio player found !!!");
		return;	
	}

	int cnt = 0;
	iPlayerHead = 0;
	iPlayerDropCount = 0;

	hDecoder = EasyG729A_init_decoder();

	iPlayerDropPercentage = 0;

	bool b_ok = true;
	int max_hdr_count = 2;

	bRunning = true;

	while(true)
	{		
		if ( !bRunning )
		{			
			Sleep(100);
			continue;
		}

		Sleep(10);
		if ( parent->GetPlayBufferLength() < 10 )continue;
		int sel = -1;

		for ( int i = 0; i < MAX_OUT_BUFFERS; i++ )
		{
			if(waveOutUnprepareHeader(hWaveOut,&waveHdrOut[i],sizeof(WAVEHDR)) == WAVERR_STILLPLAYING)continue;
			sel = i;
			break;
		}

		if ( sel < 0 )continue;

		int len = parent->FillBuffer(false,strFillBuffer,200);
		if ( len <= 0 )continue;
		if ( len > 200 )len = 200;

		waveHdrOut[sel].dwBufferLength = 0;
		for ( int i = 0; i < len; i += 10 )
		{			
			if ( b16BitAudio )
			{
				unsigned short* x = G729ToPcm(strFillBuffer + i);
				if ( x == NULL )continue;

				memcpy(waveHdrOut[sel].lpData + waveHdrOut[sel].dwBufferLength,x,160);
				waveHdrOut[sel].dwBufferLength += 160;
			}
			else
			{
				unsigned char* x = G729ToPcmu(strFillBuffer + i);
				if ( x == NULL )continue;

				memcpy(waveHdrOut[sel].lpData + waveHdrOut[sel].dwBufferLength,x,80);
				waveHdrOut[sel].dwBufferLength += 80;
			}
		}
		
		if ( waveHdrOut[sel].dwBufferLength <= 0 )continue;
		
		waveHdrOut[sel].dwLoops = 1;
		waveHdrOut[sel].lpNext = 0;
		waveHdrOut[sel].dwFlags = 0;
		waveHdrOut[sel].dwUser = 0;

		waveOutPrepareHeader(hWaveOut, &waveHdrOut[sel], sizeof(WAVEHDR));
		waveOutWrite(hWaveOut, &waveHdrOut[sel], sizeof(WAVEHDR));
	}

	EasyG729A_release_decoder( hDecoder );
	if ( iStatusViewer != NULL )iStatusViewer->SetWindowText(L"AudioThread stopped...");
}

void CAudioThread::AllocPlayBuffer(){}
void CAudioThread::AllocRecordBuffer()
{
	for (int i = 0; i < MAX_RECORD_BUFFERS; i++)
	{         
		whin[i] = new WAVEHDR;

		if (whin[i])
		{
			whin[i]->lpData = new char[RECORD_BLOCK_SIZE];
			whin[i]->dwBufferLength = RECORD_BLOCK_SIZE;
			whin[i]->dwFlags = 0;
		}
	}
}

bool CAudioThread::InitAudioPlayer()
{
	WAVEFORMATEX wfx;
	MMRESULT     rc;                                                

	iPlayBufferLength = 0;
	iPlayerHead = 0;
	iPlayerDropCount = 0;

	memset(&wfx, 0, sizeof(wfx));

	wfx.wFormatTag = WAVE_FORMAT_PCM;
	wfx.nChannels = 1;
	wfx.nSamplesPerSec = 8000;
	wfx.wBitsPerSample = 16;
	wfx.nBlockAlign = wfx.nChannels * wfx.wBitsPerSample / 8;
	wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;
	wfx.cbSize = 0;

	int Result = waveOutOpen(&hWaveOut, WAVE_MAPPER, &wfx, 0, (DWORD)GetModuleHandle(NULL),CALLBACK_NULL);
	if(Result != MMSYSERR_NOERROR)return false;

	ULONG vol = 0;
	
	rc = waveOutGetVolume(hWaveOut,&vol);
	rc = waveOutSetVolume(hWaveOut,0xFFFFFFFF); 
//	rc = waveOutGetVolume(hWaveOut,&vol);
	

    for (int i = 0; i < MAX_OUT_BUFFERS; i++)
    {
          memset (&(waveHdrOut[i]), 0, sizeof (waveHdrOut[i]));
          waveHdrOut[i].lpData = dataBufferOut[i];
          waveHdrOut[i].dwBufferLength = PLAY_BLOCK_SIZE;
          waveHdrOut[i].dwFlags = 0;
          waveHdrOut[i].dwUser = i;

          rc = waveOutPrepareHeader (hWaveOut, &(waveHdrOut[i]),sizeof(waveHdrOut[i]));
          if (rc != MMSYSERR_NOERROR)
          {
//			  if ( iStatusViewer != NULL )iStatusViewer->SetWindowText("__call_free: waveOutPrepareHeader");
			  return false;
          }
    }

	return true;
}

bool CAudioThread::InitAudioRecorder()
{
	WAVEFORMATEX wfx;

	WAVEINCAPS   wic;                                               
	UINT         nDevId;
	MMRESULT     rc;                                                
	UINT         nMaxDevices = waveInGetNumDevs();                  
	
	hWaveIn = NULL;
	bRecordError = false;
	               
	for (nDevId = 0; nDevId < nMaxDevices; nDevId++)
	{
	   rc = waveInGetDevCaps(nDevId, &wic, sizeof(wic));
	   if (rc == MMSYSERR_NOERROR)
	   {
			wfx.nChannels = 1;
			wfx.nSamplesPerSec = 8000;
			wfx.wFormatTag      = WAVE_FORMAT_PCM;                          
			wfx.wBitsPerSample  = ( ( b16BitAudio == true ) ? 16 : 8 );
			wfx.nBlockAlign     = wfx.nChannels * wfx.wBitsPerSample / 8;
			wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;     
			wfx.cbSize          = 0;

			rc = waveInOpen(&hWaveIn, nDevId, &wfx, (DWORD)(VOID*)waveInProc, (DWORD)this->m_hWnd,CALLBACK_FUNCTION);

			if (rc == MMSYSERR_NOERROR)break;
			waveInGetErrorTextA(rc,strMessage,256);
			parent->OnRecordError("Error1 at starting record");
	        return(FALSE);
	   }                                                            
	}                                                               
		
	if (hWaveIn == NULL)
	{
		parent->OnRecordError("Error at starting record");
		bRecordError = true;
		return(FALSE);
	}

	for (int i = 0; i < MAX_RECORD_BUFFERS; i++)
	{                                                               
	   rc = waveInPrepareHeader(hWaveIn, whin[i], sizeof(WAVEHDR));
	   if (rc == MMSYSERR_NOERROR)rc = waveInAddBuffer(hWaveIn, whin[i], sizeof(WAVEHDR));

	   if (rc != MMSYSERR_NOERROR)
	   {
           Stop();

	       waveInGetErrorTextA(rc, strMessage,256);
	       parent->OnRecordError(strMessage);
	       return false;
	   }
	}

	bRunning = true;
	MMRESULT ret = waveInStart(hWaveIn);
	if ( ret != MMSYSERR_NOERROR )parent->OnRecordError("Cannot start recording");
	
	MMTIME mmtime;
	mmtime.wType = TIME_SAMPLES;
	ret = waveInGetPosition(hWaveIn, &mmtime, sizeof(MMTIME));

	if (ret != MMSYSERR_NOERROR)                                  
	{                                                            
	    waveInGetErrorTextA(ret, strMessage, 256);
		parent->OnRecordError(strMessage);
	}

	return true;
}

bool CAudioThread::Init(bool b_g729,CStatic *stat_viewer)
{
	bRecordError = false;
	bPlayWavFile = false;
	strAutoAnswerBuffer = NULL;
	iAutoAnswerBufferPos = 0;
	
	bG729 = b_g729;
	iStatusViewer = stat_viewer;

	return true;
}

void CAudioThread::Stop()
{
	if ( !bRunning )return;

	bRunning = false;
	waveInStop(hWaveIn);
}

void CAudioThread::Destroy()
{
	return;
	waveInReset(hWaveIn);
	for (int i = 0; i < MAX_RECORD_BUFFERS; i++)waveInUnprepareHeader(hWaveIn, whin[i], sizeof(WAVEHDR));	
	waveInClose(hWaveIn);

	waveOutReset(hWaveOut);
	waveOutClose(hWaveOut);
}


void CAudioThread::OnInblock(WPARAM wParam, LPARAM lParam)
{
	if ( bRecordError )return;
	if ( !bRunning )return;

	LPWAVEHDR lpwh = (LPWAVEHDR)lParam;	
	if ( lpwh == NULL )return;

	MMRESULT  rc;
	MMTIME    mmtime;

	if ( b16BitAudio == true )
	{
		int len = lpwh->dwBytesRecorded;
		if ( ( len % 160 ) != 0 )len -= ( len % 160 );

		// calling denoiser.Denoise() 10000 times (that means, 10000 * 20 ms = 200 seconds ) needs 4 seconds
		// that means, pcm data of 1 second (16000bytes) is denoised within 1/50 second = 20 ms

//		short *denoised_pcm = denoiser.Denoise((short *)lpwh->lpData,len/2);			xxxx
		
		int compressed_len = 0;
		for ( int i = 0; i < len; i += 160 )
		{
			if ( !EasyG729A_encoder(hEncoder,(short *)(lpwh->lpData + i),serial) )continue;
//			if ( !EasyG729A_encoder(hEncoder,(short *)(denoised_pcm + i/2),serial) )continue;	xxxx
			memcpy(strCompressedRecordedData + compressed_len,serial,10);
			compressed_len += 10;			
		}

		if ( compressed_len > 0 )parent->OnRecordG729(strCompressedRecordedData,compressed_len);
	}
	else
	{
		int len = lpwh->dwBytesRecorded;
		if ( ( len % 80 ) != 0 )len -= ( len % 80 );

		iLastRecordedSpectrumLength = len;
		memcpy(iLastRecordedSpectrum,lpwh->lpData,len);

//		unsigned char *denoised_data = denoiser.Denoise((unsigned char *)lpwh->lpData,len);		xxxx

		for ( int i = 0; i < len; i += 80 )
		{
			if ( strAutoAnswerBuffer != NULL && iAutoAnswerBufferLength > 0 && iAutoAnswerBufferPos <= iAutoAnswerBufferLength - 10 )
			{
				for ( int j = 0; j < 80; j++ )speech[j] = ( ( strAutoAnswerBuffer[iAutoAnswerBufferPos++] - 128 ) << 8 );
			}
			else for ( int j = 0; j < 80; j++ )speech[j] = ( ( lpwh->lpData[i + j] - 128 ) << 8 );	// This line should be commented if denoiser is used
//			else for ( int j = 0; j < 80; j++ )speech[j] = ( ( denoised_data[i+j] - 128 ) << 8 );	xxxx

			if ( !EasyG729A_encoder(hEncoder, speech, serial) )
			{
				if ( iStatusViewer != NULL )iStatusViewer->SetWindowText(L"Error in encoder/decoder");
				continue;
			}
			parent->OnRecordG729(serial,10);
		}
	}
	
	
//	mmtime.wType = TIME_BYTES;
//	waveInGetPosition(hWaveIn, &mmtime, sizeof(MMTIME));
		
	rc = waveInPrepareHeader(hWaveIn, lpwh, sizeof(WAVEHDR));       
		
	if (rc == MMSYSERR_NOERROR)rc = waveInAddBuffer(hWaveIn, lpwh, sizeof(WAVEHDR));       
		
	if (rc != MMSYSERR_NOERROR)
	{                                     
		bRecordError = true;
		waveInGetErrorTextA(rc, strMessage, 256);
		parent->OnRecordError(strMessage);		
	}	
}

void CAudioThread::waveInProc(HWAVEIN hWaveIn, UINT uMsg, DWORD dwInstance, DWORD dwParam1,DWORD dwParam2)
{
	this_pointer->OnInblock(uMsg,dwParam1);
}


bool CAudioThread::SaveWavFile(char *str_path,unsigned char *c_record,int record_len)
{
	unsigned char *c = new unsigned char[record_len + 10000];
	int len = 0;

	c[len++] = 'R';		c[len++] = 'I';		c[len++] = 'F';		c[len++] = 'F';

	len += 4;	// Reserve for file length - 8
	c[len++] = 'W';		c[len++] = 'A';		c[len++] = 'V';		c[len++] = 'E';

	c[len++] = 'f';		c[len++] = 'm';		c[len++] = 't';		c[len++] = ' ';
	c[len++] = 16;		c[len++] = 0;		c[len++] = 0;		c[len++] = 0;	//chunk data size + ( extra = 0 )
	c[len++] = 1;		c[len++] = 0;		// 2 byte compression code (0: unknown, 1: pcm, 6: g711 a-law, 7: g711 a-ulaw, 49: gsm, 80: mpeg
	c[len++] = 1;		c[len++] = 0;		// number of channels
	
	c[len++] = 8000 % 256;
	c[len++] = 8000 / 256;
	c[len++] = 0;		
	c[len++] = 0;	//sample rate

	if ( 0 )	// b16BitSample
	{
		c[len++] = 16000 % 256;				//average bytes per second
		c[len++] = 16000 / 256;				//average bytes per second
		c[len++] = 0;						//average bytes per second
		c[len++] = 0;						//average bytes per second

		c[len++] = 2;						// block align = bits per sample / 8 * number of channel
		c[len++] = 0;						// block align = bits per sample / 8 * number of channel

		c[len++] = 16;						// significant bits per sample
		c[len++] = 0;						// significant bits per sample
	}
	else
	{
		c[len++] = 8000%256;				//average bytes per second
		c[len++] = 8000/256;				//average bytes per second
		c[len++] = 0;						//average bytes per second
		c[len++] = 0;						//average bytes per second

		c[len++] = 1;						// block align = bits per sample / 8 * number of channel
		c[len++] = 0;						// block align = bits per sample / 8 * number of channel

		c[len++] = 8;						// significant bits per sample
		c[len++] = 0;						// significant bits per sample
	}	

	c[len++] = 'd';				c[len++] = 'a';		c[len++] = 't';		c[len++] = 'a';
	c[len++] = record_len%256;
	c[len++] = (record_len % 65536 ) /256;
	c[len++] = record_len/65536;
	c[len++] = 0;

	memcpy(c+len,c_record,record_len);
	len += record_len;

	c[4] = 0;			c[5] = 0;			c[6] = ( len - 8 )/256;	c[7] = ( len - 8 ) % 256;

	CFile cf;
	if ( !cf.Open((LPCTSTR)str_path,CFile::modeWrite|CFile::modeCreate) )
	{
		return false;
	}
	cf.Write(c,len);
	cf.Close();
	delete c;

	return true;
}

int CAudioThread::PlayWavFile(bool play_on_remote,unsigned char* c,int len)
{
	if ( play_on_remote )
	{
		if ( strAutoAnswerBuffer != NULL )delete strAutoAnswerBuffer;
		strAutoAnswerBuffer = NULL;
		iAutoAnswerBufferPos = 0;
		iAutoAnswerBufferLength = 0;
	}

	if ( c[0] == 'R' && c[1] == 'I' && c[2] == 'F' && c[3] == 'F' );
	else return -1;

	int wave_len = ( ( c[4] << 24 ) | ( c[5] << 16 ) | ( c[6] << 8 ) | c[7] );

	if ( c[8] == 'W' && c[9] == 'A' && c[10] == 'V' && c[11] == 'E' );
	else return -2;

	int pos = 12;
	while(pos < len)
	{
		if ( c[pos] == 'f' && c[pos+1] == 'm' && c[pos+2] == 't' && c[pos+3] == ' ' );
		else
		{
			pos++;
			continue;
		}

		pos += 4;

		int chunk_size = ( c[pos+3] << 24 ) + ( c[pos+2] << 16 ) + ( c[pos+1] << 8 ) + c[pos];
		pos += 4;

		int compression_code = ( c[pos+1] << 8 ) + c[pos];
		pos += 2;

		int n_channel = c[pos+1] * 256 + c[pos];
		pos += 2;

		int sample_rate = ( ( c[pos+3] << 24 ) + ( c[pos+2] << 16 ) + ( c[pos+1] << 8 ) + c[pos] );
		pos += 4;

		int byte_per_second = ( ( c[pos+3] << 24 ) + ( c[pos+2] << 16 ) + ( c[pos+1] << 8 ) + c[pos] );
		pos += 4;

		int block_align = ( ( c[pos+1] << 8 ) + c[pos] );
		pos += 2;

		int significant_bps = ( ( c[pos+1] << 8 ) + c[pos] );
		pos += 2;

		if ( c[pos] == 'd' && c[pos+1] == 'a' && c[pos+2] == 't' && c[pos+3] == 'a' );
		else return -3;

		pos += 4;

		int recorded_len = ( ( c[pos+3] << 24 ) + ( c[pos+2] << 16 ) + ( c[pos+1] << 8 ) + c[pos] );
		pos += 4;
		
		unsigned char *str_pointer = strPlayBuffer;
		if ( play_on_remote )
		{
			strAutoAnswerBuffer = new unsigned char[recorded_len];
			iAutoAnswerBufferLength = recorded_len;
			iAutoAnswerBufferPos = 0;

			str_pointer = strAutoAnswerBuffer;
		}

//		CString str_chunk;
//		str_chunk.Format("Chunk data size: %d, compression code: %d,sample rate: %d, n_channel: %d, BPS: %d, block align: %d, significant bps: %d,recorded len: %d",chunk_size,compression_code,sample_rate,n_channel,byte_per_second,block_align,significant_bps,recorded_len);
//		AfxMessageBox(str_chunk);

		int skip_count = 1;
		int sx = sample_rate;
		if ( ( sx % 8000 ) != 0 )sx += ( 8000 - ( sample_rate % 8000 ) );
				
		skip_count = sx / 8000;
		if ( skip_count <= 0 )skip_count = 1;

		if ( n_channel == 1 && ( significant_bps == 8 || significant_bps == 16 ) )
		{
			if ( significant_bps == 8 )
			{
				if ( sample_rate == 1 )memcpy(str_pointer,c + pos,recorded_len);
				else
				{
					for ( int j = pos; j < pos + recorded_len; j += skip_count )str_pointer[(j-pos)/skip_count] = c[j];
				}
			}
			else if ( significant_bps == 16 )
			{
				for ( int j = pos; j < pos + recorded_len; j += ( 2 * skip_count ) )
				{
					unsigned short d1 = ( ( c[j+1] << 8 ) + c[j] );
					unsigned char c1 = ( ( d1 >> 8 ) + 128 );
					str_pointer[(j-pos)/(2 * skip_count)] = c1;
				}
			}
		}
		else if ( n_channel == 2 && significant_bps == 16 )
		{
			for ( int j = pos; j < pos + recorded_len; j += 4 * skip_count )
			{
				unsigned short d1 = ( ( c[j+1] << 8 ) + c[j] );
				unsigned short d2 = ( ( c[j+3] << 8 ) + c[j+3] );

				unsigned char c1 = (unsigned char)( ( d1 >> 8 ) + 128 );
				unsigned char c2 = (unsigned char)( ( d2 >> 8 ) + 128 );

				str_pointer[(j-pos)/(4 * skip_count)] = ( c1 + c2 ) / 2;
			}
		}
		else return -4;

		pos += recorded_len;

		if ( !play_on_remote )
		{
			if ( n_channel == 1 )iPlayBufferLength = ( recorded_len * 8 ) / significant_bps;
			else if ( n_channel == 2 && significant_bps == 16 )iPlayBufferLength = recorded_len/(4 * skip_count);

			iPlayerHead = 0;
			iPlayerDropCount = 0;
		}
		else
		{
			if ( n_channel == 1 )iAutoAnswerBufferLength = ( recorded_len * 8 ) / significant_bps;
			else if ( n_channel == 2 && significant_bps == 16 )iAutoAnswerBufferLength = recorded_len/(4 * skip_count);
		}

		bAudioLoop = false;
		bPlayWavFile = true;
		bRunning = true;

		break;
	}

	return 1;
}

unsigned char CAudioThread::PcmToAlaw(short	pcm_val)        /* 2's complement (16-bit range) */
{
	BYTE		seg;
	BYTE		aval;
	bool    positive = true;

//	pcm_val = pcm_val >> 3;

	if (pcm_val < 0) 
	{
		positive = false;
		pcm_val = (-pcm_val);// - 1;
	}

	/* Convert the scaled magnitude to segment number. */
	seg = search(pcm_val, seg_aend, 8);

	/* Combine the sign, segment, and quantization bits. */

	if (seg >= 8)		/* out of range, return maximum value. */
	{
		aval = 0x7f;
	}
	else 
	{
		aval = seg << SEG_SHIFT;
		if (seg < 2)
			aval |= (pcm_val >> 1) & QUANT_MASK;
		else
			aval |= (pcm_val >> seg) & QUANT_MASK;
	}

	aval |= (positive) ? SIGN_BIT : 0;
	aval ^= 0x55;

	return aval;
}

short CAudioThread::AlawToPcm(unsigned char a_val)		
{
	a_val ^= 0x55;
	bool positive = (a_val & SIGN_BIT) ? true: false;

	short t = ((a_val & QUANT_MASK) << 4);
	BYTE seg = (a_val & SEG_MASK) >> SEG_SHIFT;
	switch (seg) {
	case 0:
		t += 8;
		break;
	case 1:
		t += 0x108;
		break;
	default:
		t += 0x108;
		t <<= (seg - 1);
	}
	return (positive) ? t : -t;
}
