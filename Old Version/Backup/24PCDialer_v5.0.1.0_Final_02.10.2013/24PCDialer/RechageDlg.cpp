// RechageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "RechageDlg.h"
#include "afxdialogex.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"
#include "resource.h"
#include "FlexiAccount.h"

// RechageDlg dialog

IMPLEMENT_DYNAMIC(RechageDlg, CDialogEx)

RechageDlg::RechageDlg(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(RechageDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
}

RechageDlg::~RechageDlg()
{
}

BOOL RechageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	CButton* pButton = (CButton*)GetDlgItem(IDC_RADIO1);
	pButton->SetCheck(true);

	CStatic * m_Label;
	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(100, "Cambria Bold");
	m_Label = (CStatic *)GetDlgItem(IDC_FLEXI_BALANCE_STATIC);
	m_Label->SetFont(m_Font1);
	/*
	//Read UserInfo
	CStdioFile readFile;
	CStringA strLine;
	CFileException fileException;
	CStringA strFilePath = _T("UserInfo.dat");
	int counter = 0;
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		counter ++;
		if(counter==4)
			flexiUser = strLine;
		if(counter==5)
			flexiPassword = strLine;
		}
	readFile.Close();
	}
	//End UserInfo
	*/
	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CStringA reqKey;
	reqKey.Format("%f", sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + flexiPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	iDialerAgent -> FlexiAuthenticate(flexiUser , reqKey, hash.c_str());

	while(iDialerAgent ->GetFlexiBalanceStr().Length()==0){		
	}
	CStringA flexiReply = iDialerAgent ->GetFlexiBalanceStr().GetBuffer();
	//if(flexiReply.Mid(0,7)=="Welcome" || flexiReply.Mid(0,6)=="Access"){
		SetDlgItemText(IDC_FLEXI_BALANCE_STATIC, flexiReply);
	//}
	if(phoneNum.GetLength()>0){
		SetDlgItemText(IDC_RECHARGE_NUMBER_EDIT, phoneNum);
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}
void RechageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(RechageDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &RechageDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// RechageDlg message handlers


void RechageDlg::OnBnClickedOk()
{
	CString mobileNumber;
	GetDlgItemText(IDC_RECHARGE_NUMBER_EDIT, mobileNumber);

	CString rechargeAmount;
	GetDlgItemText(IDC_RECHARGE_AMOUNT_EDIT, rechargeAmount);

	CStringA numberType;
	int checkRadio = GetCheckedRadioButton(IDC_RADIO1, IDC_RADIO2);
	switch(checkRadio){
		case IDC_RADIO1:
			numberType = "1";
			break;
		case IDC_RADIO2:
			numberType = "2";
			break;
	}

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CStringA reqKey;
	reqKey.Format("%f", sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + flexiPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	iDialerAgent -> FlexiRecharge(flexiUser, reqKey, hash.c_str(), mobileNumber.Trim(), rechargeAmount, numberType);


	/*while(iDialerAgent ->GetFlexiMessageStr().Length()==0){		
	}
	CStringA flexiReply = iDialerAgent ->GetFlexiMessageStr().GetBuffer();
	//SetDlgItemText(IDC_FLEXI_BALANCE_STATIC, flexiReply);
	AfxMessageBox(flexiReply);*/

	//iDialerAgent -> FlexiRechargeActivity(flexiUser, reqKey, hash.c_str(), "1");


	CDialogEx::OnOK();
}
