#pragma once
#include "resource.h"
#include "DialerAgent.h"
#include "afxwin.h"

// RechageDlg dialog

class RechageDlg : public CDialogEx
{
	DECLARE_DYNAMIC(RechageDlg)

public:
	RechageDlg(CWnd* pParent,  DialerAgent* iDial);   // standard constructor
	virtual ~RechageDlg();
	CStringA flexiUser;
	CStringA flexiPassword;
	CStringA phoneNum;

// Dialog Data
	enum { IDD = IDD_RECHARGE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	DialerAgent* iDialerAgent;
	afx_msg void OnBnClickedOk();
};
