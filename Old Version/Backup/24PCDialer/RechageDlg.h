#pragma once
#include "resource.h"
//#include "DialerAgent.h"
#include "afxwin.h"
#include "SmoothDialerDlg.h"

// RechageDlg dialog

class RechageDlg : public CDialogEx, public RechargeDialogueUserInterface
{
	DECLARE_DYNAMIC(RechageDlg)

public:
	RechageDlg(CWnd* pParent,  DialerAgent* iDial);   // standard constructor
	virtual ~RechageDlg();
	CString flexiUser;
	CString flexiPassword;
	CString phoneNum;
	//RechargeDialogueUserInterface* iRechargeDialog;

// Dialog Data
	enum { IDD = IDD_RECHARGE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	DialerAgent* iDialerAgent;
	afx_msg void OnBnClickedOk();
	void OnRechargeBalanceChangeState();
};
