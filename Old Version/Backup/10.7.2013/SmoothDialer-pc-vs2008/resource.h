//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SmoothDialer.rc
//
#define IDD_SMOOTHDIALER_DIALOG         102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_LOGIN_DIALOG                129
#define IDR_MENU1                       131
#define IDC_STATIC_DISPLAY              1000
#define IDC_EDIT_CALL                   1001
#define IDC_BUTTON_CALL                 1002
#define IDC_BUTTON_END_CALL             1003
#define IDC_BUTTON_IVR                  1004
#define IDC_STATIC_STATUS               1005
#define IDC_OPCODE                      1006
#define IDC_USER                        1007
#define IDC_EDIT3                       1008
#define IDC_PASSWORD                    1008
#define IDC_BUTTON1                     1009
#define IDC_BUTTON2                     1010
#define IDC_BUTTON3                     1012
#define IDC_BUTTON4                     1013
#define IDC_BUTTON5                     1014
#define IDC_BUTTON6                     1015
#define IDC_BUTTON7                     1016
#define IDC_BUTTON8                     1017
#define IDC_BUTTON9                     1018
#define IDC_BUTTON0                     1019
#define IDC_BUTTON_STAR                 1020
#define IDC_BUTTON_HASH                 1021
#define ID_MENU_LOGOUT                  32771
#define ID_MENU_CALLLOG                 32772
#define ID_MENU_CALLLOG32773            32773
#define ID_MENU_LOGOUT32774             32774
#define ID_MENU_IVR                     32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
