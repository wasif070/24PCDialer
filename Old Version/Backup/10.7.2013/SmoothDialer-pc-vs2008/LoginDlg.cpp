// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "LoginDlg.h"
#include "afxdialogex.h"
#include "SmoothDialerDlg.h"
#include <afxwin.h>



// LoginDlg dialog

IMPLEMENT_DYNAMIC(LoginDlg, CDialogEx)

LoginDlg::LoginDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(LoginDlg::IDD, pParent)
{
		m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

LoginDlg::~LoginDlg()
{
}

BOOL LoginDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);		// Set big icon
	//SetBackgroundImage

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void LoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(LoginDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &LoginDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// LoginDlg message handlers


void LoginDlg::OnBnClickedOk()
{
	GetDlgItemText(IDC_OPCODE,opcode);
	GetDlgItemText(IDC_USER,user);
	GetDlgItemText(IDC_PASSWORD, password);

	CSmoothDialerDlg dlg(this);
	dlg.opcode = atoi(opcode);
	dlg.user.Copy(user);
	dlg.password.Copy(password);
	/*char * p;
	dlg.opcode = strtol ( opcode, & p, 10 );
	if ( * p != 0 ) {
		int Answer;
   		Answer = AfxMessageBox("Operatoor code can not contain characters",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);

		if( Answer == IDOK ){
		EndDialog(1);
		LoginDlg ldlg(this);
		ldlg.DoModal();
		}
		else // if( Answer == IDNO )
		EndDialog(1);
	}else 
	*/
	if(opcode=="" || user=="" || password==""){
	    int Answer;
   		Answer = AfxMessageBox("Opcode/ User/ Password can not be empty",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);

		if( Answer == IDOK ){
		EndDialog(1);
		LoginDlg ldlg(this);
		ldlg.DoModal();
		}
		else // if( Answer == IDNO )
		EndDialog(1);
	}else{
		EndDialog(1);
		dlg.DoModal();
	}


	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}
