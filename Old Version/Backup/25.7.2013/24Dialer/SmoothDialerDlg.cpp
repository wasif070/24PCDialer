// SmoothDialerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "SmoothDialerDlg.h"
#include "LoginDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerDlg dialog

CSmoothDialerDlg::CSmoothDialerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSmoothDialerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSmoothDialerDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	//  m_edit_call = 0;

	// CBrush::CBrush(CBitmap* pBitmap)
    CBitmap bmp;
    // Load a resource bitmap.
    bmp.LoadBitmap(IDB_BITMAP_MAIN);
    m_pEditBkBrush = new CBrush(&bmp);	  
	//m_pEditBkBrush = new CBrush(RGB(255,255,100));

}

CSmoothDialerDlg::~CSmoothDialerDlg()
{
	iPollEngine.ForceStop();
}

void CSmoothDialerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSmoothDialerDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	//DDX_Control(pDX, IDC_EDIT_CALL, m_edit_call);
	DDX_Control(pDX, IDC_COMBO_CALL, m_combo_call);
	DDX_Control(pDX, IDC_BUTTON_CALL, m_dial_call);
	DDX_Control(pDX, IDC_BUTTON_DELETE, m_button_delete);
	DDX_Control(pDX, IDC_BUTTON_END_CALL, m_end_call);
	DDX_Control(pDX, IDC_BUTTON_STAR, m_button_star);
	DDX_Control(pDX, IDC_BUTTON0, m_button_zero);
	DDX_Control(pDX, IDC_BUTTON_HASH, m_button_hash);
	DDX_Control(pDX, IDC_BUTTON7, m_button_seven);
	DDX_Control(pDX, IDC_BUTTON8, m_button_eight);
	DDX_Control(pDX, IDC_BUTTON9, m_button_nine);
	DDX_Control(pDX, IDC_BUTTON4, m_button_four);
	DDX_Control(pDX, IDC_BUTTON5, m_button_five);
	DDX_Control(pDX, IDC_BUTTON6, m_button_six);
	DDX_Control(pDX, IDC_BUTTON1, m_button_one);
	DDX_Control(pDX, IDC_BUTTON2, m_button_two);
	DDX_Control(pDX, IDC_BUTTON3, m_button_three);
}

BEGIN_MESSAGE_MAP(CSmoothDialerDlg, CDialog)
	//{{AFX_MSG_MAP(CSmoothDialerDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CALL, OnButtonCall)
	ON_BN_CLICKED(IDC_BUTTON_END_CALL, OnButtonEndCall)
	ON_BN_CLICKED(IDC_BUTTON_IVR, OnButtonIvr)
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CSmoothDialerDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSmoothDialerDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CSmoothDialerDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CSmoothDialerDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CSmoothDialerDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CSmoothDialerDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CSmoothDialerDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CSmoothDialerDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CSmoothDialerDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON0, &CSmoothDialerDlg::OnBnClickedButton0)
	ON_BN_CLICKED(IDC_BUTTON_STAR, &CSmoothDialerDlg::OnBnClickedButtonStar)
	ON_BN_CLICKED(IDC_BUTTON_HASH, &CSmoothDialerDlg::OnBnClickedButtonHash)
	ON_COMMAND(ID_MENU_CALLLOG, &CSmoothDialerDlg::OnMenuCalllog)
	ON_COMMAND(ID_MENU_LOGOUT, &CSmoothDialerDlg::OnMenuLogout)
	ON_COMMAND(ID_MENU_IVR, &CSmoothDialerDlg::OnMenuIvr)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CSmoothDialerDlg::OnBnClickedButtonDelete)
	ON_CBN_DROPDOWN(IDC_COMBO_CALL, &CSmoothDialerDlg::OnCbnDropdownComboCall)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerDlg message handlers


HBRUSH CSmoothDialerDlg::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
    switch (nCtlColor)
    {
    case CTLCOLOR_STATIC:
        pDC->SetTextColor(RGB(0, 0, 0));
		//pDC->SetBkColor(RGB(255,255,100));
		pDC->SetBkMode(TRANSPARENT);

       //return (HBRUSH)GetStockObject(NULL_BRUSH);
	   return (HBRUSH)(m_pEditBkBrush->GetSafeHandle());
    default:
        return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
    }
}

void CSmoothDialerDlg::OnDestroy()
{
          CDialog::OnDestroy();

          // Free the space allocated for the background brush
          delete m_pEditBkBrush;
}

BOOL CSmoothDialerDlg::OnInitDialog()
{

	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	iAudioEngine = NULL;
	iDialerAgent = NULL;

	m_button_one.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_1)));
	m_button_two.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_2)));
	m_button_three.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_3)));
	m_button_four.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_4)));
	m_button_five.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_5)));
	m_button_six.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_6)));
	m_button_seven.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_7)));
	m_button_eight.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_8)));
	m_button_nine.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_9)));
	m_button_zero.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_0)));
	m_button_star.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_STAR)));
	m_button_delete.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_C)));
	m_button_hash.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_HASH)));
	m_dial_call.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_DIAL_CALL)));
	m_end_call.SetBitmap(LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_END_CALL)));


	if ( iPollEngine.Start() )
	{
		//	TestPollEngine();
		StartDialerAgent();
	}
	else
	{
		AfxMessageBox("Ineternet error !!!\r\nExiting app.");
		EndDialog(IDOK);
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmoothDialerDlg::TestPollEngine()
{
	WString str_url;
	str_url.Zero();
	str_url.Copy("http://www.google.com");

	iPollEngine.FetchUrl(1,str_url,0,5000);

	str_url.Copy("http://www.yahoo.com");
	iPollEngine.FetchUrl(2,str_url,0,5000);

	iPollEngine.AddUdpSocket(3,0xD13BDAB8,13020,0);
	iPollEngine.SendData(3,(const unsigned char*)"[GETTICKET:100000:101:10:1:4343242424]",11,0,0);
}

void CSmoothDialerDlg::StartDialerAgent()
{
	const WString state_str = ":Registering...:Registered:Unregistered:Calling...:Ringing...:Connected:Call In Progress";
	const WString status_str = ":VPN Busy:Call cancelled:Call disconnected:Incoming call:Call ended:Invalid username:Invalid username/password\nChange Settings:Insufficient balance:Unknown error:Keep alive Timeout:Media error:Forbidden 403:Callee not found:Service unavailable:No reply from SIP Switch";
	const WString lstate_str = "Uninitialized:Initializing...:Authenticating...:Authenticating...:Connecting...";
	const WString lstatus_str = ":Internet error:Auth server not found:Operator code not set !:Invalid Operator code:Account not complete\nChange account:Operation cancelled:VPN not found:Custom message:Audio codec not supported:Audio server not found\\nPlease re-install dialer";
	iAudioEngine = new AudioEngineSymbian();
	iAudioEngine->iHwnd = this->m_hWnd;
	iAudioEngine->iStatViewer = (CStatic *)GetDlgItem(IDC_STATIC_STATUS);
	iAudioEngine->Configure();

	iDialerAgent = new DialerAgent(this,iAudioEngine,state_str,status_str,lstate_str,lstatus_str,"Balance","Duration");
	iDialerAgent->SetDefaultAccessPointID(1);
	iDialerAgent->SetNetworkManager(&iPollEngine);
	iPollEngine.SetListener(iDialerAgent);
/*	iMainView->ClearStatus();
	
	if ( iFixedPin == EFalse && iAccountSettingsView->GetOperatorType() == xAccountSettingsView::OPERATOR_CODE_TYPE_NOT_SET )
	{
		iDialerAgent->OnOperatorCodeNotSelected();
		ShowSettingsView();
		return;
	}
*/

	UpdateExitMenuAndRefresh();	
	ReConnect(EFalse, opcode, user, password);

	SetTimer(1001,50,NULL);
}

void CSmoothDialerDlg::UpdateExitMenuAndRefresh()
{
/*	TPtrC str_menu = iViewInfos[iViewCurrent]->GetCancelMenuStr();
	if ( str_menu.Length() == 0 )return;	

	CEikButtonGroupContainer::Current()->SetCommandL(2,SMOOTH_DIALER_MENU_EXIT,str_menu);
	CEikButtonGroupContainer::Current()->DrawNow();*/
}


void CSmoothDialerDlg::ReConnect(TBool show_accesspoint_menu, TInt opcode, TPtrC user, TPtrC password)
{
/*	if ( iAudioEngineError )return;
	if ( iFixedPin == EFalse && iAccountSettingsView->GetOperatorType() == xAccountSettingsView::OPERATOR_CODE_TYPE_NOT_SET )
	{
		ShowSettingsView();
		return;
	}

	TInt operator_code = iBasePin;
	if ( iFixedPin == EFalse )
	{
		TInt x = iAccountSettingsView->GetOperatorCode();
		TInt y = x;

		if ( y <= 0 )operator_code *= 10;
		for ( ; y > 0; y /= 10 )operator_code *= 10;
		
		operator_code += x;
	}

	iDialerAgent->Reset(show_accesspoint_menu,operator_code,iAccountSettingsView->GetSipUserName(),iAccountSettingsView->GetSipPassword(),iAccountSettingsView->GetSipPhoneNo(),iAccountSettingsView->GetBalanceUrl(),iAccountSettingsView->GetSipIP(),iAccountSettingsView->GetSipPort(),strImei,strDeviceModel);*/
	
	//iDialerAgent->Reset(show_accesspoint_menu,1011,"tup","","01818237189","balanceurl",0,5060,"123456789","ModelPCDialer");
	iDialerAgent->Reset(show_accesspoint_menu,opcode, user, password,"01818237189","balanceurl",0,5060,"123456789","ModelPCDialer");
	//iDialerAgent->Reset(show_accesspoint_menu,1111, "772013", "2424","01818237189","balanceurl",0,5060,"123456789","ModelPCDialer");
}

void CSmoothDialerDlg::OnDialerAgentChangeState()
{
	if ( iDialerAgent == NULL )return;

	CString strDisplay = "";
	
	strDisplay = strDisplay + "\r\n" + iDialerAgent->GetDialerNameStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerStateStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerStatusStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerBalanceStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	strDisplay = strDisplay + iDialerAgent->GetDialerCallDurationStr().GetBuffer();
	strDisplay = strDisplay + "\r\n\r\n";

	//strDisplay = strDisplay + iDialerAgent->GetDialerFooterStr().GetBuffer();
	//strDisplay = strDisplay + "\r\n\r\n";

	SetDlgItemText(IDC_STATIC_DISPLAY,strDisplay);

	CString footerDisplay = iDialerAgent->GetDialerFooterStr().GetBuffer() ;
	SetDlgItemText(IDC_STATIC_STATUS, footerDisplay);
}

void CSmoothDialerDlg::OnTimer(UINT nIDEvent)
{
	if ( nIDEvent != 1001 )return;
	if ( iDialerAgent == NULL )return;
	
	iDialerAgent->OnNetworkTick();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSmoothDialerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
	//Sample 01: Required Declarations
     CDC MemDC ;
     CBitmap bmp ;
     CPaintDC dc(this);
                                  
     //Sample 02: Get the Client co-ordinates
     CRect rct;
     this->GetClientRect(&rct);

     //Sample 03: Create the Dialog Compatible DC in the memory,
     //Then load the bitmap to the memory.
     MemDC.CreateCompatibleDC ( &dc ) ;
     bmp.LoadBitmap ( IDB_BITMAP_MAIN ) ;
     MemDC.SelectObject ( &bmp ) ;

     //Sample 04: Finally perform Bit Block Transfer (Blt) from memory dc to
     //dialog surface.
     dc.BitBlt ( 0, 0, rct.Width() , rct.Height() , &MemDC, 0, 0, SRCCOPY ) ;

	CDialog::OnPaint();
	}
}

HCURSOR CSmoothDialerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSmoothDialerDlg::OnButtonCall() 
{
	CString str_dest_no;
	//GetDlgItemText(IDC_EDIT_CALL,str_dest_no);
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	str_dest_no.TrimLeft();
	str_dest_no.TrimRight();

	if ( str_dest_no.IsEmpty() )return;

	iDialerAgent->Call(str_dest_no.GetBuffer(str_dest_no.GetLength()));


	/////////Write Dial Log
	CString strLine;
	CString strFile;
	CStdioFile writeToFile;
	CFileException fileException;
	CString strFilePath = _T("DialLog.dat");
	CFileStatus status;
	int counter = 0;
	if( CFile::GetStatus( strFilePath, status ) )
	{
    // Open the file without the Create flag
	writeToFile.Open( strFilePath, CFile::modeReadWrite ), &fileException;
	while (writeToFile.ReadString(strLine) && counter<9){
		if(counter!=0){
		strFile = strFile + "\n";
		}
		strFile += strLine;
		counter++;

	}
	writeToFile.Seek (0, CFile :: begin);
	CString new_call = str_dest_no + "\n";
	new_call.Append(strFile);
	writeToFile.WriteString(new_call);
	}
	else
	{
    // Open the file with the Create flag
	writeToFile.Open( strFilePath, CFile::modeCreate | CFile::modeReadWrite ), &fileException;
	while (writeToFile.ReadString(strLine) && counter<9){
		if(counter!=0){
		strFile = strFile + "\n";
		}
		strFile += strLine;
		counter++;
	}
	writeToFile.Seek (0, CFile :: begin);
	CString new_call = str_dest_no + "\n";
	new_call.Append(strFile);
	writeToFile.WriteString(new_call);
	}
	writeToFile.Close();
	////////End Call Log
}

void CSmoothDialerDlg::OnButtonEndCall() 
{
	iDialerAgent->EndCall();	
}

void CSmoothDialerDlg::OnButtonIvr() 
{
	iDialerAgent->Call(iDialerAgent->GetIVRNo());	
}



void CSmoothDialerDlg::OnBnClickedButton1()
{

/*	int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("1");*/

	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "1");

}


void CSmoothDialerDlg::OnBnClickedButton2()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("2");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "2");
}


void CSmoothDialerDlg::OnBnClickedButton3()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("3");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "3");
}


void CSmoothDialerDlg::OnBnClickedButton4()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("4");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "4");
}


void CSmoothDialerDlg::OnBnClickedButton5()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("5");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "5");
}


void CSmoothDialerDlg::OnBnClickedButton6()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("6");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "6");
}


void CSmoothDialerDlg::OnBnClickedButton7()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("7");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "7");
}


void CSmoothDialerDlg::OnBnClickedButton8()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("8");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "8");
}


void CSmoothDialerDlg::OnBnClickedButton9()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("9");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "9");
}

void CSmoothDialerDlg::OnBnClickedButton0()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("0");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "0");
}


void CSmoothDialerDlg::OnBnClickedButtonStar()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
    m_edit_call.ReplaceSel("*");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "*");
}


void CSmoothDialerDlg::OnBnClickedButtonHash()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end);
	m_edit_call.ReplaceSel("#");*/
	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no + "#");
}


void CSmoothDialerDlg::OnMenuCalllog()
{
	//AfxMessageBox("Show Call Log");
	
	/////////Read Call Log
	CFileException fileException;
	CString strFilePath = _T("CallLog.dat");
	CString strFile;

	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		strFile += strLine + "\n";

	}
	MessageBox(strFile, "Call Log");
		readFile.Close();
	}else{
	MessageBox("", "Call Log");
	}

}


void CSmoothDialerDlg::OnMenuLogout()
{
	EndDialog(1);
	LoginDlg ldlg;
	ldlg.DoModal();
}



void CSmoothDialerDlg::OnMenuIvr()
{
	iDialerAgent->Call(iDialerAgent->GetIVRNo());	
}


void CSmoothDialerDlg::OnBnClickedButtonDelete()
{
	/*int end = m_edit_call.GetWindowTextLength();
    m_edit_call.SetSel(end, end-1);
    m_edit_call.ReplaceSel("");*/

	CString str_dest_no;
	GetDlgItemText(IDC_COMBO_CALL,str_dest_no);
	m_combo_call.SetWindowTextA(str_dest_no.Mid(0, str_dest_no.GetLength()-1));
}




void CSmoothDialerDlg::OnCbnDropdownComboCall()
{
	CStdioFile readFile;
	CFileException fileException;
	CString strFilePath = _T("DialLog.dat");
	//str_dest_no.Mid(0, str_dest_no.GetLength()-1
	m_combo_call.ResetContent();
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		m_combo_call.AddString(strLine);
	}
		readFile.Close();
	}
}