#pragma once
#include "resource.h"
#include "DialerAgent.h"

// FlexiAccount dialog

class FlexiAccount : public CDialogEx
{
	DECLARE_DYNAMIC(FlexiAccount)

public:
	FlexiAccount(CWnd* pParent,  DialerAgent* iDial);   // standard constructor
	virtual ~FlexiAccount();
	CString flexiUser;
	CString flexiPassword;

// Dialog Data
	enum { IDD = IDD_FLEXI_ACCOUNT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	DialerAgent* iDialerAgent;
};
