#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "DialerAgent.h"

// ContactsDlg dialog

class ContactsDlg : public CDialogEx
{
	DECLARE_DYNAMIC(ContactsDlg)

public:
	ContactsDlg(CWnd* pParent , DialerAgent* iDial);   // standard constructor
	virtual ~ContactsDlg();	

// Dialog Data
	enum { IDD = IDD_CONTACTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_contacts_list;
	afx_msg void OnBnClickedSave();
	afx_msg void OnNMDblclkContactsList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickContactsList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedRemove();
	afx_msg void OnBnClickedNew();
	afx_msg void OnBnClickedButton3();
	DialerAgent* iDialerAgent;
};
