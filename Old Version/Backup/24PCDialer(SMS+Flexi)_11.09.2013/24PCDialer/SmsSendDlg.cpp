// SmsSendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmsSendDlg.h"
#include "afxdialogex.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"



// SmsSendDlg dialog

IMPLEMENT_DYNAMIC(SmsSendDlg, CDialogEx)

SmsSendDlg::SmsSendDlg(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(SmsSendDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
}

SmsSendDlg::~SmsSendDlg()
{
}

BOOL SmsSendDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void SmsSendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_COUNTRY_CODE, m_combo_country_code);
}


BEGIN_MESSAGE_MAP(SmsSendDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &SmsSendDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &SmsSendDlg::OnBnClickedCancel)
	ON_CBN_DROPDOWN(IDC_COMBO_COUNTRY_CODE, &SmsSendDlg::OnCbnDropdownComboCountryCode)
END_MESSAGE_MAP()


// SmsSendDlg message handlers


void SmsSendDlg::OnBnClickedOk()
{
	CString phnNum;
	CString msg;
	GetDlgItemText(IDC_SMS_PHONE_NUM_EDIT,phnNum);
	GetDlgItemText(IDC_SMS_SEND_EDIT,msg);

	if(phnNum==""){
		int Answer;
   		Answer = AfxMessageBox("Phone Number can not be empty",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);
	}else{

		SYSTEMTIME timeInMillis;
		GetSystemTime(&timeInMillis);	
		double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
		CString reqKey;
		reqKey.Format("%f", sysTime);

		std::string hash;
		crypto::errorinfo_t lasterror;
		CT2A atext(reqKey + flexiPassword);

		crypto::md5_helper_t hhelper;
		hash = hhelper.hexdigesttext(atext.m_szBuffer);
		lasterror = hhelper.lasterror();

		//CString countryCode;
		//GetDlgItemText(IDC_COMBO_COUNTRY_CODE, countryCode);

		iDialerAgent -> SendSMSMessage(flexiUser , phnNum.Trim(), msg, reqKey, hash.c_str());
	}

	CDialogEx::OnOK();
}


void SmsSendDlg::OnBnClickedCancel()
{
	EndDialog(1);
	CDialogEx::OnCancel();
}


/*void SmsSendDlg::OnCbnDropdownComboCountryCode()
{
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("CountryCode.dat");
	//str_dest_no.Mid(0, str_dest_no.GetLength()-1
	m_combo_country_code.ResetContent();
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		m_combo_country_code.AddString(strLine);
	}
		readFile.Close();
	}
}*/
