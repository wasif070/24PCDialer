#pragma once
#include "resource.h"
#include "DialerAgent.h"
#include "afxwin.h"


// SmsSendDlg dialog

class SmsSendDlg : public CDialogEx
{
	DECLARE_DYNAMIC(SmsSendDlg)

public:
	SmsSendDlg(CWnd* pParent,  DialerAgent* iDial);   // standard constructor
	virtual ~SmsSendDlg();
	CString flexiUser;
	CString flexiPassword;

// Dialog Data
	enum { IDD = IDD_SMS_SEND_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	HICON m_hIcon;
	DECLARE_MESSAGE_MAP()
public:
	DialerAgent* iDialerAgent;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	//afx_msg void OnCbnDropdownComboCountryCode();
	//CComboBox m_combo_country_code;
};
