#include "stdafx.h"
#include "DialerAgent.h"
#include "RechageDlg.h" 

using namespace std;


void DialerAgent::OnNetworkTick()
{	
	CheckDialerActivity();
	
	if ( IsAudioRunning() )
	{
		OnDialerStateChange();	// The call duration has changed
		
		iRecordBuf.Zero();
		iAudioEngine->OnNetworkTick(iRecordBuf);
		
		if ( iRecordBuf.Length() > 0 )
		{
			SendRtpFromAudioEngine(iRecordBuf,iRecordBuf.Length());
		}
	}
}

void DialerAgent::OnDialerStateChange()
{
	TInt current_state = GetDialerStateSuper();	
	
	if ( current_state == STATE_REGISTERED && iAlternateChannelState == ALT_CHANNEL_NOT_TESTED )StartAltChannelRequest();
	
	if ( current_state == STATE_REGISTERED && ( iPrevDialerStateForBalanceUpdate == STATE_CONNECTED || iPrevDialerStateForBalanceUpdate == STATE_INCOMING_CONNECTED ||  iPrevDialerStateForBalanceUpdate == STATE_RINGING || iPrevDialerStateForBalanceUpdate == STATE_CALLING ) )
	{		
		SendBalanceRequest();
	}	
	iPrevDialerStateForBalanceUpdate = current_state;		
	iDialerUser->OnDialerAgentChangeState();	
}

void DialerAgent::PauseRtpForGSM()
{
	iRtpPausedForGSM = ETrue;
	if ( iAudioEngine )iAudioEngine->ForceStop();
}

void DialerAgent::ResumeRtpForGSM()
{
	iRtpPausedForGSM = EFalse;
	if ( iAudioEngine )iAudioEngine->ResetAudioBuffer();
}

void DialerAgent::OnCallEnd(TInt duration)
{
	iDialerUser->OnCallEnd(duration);
	if ( iAudioEngine )iAudioEngine->ForceStop();

	//Write Call Log
	CString strLine;
	CString strFile;
	CString phone_no = strDialedNo.GetBuffer();
	CString dur;
	if ( duration < 3600 )dur.Format(_L8("%02d:%02d"), duration/60, duration%60);
	else dur.Format(_L8("%02d:%02d:%02d"),duration/3600,(duration % 3600) / 60, duration % 60);
	//dur.Format("%d",duration);
	CStdioFile writeToFile;
	CFileException fileException;
	CString strFilePath = _T("CallLog.dat");
	CFileStatus status;
	int counter = 0;
	if( CFile::GetStatus( strFilePath, status ) )
	{
    // Open the file without the Create flag
	writeToFile.Open( strFilePath, CFile::modeReadWrite ), &fileException;
	while (writeToFile.ReadString(strLine) && counter<9){
		if(counter!=0){
		strFile = strFile + "\n";
		}
		strFile += strLine;
		counter++;
	}
	writeToFile.Seek (0, CFile :: begin);
	CString new_call = phone_no + "\t" + dur + "\n";
	new_call.Append(strFile);
	writeToFile.WriteString(new_call);
	}
	else
	{
    // Open the file with the Create flag
	writeToFile.Open( strFilePath, CFile::modeCreate | CFile::modeReadWrite ), &fileException;
	while (writeToFile.ReadString(strLine) && counter<9){
		if(counter!=0){
		strFile = strFile + "\n";
		}
		strFile += strLine;
		counter++;
	}
	writeToFile.Seek (0, CFile :: begin);
	CString new_call = phone_no + "\t" + dur + "\n";
	new_call.Append(strFile);
	writeToFile.WriteString(new_call);
	}
	writeToFile.Close();
	//End Call Log
}

void DialerAgent::OnAccessPointDone(TBool b_success,TInt access_point_id)
{
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED )return;

	if ( b_success )
	{
		iSelectedAccessPointID = access_point_id;
		iDialerUser->OnChangeAccessPoint();
		Start();
	}
	else SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INTERNET_ERROR);
}

TBool DialerAgent::IsAudioRunning()
{
	if ( iAudioEngine == NULL )return EFalse;
	if ( iSoundTestRunning )return ETrue;
	if ( iLocalState != LOCAL_STATE_VPN_CONNECTED )return EFalse;
	
	TInt dialer_state = GetDialerStateSuper();
	if ( dialer_state == STATE_CONNECTED || dialer_state == STATE_INCOMING_CONNECTED || dialer_state == STATE_RINGING )return ETrue;
	if ( dialer_state == STATE_CALLING && iWaitingForRTPAfterCall == EFalse )return ETrue;
	
	return EFalse;
}

void DialerAgent::OnReceiveData(TInt socket_id,TPtrC8 dt,TInt len)
{
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED )return;
	
	if ( socket_id == SOCKET_FEEDBACK )
	{
#ifndef __C_PLUS_PLUS_BUILD__
		if ( dt.Find(_L8("\r\n\r\n")) > 0 )
		{
			if ( dt.Find(_L8("\r\n\r\n")) < dt.Length()-4 )iDialerUser->ShowMessage(_L("Server said"),dt.Mid(dt.Find(_L8("\r\n\r\n")) + 4),EFalse,EFalse);
		}
		else iDialerUser->ShowMessage(_L("Server said"),dt,EFalse,EFalse);
#endif
		return;
	}
	
	if ( socket_id == SOCKET_AUTH )
	{
		if ( ( iAuthReply.Length() + len ) > iAuthReply.MaxLength() )len = iAuthReply.MaxLength() - iAuthReply.Length();
		iAuthReply.Append(dt.Mid(0,len));
		return;
	}
	if(socket_id == SOCKET_FLEXI){
		
		TInt st = -1;
		TInt en = -1;
		
		if ( st < 0 && dt.Find(_L8("\r\n\r\n")) >= 0 )st = dt.Find(_L8("\r\n\r\n")) + 4;
		
		if ( st < 0 )st = 0;		
		if ( en < 0 )en = dt.Length();
		
		strFlexiBalance.Zero();

		if ( ( en - st + strFlexiBalance.Length()) > strFlexiBalance.MaxLength() )strFlexiBalance.Append(dt.Mid(st,strFlexiBalance.MaxLength()-strFlexiBalance.Length()));
		else strFlexiBalance.Append(dt.Mid(st,en-st));
		
		strFlexiBalance.Delete(0,3);

		CString flexiReply;
		flexiReply.Format("%s", strFlexiBalance);
		strFlexiBalance.Zero();
		
		if(flexiReply.Mid(0,7) == "Welcome") {
			AfxMessageBox(flexiReply, MB_ICONMASK);
		}
		else if(flexiReply.Mid(0,6) == "Access"){
			AfxMessageBox(flexiReply);
		}else if(flexiReply.Mid(0,2) == "\n["){
			flexiReply.Delete(0,1);	
			flexiReply.Replace("\"","");

		int counter = 0;
		int lineNum = 0;
		CString line = flexiReply.Tokenize(_T("}"), lineNum);
		CString strFile;
		while(!line.IsEmpty()){
			counter++;
			CString count;
			count.Format("%2d", counter);
			int nTokenPos = 0;
			CString newLine;
			CString token = line.Tokenize(_T(","), nTokenPos);
			while(!token.IsEmpty()){
				if(token.Mid(0,7)=="phoneNo") newLine += count + ". Phone Number: " + token.Mid(8, token.GetLength()) + "   ";
				else if(token .Mid(0,6)=="amount") newLine += "Amount: " + token.Mid(7, token.GetLength()) + "   ";
				else if(token.Mid(0,4)=="time") newLine += "Time: " + token.Mid(5, token.GetLength()) + "\n";
				token = line.Tokenize(_T(","), nTokenPos);
			}

			line = flexiReply.Tokenize(_T("}"), lineNum);
			strFile += newLine;
		}
		AfxMessageBox(strFile, MB_ICONMASK);
					
		}else{
			flexiReply.Delete(0,1);
			if(flexiReply == "Error=1\n") flexiReply = "Your request format is invalid. Please contact to customer service.";
			else if(flexiReply == "Error=2\n") flexiReply = "You are not valid user. Please contact to customer service."; 
			else if(flexiReply == "Error=3\n") flexiReply = "Invalid mobile number."; 
			else if(flexiReply == "Error=4\n") flexiReply = "Minimum or maximum recharge amount is not matched."; 
			else if(flexiReply == "Error=5\n") flexiReply = "This recharge amount is not available for you."; 
			else if(flexiReply == "Error=6\n") flexiReply = "You have already sent a request for same number in last 15 minutes."; 
			else if(flexiReply == "Error=7\n") flexiReply = "System Error. Please contact to customer service."; 
			AfxMessageBox(flexiReply);
		}

		return;
	}
	if(socket_id == SOCKET_SMS){
		TInt st = -1;
		TInt en = -1;
		
		if ( st < 0 && dt.Find(_L8("\r\n\r\n")) >= 0 )st = dt.Find(_L8("\r\n\r\n")) + 4;
		
		if ( st < 0 )st = 0;		
		if ( en < 0 )en = dt.Length();
		
		strSMSReply.Zero();

		if ( ( en - st + strSMSReply.Length()) > strSMSReply.MaxLength() )strSMSReply.Append(dt.Mid(st,strSMSReply.MaxLength()-strSMSReply.Length()));
		else strSMSReply.Append(dt.Mid(st,en-st));
		
		strSMSReply.Delete(0,5);

		CString smsReply;
		smsReply.Format("%s", strSMSReply);
		strSMSReply.Zero();

		if(smsReply.Mid(0,2) == "\n["){
			smsReply.Delete(0,1);	
			smsReply.Replace("\"","");

		int counter = 0;
		int lineNum = 0;
		CString line = smsReply.Tokenize(_T("}"), lineNum);
		CString strFile;
		while(!line.IsEmpty()){
			counter++;
			CString count;
			count.Format("%2d", counter);
			int nTokenPos = 0;
			CString newLine;
			CString token = line.Tokenize(_T(","), nTokenPos);
			while(!token.IsEmpty()){
				if(token.Mid(0,7)=="phoneNo") newLine += count + ". Phone: " + token.Mid(8, token.GetLength()) + "   ";
				else if(token .Mid(0,6)=="amount") newLine += "Price: " + token.Mid(7, token.GetLength()) + "   ";
				else if(token .Mid(0,7)=="message") newLine += "Message: " + token.Mid(7, token.GetLength()) + "   ";
				else if(token.Mid(0,4)=="time") newLine += "Time: " + token.Mid(5, token.GetLength()) + "\n";
				token = line.Tokenize(_T(","), nTokenPos);
			}

			line = smsReply.Tokenize(_T("}"), lineNum);
			strFile += newLine;
		}
		AfxMessageBox(strFile, MB_ICONMASK);
					
		}else{
			smsReply.Delete(0,8);
			AfxMessageBox(smsReply);
		}
		return;
	}
	if ( socket_id == SOCKET_BALANCE )
	{
		TInt st = -1;
		TInt en = -1;
		
		if ( strBalancePre.Length() > 0 )
		{
			st = dt.Find(strBalancePre);
			if ( st >= 0 )st += strBalancePre.Length();
		}
		if ( strBalancePost.Length() > 0 )en = dt.Find(strBalancePost);
		
		if ( st < 0 && dt.Find(_L8("\r\n\r\n")) >= 0 )st = dt.Find(_L8("\r\n\r\n")) + 4;
		
		if ( st < 0 )st = 0;		
		if ( en < 0 )en = dt.Length();
		
		strBalance.Zero();
		if ( ( en - st ) > 0 )
		{
			strBalance.Append(strBalanceDisplayPrefix);
			strBalance.Append(_L8(" : "));
		}
		if ( ( en - st + strBalance.Length()) > strBalance.MaxLength() )strBalance.Append(dt.Mid(st,strBalance.MaxLength()-strBalance.Length()));
		else strBalance.Append(dt.Mid(st,en-st));
		
		OnDialerStateChange();
		return;
	}
	
	iRtpReceiveBuffer.Zero();
	for ( int i = 0; i < len; i++ )
	{
		if ( dt[i] == 0 )	// Media data
		{
			if ( i >= ( len - 8 ) )continue;
			TUint32 media_seq = dt[i+1];
			TUint32 drop_percentage = dt[i+2];
			TUint32 media_length = ( dt[i+3] << 8 ) + dt[i+4];
			TUint32 padding_length = dt[i+5];		
			TUint32 packet_length = media_length + padding_length + 8;
		
			if ( packet_length > len )continue;
			if ( i > ( len - packet_length ) )continue;
		
			TUint32 calculated_checksum = ( ( media_length * 2 + padding_length * 3 + 109 + media_seq ) & 0xFFFF );
			TRACE("calculated_checksum-->%d",calculated_checksum);
			TUint32 packet_checksum = dt[i + packet_length - 2];
			packet_checksum = ( ( packet_checksum << 8 ) + dt[i + packet_length - 1] );
			TRACE("packet_checksum-->%d",packet_checksum);
			if ( packet_checksum != calculated_checksum )
			{
				continue;				// Invalid packet
			}
			
			TInt media_start = i + 6;
			i += ( packet_length - 1 );
			if ( ( iRtpReceiveBuffer.Length() + media_length ) > iRtpReceiveBuffer.MaxLength() )continue;
			
			iRtpReceiveBuffer.Append(dt.Mid(media_start,media_length));
			continue;
		}
	
		if ( dt[i] != '[' )continue;
		int st = i + 1;
		for ( ; i < len; i++ )
		{			
			if ( dt[i] == ']' )
			{
				HandleVpnMessage(dt.Mid(st,i-st),i - st,socket_id);
				break;
			}
		}
	}
	
	if ( iRtpReceiveBuffer.Length() > 0 )
	{
		if ( iWaitingForRTPAfterCall )
		{
			iWaitingForRTPAfterCall = EFalse;
			OnDialerStateChange();
		}

		TInt x = iRtpReceiveBuffer.Length();
		if ( iRtpPausedForGSM == EFalse && IsAudioRunning() != EFalse )iAudioEngine->PlayRtp(iRtpReceiveBuffer);
	}	
}

void DialerAgent::OnSocketClosed(int socket_id)
{
	iNetworkManager->RemoveSocket(socket_id);
	
	if ( socket_id != SOCKET_AUTH )return;		
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED )return;	
	
	TInt parse_reply = ParseAuthenticationReply(iAuthReply,iAuthReply.Length());
	
	if ( parse_reply < 0 )
	{
		if ( iAuthRetryNo >= 6 )
		{
			SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INTERNET_ERROR);
			iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,EFalse,ETrue);
			return;
		}
		Authenticate(iAuthRetryNo + 1);
		return;
	}
	
	if ( parse_reply == 0 )
	{
		iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,EFalse,EFalse);
		return;
	}
	
	
	TBool proceed_connecting = EFalse;
	
	if ( iOperatorCode < 0 || iOperatorType < 0 )
	{
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INVALID_OPERATORCODE);
		iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,EFalse,EFalse);
	}
	else if ( strSipUsername.Length() <= 0 )
	{
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_ACCOUNT_NOT_COMPLETE);
		iDialerUser->OnDialerAgentAuthComplete(ETrue,ETrue,EFalse,EFalse);
	}
	else if ( iOperatorType == 2 )
	{
		if ( iSwitchIPLocal == 0 || iSwitchPortLocal == 0 )
		{
			SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_ACCOUNT_NOT_COMPLETE);
			iDialerUser->OnDialerAgentAuthComplete(ETrue,ETrue,EFalse,EFalse);
		}
		else
		{
			CharArray(24,str_tmp);
			str_tmp.Zero();
			str_tmp.AppendNum((iSwitchIPLocal >> 24) & 0xFF);	str_tmp.Append('.');
			str_tmp.AppendNum((iSwitchIPLocal >> 16) & 0xFF);	str_tmp.Append('.');
			str_tmp.AppendNum((iSwitchIPLocal >> 8 ) & 0xFF);	str_tmp.Append('.');
			str_tmp.AppendNum(iSwitchIPLocal & 0xFF);			str_tmp.Append('#');
			str_tmp.AppendNum(iSwitchPortLocal & 65535);		str_tmp.Append('#');
			
			iSipTransferInfo.Insert(0,str_tmp);
			
			strBalanceUrl.Zero();			
			strBalanceUrl.Copy(strBalanceUrlLocal);
			
			proceed_connecting = ETrue;
		}
	}
	else proceed_connecting = ETrue;
	
	if ( proceed_connecting )
	{
		iDialerUser->OnDialerAgentAuthComplete(ETrue,EFalse,EFalse,EFalse);
		SetCurrentState(LOCAL_STATE_CONNECTING);
	}	
	iDialerUser->OnDialerAgentChangeState();
}

void DialerAgent::HandleVpnMessage(TPtrC8 c,TInt len,TInt socket_id)
{
	if ( c.Find(_L8("TICKET:")) == 0 )
	{
		if ( iLocalState != LOCAL_STATE_CONNECTING )return;
		iNetworkManager->RemoveRequestFromQueue(-1,DATA_TYPE_TICKET,ETrue);
		
		iEstablishedVpnSocketID = socket_id;
		iVpnUdpActive = ( ( iEstablishedVpnSocketID & 0x0F ) == SOCKET_UDP1 );
		
		for ( TInt i = 0; i < MAX_VPN_COUNT; i++ )
		{
			if ( iEstablishedVpnSocketID != ( i << 4 ) + SOCKET_UDP1 )iNetworkManager->RemoveSocket( ( i << 4 ) + SOCKET_UDP1 );
			if ( iEstablishedVpnSocketID != ( i << 4 ) + SOCKET_TCP )iNetworkManager->RemoveSocket( ( i << 4 ) + SOCKET_TCP );
		}
		
		TInt st = 7;
		TUint32 time_stamp = Globals::GetNextInt(c,st,len,':',']');
		
		iVpnRoundTripMS = CurrentTimeMS() - time_stamp;

		TUint32 ticket_index = Globals::GetNextInt(c,st,len,':',']');
		
		TPtrC8 str_ticket = Globals::GetNextString(c,st,len,':',']');

		SetCurrentState(LOCAL_STATE_VPN_CONNECTED);
		SendBalanceRequest();
		
		iAlternateChannelState = ALT_CHANNEL_NOT_TESTED;
		
		ResetDialerStateSuper();
		RegisterSuper(ticket_index,str_ticket,iOperatorCode,10,1,strImei,strSipUsername,strSipPassword,strSrcPhoneNo);
				
		return;
	}

	if ( c.Find(_L8("IMAGE:")) == 0 || c.Find(_L8("ACK:")) == 0)
	{
		OnReceiveImageSuper(c,c.Length());
		return;
	}
	
	if ( c.Find(_L8("CHANNELOK:")) == 0 )
	{
		if ( iAlternateChannelState != ALT_CHANNEL_BEING_TESTED )return;
		
		iAlternateChannelState = ALT_CHANNEL_CONNECTED;
		iNetworkManager->RemoveRequestFromQueue(-1,DATA_TYPE_CHANNEL_TEST,ETrue);
		
		TInt st = 10;
		TUint32 time_stamp = Globals::GetNextInt(c,st,len,':',']');
		TUint32 connection_no = Globals::GetNextInt(c,st,len,':',']');
		TUint32 session_id = Globals::GetNextInt(c,st,len,':',']');
		iEstablishedVpnSocketIDAlt = Globals::GetNextInt(c,st,len,':',']');
				
		for ( TInt i = 0; i < MAX_VPN_COUNT; i++ )
		{
			if ( iEstablishedVpnSocketIDAlt == ( i << 4 ) + SOCKET_UDP2 )continue;
			iNetworkManager->RemoveSocket( ( i << 4 ) + SOCKET_UDP2 );
		}
	}
}

void DialerAgent::SendToEstablishedVPN(TPtrC8 dt,TInt len,TBool b_retry_data,TUint32 wait_ms)
{
	iNetworkManager->SendData(iEstablishedVpnSocketID,dt,( ( b_retry_data == EFalse ) ? DATA_TYPE_BASIC : DATA_TYPE_BASIC_RETRY ),wait_ms);
}

void DialerAgent::RemoveRetryData()
{
	iNetworkManager->RemoveRequestFromQueue(iEstablishedVpnSocketID,DATA_TYPE_BASIC_RETRY,EFalse);
}

void DialerAgent::SendRtpFromAudioEngine(TPtrC8 dt,TInt len)
{	
	if ( iSoundTestRunning )
	{		
		iRtpReceiveBuffer.Zero();
		for ( TInt i = 0; i < len; i += 40 )
		{
			TInt l = 40;
			if ( ( i + l ) > len )l = len - i;
			
			if ( l % 10 )l -= ( l % 10 );
			
			iRtpReceiveBuffer.Append(l);
			iRtpReceiveBuffer.Append(dt.Mid(i,l));
		}
		
		iAudioEngine->PlayRtp(iRtpReceiveBuffer);
		return;
	}
	
	if ( IsAudioRunning() == EFalse )return;
	if ( iRtpPausedForGSM )return;
	
	len = dt.Length();
	if ( len <= 0 )return;
	
	if ( len > 1000 )len = 1000;
	
	TUint padding_len = 0;
	if ( iRtpPaddingSize > 0 )padding_len = ( iAudioEngine->GetCurrentTimeMicroSecond() + 0xFF ) % iRtpPaddingSize;
	TUint32 checksum_val = ( ( len * 2 + padding_len * 3 + 109 + ( iRtpSendSeq & 0xFF ) ) & 0xFFFF );
	
	iRtpSendBuffer.Zero();
	iRtpSendBuffer.Append(0);							// RTP IDENTIFIER
	iRtpSendBuffer.Append( ( iVpnConnectionNo >> 8 ) & 0xFF );
	iRtpSendBuffer.Append( iVpnConnectionNo & 0xFF );
	iRtpSendBuffer.Append( iVpnSessionID >> 8 );
	iRtpSendBuffer.Append( iVpnSessionID & 0xFF );
	iRtpSendBuffer.Append( ( iRtpSendSeq & 0xFF ) );
	iRtpSendBuffer.Append(0);							// Drop percentage
	iRtpSendBuffer.Append( ( len >> 8 ) & 0xFF );
	iRtpSendBuffer.Append(len & 0xFF);
	iRtpSendBuffer.Append(padding_len);
	iRtpSendBuffer.Append(dt);
	for ( TInt i = 0; i < padding_len; i++ )iRtpSendBuffer.Append( ( padding_len + i ) & 0xFF );
	iRtpSendBuffer.Append( ( checksum_val >> 8 ) & 0xFF );
	iRtpSendBuffer.Append( checksum_val & 0xFF );
	
	if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )iNetworkManager->SendData(iEstablishedVpnSocketIDAlt,iRtpSendBuffer,DATA_TYPE_RTP,0);
	else iNetworkManager->SendData(iEstablishedVpnSocketID,iRtpSendBuffer,DATA_TYPE_RTP,0);
	
	iRtpSendSeq++;
}

void DialerAgent::SendBalanceRequest()
{
	if ( strBalanceUrl.Length() < 7 )return;
	if ( strBalanceUrl.Find(_L8("REPLACE")) > 0 )strBalanceUrl.Replace(strBalanceUrl.Find(_L8("REPLACE")),7,strSipUsername);
		
	iNetworkManager->FetchUrl(SOCKET_BALANCE,strBalanceUrl,DATA_TYPE_BALANCE_REQUEST,0);
}

void DialerAgent::SetCurrentState(int current_state,int current_state_message)
{
	if ( current_state >= 0 )
	{
		iLocalState = current_state;
		iLocalStateOptional = 0;

		iLocalStateStartTime = CurrentTime();
	}

	iLocalStateMessage = current_state_message;
	iLocalStateMessageStartTime = CurrentTime();
	
	if ( iDialerUser != NULL )iDialerUser->OnDialerAgentChangeState();
}


void DialerAgent::Call(TPtrC8 dest_no)
{
	 if ( dest_no.Length() == 0 )return;
	 
	 iAudioEngine->SetSpeaker(EFalse);
	 
	 tmCallStart = CurrentTime();
	 strDialedNo.Zero();
	 strDialedNo.Copy(dest_no);

	 iWaitingForRTPAfterCall = ETrue;
	 
	 iRtpSendSeq = 0;
	 InternalCallSuper(strDialedNo);
	 
	 iAudioEngine->ResetAudioBuffer();	 
}

void DialerAgent::EndCall()
{
	iSoundTestRunning = EFalse;
		
	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )CancelPressedSuper();
	else if ( iLocalState == LOCAL_STATE_UNINITIALIZED );	
	else
	{
		iNetworkManager->StopAndResetAll();
		
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_SELF_CANCELLED);
		if ( iLocalState == LOCAL_STATE_AUTHENTICATING || iLocalState == LOCAL_STATE_AUTHENTICATE_REQUEST )iDialerUser->OnDialerAgentAuthComplete(EFalse,EFalse,ETrue,EFalse);			
	}
	
	if ( iDialerUser != NULL )iDialerUser->OnDialerAgentChangeState();
}

void DialerAgent::Reset(TBool show_accesspoint_menu,TInt operator_code,TPtrC str_user,TPtrC str_password,TPtrC str_src_phone,TPtrC str_balance_url,TUint32 switch_ip,TInt switch_port,TPtrC8 str_imei,TPtrC8 str_device_model)
{
	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )UnRegisterSuper();
	
	iOperatorCode = operator_code;
	
	strSipUsername.Zero();		strSipUsername.Copy(str_user);
	strSipPassword.Zero();		strSipPassword.Copy(str_password);
	strSrcPhoneNo.Zero();		strSrcPhoneNo.Copy(str_src_phone);
	strBalanceUrlLocal.Zero();	strBalanceUrlLocal.Copy(str_balance_url);
	strImei.Zero();				strImei.Copy(str_imei);
	strDeviceModel.Zero();		strDeviceModel.Copy(str_device_model);
	
	iSwitchIPLocal = switch_ip;
	iSwitchPortLocal = switch_port;
	
	ResetDialerStateSuper();
	SetCurrentState(LOCAL_STATE_INITIALIZING);
	
	iNetworkManager->ResetAndSelectAccessPoint((show_accesspoint_menu == EFalse) ? iSelectedAccessPointID : -1 );
	
	if ( iAudioEngine )iAudioEngine->ForceStop();
}

DialerAgent::DialerAgent(DialerAgentUserInterface* dialer_user,AudioEngineSymbian* audio_engine,TPtrC8 str_state,TPtrC8 str_status,TPtrC8 str_lstate,TPtrC8 str_lstatus,TPtrC8 balance_prefix,TPtrC8 duration_prefix)
{
	iDialerUser = dialer_user;
	iAudioEngine = audio_engine;
	iNetworkManager = NULL;

	iStateStr.Zero();	iStateStr.Copy(str_state);
	iStatusStr.Zero();	iStatusStr.Copy(str_status);
	
	iLStateStr.Zero();	iLStateStr.Copy(str_lstate);
	iLStatusStr.Zero();	iLStatusStr.Copy(str_lstatus);
	
	strBalanceDisplayPrefix.Zero();		strBalanceDisplayPrefix.Copy(balance_prefix);
	strDurationDisplayPrefix.Zero();	strDurationDisplayPrefix.Copy(duration_prefix);
	
	iSoundTestRunning = EFalse;
	iVpnRoundTripMS = 0;	

	iLocalState = LOCAL_STATE_UNINITIALIZED;
	iLocalStateMessage = LOCAL_STATE_MESSAGE_NONE;

	iOperatorCode = 2010;
	iSelectedAccessPointID = -1;
	
	strImei.Zero();
	strDeviceModel.Zero();
	
	iAuthReply.SetLength(3000);			iAuthReply.SetLength(0);
	iRtpSendBuffer.SetLength(1000);		iRtpSendBuffer.SetLength(0);
	iRtpReceiveBuffer.SetLength(1000);	iRtpReceiveBuffer.SetLength(0);
	strTemp.SetLength(1000);			strTemp.SetLength(0);
	//iRecordBuf.SetLength(1000);			iRecordBuf.SetLength(0);
	strVpnRequest.SetLength(512);		strVpnRequest.SetLength(0);

	InitStatusMessage();

	if ( iDialerUser != NULL )iDialerUser->OnDialerAgentChangeState();
}

TPtrC DialerAgent::GetChannelType()
{
#ifndef __C_PLUS_PLUS_BUILD__
	if ( iVpnUdpActive )
	{
		if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return _L("UDP 2 CHANNEL");
		if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return _L("UDP TEST CHANNEL");
		return _L("UDP 1 CHANNEL");
	}

	if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return _L("TCP UDP CHANNEL");
	if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return _L("TCP TEST CHANNEL");
	return _L("TCP 1 CHANNEL");
#else
	if ( iVpnUdpActive )
	{
		if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return Globals::TempWString("UDP 2 CHANNEL");
		if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return Globals::TempWString("UDP TEST CHANNEL");
		return Globals::TempWString("UDP 1 CHANNEL");
	}

	if ( iAlternateChannelState == ALT_CHANNEL_CONNECTED )return Globals::TempWString("TCP UDP CHANNEL");
	if ( iAlternateChannelState == ALT_CHANNEL_NOT_TESTED || iAlternateChannelState == ALT_CHANNEL_BEING_TESTED )return Globals::TempWString("TCP TEST CHANNEL");
	return Globals::TempWString("TCP 1 CHANNEL");
#endif
}

TUint32 DialerAgent::GetRoundTripMS()
{
	return iVpnRoundTripMS;
}

void DialerAgent::OnOperatorCodeNotSelected()
{
	strDialerName.Zero();
	strDialerFooter.Zero();
	
	SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_OPERATORCODE_NOT_SET);
}

void DialerAgent::OnInternetCancelled()
{
	strDialerName.Zero();
	strDialerFooter.Zero();
		
	SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_INTERNET_ERROR);	
}

void DialerAgent::OnAudioServiceError(TBool service_not_found)
{
	strDialerName.Zero();
	strDialerFooter.Zero();
	
	if ( service_not_found == EFalse )SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_CODEC_NOT_SUPPORTED);
	else SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_AUDIO_SERVICE_NOT_FOUND);
}

bool DialerAgent::Start()
{
	iEstablishedVpnSocketID = -1;
	iEstablishedVpnSocketIDAlt = -1;
	
	iOperatorType = -1;	
	iAlternateChannelState = ALT_CHANNEL_NOT_TESTED;

	iSoundTestRunning = EFalse;
	
	strBalance.Zero();
	iPrevDialerStateForBalanceUpdate = 0;
	
	SetCurrentState(LOCAL_STATE_AUTHENTICATE_REQUEST);
	return true;
}

TBool DialerAgent::StartSoundTest()
{
	if ( iSoundTestRunning )return ETrue;
	
	CharArray(201,temp_buf);
	temp_buf.Zero();
	temp_buf.Append(200);
	for ( TInt i = 0; i < 200; i++ )temp_buf.Append(i+1);
	
	iAudioEngine->PlayRtp(temp_buf);
	
	iSoundTestRunning = ETrue;
	OnDialerStateChange();
	return iSoundTestRunning;
}

TBool DialerAgent::SendFeedbackToAuthServer(TPtrC8 dt)
{
	if ( iLocalState == LOCAL_STATE_UNINITIALIZED || iLocalState == LOCAL_STATE_AUTHENTICATING || iLocalState == LOCAL_STATE_AUTHENTICATE_REQUEST )return EFalse;
	iNetworkManager->FetchUrl(SOCKET_FEEDBACK,dt,DATA_TYPE_BASIC,0);
}

void DialerAgent::Authenticate(TInt retry_no)
{
	SetCurrentState(LOCAL_STATE_AUTHENTICATING);
	iRtpPausedForGSM = EFalse;
	
	iOperatorType = -1;
	iSipTransferInfo.Zero();
	
	CharArray(256,str_url);
	str_url.Zero();
	
	iAuthRandom = 0;
	for ( TInt i = 0; i < strImei.Length(); i++ )iAuthRandom = iAuthRandom * 10 + strImei[i] - '0';
	
	iAuthRetryNo = retry_no;
	
	/*if ( iAuthRetryNo == 0 )str_url.Append(_L8("http://dialer.opentechbd.com/dialersettings.php?pin="));
	else if ( iAuthRetryNo == 1 )str_url.Append(_L8("http://209.59.218.184/dialersettings.php?pin="));				// vpn.opentechbd.com
	else if ( iAuthRetryNo == 2 )str_url.Append(_L8("http://smoothsolution.org/dialersettings.php?pin="));
	else if ( iAuthRetryNo == 3 )str_url.Append(_L8("http://smoothdialer.com/dialersettings.php?pin="));
	else if ( iAuthRetryNo == 4 )str_url.Append(_L8("http://204.15.78.227/dialersettings.php?pin="));			// vpn_227	
	else str_url.Append(_L8("http://dialer.opentechbd.com/dialersettings.php?pin="));
	*/
	
	str_url.Append(_L8("http://38.108.92.243/dialer/dialersettings.php?pin="));

	str_url.AppendNum(iOperatorCode);
	str_url.Append(_L8("&auth="));
	str_url.AppendNum(iAuthRandom);
	str_url.Append(_L8("&v=1&type=dialer&dialerimei="));
	str_url.Append(strImei);
	str_url.Append(_L8("&dialeros=10&model="));
	str_url.Append(strDeviceModel);
	str_url.Append(_L8("&usr="));
	str_url.Append(strSipUsername);
	str_url.Append(_L8("&u_agent=4"));
	
	iAuthReply.Zero();
	iNetworkManager->FetchUrl(SOCKET_AUTH,str_url,DATA_TYPE_AUTHENTICATE,0);
}


void DialerAgent::FlexiAuthenticate(CString userID, CString requestKey, CString requestCode){
	CharArray(256,str_url);
	str_url.Zero();
	//str_url.Append(GetFlexiGateway());
	//str_url.Append(_L8("38.127.68.201/m.24topup/mobile/"));
	str_url.Append(_L8("192.168.1.45:8084/m.inet/mobile/"));
	str_url.Append(_L8("loginapi.jsp?RequestKey="));
	str_url.Append(requestKey);
	str_url.Append(_L8("&RequestCode="));
	str_url.Append(requestCode);
	str_url.Append(_L8("&UserId="));
	str_url.Append(userID);
	//str_url.Delete(0,8);  //Removing http:// 

	iNetworkManager->FetchUrl(SOCKET_FLEXI,str_url,DATA_TYPE_BASIC,0);
	/*CString flexiReply = GetFlexiBalanceStr().GetBuffer();
	AfxMessageBox(flexiReply);*/


	//CString str;
	//str.Format("%s", str_url);

	/*WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0) {
		AfxMessageBox("WSAStartup failed");
    }

	/*SOCKET Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    struct hostent *host;
	host = gethostbyname("38.127.68.201");
    SOCKADDR_IN SockAddr;

    SockAddr.sin_port=htons(80);
    SockAddr.sin_family=AF_INET;
    SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

   if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr)) != 0){
	   AfxMessageBox("Could not connect");
    }
   
	SOCKET Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_UDP);
	struct hostent *host;
	host = gethostbyname("38.127.68.201");
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = *((unsigned long*)host->h_addr);
	addr.sin_port = htons(80);

	//GET /m.24topup/mobile/loginapi.jsp?RequestKey=1377488154586.000000&RequestCode=88a5a72c77b10efdfbeee56e32cfd3b1&UserId=support1 HTTP/1.0\r\nHost:38.127.68.201\r\n\r\n
	sendto(Socket,"GET /m.24topup/mobile/loginapi.jsp?RequestKey=1377488154586.000000&RequestCode=88a5a72c77b10efdfbeee56e32cfd3b1&UserId=support1 HTTP/1.0\r\nHost:38.127.68.201\r\n\r\n",strlen("GET /m.24topup/mobile/loginapi.jsp?RequestKey=1377488154586.000000&RequestCode=88a5a72c77b10efdfbeee56e32cfd3b1&UserId=support1 HTTP/1.0\r\nHost:38.127.68.201\r\n\r\n"),0,(struct sockaddr*)(&addr),sizeof(addr));
    //send(Socket,"GET / HTTP/1.1\r\nHost: www.google.com\r\nConnection: close\r\n\r\n", strlen("GET / HTTP/1.1\r\nHost: www.google.com\r\nConnection: close\r\n\r\n"),0);
    //send(Socket,"GET / HTTP/1.1\r\nHost: "+ str + "\r\nConnection: close\r\n\r\n", strlen("GET / HTTP/1.1\r\nHost: "+ str + "\r\nConnection: close\r\n\r\n"),0);
	//send(Socket, str, strlen(str),0);
	//sendto(Socket, "GET //38.127.68.201/m.24topup/mobile/loginapi.jsp?RequestKey=1376983802965.000000&RequestCode=fa04c10b2838e93ba15fe71503d6e458&UserId=support1 HTTP/1.1\r\nHost: http\r\n\r\n", strlen("GET //38.127.68.201/m.24topup/mobile/loginapi.jsp?RequestKey=1376983802965.000000&RequestCode=fa04c10b2838e93ba15fe71503d6e458&UserId=support1 HTTP/1.1\r\nHost: http\r\n\r\n"),0);
    char buffer[10000];
    int nDataLength;
    while ((nDataLength = recv(Socket,buffer,10000,0)) > 0){        
        int i = 0;
        while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r') {
            //cout << buffer[i];			
            i += 1;
        }
		
    }

	AfxMessageBox(buffer);
    closesocket(Socket);
    WSACleanup();
	*/
	
}
void DialerAgent::FlexiRecharge(CString userID, CString requestKey, CString requestCode, CString mobileNum, CString moneyAmount, CString numberType){
	CharArray(256,str_url);
	str_url.Zero();
	//str_url.Append(GetFlexiGateway());
	//str_url.Append(_L8("38.127.68.201/m.24topup/mobile/"));
	str_url.Append(_L8("192.168.1.45:8084/m.inet/mobile/"));
	str_url.Append(_L8("flexiapi.jsp?RequestKey="));
	str_url.Append(requestKey);
	str_url.Append(_L8("&RequestCode="));
	str_url.Append(requestCode);
	str_url.Append(_L8("&UserId="));
	str_url.Append(userID);
	str_url.Append(_L8("&MobileNumber="));
	str_url.Append(mobileNum);
	str_url.Append(_L8("&Amount="));
	str_url.Append(moneyAmount);
	str_url.Append(_L8("&NumberType="));
	str_url.Append(numberType);
	//str_url.Delete(0,8);  //Removing http:// 

	iNetworkManager->FetchUrl(SOCKET_FLEXI,str_url,DATA_TYPE_BASIC,0);
}

void DialerAgent::FlexiRechargeActivity(CString userID, CString requestKey, CString requestCode, CString listType){
	CharArray(256,str_url);
	str_url.Zero();
	//str_url.Append(GetFlexiGateway());
	//str_url.Append(_L8("38.127.68.201/m.24topup/mobile/"));
	str_url.Append(_L8("192.168.1.45:8084/m.inet/mobile/"));
	str_url.Append(_L8("jsonlistapi.jsp?RequestKey="));
	str_url.Append(requestKey);
	str_url.Append(_L8("&RequestCode="));
	str_url.Append(requestCode);
	str_url.Append(_L8("&UserId="));
	str_url.Append(userID);
	str_url.Append(_L8("&listType="));
	str_url.Append(listType);
	//str_url.Delete(0,8);  //Removing http:// 

	iNetworkManager->FetchUrl(SOCKET_FLEXI,str_url,DATA_TYPE_BASIC,0);
}

void DialerAgent::SendSMSMessage(CString userID, CString phoneNum, CString message, CString requestKey, CString requestCode){
	CharArray(256,str_url);
	str_url.Zero();
	//str_url.Append(GetSMSGateway());
	str_url.Append(_L8("192.168.1.45:8084/m.inet/mobile/"));
	str_url.Append(_L8("smsapi.jsp?RequestKey="));
	str_url.Append(requestKey);
	str_url.Append(_L8("&RequestCode="));
	str_url.Append(requestCode);
	str_url.Append(_L8("&UserId="));
	str_url.Append(userID);
	str_url.Append(_L8("&MobileNumber="));
	str_url.Append(phoneNum);
	str_url.Append(_L8("&Message="));
	str_url.Append(message);
	//str_url.Delete(0,8);  //Removing http:// 

	iNetworkManager->FetchUrl(SOCKET_SMS,str_url,DATA_TYPE_BASIC,0);
}

void DialerAgent::SMSHistoryActivity(CString userID, CString requestKey, CString requestCode, CString listType){
	CharArray(256,str_url);
	str_url.Zero();
	//str_url.Append(GetFlexiGateway());
	//str_url.Append(_L8("38.127.68.201/m.24topup/mobile/"));
	str_url.Append(_L8("192.168.1.45:8084/m.inet/mobile/"));
	str_url.Append(_L8("smslistapi.jsp?RequestKey="));
	str_url.Append(requestKey);
	str_url.Append(_L8("&RequestCode="));
	str_url.Append(requestCode);
	str_url.Append(_L8("&UserId="));
	str_url.Append(userID);
	str_url.Append(_L8("&listType="));
	str_url.Append(listType);
	//str_url.Delete(0,8);  //Removing http:// 

	iNetworkManager->FetchUrl(SOCKET_SMS,str_url,DATA_TYPE_BASIC,0);
}

void DialerAgent::CheckDialerActivity()			// This function should return immediately
{
	if ( iLocalState == LOCAL_STATE_AUTHENTICATE_REQUEST )
	{
		Authenticate();
		return;
	}

	if ( iLocalState == LOCAL_STATE_CONNECTING )
	{
		if ( CurrentTimeMS() < iVpnConnectTimeoutMS )return;		
		SetCurrentState(LOCAL_STATE_UNINITIALIZED,LOCAL_STATE_MESSAGE_VPN_NOT_FOUND);		
		return;
	}

	if ( iLocalState == LOCAL_STATE_VPN_CONNECTED )
	{
		if ( iAlternateChannelState == ALT_CHANNEL_BEING_TESTED && CurrentTimeMS() >= iVpnConnectTimeoutMSAlt )
		{
			iAlternateChannelState = ALT_CHANNEL_NOT_CONNECTED;		
		}
		OnDialerStateTickSuper();
	}
}
