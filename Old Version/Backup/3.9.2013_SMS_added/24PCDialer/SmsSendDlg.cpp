// SmsSendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmsSendDlg.h"
#include "afxdialogex.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"



// SmsSendDlg dialog

IMPLEMENT_DYNAMIC(SmsSendDlg, CDialogEx)

SmsSendDlg::SmsSendDlg(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(SmsSendDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
}

SmsSendDlg::~SmsSendDlg()
{
}

BOOL SmsSendDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void SmsSendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(SmsSendDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &SmsSendDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &SmsSendDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// SmsSendDlg message handlers


void SmsSendDlg::OnBnClickedOk()
{
	CString phnNum;
	CString msg;
	GetDlgItemText(IDC_SMS_PHONE_NUM_EDIT,phnNum);
	GetDlgItemText(IDC_SMS_SEND_EDIT,msg);

	if(phnNum==""){
		int Answer;
   		Answer = AfxMessageBox("Phone Number can not be empty",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);
	}else{

		SYSTEMTIME timeInMillis;
		GetSystemTime(&timeInMillis);	
		double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
		CString reqKey;
		reqKey.Format("%f", sysTime);

		std::string hash;
		crypto::errorinfo_t lasterror;
		CT2A atext(reqKey + flexiPassword);

		crypto::md5_helper_t hhelper;
		hash = hhelper.hexdigesttext(atext.m_szBuffer);
		lasterror = hhelper.lasterror();

		iDialerAgent -> SendSMSMessage(flexiUser , phnNum.Trim(), msg, reqKey, hash.c_str());
	}

	CDialogEx::OnOK();
}


void SmsSendDlg::OnBnClickedCancel()
{
	EndDialog(1);
	CDialogEx::OnCancel();
}
