#pragma once
#include "resource.h"
#include "afxcmn.h"

// FlexitListDlg dialog

class FlexitListDlg : public CDialogEx
{
	DECLARE_DYNAMIC(FlexitListDlg)

public:
	FlexitListDlg(CString str);   // standard constructor
	virtual ~FlexitListDlg();

// Dialog Data
	enum { IDD = IDD_FLEXI_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_flexi_list;
	CString strMsg;
};
