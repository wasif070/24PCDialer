// SmsSendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmsSendDlg.h"
#include "afxdialogex.h"
#include <windows.h>
#include <ctime>
#include "cryptohash.h"




// SmsSendDlg dialog

IMPLEMENT_DYNAMIC(SmsSendDlg, CDialogEx)

SmsSendDlg::SmsSendDlg(CWnd* pParent /*=NULL*/, DialerAgent * iDial)
	: CDialogEx(SmsSendDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	iDialerAgent = iDial;
	iDialerAgent -> setSendSMSDlg(this);
}

SmsSendDlg::~SmsSendDlg()
{
}

BOOL SmsSendDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	CStatic * m_Label;
	CFont *m_Font1 = new CFont;
	m_Font1->CreatePointFont(100, "Cambria Bold");
	m_Label = (CStatic *)GetDlgItem(IDC_FLEXI_BALANCE_STATIC);
	m_Label->SetFont(m_Font1);

	SYSTEMTIME timeInMillis;
	GetSystemTime(&timeInMillis);	
	double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
	CString reqKey;
	reqKey.Format("%f", sysTime);

	std::string hash;
	crypto::errorinfo_t lasterror;
	CT2A atext(reqKey + flexiPassword);

	crypto::md5_helper_t hhelper;
	hash = hhelper.hexdigesttext(atext.m_szBuffer);
	lasterror = hhelper.lasterror();

	iDialerAgent -> FlexiAuthenticate(flexiUser , reqKey, hash.c_str());

	if(phoneNum.GetLength()>0){
		SetDlgItemText(IDC_SMS_PHONE_NUM_EDIT, phoneNum);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void SmsSendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(SmsSendDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &SmsSendDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &SmsSendDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


void SmsSendDlg::OnSendSMSDlgChangeState(){
	CString flexiReply = iDialerAgent ->GetFlexiBalanceStr().GetBuffer();
	SetDlgItemText(IDC_FLEXI_BALANCE_STATIC, flexiReply);
	CString smsReply = iDialerAgent -> GetSMSStr().GetBuffer();
	if(smsReply.GetLength()>0){		
		SetDlgItemText(IDC_FLEXI_BALANCE_STATIC, smsReply);
		if(smsReply.Find("successfully")>0){
			EndDialog(1);
		}
	}

}

void SmsSendDlg::OnBnClickedOk()
{
	CString phnNum;
	CString msg;
	GetDlgItemText(IDC_SMS_PHONE_NUM_EDIT,phnNum);
	//SmsSendDl
	GetDlgItemText(IDC_SMS_SEND_EDIT,msg);


	if(phnNum==""){
		int Answer;
   		Answer = AfxMessageBox("Phone Number can not be empty",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);
	}else{

		SYSTEMTIME timeInMillis;
		GetSystemTime(&timeInMillis);	
		double sysTime = time(0) * 1000 + timeInMillis.wMilliseconds;	
			
		CString reqKey;
		reqKey.Format("%f", sysTime);

		std::string hash;
		crypto::errorinfo_t lasterror;
		CT2A atext(reqKey + flexiPassword);

		crypto::md5_helper_t hhelper;
		hash = hhelper.hexdigesttext(atext.m_szBuffer);
		lasterror = hhelper.lasterror();

		int index;
		CString encode;
		for(int i=0; i<msg.GetLength(); i++){
		//	if(IsCharAlphaNumeric(msg[i])){
	//			encode += msg[i];
		//	}else{
				char hexval[2];
				index = i;
				int intval = msg[i];
				sprintf(hexval,"%02X",intval);
				i = index;
				encode += "%";
				encode += hexval;
		//	}			
		}
		iDialerAgent -> SendSMSMessage(flexiUser , phnNum.Trim(), encode, reqKey, hash.c_str());
	}
	//SetDlgItemText(IDC_SMS_PHONE_NUM_EDIT,"");
	//SetDlgItemText(IDC_SMS_SEND_EDIT,"");
	/*iDialerAgent ->status = FALSE;
	while(iDialerAgent ->status == TRUE){		
		CDialogEx::OnOK();
	}*/
}


void SmsSendDlg::OnBnClickedCancel()
{
	EndDialog(1);
	CDialogEx::OnCancel();
}


/*void SmsSendDlg::OnCbnDropdownComboCountryCode()
{
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("CountryCode.dat");
	//str_dest_no.Mid(0, str_dest_no.GetLength()-1
	m_combo_country_code.ResetContent();
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		m_combo_country_code.AddString(strLine);
	}
		readFile.Close();
	}
}*/
