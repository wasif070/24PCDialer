// ListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ListDlg.h"
#include "afxdialogex.h"


// ListDlg dialog

IMPLEMENT_DYNAMIC(ListDlg, CDialogEx)

ListDlg::ListDlg(CString str)
	: CDialogEx(ListDlg::IDD)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	strMsg = str;

}

ListDlg::~ListDlg()
{
}

void ListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_msg_list);
}


BEGIN_MESSAGE_MAP(ListDlg, CDialogEx)
END_MESSAGE_MAP()

static void AddData(CListCtrl &ctrl, int row, int col, const char *str)
{
    LVITEM lv;
    lv.iItem = row;
    lv.iSubItem = col;
    lv.pszText = (LPSTR) str;
    lv.mask = LVIF_TEXT;
    if(col == 0)
        ctrl.InsertItem(&lv);
    else
        ctrl.SetItem(&lv);   
}

BOOL ListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_msg_list.DeleteAllItems();
	m_msg_list.InsertColumn(0, "Phone Number");
    m_msg_list.SetColumnWidth(0, 100);

    m_msg_list.InsertColumn(1, "Message");
    m_msg_list.SetColumnWidth(1, 100);
    
    m_msg_list.InsertColumn(2, "Price");
    m_msg_list.SetColumnWidth(2, 40);

	m_msg_list.InsertColumn(3, "Time");
    m_msg_list.SetColumnWidth(3, 120);


	int counter = 0;
	int lineNum = 0;
	CString line = strMsg.Tokenize(_T("}"), lineNum);
	BOOL sts;
	CString strFile;
	while(!line.IsEmpty()){	
		
			int nTokenPos = 0;
			//CString newLine;
			CString token = line.Tokenize(_T(","), nTokenPos);
			while(!token.IsEmpty()){
				if(token.Find("status:")>0){	
					if(token.Find("status:success")>0){
						sts = TRUE;		
					}else{
						sts = FALSE;		
					}
				}else if(token.Mid(0,7)=="message"){
				     AddData(m_msg_list, counter , 1, token.Mid(8, token.GetLength()));		
				}else if(token.Mid(0,7)=="phoneNo"){ 
					AddData(m_msg_list, counter , 0, token.Mid(8, token.GetLength()));
					//newLine += token.Mid(8, token.GetLength()) + ","; //phone
				}
				else if(token .Mid(0,6)=="amount"){
					AddData(m_msg_list, counter , 2, token.Mid(7, token.GetLength()));
					//newLine += token.Mid(7, token.GetLength()) + ","; //price
				}
				else if(token.Mid(0,4)=="time") {
					AddData(m_msg_list, counter , 3, token.Mid(5, token.GetLength()));
					//newLine += token.Mid(5, token.GetLength()) + "\n"; //time
				}
				token = line.Tokenize(_T(","), nTokenPos);
			}
			line = strMsg.Tokenize(_T("}"), lineNum);
			counter++;
		}

	return TRUE; 
}
