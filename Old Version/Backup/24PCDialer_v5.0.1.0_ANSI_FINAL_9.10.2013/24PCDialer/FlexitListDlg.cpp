// FlexitListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FlexitListDlg.h"
#include "afxdialogex.h"


// FlexitListDlg dialog

IMPLEMENT_DYNAMIC(FlexitListDlg, CDialogEx)

FlexitListDlg::FlexitListDlg(CString str)
	: CDialogEx(FlexitListDlg::IDD)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	strMsg = str;
}

FlexitListDlg::~FlexitListDlg()
{
}

void FlexitListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FLEXI_LIST, m_flexi_list);
}


BEGIN_MESSAGE_MAP(FlexitListDlg, CDialogEx)
END_MESSAGE_MAP()


static void AddData(CListCtrl &ctrl, int row, int col, const char *str)
{
    LVITEM lv;
    lv.iItem = row;
    lv.iSubItem = col;
    lv.pszText = (LPSTR) str;
    lv.mask = LVIF_TEXT;
    if(col == 0)
        ctrl.InsertItem(&lv);
    else
        ctrl.SetItem(&lv);   
}

BOOL FlexitListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_flexi_list.DeleteAllItems();
	m_flexi_list.InsertColumn(0, "Phone Number");
    m_flexi_list.SetColumnWidth(0, 100);

    m_flexi_list.InsertColumn(1, "TopUp Balance");
    m_flexi_list.SetColumnWidth(1, 100);

	m_flexi_list.InsertColumn(2, "Time");
    m_flexi_list.SetColumnWidth(2, 100);


	int counter = 0;
	int lineNum = 0;
	CString line = strMsg.Tokenize(_T("}"), lineNum);
	BOOL sts;
	CString strFile;
	while(!line.IsEmpty()){			
			int nTokenPos = 0;
			//CString newLine;
			CString token = line.Tokenize(_T(","), nTokenPos);
			while(!token.IsEmpty()){
				if(token.Find("status:")>0){	
					if(token.Find("status:success")>0){
						sts = TRUE;		
					}else{
						sts = FALSE;		
					}
				}else if(token.Mid(0,7)=="phoneNo"){ 
					AddData(m_flexi_list, counter , 0, token.Mid(8, token.GetLength()));
				}
				else if(token .Mid(0,6)=="amount"){
					AddData(m_flexi_list, counter , 1, token.Mid(7, token.GetLength()));
					//newLine += token.Mid(7, token.GetLength()) + ","; //price
				}
				else if(token.Mid(0,4)=="time") {
					AddData(m_flexi_list, counter , 2, token.Mid(5, token.GetLength()));
				}
				token = line.Tokenize(_T(","), nTokenPos);
			}
			line = strMsg.Tokenize(_T("}"), lineNum);
			counter++;
		}

	return TRUE; 
}
