#ifndef __WPOLLER_8_AUGUST_2012_ADDED__
#define __WPOLLER_8_AUGUST_2012_ADDED__

#include "Globals.h"
#include "WThread.h"

#include <winsock.h>
#define GetCurrentTimeMS() clock()

class WPollerListener
{
public:
	virtual void OnAccessPointDone(TBool b_ok,TInt access_point_id ) = 0;
	virtual void OnSocketClosed(TInt socket_id) = 0;
	virtual void OnReceiveData(TInt socket_id,TPtrC8 dt,TInt len) = 0;
};

class TInetAddr
{
public:	
	unsigned int iAddress;
	int iPort;

	unsigned int Address()
	{
		return iAddress;
	}
};

class WPoller : public WThread
{
private:
	enum
	{
		SOCK_UDP = 1,
		SOCK_TCP = 2,
		SOCK_ACTION_CONNECTING = 4,
		SOCK_ACTION_CONNECTED = 8,
		SOCK_ACTION_SENDING = 16,

		MAX_SOCKET_COUNT = 20,
//		POLLIN = 1,
//		POLLOUT = 2,
		MAX_SEND_QUEUE = 100000,
		MAX_RECEIVE_QUEUE = 4096,

		QUEUE_COMMAND_RESET = 0,
		QUEUE_COMMAND_RESET_AND_SELECT_ACCESS_POINT = 1,
		QUEUE_COMMAND_FETCH_URL = 2,
		QUEUE_COMMAND_SEND = 3,
		QUEUE_COMMAND_ADD_TCP_SOCKET = 4,
		QUEUE_COMMAND_ADD_UDP_SOCKET = 5,
		QUEUE_COMMAND_REMOVE_SOCKET = 6
	};


	WPollerListener* iListener;
	TInetAddr iAddrTemp;

	unsigned int iCurrentTimeUS;

	fd_set	m_ActiveReadFdSet;
	fd_set	m_ActiveWriteFdSet;

	bool	bIsEmpty[MAX_SOCKET_COUNT];

	int		iSockHandle[MAX_SOCKET_COUNT];
	int		iSockID[MAX_SOCKET_COUNT];
	int		iSockDataType[MAX_SOCKET_COUNT];
	
	int		iSockType[MAX_SOCKET_COUNT];

	unsigned int iSockDestIP[MAX_SOCKET_COUNT];
	int		iSockDestPort[MAX_SOCKET_COUNT];
	
	char* iReceiveQueue;
	WString	iReceiveString;
	unsigned char* iSendQueue;

	unsigned int iSendQueueSize;

	char	iAddrStr[256];
	char	iAddrGetParam[256];
	char	iAddrGetRequest[512];
	char	iAddrHost[256];

	char	iNetworkErrorStr[512];

	int	IndexFromHandle(int sock_handle)
	{
		for ( int i = 0; i < MAX_SOCKET_COUNT; i++ )if ( bIsEmpty[i] == false && iSockHandle[i] == sock_handle )return i;
		return -1;
	}

	int	IndexFromID(int sock_id)
	{
		for ( int i = 0; i < MAX_SOCKET_COUNT; i++ )if ( bIsEmpty[i] == false && iSockID[i] == sock_id )return i;
		return -1;
	}

	bool AddRequestToQueue(int socket_id,int command,int data_type,const unsigned char* dt,int len,int wait_ms)
	{
		if ( ( iSendQueueSize + len + 14 ) > MAX_SEND_QUEUE )
		{
			AfxMessageBox("Request Queue overflowed");
			return false;
		}
		
		if ( socket_id < 0 )socket_id += 65536;
		if ( data_type < 0 )data_type += 65536;

		int execute_time_ms = GetCurrentTimeMS() + wait_ms;

		iSendQueue[iSendQueueSize++] = ( len + 14 ) / 256;
		iSendQueue[iSendQueueSize++] = ( len + 14 ) % 256;

		iSendQueue[iSendQueueSize++] = socket_id / 256;
		iSendQueue[iSendQueueSize++] = socket_id % 256;

		iSendQueue[iSendQueueSize++] = command / 256;
		iSendQueue[iSendQueueSize++] = command % 256;

		iSendQueue[iSendQueueSize++] = data_type / 256;
		iSendQueue[iSendQueueSize++] = data_type % 256;

		iSendQueue[iSendQueueSize++] = ( ( execute_time_ms >> 24 ) & 0xFF );
		iSendQueue[iSendQueueSize++] = ( ( execute_time_ms >> 16 ) & 0xFF );
		iSendQueue[iSendQueueSize++] = ( ( execute_time_ms >> 8 ) & 0xFF );
		iSendQueue[iSendQueueSize++] = ( execute_time_ms & 0xFF );

		memcpy(iSendQueue + iSendQueueSize,dt,len);
		iSendQueueSize += len;

		iSendQueue[iSendQueueSize++] = 0;
		iSendQueue[iSendQueueSize++] = 0;
		
		return true;
	}

	bool RemoveRequestFromQueueX(int pos)
	{
		if ( pos > ( iSendQueueSize - 14 ) )return false;
		
		int len = iSendQueue[pos] * 256 + iSendQueue[pos+1];
		if ( ( pos + len ) > iSendQueueSize )return false;

		for ( int i = pos; i < ( iSendQueueSize - len ); i++ )iSendQueue[i] = iSendQueue[i+len];
		iSendQueueSize -= len;

		return true;
	}

	const unsigned char* ParseRequest(int pos,int& socket_id,int& command,int& data_type,int& len,int& wait_ms)
	{
		if ( pos > ( iSendQueueSize - 14 ) )return NULL;

		len = iSendQueue[pos] * 256 + iSendQueue[pos+1];
		if ( ( pos + len ) > iSendQueueSize )return NULL;

		socket_id = iSendQueue[pos+2] * 256 + iSendQueue[pos+3];
		command = iSendQueue[pos+4] * 256 + iSendQueue[pos+5];
		data_type = iSendQueue[pos+6] * 256 + iSendQueue[pos+7];
		wait_ms = iSendQueue[pos+8] * 256 * 256 * 256 + iSendQueue[pos+9] * 256 * 256 + iSendQueue[pos+10] * 256 + iSendQueue[pos+11];

		if ( socket_id >= 32768 )socket_id -= 65536;
		if ( data_type >= 32768 )data_type -= 65536;

		len -= 14;
		return (const unsigned char *)(iSendQueue + pos + 12); // It is 12, not 14, because 2 0's are appended at end
	}

	bool StepToNextRequest(int& pos)
	{
		if ( pos >= ( iSendQueueSize - 2 ) )return false;

		int len = iSendQueue[pos] * 256 + iSendQueue[pos+1];
		if ( ( pos + len ) > iSendQueueSize )return false;

		pos += len;
		return true;
	}

public:
	enum
	{
		NETWORK_SOCKET_PULSE_MS	= 60
	};

	WPoller()
	{
		iReceiveQueue = new char[MAX_RECEIVE_QUEUE + 1000];
		iSendQueue = new unsigned char[MAX_SEND_QUEUE + 1000];
		iReceiveString.Zero();

		iListener = NULL;
		ResetSockets();
	}

	~WPoller()
	{		
		if ( iReceiveQueue )delete iReceiveQueue;
		if ( iSendQueue )delete iSendQueue;

		iReceiveQueue = NULL;
		iSendQueue = NULL;
	}

	void SetListener(WPollerListener* listener)
	{
		iListener = listener;
	}

	int Start()
	{
		WSADATA wsaData;
		u_long iMode = 0;

		if ( WSAStartup(MAKEWORD(2,2), &wsaData) != NO_ERROR )
		{
			AfxMessageBox("Error at WSAStartup()\n");
			return 0;
		}

		
		iCurrentTimeUS = 0;

		CreateThread(CREATE_SUSPENDED,10000);
		ResumeThread();

		return 1;
	}

	void SendData(int socket_id,WString& dt,int data_type,int wait_ms)
	{
		AddRequestToQueue(socket_id,QUEUE_COMMAND_SEND,data_type,dt.GetBuffer(),dt.Length(),wait_ms);
	}

	void SendData(int socket_id,const unsigned char* dt,int len,int data_type,int wait_ms)
	{
		AddRequestToQueue(socket_id,QUEUE_COMMAND_SEND,data_type,dt,len,wait_ms);
	}

	void FetchUrl(int socket_id,WString& str_url,int data_type,int wait_ms)
	{
		AddRequestToQueue(socket_id,QUEUE_COMMAND_FETCH_URL,data_type,str_url.GetBuffer(),str_url.Length(),wait_ms);
	}

	void StopAndResetAll()
	{
		AddRequestToQueue(0,QUEUE_COMMAND_RESET,0,NULL,0,0);
	}

	void ResetAndSelectAccessPoint(int x)
	{
		AddRequestToQueue(0,QUEUE_COMMAND_RESET_AND_SELECT_ACCESS_POINT,0,NULL,0,0);
	}

	bool RemoveSocket(int socket_id)
	{
		RemoveRequestFromQueue(socket_id,-1,true);
		AddRequestToQueue(socket_id,QUEUE_COMMAND_REMOVE_SOCKET,0,NULL,0,0);
		return true;
	}

	int AddUdpSocket(int socket_id, unsigned int dest_ip, int dest_port, int timeout_ms ) 
	{
		unsigned char dt[8];
		dt[0] = ( ( dest_ip >> 24 ) & 0xFF );
		dt[1] = ( ( dest_ip >> 16 ) & 0xFF );
		dt[2] = ( ( dest_ip >> 8  ) & 0xFF );
		dt[3] = ( dest_ip & 0xFF );
		
		dt[4] = ( ( dest_port >> 8 ) & 0xFF );
		dt[5] = ( dest_port & 0xFF );

		dt[6] = ( ( timeout_ms >> 8 ) & 0xFF );
		dt[7] = ( timeout_ms & 0xFF );

		return AddRequestToQueue(socket_id,QUEUE_COMMAND_ADD_UDP_SOCKET,0,dt,8,0);
	}

	int AddTcpSocket(int socket_id, unsigned int dest_ip, int dest_port, int timeout_ms ) 
	{
		unsigned char dt[8];

		dt[0] = ( ( dest_ip >> 24 ) & 0xFF );
		dt[1] = ( ( dest_ip >> 16 ) & 0xFF );
		dt[2] = ( ( dest_ip >> 8  ) & 0xFF );
		dt[3] = ( dest_ip & 0xFF );
		
		dt[4] = ( ( dest_port >> 8 ) & 0xFF );
		dt[5] = ( dest_port & 0xFF );

		dt[6] = ( ( timeout_ms >> 8 ) & 0xFF );
		dt[7] = ( timeout_ms & 0xFF );

		return AddRequestToQueue(socket_id,QUEUE_COMMAND_ADD_TCP_SOCKET,0,dt,8,0);
	}

	void RemoveRequestFromQueue(int socket_id,int data_type,bool remove_socket_creation_request)
	{
		int pos = 0;
		int socket_id1 = 0;
		int command = 0;
		int data_type1 = 0;
		int len = 0;
		int wait_ms = 0;

		while(1)
		{
			const unsigned char* dt = ParseRequest(pos,socket_id1,command,data_type1,len,wait_ms);
			if ( dt == NULL )break;

			if ( ( socket_id != -1 && socket_id != socket_id1 ) || ( data_type != -1 && data_type != data_type1 ) )
			{
				StepToNextRequest(pos);
				continue;
			}

			RemoveRequestFromQueueX(pos);
		}
	}

private:
	void ResetSockets()
	{
		for ( int i = 0; i < MAX_SOCKET_COUNT; i++)
		{
			bIsEmpty[i] = true;
			iSockHandle[i] = NULL;
			iSockType[i] = SOCK_UDP;
		}


		FD_ZERO(&m_ActiveReadFdSet);
		FD_ZERO(&m_ActiveWriteFdSet);

		iSendQueueSize = 0;
		iNetworkErrorStr[0] = 0;
	}

	bool SendDataInternal(int socket_id,int data_type,const unsigned char* dt,int len)
	{
		int socket_index = IndexFromID(socket_id);
		if ( socket_index < 0 )
		{
//			AfxMessageBox("Sending data through unknown socket");
			return true;		// this forces the request to be removed from queue
		}

		
		iSockDataType[socket_index] = data_type;
		/*if(socket_id==131){
			iSockType[socket_index] = 1;
		}
		*/
		if ( iSockType[socket_index] & SOCK_UDP)
		{
			struct sockaddr_in addr;
			addr.sin_family = AF_INET;
			addr.sin_addr.s_addr = htonl(iSockDestIP[socket_index]);
			addr.sin_port = htons(iSockDestPort[socket_index]);

			sendto(iSockHandle[socket_index],(const char*)dt,len,0,(struct sockaddr*)(&addr),sizeof(addr));
		}
		else if ( iSockType[socket_index] & SOCK_TCP )
		{
			if ( iSockType[socket_index] & SOCK_ACTION_CONNECTED )
			{
				send(iSockHandle[socket_index],(const char*)dt,len,0);			
				return true;
			}else return false;
		}
		return true;
	}

	void FetchUrlInternal(int socket_id,int data_type,const char* str_url,int len)
	{
		TInetAddr dest_addr = StrToInetAddr(str_url);
		int socket_index = IndexFromID(socket_id);

		if ( socket_index < 0 )
		{
			if ( !AddTcpSocketInternal(socket_id,dest_addr.iAddress,dest_addr.iPort,0) )
			{
				AfxMessageBox("Error adding tcp socket");
				return;
			}

			socket_index = IndexFromID(socket_id);
		}

		iSockDestIP[socket_index] = dest_addr.iAddress;
		iSockDestPort[socket_index] = dest_addr.iPort;
		
		iSockType[socket_index] |= SOCK_ACTION_CONNECTING;
		int send_len = sprintf(iAddrGetRequest,"GET /%s HTTP/1.0\r\nHost:%s\r\n\r\n",iAddrGetParam,iAddrHost);		
		iAddrGetRequest[send_len] = 0;

		SendData(socket_id,(const unsigned char*)iAddrGetRequest,send_len,data_type,0);

		struct sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = htonl(iSockDestIP[socket_index]);
		addr.sin_port = htons(iSockDestPort[socket_index]);

		connect(iSockHandle[socket_index],(struct sockaddr *)(&addr),sizeof(addr));
	}

	void StopAndResetAllInternal()
	{
		ResetSockets();
	}

	bool MakeSocketNonBlocking(int sock_handle)
	{
		u_long iMode = 1;		// 1 for blocking
		int iResult = ioctlsocket(sock_handle, FIONBIO, &iMode);

		if (iResult != NO_ERROR)return false;
		return true;
	}

	bool AddSocket(int socket_type,int socket_id,int socket_handle,unsigned int dest_ip,int dest_port,int timeout_ms )
	{
		if ( !MakeSocketNonBlocking(socket_handle) )return false;

		int index = -1;
		for ( int i = 0; i < MAX_SOCKET_COUNT; i++ )
		{
			if ( bIsEmpty[i] == true )
			{
				 index = i;
				 break;
			}
		}
		if ( index < 0 )return false;

		FD_SET(socket_handle, &m_ActiveWriteFdSet);
		if(!FD_ISSET(socket_handle,&m_ActiveWriteFdSet))return false;
		
		FD_SET(socket_handle, &m_ActiveReadFdSet);
		if(!FD_ISSET(socket_handle,&m_ActiveReadFdSet))return false;
		
		bIsEmpty[index] = false;

		iSockHandle[index] = socket_handle;
		iSockID[index] = socket_id;		
		iSockType[index] = socket_type;
		iSockDataType[index] = 0;

		iSockDestIP[index] = dest_ip;
		iSockDestPort[index] = dest_port;

		return true;
	}


	bool RemoveSocketInternal(int socket_id)
	{
		int socket_index = IndexFromID(socket_id);
		if ( socket_index < 0 )return false;

		int socket_handle = iSockHandle[socket_index];

		FD_CLR(socket_handle, &m_ActiveReadFdSet);
		FD_CLR(socket_handle, &m_ActiveWriteFdSet);

		if(FD_ISSET(socket_handle, &m_ActiveReadFdSet) != false || FD_ISSET(socket_handle, &m_ActiveWriteFdSet) != false )return false;

		bIsEmpty[socket_index] = true;
		return true;
	}

	int AddUdpSocketInternal(int socket_id, unsigned int dest_ip, int dest_port, int timeout_ms ) 
	{
		int handle = socket(PF_INET, SOCK_DGRAM, getprotobyname("udp")->p_proto);
		if ( handle == -1 )return 0;

		if ( !AddSocket(SOCK_UDP,socket_id,handle,dest_ip,dest_port,timeout_ms))return 0;
		return handle;
	}

	int AddTcpSocketInternal(int socket_id, unsigned int dest_ip, int dest_port, int timeout_ms ) 
	{
		int handle = socket(PF_INET, SOCK_STREAM, getprotobyname("tcp")->p_proto);
		if ( handle == -1 )return 0;

		struct sockaddr_in addr;
		addr.sin_family = AF_INET;

		if ( !AddSocket(SOCK_TCP,socket_id,handle,dest_ip,dest_port,timeout_ms) )return 0;
		return handle;
	}

	void CheckRequestQueueInternal()
	{
		int pos = 0;
		int socket_id = 0;
		int command = 0;
		int data_type = 0;
		int len = 0;
		int execute_time_ms = 0;

		
		int current_time_ms = GetCurrentTimeMS();		
		
		while(1)
		{
			const unsigned char* dt = ParseRequest(pos,socket_id,command,data_type,len,execute_time_ms);
			if ( dt == NULL )break;
						
			if ( current_time_ms < execute_time_ms )
			{
				StepToNextRequest(pos);
				continue;
			}

			if ( command == QUEUE_COMMAND_RESET )
			{
				ResetSockets();
				iSendQueueSize = 0;
				break;
			}
			
			if ( command == QUEUE_COMMAND_RESET_AND_SELECT_ACCESS_POINT )
			{
				ResetSockets();
				iSendQueueSize = 0;
				if ( iListener )iListener->OnAccessPointDone(ETrue,1);
				break;
			}

			if ( command == QUEUE_COMMAND_FETCH_URL )
			{
				FetchUrlInternal(socket_id,data_type,(const char *)dt,len);
				RemoveRequestFromQueueX(pos);
			}
			else if ( command == QUEUE_COMMAND_SEND )
			{
				if ( !SendDataInternal(socket_id,data_type,dt,len) )
				{
					StepToNextRequest(pos);
					continue;
				}
				RemoveRequestFromQueueX(pos);
			}						
			else if ( command == QUEUE_COMMAND_ADD_TCP_SOCKET || command == QUEUE_COMMAND_ADD_UDP_SOCKET )
			{
				unsigned int ip = ( dt[0] << 24 ) | ( dt[1] << 16 ) | ( dt[2] << 8 ) | dt[3];
				int port = dt[4] * 256 + dt[5];
				int timeout_ms = dt[6] * 256 + dt[7];

				RemoveRequestFromQueueX(pos);
				if ( command == QUEUE_COMMAND_ADD_TCP_SOCKET )AddTcpSocketInternal(socket_id,ip,port,timeout_ms);
				else AddUdpSocketInternal(socket_id,ip,port,timeout_ms);
			}
			else if ( command == QUEUE_COMMAND_REMOVE_SOCKET )
			{
				RemoveRequestFromQueueX(pos);
				RemoveSocketInternal(socket_id);
			}
			else StepToNextRequest(pos);
		}		
		


	}

	void RunLoop(bool& bStopRequest)
	{
		struct sockaddr_in from;
		int fromlen = sizeof(from);

		fd_set read_fd_set;
		fd_set write_fd_set;

		int iSelectedIndex[MAX_SOCKET_COUNT * 2];

		int iRetSelect;
		int iSelectionCount = 0;
		int iReadSelectionCount = 0;
		int i = 0;

		timeval t;
		t.tv_sec = 1;
		t.tv_usec = 0;
		
		while(!bStopRequest)
		{
			CheckRequestQueueInternal();

			if ( m_ActiveReadFdSet.fd_count <= 0 && m_ActiveWriteFdSet.fd_count <= 0 )
			{
				Sleep(500);
				continue;
			}

			read_fd_set = m_ActiveReadFdSet;
			write_fd_set = m_ActiveWriteFdSet;

			iRetSelect = select(MAX_SOCKET_COUNT, &read_fd_set, &write_fd_set, NULL, &t);

			if(iRetSelect == SOCKET_ERROR)
			{
				int err = WSAGetLastError();
				switch(err)
				{
					case WSANOTINITIALISED:		strcpy(iNetworkErrorStr,"Network Error: WSANOTINITIALISED");
					case WSAENETDOWN:			strcpy(iNetworkErrorStr,"Network Error: WSAENETDOWN");
					case WSAEFAULT:				strcpy(iNetworkErrorStr,"Network Error: WSAEFAULT");
					case WSAENOTCONN:			strcpy(iNetworkErrorStr,"Network Error: WSAENOTCONN");
					case WSAEINTR:				strcpy(iNetworkErrorStr,"Network Error: WSAEINTR");
					case WSAEINPROGRESS:		strcpy(iNetworkErrorStr,"Network Error: WSAEINPROGRESS");
					case WSAENETRESET:			strcpy(iNetworkErrorStr,"Network Error: WSAENETRESET");
					case WSAENOTSOCK:			strcpy(iNetworkErrorStr,"Network Error: WSAENOTSOCK");
					case WSAEOPNOTSUPP:			strcpy(iNetworkErrorStr,"Network Error: WSAEOPNOTSUPP");
					case WSAESHUTDOWN:			strcpy(iNetworkErrorStr,"Network Error: WSAESHUTDOWN");
					case WSAEWOULDBLOCK:		strcpy(iNetworkErrorStr,"Network Error: WSAEWOULDBLOCK");
					case WSAEMSGSIZE:			strcpy(iNetworkErrorStr,"Network Error: WSAEMSGSIZE");
					case WSAEINVAL:				strcpy(iNetworkErrorStr,"Network Error: WSAEINVAL");
					case WSAECONNABORTED:		strcpy(iNetworkErrorStr,"Network Error: WSAECONNABORTED");
					case WSAETIMEDOUT:			strcpy(iNetworkErrorStr,"Network Error: WSAETIMEDOUT");
					case WSAECONNRESET:			strcpy(iNetworkErrorStr,"Network Error: WSAECONNRESET");
					default:					strcpy(iNetworkErrorStr,"Network Error: Unknown");
				}

				AfxMessageBox(iNetworkErrorStr);

				Sleep(100);
				continue;
			}

			iSelectionCount = 0;
			for ( i = 0; i < read_fd_set.fd_count; i++ )iSelectedIndex[iSelectionCount++] = IndexFromHandle(read_fd_set.fd_array[i]);
			iReadSelectionCount = iSelectionCount;
			for ( i = 0; i < write_fd_set.fd_count; i++ )iSelectedIndex[iSelectionCount++] = IndexFromHandle(write_fd_set.fd_array[i]);

			for ( i = 0; i < iSelectionCount; i++ )
			{
				int index = iSelectedIndex[i];
				if ( index < 0 )continue;

				int current_socket = iSockHandle[index];
				int current_id = iSockID[index];
				int socket_type = iSockType[index];

				bool b_read = ( i < iReadSelectionCount );
				int ret = -1;

				if ( b_read )
				{
					if ( socket_type & SOCK_UDP )
					{
						ret = recvfrom(current_socket, iReceiveQueue, MAX_RECEIVE_QUEUE, 0, (struct sockaddr *)&from, &fromlen);
						if ( ret > 0 )
						{
							iReceiveQueue[ret] = 0;

							iReceiveString.Zero();
							iReceiveString.Copy((const unsigned char*)iReceiveQueue,ret);

							if ( iListener )iListener->OnReceiveData(current_id,iReceiveString,ret);
							else AfxMessageBox((char *)iReceiveQueue);
						}
					}
					else if ( socket_type & SOCK_TCP )
					{
						ret = recv(current_socket, iReceiveQueue, MAX_RECEIVE_QUEUE, 0);
						if ( ret <= 0 )				// socket closed by remote
						{
							RemoveSocket(current_id);
							closesocket(current_socket);
							if ( iListener )iListener->OnSocketClosed(current_id);
							continue;
						}

						iReceiveQueue[ret] = 0;
						iReceiveString.Zero();
						iReceiveString.Copy((const unsigned char*)iReceiveQueue,ret);

						if ( iListener )iListener->OnReceiveData(current_id,iReceiveString,ret);
						else AfxMessageBox((char *)iReceiveQueue);
					}
				}
				else
				{
					if ( ( socket_type & SOCK_TCP ) != 0 && ( socket_type & SOCK_ACTION_CONNECTING ) != 0 )
					{
						ret = recv(current_socket,iReceiveQueue,MAX_RECEIVE_QUEUE,0);
						if ( ret == 0 )
						{
							RemoveSocket(current_id);
							closesocket(current_socket);

							if ( iListener )iListener->OnSocketClosed(current_id);

							AfxMessageBox("TCP connect failed");
							continue;
						}


						iSockType[index] &= ~(SOCK_ACTION_CONNECTING);
						iSockType[index] |= SOCK_ACTION_CONNECTED;

						continue;
					}
				}

			}


			Sleep(50);
		}
	}	

public:
	TInetAddr StrToInetAddr(TPtrC8 x)
	{
		int addr_len = x.Length();
		if ( x.Length() > 255 )addr_len = 255;

		memcpy(iAddrStr,(const char*)x.GetBuffer(),addr_len);
		iAddrStr[addr_len] = 0;

		iAddrTemp.iAddress = 0;
		iAddrTemp.iPort = 0;

		iAddrGetParam[0] = 0;
		iAddrHost[0] = 0;

		if ( addr_len <= 1 )return iAddrTemp;

		if ( addr_len >= 7 && strnicmp(iAddrStr,"http://",7) == 0 )
		{
			memcpy(iAddrStr,iAddrStr + 7,addr_len - 7);
			addr_len -= 7;
		}

		iAddrStr[addr_len] = 0;

		int j = 0;

		for ( j = 0; j < addr_len; j++ )
		{
			if ( iAddrStr[j] == ':' || iAddrStr[j] == '/' )break;
			iAddrHost[j] = iAddrStr[j];
		}
		iAddrHost[j] = 0;

		for ( j = 0; j < addr_len; j++ )if ( iAddrStr[j] == '/' )break;
		if ( j < addr_len )
		{
			memcpy(iAddrGetParam,iAddrStr + j + 1,addr_len - j - 1);
			iAddrGetParam[addr_len-j-1] = 0;
		}

		for ( j = 0; j < addr_len; j++ )
		{
			if ( iAddrStr[j] == '/' )break;
			if ( iAddrStr[j] != ':' )continue;

			iAddrTemp.iPort = 0;
			for ( int k = j + 1; k < addr_len; k++ )
			{
				if ( iAddrStr[k] >= '0' && iAddrStr[k] <= '9' )iAddrTemp.iPort = iAddrTemp.iPort * 10 + iAddrStr[k] - '0';
				else break;
			}
			
			break;
		}

		if ( iAddrTemp.iPort == 0 )iAddrTemp.iPort = 80;

		for ( j = 0; j < addr_len; j++ )if ( iAddrStr[j] == ':' || iAddrStr[j] == '/' )break;
		addr_len = j;
		iAddrStr[j] = 0;

		bool b_ip = true;
		for ( j = 0; j < addr_len; j++ )
		{
			if ( iAddrStr[j] >= '0' && iAddrStr[j] <= '9' )continue;
			if ( iAddrStr[j] == '.' )continue;

			b_ip = false;
			break;
		}

		if ( !b_ip )
		{
			struct hostent *hstnm = gethostbyname (iAddrStr);
			if (!hstnm)return iAddrTemp;
			
			unsigned int host_addr = 0;
			memcpy(&host_addr,hstnm->h_addr,sizeof(unsigned int));
			host_addr = htonl(host_addr);

			iAddrTemp.iAddress = host_addr;
			return iAddrTemp;
		}

		unsigned int addr = 0;
		int tmp = 0;
		int cnt = 0;

		for ( int i = 0; i <= addr_len; i++ )
		{
			if ( i == addr_len || iAddrStr[i] == '.' )
			{
				if ( i == addr_len )
				{
					addr = addr * 256 + tmp;

					cnt++;
					break;
				}
				addr = addr * 256 + tmp;

				tmp = 0;
				cnt++;
				continue;
			}
			if ( iAddrStr[i] >= '0' && iAddrStr[i] <= '9' )tmp = tmp * 10 + iAddrStr[i] - '0';
		}
		
		if ( cnt != 4 )addr = 0;
		iAddrTemp.iAddress = addr;

		return iAddrTemp;
	}
};

#endif