#ifndef __AUDIO_THREAD_ADDED__
#define __AUDIO_THREAD_ADDED__

#include "WThread.h"
#include <mmsystem.h>
//#include <pthread.h>
#include "EasyG729A.h"

//#include "xDialer.h"
//#include "Denoiser.h"

#define MAX_OUT_BUFFERS		4

#define MAX_RECORD_BUFFERS	2		// double buffer
#define RECORD_BLOCK_SIZE	1600	// 100 ms 16 bit pcm data
#define PLAY_BLOCK_SIZE		3200	// 200 ms 16 bit pcm data

#define	BIAS		 (0x84)		// Bias for linear code. 
#define CLIP         (8159)

#define SIGN_BIT     (0x80)
#define QUANT_MASK   (0xf)
#define NSEGS        (8)
#define SEG_SHIFT    (4)
#define SEG_MASK     (0x70)

class CAudioRecordInterface
{
public:
	virtual void OnRecordG729(unsigned char *c,int len) = 0;
	virtual void OnRecordError(const char *str_msg) = 0;
	virtual void OnPlayError(const char *str_msg) = 0;	
	virtual int FillBuffer(bool is_anything_playing,unsigned char *str_buffer,int max_len) = 0;
	virtual int GetPlayBufferLength() = 0;
};

	

class CAudioThread : public WThread, WThread1
{
private:
	CAudioRecordInterface *parent;
//	CPcmDenoiser denoiser;

	bool bRecordError;

	bool bRunning;
	bool bPlayWavFile;

	bool b16BitAudio;

	CODER_HANDLE hEncoder;
	CODER_HANDLE hDecoder;

	short			speech[L_G729A_FRAME];
	unsigned char	serial[L_G729A_FRAME_COMPRESSED];
	short			synth[L_G729A_FRAME];
	unsigned char	synthx[L_G729A_FRAME * 4];

	unsigned char	strCompressedRecordedData[1024];

	HWND	 m_hWnd;									
	HWAVEIN  hWaveIn;
	LPWAVEHDR  whin[MAX_RECORD_BUFFERS];

	void AllocPlayBuffer();
	void AllocRecordBuffer();

	void StartRecordTest();
	void StopRecordTest();
	CStatic *iStatusViewer;

	static CAudioThread *this_pointer;
	static void CALLBACK waveInProc(HWAVEIN hWaveIn, UINT uMsg, DWORD dwInstance, DWORD dwParam1,DWORD dwParam2);

	unsigned int waveoutDeviceID;
	WAVEHDR waveHdrOut[MAX_OUT_BUFFERS];
	HWAVEOUT hWaveOut;
	char dataBufferOut[MAX_OUT_BUFFERS][PLAY_BLOCK_SIZE];

	unsigned char *strPlayBuffer;
	int iPlayBufferLength;
	int iPlayerHead;
	int iPlayerDropCount;
	int iPlayerDropPercentage;
	
	unsigned char *strAutoAnswerBuffer;
	int  iAutoAnswerBufferLength;
	int  iAutoAnswerBufferPos;

	bool SaveWavFile(char *str_path,unsigned char *c,int len);

	char strMessage[256];
	unsigned char strFillBuffer[1000];
	
	unsigned char *G729ToPcmu(unsigned char *str);		//must be 10 byte input
	unsigned short *G729ToPcm(unsigned char *str);

	bool bG729;
	bool bRecord;
	bool bAudioLoop;

	void OutputThreadLoop();

	int iGainPercentage;
	int iVolumePercentage;

/*	static void *ThreadRunner(void *context)
	{
		CAudioThread *thr = (CAudioThread *)context;
		thr->RunAudioLoop();
		return NULL;
	}*/

	virtual void RunLoop(bool& b_stop_request)		// WThread CallBack
	{
		RunAudioPlayer();
	}

	virtual void RunLoop1(bool& b_stop_request)		// WThread1 CallBack
	{
		RunAudioRecorder();	
	}

	void RunAudioPlayer();
	void RunAudioRecorder();

	void OnInblock(WPARAM wParam, LPARAM lParam);
		
	int seg_aend[8];
	unsigned char G711AEncodeTable[1+65536/8];

	unsigned char search( int val, const int *table, unsigned size)
	{
		for (unsigned char i = 0; i < size; i++)if (val <= *table++)return (i);
		return size;
	};

//	const unsigned char* CAudioThread::FillBuffer(int& len);

public:
	unsigned char iLastPlayedSpectrum[1000];
	int iLastPlayedSpectrumLength;
	unsigned char iLastRecordedSpectrum[1000];
	int iLastRecordedSpectrumLength;
	
	CAudioThread(CAudioRecordInterface *listener,HWND m_hnd)
	{
		parent = listener;
		b16BitAudio = true;

		m_hWnd = m_hnd;														

		iPlayBufferLength = 0;
		strPlayBuffer = new unsigned char[2000000];

		iGainPercentage = 50;
		iVolumePercentage = 50;

		seg_aend[0] = 0x1F; seg_aend[1] = 0x3F; seg_aend[2] = 0x7F; seg_aend[3] = 0xFF;
		seg_aend[4] = 0x1FF; seg_aend[5] = 0x3FF; seg_aend[6] = 0x7FF; seg_aend[8] = 0xFFF;

		iLastPlayedSpectrumLength = 0;
		iLastRecordedSpectrumLength = 0;

		this_pointer = this;

/*		pthread_t server;
		pthread_attr_t server_attr;
		pthread_attr_init(&server_attr);

		if(pthread_create(&server, &server_attr,ThreadRunner,this ) != 0 )
		{
			AfxMessageBox("Cannot start audio thread...");
		}*/

		StartWThread();
		StartWThread1();
//		RunAudioRecorder();
	}

	~CAudioThread()
	{
		Destroy();
	}
	

	bool InitAudioPlayer();
	bool InitAudioRecorder();
	bool Init(bool b_g729 = true,CStatic *stat_viewer = NULL);
	void Destroy();

	unsigned char PcmToAlaw(short pcm_val);
	short AlawToPcm(unsigned char a_val);

	void Stop();

	void PlayBuffer(bool b_g729,const unsigned char *c,int len);
	bool HasRecordedData()
	{
		 return ( iPlayBufferLength > 0 );
	}
	void StartRecord(bool b_resume)
	{
		 if ( !b_resume )iPlayBufferLength = 0;
		 bRecord = true;
	}
	int  GetPlayerDropPercentage()
	{
		 return iPlayerDropPercentage;
	}
	void StopRecord()
	{
		 bRecord = false;
	}
	bool IsRecording()
	{
		 return bRecord;
	}
	int SaveRecord(char *str_path)	//1: success, -1: could not write file, -2: there is nothing to write
	{
		 if ( iPlayBufferLength <= 0 )return -2;
		 if ( SaveWavFile(str_path,strPlayBuffer,iPlayBufferLength) )return 1;
		 return -1;
	}
	void SetG729(bool b_g729){	bG729 = b_g729;	}
	bool IsG729(){return bG729;}
	bool IsRunning(){return bRunning;}
	int PlayWavFile(bool play_on_remote,unsigned char *c,int len);
};

#endif