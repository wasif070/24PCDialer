// SmoothDialer.h : main header file for the SMOOTHDIALER application
//

#if !defined(AFX_SMOOTHDIALER_H__8892C378_404D_4238_B6B2_D87FB0576E60__INCLUDED_)
#define AFX_SMOOTHDIALER_H__8892C378_404D_4238_B6B2_D87FB0576E60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSmoothDialerApp:
// See SmoothDialer.cpp for the implementation of this class
//

class CSmoothDialerApp : public CWinApp
{
public:
	CSmoothDialerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSmoothDialerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSmoothDialerApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMOOTHDIALER_H__8892C378_404D_4238_B6B2_D87FB0576E60__INCLUDED_)
