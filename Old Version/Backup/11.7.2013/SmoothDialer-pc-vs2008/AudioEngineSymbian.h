#ifndef __AUDIO_ENGINE_30_MAY_2011_INCLUDED__
#define __AUDIO_ENGINE_30_MAY_2011_INCLUDED__

#include "Globals.h"

#include "AudioThread.h"

#define KErrNone 0
#define KErrNotSupported -1
#define KErrNotReady -2

class AudioEngineSymbian : public CAudioRecordInterface
{
public:
	enum
	{
		SESSION_PLAY_QUEUE_THRESHOLD_MS = 1000,
	};
	
	TBool		iConfigured;
	TInt		iSelectedCodec;

	HWND		iHwnd;
	CStatic*	iStatViewer;

	virtual void OnRecordG729(unsigned char *c,int len)
	{
			if ( iRecordQueue.Length() + len > 2000 )iRecordQueue.Delete(0,iRecordQueue.Length() + len - 2000 );
			iRecordQueue.Append(c,len);
	}
	virtual void OnRecordError(const char *str_msg){}
	virtual void OnPlayError(const char *str_msg){}
	virtual int FillBuffer(bool is_anything_playing,unsigned char *str_buffer,int max_len)
	{
			if ( iPlayQueue.Length() <= 0 )return 0;
			
			int len = iPlayQueue.Length();
			if ( len > max_len )len = max_len;

			if ( len % 10 )len -= ( len % 10 );
			memcpy(str_buffer,iPlayQueue.GetBuffer(),len);
			iPlayQueue.Delete(0,len);

			return len;
	}

private:
	static TTime	iBaseTime;
	static TTime	iCurrentTime;
	
	CAudioThread*	iAudioThread;

	TBool			iLoudSpeaker;
	TInt			iVolumePercentage;
	
	TInt			iRtpBundleSize;
	TInt			iPacketSizeDecoded;
	
	CharArray(2000,		iPlayQueue);
	CharArray(1000,		iPlayQueueTemp);
	CharArray(2000,		iRecordQueue);
	CharArray(8200,		iTransactionBuf);
	
	TBool			iSessionPlayStarted;
	TInt			iSessionPlaySize;
	TInt			iSessionDropSize;
	TInt			iSessionDropCountDown;			// To protect from sequencial drop
	
	TInt			iSessionRecordSizeOriginal;
	TInt			iSessionRecordCountOriginal;
	TInt			iSessionRecordSize;
	TInt			iSessionRecordDropSize;
	
private:	
	TPtrC GetCodecName(TBool b_aps,TInt codec_id);
	void  OnPlayTick();
	
	void ResetRtpAnalyzer();
	
public:
	AudioEngineSymbian();
	~AudioEngineSymbian();
	
	TInt Configure();	// returns selected codec
	
	void PlayRtp(TPtrC8 dt);	
	static TUint32 GetCurrentTimeMS();
	static TUint32 GetCurrentTimeMicroSecond();	
	
	void ForceStop();
	
	void SetSpeaker(TBool b_speaker);
	TBool GetSpeaker();
	void SetVolume(TInt vol_percentage);
	TInt GetVolume();
	void SetRtpBundleSize(TInt bundle_size);
	
	void ResetAudioBuffer();
	void GetLog(TDes& str_log);
	
	TInt PlayRingTone(TPtrC8 str_path);
	TInt StopRingTone();
	
	TBool OnNetworkTick(TDes8& recorded_buf);		// returns ETrue is record_buf has data 
};

#endif
