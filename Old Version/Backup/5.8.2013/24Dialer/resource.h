//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SmoothDialer.rc
//
#define IDD_SMOOTHDIALER_DIALOG         102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_LOGIN_DIALOG                129
#define IDR_MENU1                       131
#define IDB_BITMAP1                     134
#define IDB_BITMAP_MAIN                 134
#define IDB_BITMAP2                     137
#define IDB_BITMAP_DIAL_CALL            137
#define IDB_BITMAP_HASH                 138
#define IDB_BITMAP_STAR                 139
#define IDB_BITMAP_0                    140
#define IDB_BITMAP_1                    141
#define IDB_BITMAP_2                    142
#define IDB_BITMAP_3                    143
#define IDB_BITMAP_4                    144
#define IDB_BITMAP_5                    145
#define IDB_BITMAP_6                    146
#define IDB_BITMAP_7                    147
#define IDB_BITMAP_8                    148
#define IDB_BITMAP_9                    149
#define IDB_BITMAP_C                    150
#define IDB_BITMAP_CANCEL               151
#define IDB_BITMAP_END_CALL             152
#define IDB_BITMAP_OK                   153
#define IDB_BITMAP3                     154
#define IDB_BITMAP_LOGIN                154
#define IDD_CONTACTS_DIALOG             155
#define IDC_STATIC_DISPLAY              1000
#define IDC_EDIT_CALL                   1001
#define IDC_BUTTON_CALL                 1002
#define IDC_BUTTON_END_CALL             1003
#define IDC_BUTTON_IVR                  1004
#define IDC_STATIC_STATUS               1005
#define IDC_OPCODE                      1006
#define IDC_USER                        1007
#define IDC_EDIT3                       1008
#define IDC_PASSWORD                    1008
#define IDC_CONTACTS_ADDRESS_EDIT       1008
#define IDC_BUTTON1                     1009
#define IDC_BUTTON2                     1010
#define IDC_BUTTON3                     1012
#define IDC_BUTTON4                     1013
#define IDC_BUTTON5                     1014
#define IDC_BUTTON6                     1015
#define IDC_BUTTON7                     1016
#define IDC_BUTTON8                     1017
#define IDC_BUTTON9                     1018
#define IDC_BUTTON0                     1019
#define IDC_BUTTON_STAR                 1020
#define IDC_BUTTON_HASH                 1021
#define IDC_BUTTON_CANCEL               1024
#define IDC_BUTTON_DELETE               1025
#define IDC_CHECK_SAVEINFO              1026
#define IDC_COMBO_CALL                  1027
#define IDC_CONTACTS_LIST               1028
#define ID_SAVE                         1029
#define ID_REMOVE                       1030
#define IDC_CONTACTS_NAME_EDIT          1031
#define IDC_CONTACTS_NUMBER_EDIT        1032
#define IDC_NEW                         1033
#define IDC_CONTACTS_EMAIL_EDIT         1035
#define IDC_CONTACTS_EMAIL              1035
#define IDC_SLIDER_LEFT                 1038
#define IDC_SLIDER_RIGHT                1039
#define ID_MENU_LOGOUT                  32771
#define ID_MENU_CALLLOG                 32772
#define ID_MENU_CALLLOG32773            32773
#define ID_MENU_LOGOUT32774             32774
#define ID_MENU_IVR                     32775
#define ID_CONTACTS_ALLINFO             32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        156
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
