#ifndef __GLOBALS_H_ADDED_30_MAY_2011__
#define __GLOBALS_H_ADDED_30_MAY_2011__

#define __C_PLUS_PLUS_BUILD__
#include <time.h>
#include "WString.h"

typedef WString TPtrC8;
typedef WString TPtrC;
typedef WString TDes8;

typedef int TInt;
typedef unsigned int TUint;
typedef unsigned int TUint32;
typedef bool TBool;
typedef WString TDes;

#define ETrue true
#define EFalse false
#define _L8(x)	x
#define CharArray(x,y) WString y

class TTimeInterval
{
private:
		__int64 duration_us;
public:		
		TTimeInterval(__int64 t)
		{
			duration_us = t;
		}
		__int64 Int64()
		{
			return duration_us;
		}
};

class TTime
{
private:
		__int64 t;

public:
		void HomeTime()
		{
			t = clock();
		}
		TTimeInterval MicroSecondsFrom(const TTime& from_time)
		{
			return ( t - from_time.t ) * 1000;
		}
};

class Globals
{
private:
	static WString iTempWString;

public:
	static WString& EmptyWString()
	{
		iTempWString.SetLength(0);
		return iTempWString;
	}

	static WString& TempWString(const char* str)
	{
		iTempWString.Zero();
		iTempWString.Copy(str);

		return iTempWString;
	}

	static WString& TempWString(const char* str,int len)
	{
		iTempWString.Zero();
		for ( int i = 0; i < len; i++ )iTempWString += str[i];
		iTempWString.TerminateWithZero();

		return iTempWString;
	}

	static const WString& GetNextString(TPtrC8 c,TInt& st,TInt len,char c_separator,char c_separator1 = 0)
	{
		if ( c.Length() == 0 || st >= c.Length() )return EmptyWString();

		iTempWString.Zero();

		for ( ; st < c.Length(); st++ )
		{
			if ( c[st] == c_separator || c[st] == c_separator1 )break;
			iTempWString += c[st];
		}
		st++;
		
		iTempWString.TerminateWithZero();
		return iTempWString;
	}

	static TUint32 GetNextInt(TPtrC8 c,TInt& st,TInt len,char c_separator,char c_separator1 = 0)
	{
		if ( c.Length() == 0 || st >= c.Length() )return 0;
		
		unsigned int val = 0;

		for ( ; st < c.Length(); st++ )
		{
			if ( c[st] == c_separator || c[st] == c_separator1 )break;
			if ( c[st] >= '0' && c[st] <= '9' )val = val * 10 + c[st] - '0';
		}
		st++;
		return val;
	}


	static TPtrC8 FindToken(TPtrC8 c,TInt len,TPtrC8 str_token)
	{
		return FindToken(c,len,(const char*)str_token.GetBuffer());
	}

	static TPtrC8 FindToken(TPtrC8 c,TInt len,const char* str_token)
	{
		iTempWString.Zero();
		
		int p = c.FindTokenPos(str_token);
		if ( p < 0 )return iTempWString;

		p += ( strlen(str_token) + 2 );
		for ( ; p < len; p++ )
		{
			if ( c[p] == '<' )break;
			iTempWString += c[p];
		}

		iTempWString.TerminateWithZero();
		return iTempWString;
	}

	static TUint32 FindTokenAsInt(TPtrC8 c,TInt len,TPtrC8 str_token)
	{
		return FindTokenAsInt(c,len,(const char*)str_token.GetBuffer());
	}

	static TUint32 FindTokenAsInt(TPtrC8 c,TInt len,const char* str_token)
	{
		int p = c.FindTokenPos(str_token);
		if ( p < 0 )return 0;

		p += ( strlen(str_token) + 2 );

		unsigned int val = 0;

		for ( ; p < len; p++ )
		{
			if ( c[p] == '<' )break;
			if ( c[p] >= '0' && c[p] <= '9' )val = val * 10 + c[p] - '0';
		}
		return val;
	}

	static TPtrC8 GetStateItem(TPtrC8 dt,TInt serial)
	{
		iTempWString.Zero();
		if ( serial == 0 )
		{
			TInt p = 0;
			for ( ; p < dt.Length(); p++ )
			{
				if ( dt[p] == ':' )break;
				iTempWString += dt[p];
			}

			iTempWString.TerminateWithZero();
			return iTempWString;
		}

		TInt cnt = 0;
		TInt i = 0;
		TInt j = 0;

		for ( ; i < dt.Length(); i++ )
		{
			if ( dt[i] != ':' )continue;
			cnt++;
			if ( cnt < serial )continue;

			TInt j = i+1;
			for ( ; j < dt.Length(); j++ )
			{
				if ( dt[j] == ':' )break;
				iTempWString += dt[j];
			}

			iTempWString.TerminateWithZero();
			return iTempWString;
		}

		return EmptyWString();
	}
};

#endif
