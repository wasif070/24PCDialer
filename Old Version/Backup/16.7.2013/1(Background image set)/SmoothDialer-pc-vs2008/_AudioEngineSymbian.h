#ifndef __AUDIO_ENGINE_SYMBIAN_WINDOWS__
#define __AUDIO_ENGINE_SYMBIAN_WINDOWS__

#include <stdlib.h>
#include "EasyG729A.h"

class AudioEngineSymbian
{
private:
	CODER_HANDLE	hEncoder;
	CODER_HANDLE	hDecoder;

	unsigned char	iEncodedFrame[L_G729A_FRAME_COMPRESSED * 20];		// for 200 ms
	short			iDecodedFrame[L_G729A_FRAME * 20];					// for 200 ms

	int InitCodec()
	{	
		hEncoder = EasyG729A_init_encoder();
		hDecoder = EasyG729A_init_decoder();

		return 80 * 4;							// for 40 ms data
	}

	void ResetCodec()
	{
		DestroyCodec();
		InitCodec();
	}

	void DestroyCodec()
	{
		EasyG729A_release_encoder( hEncoder );
		EasyG729A_release_decoder( hDecoder );
	}

	unsigned char* PcmToG729(short* dt,int& len)			// len is IN and OUT
	{
		if ( ( len % 10 ) != 0 )len -= ( len % 10 );
		if ( len < 80 )
		{
			len = 0;
			return NULL;
		}
	
		if ( len > 1600 )len = 1600;			// max 200 ms

		int encoded_len = 0;
		for ( int i = 0; i < len; i += 80 )
		{
			if ( !EasyG729A_encoder(hEncoder,dt + i,iEncodedFrame + encoded_len) )continue;
			encoded_len += 10;			
		}

		len = encoded_len;
		return iEncodedFrame;
	}

	short* G729ToPcm(unsigned char* dt,int& len)  // len is IN and OUT
	{
		if ( ( len % 10 ) != 0 )len -= ( len % 10 );
		if ( len < 10 )
		{
			len = 0;
			return NULL;
		}

		if ( len > 200 )len = 200;

		int decoded_length = 0;
		for ( int i = 0; i < len; i += 10 )
		{
			if ( !EasyG729A_decoder(hDecoder,dt + i, iDecodedFrame + decoded_length ) )continue;
			decoded_length += 80;
		}
		len = decoded_length;
		return iDecodedFrame;
	}

public:
	int OnNetworkTick(WString& x)
	{
		return 0;
	}

	void ForceStop()
	{

	}

	void ResetAudioBuffer()
	{

	}

	void PlayRtp(WString& x)
	{
		int len = x.Length();
		len++;
	}

	int GetCurrentTimeMicroSecond()
	{
		return clock() * 1000;
	}

	void SetSpeaker(bool is_speaker){}
	void SetRtpBundleSize(int x){}
};

#endif