// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SmoothDialer.h"
#include "LoginDlg.h"
#include "afxdialogex.h"
#include "SmoothDialerDlg.h"
#include <afxwin.h>



// LoginDlg dialog

IMPLEMENT_DYNAMIC(LoginDlg, CDialogEx)

LoginDlg::LoginDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(LoginDlg::IDD, pParent)
{
		m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

LoginDlg::~LoginDlg()
{
}

BOOL LoginDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);		// Set big icon

	/////////Read UserInfo
	CStdioFile readFile;
	CString strLine;
	CFileException fileException;
	CString strFilePath = _T("UserInfo.dat");
	int counter = 0;
	if (readFile.Open(strFilePath, CFile::modeRead, &fileException)){
	while (readFile.ReadString(strLine)){
		counter ++;
		if(counter==1)
			SetDlgItemText(IDC_OPCODE,strLine);
		if(counter==2)
			SetDlgItemText(IDC_USER,strLine);
		if(counter==3)
			SetDlgItemText(IDC_PASSWORD,strLine);
		}
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void LoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(LoginDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &LoginDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &LoginDlg::OnBnClickedButtonCancel)
END_MESSAGE_MAP()


// LoginDlg message handlers


void LoginDlg::OnBnClickedOk()
{

	CButton *m_ctlCheck = (CButton*) GetDlgItem(IDC_CHECK_SAVEINFO);
	int ChkBox = m_ctlCheck->GetCheck();

	GetDlgItemText(IDC_OPCODE,opcode);
	GetDlgItemText(IDC_USER,user);
	GetDlgItemText(IDC_PASSWORD, password);

	 if(ChkBox == BST_CHECKED){
		//Write User Info
		CString strLine;
		CStdioFile writeToFile;
		CFileException fileException;
		CString strFilePath = _T("UserInfo.dat");
		CFileStatus status;
		//int counter = 0;
		if( CFile::GetStatus( strFilePath, status ) ){
		 // Open the file without the Create flag
		CFile::Remove(strFilePath);
		writeToFile.Open( strFilePath, CFile:: modeCreate | CFile::modeWrite ), &fileException;
		writeToFile.Seek (0, CFile :: begin);	
		writeToFile.WriteString(opcode + "\n" + user + "\n" + password);
		}else{
		// Open the file with the Create flag
		writeToFile.Open( strFilePath, CFile::modeCreate | CFile::modeWrite ), &fileException;
		writeToFile.Seek (0, CFile :: begin);
		writeToFile.WriteString(opcode + "\n" + user + "\n" + password);
		}
		writeToFile.Close();
		////////End User Info
	 }

	CSmoothDialerDlg dlg(this);
	dlg.opcode = atoi(opcode);
	dlg.user.Copy(user);
	dlg.password.Copy(password);
	if(opcode=="" || user=="" || password==""){
	    int Answer;
   		Answer = AfxMessageBox("Opcode/ User/ Password can not be empty",
		MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);

		if( Answer == IDOK ){
		EndDialog(1);
		LoginDlg ldlg(this);
		ldlg.DoModal();
		}
		else // if( Answer == IDNO )
		EndDialog(1);
	}else{
		EndDialog(1);
		dlg.DoModal();
	}

	CDialogEx::OnOK();
}


void LoginDlg::OnBnClickedButtonCancel()
{
	exit(1);
}
